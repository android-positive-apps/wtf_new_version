package com.wtf.activity.objects;

/**
 * Created by Niv on 3/7/2016.
 */
public class Coupon{
    private String ID;
    private String Image;
    private String BusinessID;
    private String Title;
    private String Description;
    private String SmallLetters;
    private String Password;
    private String NumItems;
    private String ItemsLeft;
    private String Status;
    private String Expiration;
    private String BusinessName;
    private String Created;



    public String getID() {
        return ID;
    }

    public String getImage() {
        return Image;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public void setImage(String image) {
        Image = image;
    }

    public String getBusinessID() {
        return BusinessID;
    }

    public void setBusinessID(String businessID) {
        BusinessID = businessID;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getSmallLetters() {
        return SmallLetters;
    }

    public void setSmallLetters(String smallLetters) {
        SmallLetters = smallLetters;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getNumItems() {
        return NumItems;
    }

    public void setNumItems(String numItems) {
        NumItems = numItems;
    }

    public String getItemsLeft() {
        return ItemsLeft;
    }

    public void setItemsLeft(String itemsLeft) {
        ItemsLeft = itemsLeft;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getExpiration() {
        return Expiration;
    }

    public void setExpiration(String expiration) {
        Expiration = expiration;
    }

    public String getBusinessName() {
        return BusinessName;
    }

    public void setBusinessName(String businessName) {
        BusinessName = businessName;
    }

    public String getCreated() {
        return Created;
    }

    public void setCreated(String created) {
        Created = created;
    }
}