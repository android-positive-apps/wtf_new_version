package com.wtf.activity.objects;

import android.graphics.drawable.Drawable;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.annotations.SerializedName;
import com.google.maps.android.clustering.ClusterItem;
import com.wtf.activity.util.ImageLoader;

/**
 * Created by Niv on 3/2/2016.
 */
public class Friend implements ClusterItem {

    private long user_id;
    private String FBID="";
    private String Lat="";
    private String Lng="";
    private String ID="";
    private String firstName="";
    private String phone="";

    @SerializedName("UserActivity")
    private UserActivity userActivity;

    @SerializedName("FirstName")
    private String FirstNameSearch="";

    private String lastName="";

    @SerializedName("LastName")
    private String LastNameSearch="";

    private String image="";
    private String locationDesc="";
    private String Distance="";


    @SerializedName("LastRated")
    private LastRated lastRated;

    @SerializedName("LastChecked")
    private LastChecked lastChecked;

    public Friend(long user_id,String FBID, String lat, String lng, String ID, String firstName, String lastName, String image, String locationDesc,String phone) {
        this.FBID = FBID;
        Lat = lat;
        Lng = lng;
        this.ID = ID;
        this.firstName = firstName;
        this.lastName = lastName;
        this.image = image;
        this.locationDesc = locationDesc;
        this.user_id= user_id;
        this.phone=phone;
    }

    public Friend(String FBID, String lat, String lng, String ID, String firstName, String lastName, String image, String locationDesc,String phone) {
        this.FBID = FBID;
        Lat = lat;
        Lng = lng;
        this.ID = ID;
        this.firstName = firstName;
        this.lastName = lastName;
        this.image = image;
        this.locationDesc = locationDesc;
        this.phone=phone;
    }
    public Friend(long user_id, String FBID, String lat, String lng, String ID) {
        this.user_id = user_id;
        this.FBID = FBID;
        Lat = lat;
        Lng = lng;
        this.ID = ID;
    }

    public Friend(String FBID,  String firstName,String image) {
        this.FBID = FBID;
        this.firstName = firstName;
        this.image = image;


    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public UserActivity getUserActivity() {
        return userActivity;
    }

    public void setUserActivity(UserActivity userActivity) {
        this.userActivity = userActivity;
    }

    public String getDistance() {
        return Distance;
    }

    public String getFirstNameSearch() {
        return FirstNameSearch;
    }

    public String getLastNameSearch() {
        return LastNameSearch;
    }

    public LastRated getLastRated() {
        return lastRated;
    }

    public LastChecked getLastChecked() {
        return lastChecked;
    }

    public long getUser_id() {
        return user_id;
    }

    public void setUser_id(long user_id) {
        this.user_id = user_id;
    }

    public String getFBID() {
        return FBID;
    }

    public void setFBID(String FBID) {
        this.FBID = FBID;
    }

    public String getLat() {
        return Lat;
    }

    public void setLat(String lat) {
        Lat = lat;
    }

    public String getLng() {
        return Lng;
    }

    public void setLng(String lng) {
        Lng = lng;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getLocationDesc() {
        return locationDesc;
    }

    public void setLocationDesc(String locationDesc) {
        this.locationDesc = locationDesc;
    }

    @Override
    public LatLng getPosition() {

        return new LatLng(Double.parseDouble(getLat()),Double.parseDouble(getLng()));

    }

}
