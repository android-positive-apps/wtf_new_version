package com.wtf.activity.objects;

/**
 * Created by Niv on 3/6/2016.
 */
public class AppuserGift {
    private String GiftID;
    private String GiftType;
    private String AppuserID;
    private String Lat;
    private String Lng;
    private String Created;
    private String Title;
    private String Image;
    private String SmallLetters;
    private String Description;
    private String BusinessName;
    private String ReceiverID;
    private String Status;


    public AppuserGift(String giftID, String giftType, String appuserID, String lat, String lng, String created, String title, String image, String smallLetters, String description, String businessName, String receiverID, String status) {
        GiftID = giftID;
        GiftType = giftType;
        AppuserID = appuserID;
        Lat = lat;
        Lng = lng;
        Created = created;
        Title = title;
        Image = image;
        SmallLetters = smallLetters;
        Description = description;
        BusinessName = businessName;
        ReceiverID = receiverID;
        Status = status;
    }

    public String getGiftID() {
        return GiftID;
    }

    public void setGiftID(String giftID) {
        GiftID = giftID;
    }

    public String getGiftType() {
        return GiftType;
    }

    public void setGiftType(String giftType) {
        GiftType = giftType;
    }

    public String getAppuserID() {
        return AppuserID;
    }

    public void setAppuserID(String appuserID) {
        AppuserID = appuserID;
    }

    public String getLat() {
        return Lat;
    }

    public void setLat(String lat) {
        Lat = lat;
    }

    public String getLng() {
        return Lng;
    }

    public void setLng(String lng) {
        Lng = lng;
    }

    public String getCreated() {
        return Created;
    }

    public void setCreated(String created) {
        Created = created;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    public String getSmallLetters() {
        return SmallLetters;
    }

    public void setSmallLetters(String smallLetters) {
        SmallLetters = smallLetters;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getBusinessName() {
        return BusinessName;
    }

    public void setBusinessName(String businessName) {
        BusinessName = businessName;
    }

    public String getReceiverID() {
        return ReceiverID;
    }

    public void setReceiverID(String receiverID) {
        ReceiverID = receiverID;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }
}
