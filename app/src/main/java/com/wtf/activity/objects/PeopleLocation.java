package com.wtf.activity.objects;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

/**
 * Created by Niv on 3/27/2016.
 */
public class PeopleLocation implements ClusterItem {
    private double Lat;
    private double Lng;
    private String Distance;

    public PeopleLocation(double lat, double lng, String distance) {
        Lat = lat;
        Lng = lng;
        Distance = distance;
    }
    @Override
    public LatLng getPosition() {

        return new LatLng(getLat(),getLng());

    }
    public double getLat() {
        return Lat;
    }

    public double getLng() {
        return Lng;
    }

    public String getDistance() {
        return Distance;
    }

    public void setLat(double lat) {
        Lat = lat;
    }

    public void setLng(double lng) {
        Lng = lng;
    }

    public void setDistance(String distance) {
        Distance = distance;
    }
}
