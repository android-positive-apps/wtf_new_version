package com.wtf.activity.objects;

/**
 * Created by Niv on 3/27/2016.
 */
public class LastRated {
    private String BusinessName;
    private long BusinessId;
    private int Rate;

    public LastRated(String businessName, long businessId, int rate) {
        BusinessName = businessName;
        BusinessId = businessId;
        Rate = rate;
    }

    public String getBusinessName() {
        return BusinessName;
    }

    public long getBusinessId() {
        return BusinessId;
    }

    public int getRate() {
        return Rate;
    }
}
