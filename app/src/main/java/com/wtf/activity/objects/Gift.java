package com.wtf.activity.objects;

/**
 * Created by Niv on 3/2/2016.
 */
public class Gift {
//
//    private String BusinessID;
//    private String Title;
//    private String Description;
//    private String Image;
//    private String Password;
//    private String Status;
//    private String Expiration;
//    private String ID;
//    private String Created;
//    private int Type;
//    private String SmallLetters;
//    private String BusinessName;

//
//    "GiftID": "1",
//            "AppuserID": "641",
//            "ReceiverID": "645",
//            "Status": "0",
//            "GiftType": "0",
//            "Lat": "0.0000000",
//            "Lng": "0.0000000",
//            "Created": "2016-04-12 17:40:39",
//            "Title": "משקה חריף",
//            "Image": "http://www.mypushserver.com/dev/bmm/upload/1406106170_92509.jpg",
//            "SmallLetters": "",
//            "Description": "צ'ייסר אחד על חשבון הבית, בארוחה עסקית",
//            "BusinessName": "אבו שוקרי"
    private String GiftID;
    private String AppuserID;
    private String ReceiverID;
    private String GiftType;
    private String Lat;
    private String Lng;
    private String Created;
    private String Title;
    private String Image;
    private String SmallLetters;
    private String Description;
    private String BusinessName;


    public Gift(String giftID, String appuserID, String receiverID, String giftType, String lat, String lng, String created, String title, String image, String smallLetters, String description, String businessName) {
        GiftID = giftID;
        AppuserID = appuserID;
        ReceiverID = receiverID;
        GiftType = giftType;
        Lat = lat;
        Lng = lng;
        Created = created;
        Title = title;
        Image = image;
        SmallLetters = smallLetters;
        Description = description;
        BusinessName = businessName;
    }

    public String getGiftID() {
        return GiftID;
    }

    public void setGiftID(String giftID) {
        GiftID = giftID;
    }

    public String getAppuserID() {
        return AppuserID;
    }

    public void setAppuserID(String appuserID) {
        AppuserID = appuserID;
    }

    public String getReceiverID() {
        return ReceiverID;
    }

    public void setReceiverID(String receiverID) {
        ReceiverID = receiverID;
    }

    public String getGiftType() {
        return GiftType;
    }

    public void setGiftType(String giftType) {
        GiftType = giftType;
    }

    public String getLat() {
        return Lat;
    }

    public void setLat(String lat) {
        Lat = lat;
    }

    public String getLng() {
        return Lng;
    }

    public void setLng(String lng) {
        Lng = lng;
    }

    public String getCreated() {
        return Created;
    }

    public void setCreated(String created) {
        Created = created;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    public String getSmallLetters() {
        return SmallLetters;
    }

    public void setSmallLetters(String smallLetters) {
        SmallLetters = smallLetters;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getBusinessName() {
        return BusinessName;
    }

    public void setBusinessName(String businessName) {
        BusinessName = businessName;
    }
}
