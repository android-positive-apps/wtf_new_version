package com.wtf.activity.objects;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Niv on 3/2/2016.
 */
public class ReceivedGifts {

    public long GiftID;
    public long AppuserID;
    public long ReceiverID;
    public int Status;
    public int GiftType;
    public double Lat;
    public double Lng;

    @SerializedName("Created")
    public String Ccreated;


}
