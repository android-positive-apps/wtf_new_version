package com.wtf.activity.objects;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Niv on 3/2/2016.
 */
public class Appuser {

    private String MyPushServerID;
    private String FirstName;
    private String LastName;
    private String Password;
    private String UDID;
    private String CountryCode;
    private String UpdateInterval;
    private String Radius="16";
    private int CorrectCounter;
    private String CountryName;
    private String City;
    private String State;
    private String About;
    private String GCM;
    private String PushToken;
    private String Email;
    private String FBID;
    private String FBAvatar;
    private String Phone;
    private int Score;
    private double LastLat;
    private double LastLng;
    private String LastLocationUpdate;
    private String ID;
    private String Created;
    private String IP;
    private int Points;
    private int AppuserLocation;

    public Appuser(String ID, String radius) {
        this.ID = ID;
        Radius = radius;
    }

    public String getMyPushServerID() {
        return MyPushServerID;
    }

    public void setMyPushServerID(String myPushServerID) {
        MyPushServerID = myPushServerID;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getUDID() {
        return UDID;
    }

    public void setUDID(String UDID) {
        this.UDID = UDID;
    }

    public String getCountryCode() {
        return CountryCode;
    }

    public void setCountryCode(String countryCode) {
        CountryCode = countryCode;
    }

    public String getUpdateInterval() {
        return UpdateInterval;
    }

    public void setUpdateInterval(String updateInterval) {
        UpdateInterval = updateInterval;
    }

    public String getRadius() {
        return Radius;
    }

    public void setRadius(String radius) {
        Radius = radius;
    }

    public int getCorrectCounter() {
        return CorrectCounter;
    }

    public void setCorrectCounter(int correctCounter) {
        CorrectCounter = correctCounter;
    }

    public String getCountryName() {
        return CountryName;
    }

    public void setCountryName(String countryName) {
        CountryName = countryName;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public String getState() {
        return State;
    }

    public void setState(String state) {
        State = state;
    }

    public String getAbout() {
        return About;
    }

    public void setAbout(String about) {
        About = about;
    }

    public String getGCM() {
        return GCM;
    }

    public void setGCM(String GCM) {
        this.GCM = GCM;
    }

    public String getPushToken() {
        return PushToken;
    }

    public void setPushToken(String pushToken) {
        PushToken = pushToken;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getFBID() {
        return FBID;
    }

    public void setFBID(String FBID) {
        this.FBID = FBID;
    }

    public String getFBAvatar() {
        return FBAvatar;
    }

    public void setFBAvatar(String FBAvatar) {
        this.FBAvatar = FBAvatar;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public int getScore() {
        return Score;
    }

    public void setScore(int score) {
        Score = score;
    }

    public double getLastLat() {
        return LastLat;
    }

    public void setLastLat(double lastLat) {
        LastLat = lastLat;
    }

    public double getLastLng() {
        return LastLng;
    }

    public void setLastLng(double lastLng) {
        LastLng = lastLng;
    }

    public String getLastLocationUpdate() {
        return LastLocationUpdate;
    }

    public void setLastLocationUpdate(String lastLocationUpdate) {
        LastLocationUpdate = lastLocationUpdate;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getCreated() {
        return Created;
    }

    public void setCreated(String created) {
        Created = created;
    }

    public String getIP() {
        return IP;
    }

    public void setIP(String IP) {
        this.IP = IP;
    }

    public int getPoints() {
        return Points;
    }

    public void setPoints(int points) {
        Points = points;
    }

    public int getAppuserLocation() {
        return AppuserLocation;
    }

    public void setAppuserLocation(int appuserLocation) {
        AppuserLocation = appuserLocation;
    }
}
