package com.wtf.activity.objects;

/**
 * Created by Niv on 3/6/2016.
 */
public class CheckIn {
    private String FBID;
    private String Created;
    private long AppuserID;
    private String UserName;

    public CheckIn(String FBID, String created) {
        this.FBID = FBID;
        Created = created;
    }

    public CheckIn(String FBID, String created, long appuserID, String userName) {
        this.FBID = FBID;
        Created = created;
        AppuserID = appuserID;
        UserName = userName;
    }

    public String getCreated() {
        return Created;
    }

    public void setCreated(String created) {
        Created = created;
    }

    public String getFBID() {
        return FBID;
    }

    public void setFBID(String FBID) {
        this.FBID = FBID;
    }

    public long getAppuserID() {
        return AppuserID;
    }

    public String getUserName() {
        return UserName;
    }
}
