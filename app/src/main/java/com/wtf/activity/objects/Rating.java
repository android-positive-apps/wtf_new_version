package com.wtf.activity.objects;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Niv on 3/28/2016.
 */
public class Rating {

//    @SerializedName("Q1")
//    private float NumOfRates;

    @SerializedName("Q1")
    private float ServiceRate;

    @SerializedName("Q2")
    private float AmbienceRate;

    @SerializedName("Q3")
    private float PriceRate;

    @SerializedName("Q4")
    private float OverallRate;

//    public float getNumOfRates() {
//        return NumOfRates;
//    }

    public float getServiceRate() {
        return ServiceRate;
    }

    public float getAmbienceRate() {
        return AmbienceRate;
    }

    public float getPriceRate() {
        return PriceRate;
    }

    public float getOverallRate() {
        return OverallRate;
    }
}
