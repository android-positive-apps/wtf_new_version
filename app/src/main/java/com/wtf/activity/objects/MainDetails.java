package com.wtf.activity.objects;

import com.wtf.activity.main.MyApp;

/**
 * Created by Niv on 3/31/2016.
 */
public class MainDetails {

    private String friends="0";
    private String businesses="0";
    private String points="0";
    private String trendySpots="0";
    private String ratedPlaces= "counting";
    private String checkIns= "counting";
    private String userAround="counting";

    public MainDetails(String friends, String businesses, String points, String trendySpots, String ratedPlaces, String checkIns, String userAround) {
        this.friends = friends;
        this.businesses = businesses;
        this.points = points;
        this.trendySpots = trendySpots;
        this.ratedPlaces = ratedPlaces;
        this.checkIns = checkIns;
        this.userAround = userAround;
    }

    public String getFriends() {
        return friends;
    }

    public void setFriends(String friends) {
        this.friends = friends;
    }

    public String getBusinesses() {
        return businesses;
    }

    public void setBusinesses(String businesses) {
        this.businesses = businesses;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public String getTrendySpots() {
        return trendySpots;
    }

    public void setTrendySpots(String trendySpots) {
        this.trendySpots = trendySpots;
    }

    public String getRatedPlaces() {
        return ratedPlaces;
    }

    public void setRatedPlaces(String ratedPlaces) {
        this.ratedPlaces = ratedPlaces;
    }

    public String getCheckIns() {
        return checkIns;
    }

    public void setCheckIns(String checkIns) {
        this.checkIns = checkIns;
    }

    public String getUserAround() {
        return userAround;
    }

    public void setUserAround(String userAround) {
        this.userAround = userAround;
    }
}
