package com.wtf.activity.objects;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Niv on 4/10/2016.
 */
public class UserActivity {
    private String Activity;
    private String Date;
    private String Rank;

    @SerializedName("Business")
    private Business business;

    public String getActivity() {
        return Activity;
    }

    public String getDate() {
        return Date;
    }

    public String getRank() {
        return Rank;
    }

    public Business getBusiness() {
        return business;
    }
}
