package com.wtf.activity.objects;

/**
 * Created by Niv on 3/28/2016.
 */
public class SpecialOffer {
    private long ID;
    private String Title;
    private String Description;
    private String Image;
    private int price;
    private int Points;

    public SpecialOffer(long ID, String title, String description, String image, int price, int points) {
        this.ID = ID;
        Title = title;
        Description = description;
        Image = image;
        this.price = price;
        Points = points;
    }

    public long getID() {
        return ID;
    }

    public String getTitle() {
        return Title;
    }

    public String getDescription() {
        return Description;
    }

    public String getImage() {
        return Image;
    }

    public int getPrice() {
        return price;
    }

    public int getPoints() {
        return Points;
    }
}
