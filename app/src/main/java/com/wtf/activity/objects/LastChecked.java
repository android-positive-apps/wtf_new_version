package com.wtf.activity.objects;

/**
 * Created by Niv on 3/27/2016.
 */
public class LastChecked  {
    private String BusinessName;
    private long BusinessId;

    public String getBusinessName() {
        return BusinessName;
    }

    public long getBusinessId() {
        return BusinessId;
    }
}
