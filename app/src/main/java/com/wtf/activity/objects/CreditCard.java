package com.wtf.activity.objects;

/**
 * Created by Niv on 3/7/2016.
 */
public class CreditCard {

    private String ID;
    private int cardIdForAdapter;
    private String Name;
    private String Image;
    private String Created;

    public CreditCard(String name, String image, String created, String ID) {
        Name = name;
        Image = image;
        Created = created;
        this.ID = ID;
    }

    public CreditCard(int cardIdForAdapter, String name, String image) {

        Name = name;
        Image = image;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    public String getCreated() {
        return Created;
    }

    public void setCreated(String created) {
        Created = created;
    }

    public int getCardIdForAdapter() {
        return cardIdForAdapter;
    }

    public void setCardIdForAdapter(int cardIdForAdapter) {
        this.cardIdForAdapter = cardIdForAdapter;
    }
}
