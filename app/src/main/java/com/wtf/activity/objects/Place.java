package com.wtf.activity.objects;

/**
 * Created by Niv on 3/2/2016.
 */
public class Place {
    private static final long serialVersionUID = 1L;

    public static final String KEY_PLACES = "KeyPlacess";

    private String name;
    private double lat;
    private double lon;
    private String date;
    private String businessID;
    private String businessExternalID;
    private int categoryType;
}
