package com.wtf.activity.objects;

import android.util.Log;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Niv on 3/2/2016.
 */
public class Business implements Serializable {

    public static String FOOD ="אוכל";
    public static String SPORT ="ספורט";
    public static String ENTERTAINMENT ="פנאי";
    public static String FASHION ="אופנה";
    public static String NIGHT ="לילה";
    public static String SERVICES ="שירותים";

    public static String[] categories = {"אוכל", "ספורט","פנאי","אופנה","לילה","שירותים"};



    ArrayList<Coupon>Coupons;

    private String Name="";
    private String ExternalID="";
    private String ID="";
    private long tableID;
    private String Phone;
    private String Phone2;
    private String Opening;
    private String Image;
    private String MapImage;
    private float SpecialRank;
    private int NumGifts;
    private String Category="";
    private String Fax;
    private String Country;
    private String City="";
    private String Street;
    private String HouseNum;
    private String Website;
    private String FacebookLink;
    private String BusinessDesc;
    private int Rank;
    private int CheckIns;
    private int NumCoupons;
    private String Status;
    private String Edited;
    private String Lat="";
    private String Lng="";
    private String AdminRank;
    private String ShowAdminRank;
    private String Created;
    private String NumRankers;
    private String Sort;
    private String Distance;
    private String dateCheckedIn="";


    private boolean TrendySpot;
//    private ArrayList<CheckIn> CheckIns;
    private ArrayList<SpecialOffer> SpecialOffers;

    @SerializedName("Rating")
    private Rating rating;



    public Business() {
    }

    public Business(ArrayList<Coupon> coupons, String name, String externalID, String ID,String phone, String phone2, String opening, String image, String mapImage, int checkIns, float specialRank, int numGifts, String category, String fax, String country, String city, String street, String houseNum, String website, String facebookLink, String businessDesc, int rank, int numCoupons, String status, String edited, String lat, String lng, String adminRank, String showAdminRank, String created, String numRankers, String sort, String distance) {
        Coupons = coupons;
        Name = name;
        ExternalID = externalID;
        this.ID = ID;
        Phone = phone;
        Phone2 = phone2;
        Opening = opening;
        Image = image;
        MapImage = mapImage;
//        CheckIns = checkIns;
        SpecialRank = specialRank;
        NumGifts = numGifts;
        Category = category;
        Fax = fax;
        Country = country;
        City = city;
        Street = street;
        HouseNum = houseNum;
        Website = website;
        FacebookLink = facebookLink;
        BusinessDesc = businessDesc;
        Rank = rank;
        NumCoupons = numCoupons;
        Status = status;
        Edited = edited;
        Lat = lat;
        Lng = lng;
        AdminRank = adminRank;
        ShowAdminRank = showAdminRank;
        Created = created;
        NumRankers = numRankers;
        Sort = sort;
        Distance = distance;
    }

    public Business(long id, String name, String category, String lat, String lng, String city, String date, String businesID, String extID) {
        Lng = lng;
        Lat = lat;
        tableID = id;
        ExternalID = extID;
        Name = name;
        City = city;
        Category = category;
        dateCheckedIn = date;
        ID =businesID;
    }
    public Business(String name, String category, String lat, String lng, String city, String date, String businesID, String extID) {
        Lng = lng;
        Lat = lat;

        ExternalID = extID;
        Name = name;
        City = city;
        Category = category;
        dateCheckedIn = date;
        ID =businesID;
    }

    public Business(long id, String name, String category, String lat, String lng, String city, String date, String businesID, String extID, String image) {
        Lng = lng;
        Lat = lat;
        tableID = id;
        ExternalID = extID;
        Name = name;
        City = city;
        Category = category;
        dateCheckedIn = date;
        ID =businesID;
        this.Image =image;
    }

    public Business(String name, String category, String lat, String lng, String city, String date, String id, String externalID, String image) {
        Lng = lng;
        Lat = lat;
        ExternalID = externalID;
        Name = name;
        City = city;
        Category = category;
        dateCheckedIn = date;
        ID=id;
        Image=image;
    }

    public long getTableID() {
        return tableID;
    }

    public void setTableID(long tableID) {
        this.tableID = tableID;
    }

    public String getDateCheckedIn() {
        return dateCheckedIn;
    }

    public void setDateCheckedIn(String dateCheckedIn) {
        this.dateCheckedIn = dateCheckedIn;
    }

    public boolean isTrendySpot() {
        return TrendySpot;
    }

    public ArrayList<Coupon> getCoupons() {
        return Coupons;
    }

    public String[] getCategories() {
        return categories;
    }

    public void setCategories(String[] categories) {
        this.categories = categories;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getExternalID() {
        return ExternalID;
    }

    public void setExternalID(String externalID) {
        ExternalID = externalID;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public String getPhone2() {
        return Phone2;
    }

    public void setPhone2(String phone2) {
        Phone2 = phone2;
    }

    public String getOpening() {
        return Opening;
    }

    public void setOpening(String opening) {
        Opening = opening;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    public String getMapImage() {
        return MapImage;
    }

    public void setMapImage(String mapImage) {
        MapImage = mapImage;
    }

    public int getCheckIns() {
        return CheckIns;
    }

    public void setCheckIns(int checkIns) {
        CheckIns = checkIns;
    }

    public String getDistance() {
        return Distance;
    }

    public void setDistance(String distance) {
        Distance = distance;
    }

    public float getSpecialRank() {
        return SpecialRank;
    }

    public void setSpecialRank(float specialRank) {
        SpecialRank = specialRank;
    }

    public int getNumGifts() {
        return NumGifts;
    }

    public void setNumGifts(int numGifts) {
        NumGifts = numGifts;
    }

    public String getCategory() {
        return Category;
    }

    public void setCategory(String category) {
        Category = category;
    }

    public String getFax() {
        return Fax;
    }

    public void setFax(String fax) {
        Fax = fax;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String country) {
        Country = country;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public String getStreet() {
        return Street;
    }

    public void setStreet(String street) {
        Street = street;
    }

    public String getHouseNum() {
        return HouseNum;
    }

    public void setHouseNum(String houseNum) {
        HouseNum = houseNum;
    }

    public String getWebsite() {
        return Website;
    }

    public void setWebsite(String website) {
        Website = website;
    }

    public String getFacebookLink() {
        return FacebookLink;
    }

    public void setFacebookLink(String facebookLink) {
        FacebookLink = facebookLink;
    }

    public String getBusinessDesc() {
        return BusinessDesc;
    }

    public void setBusinessDesc(String businessDesc) {
        BusinessDesc = businessDesc;
    }

    public int getRank() {
        return Rank;
    }

    public void setRank(int rank) {
        Rank = rank;
    }

    public int getNumCoupons() {
        return NumCoupons;
    }

    public void setNumCoupons(int numCoupons) {
        NumCoupons = numCoupons;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getEdited() {
        return Edited;
    }

    public void setEdited(String edited) {
        Edited = edited;
    }

    public String getLat() {
        return Lat;
    }

    public void setLat(String lat) {
        Lat = lat;
    }

    public String getLng() {
        return Lng;
    }

    public void setLng(String lng) {
        Lng = lng;
    }

    public String getAdminRank() {
        return AdminRank;
    }

    public void setAdminRank(String adminRank) {
        AdminRank = adminRank;
    }

    public String getShowAdminRank() {
        return ShowAdminRank;
    }

    public void setShowAdminRank(String showAdminRank) {
        ShowAdminRank = showAdminRank;
    }

    public String getCreated() {
        return Created;
    }

    public void setCreated(String created) {
        Created = created;
    }

    public String getNumRankers() {
        return NumRankers;
    }

    public void setNumRankers(String numRankers) {
        NumRankers = numRankers;
    }

    public String getSort() {
        return Sort;
    }

    public void setSort(String sort) {
        Sort = sort;
    }


    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }
}
