package com.wtf.activity.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.wtf.activity.R;
import com.wtf.activity.objects.CheckIn;
import com.wtf.activity.objects.Friend;
import com.wtf.activity.ui.RoundedImageView;
import com.wtf.activity.util.ImageLoader;

import java.util.List;

/**
 * Created by Niv on 3/29/2016.
 */
public class CheckInsArrayAdapter  extends ArrayAdapter<CheckIn> {
    private Context context;

    public CheckInsArrayAdapter(Context context, List<CheckIn> checkins) {
        super(context, 0, checkins);
        this.context=context;
    }
    private class ViewHolder{
        TextView textView;
        RoundedImageView imageView;

    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final CheckIn checkIn = getItem(position);
//        System.out.println("--->  ada"+checkIn.getFBID()+" "+checkIn.getAppuserID()+" "+checkIn.getUserName());
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.check_ins_grid_new, parent, false);
            holder = new ViewHolder();
            holder.textView = (TextView) convertView.findViewById(R.id.friendGridName);
            holder.imageView = (RoundedImageView) convertView.findViewById(R.id.gridImage);
            convertView.setTag(holder);
        }else {
            holder = (ViewHolder) convertView.getTag();
        }
        if (checkIn.getFBID()!=null) {
            if (!checkIn.getFBID().equals("-1")) {
                String name = checkIn.getUserName();
                holder.textView.setTextColor(Color.BLACK);
                holder.textView.setText(name);
                String image = "https://graph.facebook.com/" + checkIn.getFBID() + "/picture?type=normal";
                ImageLoader.ByUrl(image, holder.imageView);

            } else {
                holder.textView.setText("(" + checkIn.getCreated() + ")");
                holder.imageView.setImageResource(R.drawable.show_more_button_3x);
            }
        }
        return convertView;

    }
}
