package com.wtf.activity.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wtf.activity.R;
import com.wtf.activity.objects.Business;

import java.util.List;

/**
 * Created by Niv on 3/8/2016.
 */
public class BusnissesMapListAdapter extends ArrayAdapter<Business> {


    public BusnissesMapListAdapter(Context context, List<Business> businesses) {
        super(context, 0, businesses);
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Business business = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.search_places_map_item_list, parent, false);
        }

        ImageView placeImageView = (ImageView) convertView.findViewById(R.id.placeImageView);

        TextView placeNameTextView = (TextView) convertView.findViewById(R.id.placeNameTextView);
//
        String name = business.getName();

        placeNameTextView.setText(name);



        String category= business.getCategory();
        if (category.equals(Business.ENTERTAINMENT)){
            placeImageView.setImageResource(R.drawable.entertainment_button3x);
        }
        if (category.equals(Business.FASHION)){
            placeImageView.setImageResource(R.drawable.fashion_button3x);
        }
        if (category.equals(Business.FOOD)){
            placeImageView.setImageResource(R.drawable.food_button3x);
        }
        if (category.equals(Business.NIGHT)){
            placeImageView.setImageResource(R.drawable.nightlife_button3x);
        }
        if (category.equals(Business.SPORT)){
            placeImageView.setImageResource(R.drawable.sport_button3x);
        }
        if (category.equals(Business.SERVICES)){
            placeImageView.setImageResource(R.drawable.hotel_button3x);

        }

//        dialog_list_item_linear.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dialog_list_item_linear.setb
//            }
//        });

        return convertView;

    }
}
