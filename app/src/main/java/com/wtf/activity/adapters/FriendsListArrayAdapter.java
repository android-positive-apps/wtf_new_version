package com.wtf.activity.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.wtf.activity.R;
import com.wtf.activity.main.MyApp;
import com.wtf.activity.objects.Friend;
import com.wtf.activity.ui.RoundedImageView;
import com.wtf.activity.util.BitmapUtil;
import com.wtf.activity.util.ImageLoader;

import java.util.List;

/**
 * Created by Niv on 3/28/2016.
 */
public class FriendsListArrayAdapter extends ArrayAdapter<Friend> {

    private Context context;

    public FriendsListArrayAdapter(Context context, List<Friend> friends) {
        super(context, 0, friends);
        this.context=context;
    }
    private class ViewHolder{
        TextView textView;
        RoundedImageView imageView;

    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Friend friend = getItem(position);
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.search_friends_list_item_map, parent, false);
            holder = new ViewHolder();
            holder.textView = (TextView) convertView.findViewById(R.id.friendTextView);
            holder.imageView = (RoundedImageView) convertView.findViewById(R.id.friendImageView);
            convertView.setTag(holder);

        }else {
            holder = (ViewHolder) convertView.getTag();
        }
        //map
//        String name = friend.getFirstNameSearch()+" "+ friend.getLastNameSearch();
        //gifts:
        String name = friend.getFirstName()+" "+ friend.getLastName();
        System.out.println(name);
        holder.textView.setText(name);
        String image = "https://graph.facebook.com/"+friend.getFBID()+"/picture";

        Bitmap bit = BitmapUtil.loadImageFromStorage(MyApp.appManager.getImagePath(), friend.getFBID());
        if (bit!=null)
            holder.imageView.setImageBitmap(bit);
        else
            ImageLoader.ByUrl(friend.getImage(), holder.imageView);
//            ImageLoader.ByUrl(image,holder.imageView);


        return convertView;

    }
}
