package com.wtf.activity.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.wtf.activity.R;
import com.wtf.activity.main.MyApp;
import com.wtf.activity.objects.Friend;
import com.wtf.activity.objects.Gift;
import com.wtf.activity.ui.RoundedImageView;
import com.wtf.activity.util.BitmapUtil;
import com.wtf.activity.util.ImageLoader;

import java.util.List;

/**
 * Created by Niv on 3/28/2016.
 */
public class GiftsGridArrayAdapter extends ArrayAdapter<Gift> {

    private Context context;

    public GiftsGridArrayAdapter(Context context, List<Gift> gifts) {
        super(context, 0, gifts);
        this.context=context;
    }
    private class ViewHolder{
        TextView textView;
        RoundedImageView imageView;

    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Gift gift = getItem(position);
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.gift_grid_item, parent, false);
            holder = new ViewHolder();
            holder.textView = (TextView) convertView.findViewById(R.id.friendGridName);
            holder.imageView = (RoundedImageView) convertView.findViewById(R.id.gridImage);
            convertView.setTag(holder);

        }else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.textView.setText(gift.getTitle());
        ImageLoader.ByUrl(gift.getImage(),holder.imageView,R.mipmap.ic_launcher);

        return convertView;

    }
}
