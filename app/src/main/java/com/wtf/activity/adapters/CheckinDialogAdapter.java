package com.wtf.activity.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wtf.activity.R;
import com.wtf.activity.main.MapActivity;
import com.wtf.activity.objects.Business;
import com.wtf.activity.ui.RoundedImageView;
import com.wtf.activity.util.ImageLoader;

import java.util.List;

/**
 * Created by Niv on 3/8/2016.
 */
public class CheckinDialogAdapter extends ArrayAdapter<Business> {

    private class ViewHolder{
        TextView placeNameTv;
        TextView addressPlaceNameTv;
        TextView cityPlaceNameTv;
        ImageView dialogCategoryImageView;

    }
    public CheckinDialogAdapter(Context context, List<Business> businesses) {
        super(context, 0, businesses);
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Business business = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view

        ViewHolder holder = null;
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.check_in_item_dialog_list, parent, false);
            holder = new ViewHolder();

            holder.dialogCategoryImageView = (ImageView) convertView.findViewById(R.id.dialogCategoryImageView);
            holder.placeNameTv = (TextView) convertView.findViewById(R.id.placeNameTv);
            holder.addressPlaceNameTv = (TextView) convertView.findViewById(R.id.addressPlaceNameTv);
            holder.cityPlaceNameTv = (TextView) convertView.findViewById(R.id.cityPlaceNameTv);
//            LinearLayout dialog_list_item_linear = (LinearLayout) convertView.findViewById(R.id.dialog_list_item_linear);
            convertView.setTag(holder);

        }else {
            holder = (ViewHolder) convertView.getTag();
        }
//
        String name = business.getName();
        String address= business.getStreet()+" "+business.getHouseNum();
        String city= business.getCity();
        holder.placeNameTv.setText(name);
        holder.addressPlaceNameTv.setText(address);
        holder.cityPlaceNameTv.setText(city);

        String category= business.getCategory();

        String image= business.getImage();
        if (!image.isEmpty()) {
            ImageLoader.ByUrl(image, holder.dialogCategoryImageView, /*default*/
                    category.equals(Business.ENTERTAINMENT) ? R.drawable.entertainment_green_3x :
                            category.equals(Business.FASHION) ? R.drawable.fashion_green_3x :
                                    category.equals(Business.FOOD) ? R.drawable.food_green_3x :
                                            category.equals(Business.NIGHT) ? R.drawable.nightlife_green_3x :
                                                    category.equals(Business.SPORT) ? R.drawable.sport_green_3x :
                                                            R.drawable.hotel_green_3x /*default*/
            );
        }else {

            if (category.equals(Business.ENTERTAINMENT)) {
                holder.dialogCategoryImageView.setImageResource(R.drawable.entertainment_green_3x);
            }
            if (category.equals(Business.FASHION)) {
                holder.dialogCategoryImageView.setImageResource(R.drawable.fashion_green_3x);
            }
            if (category.equals(Business.FOOD)) {
                holder.dialogCategoryImageView.setImageResource(R.drawable.food_green_3x);
            }
            if (category.equals(Business.NIGHT)) {
                holder.dialogCategoryImageView.setImageResource(R.drawable.nightlife_green_3x);
            }
            if (category.equals(Business.SPORT)) {
                holder.dialogCategoryImageView.setImageResource(R.drawable.sport_green_3x);
            }
            if (category.equals("")) {
                holder.dialogCategoryImageView.setImageResource(R.drawable.hotel_green_3x);

            }
        }
//        dialog_list_item_linear.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dialog_list_item_linear.setb
//            }
//        });

        return convertView;

    }
}
