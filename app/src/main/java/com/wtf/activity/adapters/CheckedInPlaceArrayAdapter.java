package com.wtf.activity.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wtf.activity.R;
import com.wtf.activity.fragments.BusinessPageFragment;
import com.wtf.activity.main.BusinessActivity;
import com.wtf.activity.main.MapActivity;
import com.wtf.activity.main.MyApp;
import com.wtf.activity.objects.Business;
import com.wtf.activity.objects.Business;
import com.wtf.activity.ui.RoundedImageView;
import com.wtf.activity.util.ImageLoader;
import com.wtf.activity.util.ToastUtil;

import java.util.List;

/**
 * Created by Niv on 3/29/2016.
 */
public class CheckedInPlaceArrayAdapter extends ArrayAdapter<Business> {


    private Context context;

    public CheckedInPlaceArrayAdapter(Context context, List<Business> businesss) {
        super(context, 0, businesss);
        this.context=context;
    }
    private class ViewHolder{
        TextView checkinNameTextView;
        TextView checkinCityTv;
        TextView dateCheckInTv;
        ImageView checkedPlaceImage;
        LinearLayout locateLinear;
        LinearLayout Linear;


    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Business business = getItem(position);
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.check_in_item_profile, parent, false);
            holder = new ViewHolder();
            holder.checkinNameTextView = (TextView) convertView.findViewById(R.id.checkinNameTextView);
            holder.checkinCityTv = (TextView) convertView.findViewById(R.id.checkinCityTv);
            holder.dateCheckInTv = (TextView) convertView.findViewById(R.id.dateCheckInTv);
            holder.checkedPlaceImage = (ImageView) convertView.findViewById(R.id.checkedPlaceImage);
            holder.locateLinear = (LinearLayout) convertView.findViewById(R.id.locateLinear);
            holder.Linear = (LinearLayout) convertView.findViewById(R.id.rateLinear);
            convertView.setTag(holder);
        }else {
            holder = (ViewHolder) convertView.getTag();
        }
        String name = business.getName();

        holder.checkinNameTextView.setText(name);
        holder.checkinCityTv.setText(business.getCity());
        holder.dateCheckInTv.setText(business.getDateCheckedIn());

        holder.locateLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(context,MapActivity.class);
                i.setAction(MapActivity.ACTION_RATE_PLACE);
                i.putExtra("businessID", business.getID());
                i.putExtra("businessExID",business.getExternalID());
                context.startActivity(i);
                ((Activity)context).finish();
            }
        });

        holder.Linear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyApp.appManager.setTempBusiness(business);
                Intent i = new Intent(context,BusinessActivity.class);
                i.setAction(BusinessPageFragment.ACTION_PROFILE);
                context.startActivity(i);
            }
        });
        holder.checkedPlaceImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyApp.appManager.setTempBusiness(business);
                Intent i = new Intent(context,BusinessActivity.class);
                i.setAction(BusinessPageFragment.ACTION_PROFILE);
                context.startActivity(i);
            }
        });

//        String category= business.getCategory();
//        if (category.equals(Business.ENTERTAINMENT)){
//            holder.checkedPlaceImage.setImageResource(R.drawable.entertainment_button3x);
//        }
//        if (category.equals(Business.FASHION)){
//            holder.checkedPlaceImage.setImageResource(R.drawable.fashion_button3x);
//        }
//        if (category.equals(Business.FOOD)){
//            holder.checkedPlaceImage.setImageResource(R.drawable.food_button3x);
//        }
//        if (category.equals(Business.NIGHT)){
//            holder.checkedPlaceImage.setImageResource(R.drawable.nightlife_button3x);
//        }
//        if (category.equals(Business.SPORT)){
//            holder.checkedPlaceImage.setImageResource(R.drawable.sport_button3x);
//        }
//        if (category.equals("")){
//            holder.checkedPlaceImage.setImageResource(R.drawable.hotel_button3x);
//
//        }

        ///////////////////////
        String category= business.getCategory();

        String image= business.getImage();
        if (!image.isEmpty()) {
            ImageLoader.ByUrl(image, holder.checkedPlaceImage, /*default*/
                    category.equals(Business.ENTERTAINMENT) ? R.drawable.entertainment_green_3x :
                            category.equals(Business.FASHION) ? R.drawable.fashion_green_3x :
                                    category.equals(Business.FOOD) ? R.drawable.food_green_3x :
                                            category.equals(Business.NIGHT) ? R.drawable.nightlife_green_3x :
                                                    category.equals(Business.SPORT) ? R.drawable.sport_green_3x :
                                                      R.drawable.hotel_green_3x/*default*/
            );
        }else {

            if (category.equals(Business.ENTERTAINMENT)) {
                holder.checkedPlaceImage.setImageResource(R.drawable.entertainment_green_3x);
            }
            if (category.equals(Business.FASHION)) {
                holder.checkedPlaceImage.setImageResource(R.drawable.fashion_green_3x);
            }
            if (category.equals(Business.FOOD)) {
                holder.checkedPlaceImage.setImageResource(R.drawable.food_green_3x);
            }
            if (category.equals(Business.NIGHT)) {
                holder.checkedPlaceImage.setImageResource(R.drawable.nightlife_green_3x);
            }
            if (category.equals(Business.SPORT)) {
                holder.checkedPlaceImage.setImageResource(R.drawable.sport_green_3x);
            }
            if (category.equals("")) {
                holder.checkedPlaceImage.setImageResource(R.drawable.hotel_green_3x);
            }
        }
        



        //////////////
        return convertView;

    }
}

