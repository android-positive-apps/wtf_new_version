package com.wtf.activity.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wtf.activity.R;
import com.wtf.activity.fragments.FriendsListFragment;
import com.wtf.activity.main.MapActivity;
import com.wtf.activity.main.MyApp;
import com.wtf.activity.objects.Friend;
import com.wtf.activity.ui.RoundedImageView;
import com.wtf.activity.util.BitmapUtil;
import com.wtf.activity.util.FragmentsUtil;
import com.wtf.activity.util.ImageLoader;
import com.wtf.activity.util.ToastUtil;

/**
 * Created by Niv on 3/31/2016.
 */
public class FriendListFragmentCursorAdapter extends CursorAdapter {
    class ViewHolder {
        //insert attributes to holder
        long id;
        public TextView textView;
        public RoundedImageView imageView;
        public LinearLayout locateFriendLinear;
    }


    public FriendListFragmentCursorAdapter(Context context, Cursor c) {
        super(context, c);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View view = LayoutInflater.from(context).inflate(R.layout.friend_list_fragment_item, parent, false);
        ViewHolder holder = new ViewHolder();
        holder.textView = (TextView)view.findViewById(R.id.friendsTextView2);

        holder.imageView=(RoundedImageView)view.findViewById(R.id.friendImage);
        holder.locateFriendLinear=(LinearLayout)view.findViewById(R.id.locateFriendLinear);
        view.setTag(holder);



        return view;
    }

    @Override
    public void bindView(View view, final Context context, Cursor cursor) {

        final Friend friend = MyApp.tableFriends.cursorToEntity(cursor);
        ViewHolder holder = (ViewHolder) view.getTag();

        String name = friend.getFirstName()+" "+ friend.getLastName();

        holder.textView.setText(name);
        Bitmap bit = BitmapUtil.loadImageFromStorage(MyApp.appManager.getImagePath(), friend.getFBID());
        if (bit!=null)
            holder.imageView.setImageBitmap(bit);
        else
            ImageLoader.ByUrl(friend.getImage(), holder.imageView);
//        ImageLoader.ByUrl(friend.getImage(), holder.imageView);

        //listeners:
        holder.locateFriendLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!friend.getLat().equals("")&&!friend.getLng().equals("")) {
                    Intent i = new Intent(context, MapActivity.class);
                    i.setAction(MapActivity.ACTION_LOCATE_FRIEND);
                    i.putExtra("Lat", friend.getLat());
                    i.putExtra("Lng", friend.getLng());
                    context.startActivity(i);
                    ((Activity) context).finish();
                }else {
                    ToastUtil.toaster(context.getResources().getString(R.string.unable_locate_friend),false);
                }
            }
        });

        holder.textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                    try {
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.facebook.com/"+friend.getFBID()));
                        context.startActivity(intent);
                    } catch(Exception e) {
//                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.facebook.com/"+friend.getFirstName()+"."+friend.getLastName())));
                        ToastUtil.toaster(context.getResources().getString(R.string.unable_locate_friend), false);

                    }
            }
        });

    }
}
