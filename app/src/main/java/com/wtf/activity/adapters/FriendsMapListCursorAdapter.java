package com.wtf.activity.adapters;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.wtf.activity.R;
import com.wtf.activity.main.MyApp;
import com.wtf.activity.objects.Friend;
import com.wtf.activity.ui.RoundedImageView;
import com.wtf.activity.util.BitmapUtil;
import com.wtf.activity.util.ImageLoader;

/**
 * Created by Niv on 3/17/2016.
 */
public class FriendsMapListCursorAdapter extends CursorAdapter {
    class ViewHolder {
        //insert attributes to holder
        long id;
        public TextView textView;
        public RoundedImageView imageView;
    }


    public FriendsMapListCursorAdapter(Context context, Cursor c) {
        super(context, c);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View view = LayoutInflater.from(context).inflate(R.layout.search_friends_list_item_map, parent, false);
        ViewHolder holder = new ViewHolder();
        holder.textView = (TextView)view.findViewById(R.id.friendTextView);

        holder.imageView=(RoundedImageView)view.findViewById(R.id.friendImageView);
        view.setTag(holder);



        return view;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {

        Friend friend = MyApp.tableFriends.cursorToEntity(cursor);
        ViewHolder holder = (ViewHolder) view.getTag();

        String name = friend.getFirstName()+" "+ friend.getLastName();

        holder.textView.setText(name);

        Bitmap bit = BitmapUtil.loadImageFromStorage(MyApp.appManager.getImagePath(), friend.getFBID());
        if (bit!=null)
            holder.imageView.setImageBitmap(bit);
        else
            ImageLoader.ByUrl(friend.getImage(), holder.imageView);

    }
}
