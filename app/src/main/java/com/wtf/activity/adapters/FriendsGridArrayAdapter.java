package com.wtf.activity.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.wtf.activity.R;
import com.wtf.activity.main.MyApp;
import com.wtf.activity.objects.Business;
import com.wtf.activity.objects.Friend;
import com.wtf.activity.ui.RoundedImageView;
import com.wtf.activity.util.BitmapUtil;
import com.wtf.activity.util.ImageLoader;

import java.util.List;

/**
 * Created by Niv on 3/28/2016.
 */
public class FriendsGridArrayAdapter extends ArrayAdapter<Friend> {

    private Context context;

    public FriendsGridArrayAdapter(Context context, List<Friend> friends) {
        super(context, 0, friends);
        this.context=context;
    }
    private class ViewHolder{
        TextView textView;
        RoundedImageView imageView;

    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Friend friend = getItem(position);
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.friends_grid_item, parent, false);
            holder = new ViewHolder();
            holder.textView = (TextView) convertView.findViewById(R.id.friendGridName);
            holder.imageView = (RoundedImageView) convertView.findViewById(R.id.gridImage);
            convertView.setTag(holder);

        }else {
            holder = (ViewHolder) convertView.getTag();
        }
        if (!friend.getFBID().equals("-1")) {
            String name = friend.getFirstName() + " " + friend.getLastName();

            holder.textView.setText(name);
            Bitmap bit = BitmapUtil.loadImageFromStorage(MyApp.appManager.getImagePath(), friend.getFBID());
            if (bit!=null)
                holder.imageView.setImageBitmap(bit);
            else
                ImageLoader.ByUrl(friend.getImage(), holder.imageView);


        }else {
            holder.imageView.setImageResource(R.drawable.show_all_button_3x);
            String more = friend.getImage()+" "+context.getResources().getString(R.string.more);
            holder.textView.setText(more);
        }

        return convertView;

    }
}
