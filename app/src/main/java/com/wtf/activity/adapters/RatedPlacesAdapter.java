package com.wtf.activity.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.wtf.activity.R;
import com.wtf.activity.fragments.BusinessPageFragment;
import com.wtf.activity.main.BusinessActivity;
import com.wtf.activity.main.MapActivity;
import com.wtf.activity.main.MyApp;
import com.wtf.activity.objects.Business;
import com.wtf.activity.util.ImageLoader;
import com.wtf.activity.util.ToastUtil;

import java.util.List;

/**
 * Created by Niv on 3/24/2016.
 */
public class RatedPlacesAdapter extends ArrayAdapter<Business> {

    private Context context;
    private class ViewHolder{
        TextView placeNameTextView;
        TextView ratedPLaceCityRv;
        LinearLayout locateLinear;
        ImageView ratePlaceImage;
        RatingBar ratingBar;
        LinearLayout rateLinear;
        FrameLayout rateFrame;
    }
    public RatedPlacesAdapter(Context context, List<Business> businesses) {
        super(context, 0, businesses);
        this.context=context;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Business business = getItem(position);
        ViewHolder holder = null;

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.rated_place_item, parent, false);
            holder = new ViewHolder();
            holder.ratePlaceImage = (ImageView) convertView.findViewById(R.id.ratePlaceImage);
            holder.placeNameTextView = (TextView) convertView.findViewById(R.id.placeNameTextView);
            holder.ratedPLaceCityRv = (TextView) convertView.findViewById(R.id.ratedPLaceCityRv);
            holder.locateLinear = (LinearLayout) convertView.findViewById(R.id.locateLinear);
            holder.ratingBar = (RatingBar)convertView.findViewById(R.id.ratePlaceRating);
            holder.rateLinear = (LinearLayout) convertView.findViewById(R.id.rateLinear);
            holder.rateFrame = (FrameLayout) convertView.findViewById(R.id.rateFrame);
            convertView.setTag(holder);
        }else {
            holder = (ViewHolder) convertView.getTag();
        }
//
//        ImageView ratePlaceImage = (ImageView) convertView.findViewById(R.id.ratePlaceImage);
//
//        TextView placeNameTextView = (TextView) convertView.findViewById(R.id.placeNameTextView);
//        TextView ratedPLaceCityRv = (TextView) convertView.findViewById(R.id.ratedPLaceCityRv);
//        LinearLayout locateLinear = (LinearLayout) convertView.findViewById(R.id.locateLinear);
//        RatingBar ratingBar = (RatingBar)convertView.findViewById(R.id.ratePlaceRating);
//        LinearLayout rateLinear = (LinearLayout) convertView.findViewById(R.id.rateLinear);
//        FrameLayout rateFrame = (FrameLayout) convertView.findViewById(R.id.rateFrame);
//

        String name = business.getName();
        String city= business.getCity();
        holder.placeNameTextView.setText(name);
        holder.ratedPLaceCityRv.setText(city);

        holder.ratingBar.setRating(business.getRank()/20f);
        if (business.getAdminRank()!=null){
            if (!business.getAdminRank().equals("")) {
                int num = Integer.parseInt(business.getAdminRank());
                holder.ratingBar.setRating(num / 20f);
            }
        }

        holder.locateLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(context,MapActivity.class);
                i.setAction(MapActivity.ACTION_RATE_PLACE);
                i.putExtra("businessID", business.getID());
                i.putExtra("businessExID",business.getExternalID());
                context.startActivity(i);
                ((Activity)context).finish();

            }
        });

        holder.rateFrame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openBusinessPage(business);
            }
        });
        holder.rateLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openBusinessPage(business);

            }
        });
//        dialog_list_item_linear.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dialog_list_item_linear.setb
//            }
//        });

        String category= business.getCategory();
        String image= business.getImage();

        if (!image.isEmpty()) {
            ImageLoader.ByUrl(image, holder.ratePlaceImage, /*default*/
                    category.equals(Business.ENTERTAINMENT) ? R.drawable.entertainment_red3x :
                            category.equals(Business.FASHION) ? R.drawable.fashion_red_x :
                                    category.equals(Business.FOOD) ? R.drawable.food_red3x :
                                            category.equals(Business.NIGHT) ? R.drawable.nightlife_red3x :
                                                    category.equals(Business.SPORT) ? R.drawable.sport_red3x :
                                                            R.drawable.hotel_red3x /*default*/
            );
        }else {

            if (category.equals(Business.ENTERTAINMENT)) {
                holder.ratePlaceImage.setImageResource(R.drawable.entertainment_red3x);
            }
            if (category.equals(Business.FASHION)) {
                holder.ratePlaceImage.setImageResource(R.drawable.fashion_red_x);
            }
            if (category.equals(Business.FOOD)) {
                holder.ratePlaceImage.setImageResource(R.drawable.food_red3x);
            }
            if (category.equals(Business.NIGHT)) {
                holder.ratePlaceImage.setImageResource(R.drawable.nightlife_red3x);
            }
            if (category.equals(Business.SPORT)) {
                holder.ratePlaceImage.setImageResource(R.drawable.sport_red3x);
            }
            if (category.equals("")) {
                holder.ratePlaceImage.setImageResource(R.drawable.hotel_red3x);
            }
        }
        return convertView;

    }

    private void openBusinessPage(Business business) {
        MyApp.appManager.setTempBusiness(business);
        Intent i = new Intent(context,BusinessActivity.class);
        i.setAction(BusinessPageFragment.ACTION_PROFILE);
        context.startActivity(i);
    }
}
