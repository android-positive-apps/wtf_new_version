package com.wtf.activity.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.wtf.activity.R;
import com.wtf.activity.objects.CheckIn;
import com.wtf.activity.objects.CreditCard;
import com.wtf.activity.ui.RoundedImageView;
import com.wtf.activity.util.ImageLoader;

import java.util.List;

/**
 * Created by Niv on 4/3/2016.
 */
public class CreditCardsArrayAdapter extends ArrayAdapter<CreditCard>{
//    private Context context;

    public CreditCardsArrayAdapter(Context context, List<CreditCard> creditCards) {
        super(context, 0, creditCards);
//        this.context=context;
    }
    private class ViewHolder{
        TextView textView;
        ImageView imageView;

    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final CreditCard creditCard = getItem(position);
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.cc_grid_item, parent, false);
            holder = new ViewHolder();
            holder.textView = (TextView) convertView.findViewById(R.id.gridName);
            holder.imageView = (ImageView) convertView.findViewById(R.id.gridImage);
//            System.out.println("--->  ccs gridImage");

            convertView.setTag(holder);
        }else {
            holder = (ViewHolder) convertView.getTag();
        }

//        int num = Integer.parseInt(creditCard.getID());
//        switch (num){
//            case 1:
//        System.out.println("--->  ccs gridImage"+creditCard.getName());

        holder.textView.setText(creditCard.getName());
                ImageLoader.ByUrl(creditCard.getImage(),holder.imageView);
//                break;
//            case 2:
//                holder.imageView.setImageResource(R.drawable.visa);
//                break;
//            case 3:
//                holder.imageView.setImageResource(R.drawable.hever);
//                break;
//        }
        return convertView;

    }
}
