/**
 * 
 */
package com.wtf.activity.animations;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;

/**
 * @author natiapplications
 *
 */
public class MyAnimationListener implements AnimationListener,AnimatorListener,com.nineoldandroids.animation.Animator.AnimatorListener {

	/* (non-Javadoc)
	 * @see android.view.animation.Animation.AnimationListener#onAnimationStart(android.view.animation.Animation)
	 */
	@Override
	public void onAnimationStart(Animation animation) {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see android.view.animation.Animation.AnimationListener#onAnimationEnd(android.view.animation.Animation)
	 */
	@Override
	public void onAnimationEnd(Animation animation) {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see android.view.animation.Animation.AnimationListener#onAnimationRepeat(android.view.animation.Animation)
	 */
	@Override
	public void onAnimationRepeat(Animation animation) {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see android.animation.Animator.AnimatorListener#onAnimationStart(android.animation.Animator)
	 */
	@Override
	public void onAnimationStart(Animator animation) {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see android.animation.Animator.AnimatorListener#onAnimationEnd(android.animation.Animator)
	 */
	@Override
	public void onAnimationEnd(Animator animation) {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see android.animation.Animator.AnimatorListener#onAnimationCancel(android.animation.Animator)
	 */
	@Override
	public void onAnimationCancel(Animator animation) {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see android.animation.Animator.AnimatorListener#onAnimationRepeat(android.animation.Animator)
	 */
	@Override
	public void onAnimationRepeat(Animator animation) {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see com.nineoldandroids.animation.Animator.AnimatorListener#onAnimationCancel(com.nineoldandroids.animation.Animator)
	 */
	@Override
	public void onAnimationCancel(com.nineoldandroids.animation.Animator arg0) {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see com.nineoldandroids.animation.Animator.AnimatorListener#onAnimationEnd(com.nineoldandroids.animation.Animator)
	 */
	@Override
	public void onAnimationEnd(com.nineoldandroids.animation.Animator arg0) {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see com.nineoldandroids.animation.Animator.AnimatorListener#onAnimationRepeat(com.nineoldandroids.animation.Animator)
	 */
	@Override
	public void onAnimationRepeat(com.nineoldandroids.animation.Animator arg0) {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see com.nineoldandroids.animation.Animator.AnimatorListener#onAnimationStart(com.nineoldandroids.animation.Animator)
	 */
	@Override
	public void onAnimationStart(com.nineoldandroids.animation.Animator arg0) {
		// TODO Auto-generated method stub
		
	}

}
