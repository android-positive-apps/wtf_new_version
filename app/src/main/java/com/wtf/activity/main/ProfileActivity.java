package com.wtf.activity.main;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.facebook.FacebookSdk;
import com.facebook.applinks.AppLinkData;
import com.sromku.simple.fb.SimpleFacebook;
import com.wtf.activity.R;
import com.wtf.activity.fragments.ProfileFragment;
import com.wtf.activity.util.FragmentsUtil;
import com.wtf.activity.util.SwitchUtil;

import java.net.URL;

import bolts.AppLinks;


public class ProfileActivity extends BaseActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {

    private TextView backProfile;
    private ImageView leftArrowProfile;
    private TextView titleProfileActivity;
    private SwitchCompat profileSwitch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        if (savedInstanceState==null){
            FragmentsUtil.addFragment(getSupportFragmentManager(), ProfileFragment.newInstance(),R.id.profileContainer);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

            window.setStatusBarColor(this.getResources().getColor(R.color.wtf_dark_blue));
        }
        findViews();
        setListeners();

        profileSwitch.setChecked(MyApp.userManager.isIncognito());
        SwitchUtil.switchColor(MyApp.userManager.isIncognito(),profileSwitch,this);


//        SimpleFacebook.initialize(this);
//        FacebookSdk.sdkInitialize(getApplication());
//
//        Uri targetUrl = AppLinks.getTargetUrlFromInboundIntent(ProfileActivity.this, getIntent());
//        if (targetUrl != null) {
//            Log.i("Activity", "App Link Target URL: " + targetUrl.toString());
//        } else {
//
//
//            AppLinkData.fetchDeferredAppLinkData(
//                    this,
//                    new AppLinkData.CompletionHandler() {
//                        @Override
//                        public void onDeferredAppLinkDataFetched(AppLinkData appLinkData) {
//                            //process applink data
//                            Log.i("Activity", "App Link Target URL: " + appLinkData);
//
//                        }
//                    });
//        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (MyApp.appManager.isBackFromBusiness()){
            MyApp.appManager.setBackFromBusiness(false);
            Intent i = new Intent(ProfileActivity.this, MapActivity.class);
            startActivity(i);
            finish();
        }
    }

    private void findViews(){
        backProfile=(TextView)findViewById(R.id.backProfile);
        leftArrowProfile=(ImageView)findViewById(R.id.leftArrowProfile);
        titleProfileActivity=(TextView)findViewById(R.id.titleProfileActivity);
        profileSwitch= (SwitchCompat) findViewById(R.id.profileSwitch);

    }
    private void setListeners() {
        backProfile.setOnClickListener(this);
        leftArrowProfile.setOnClickListener(this);
        profileSwitch.setOnCheckedChangeListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.backProfile:
                onBackPressed();
                break;
            case R.id.leftArrowProfile:
                onBackPressed();
                break;
        }

    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            MyApp.userManager.setIsIncognito(isChecked);
            MyApp.userManager.save();
        SwitchUtil.switchColor(isChecked,profileSwitch,this);
    }
}
