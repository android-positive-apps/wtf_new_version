package com.wtf.activity.main;

import android.animation.Animator;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.app.Activity;
import android.os.CountDownTimer;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnticipateOvershootInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.wtf.activity.R;
import com.wtf.activity.animations.MyAnimationListener;
import com.wtf.activity.objects.Appuser;
import com.wtf.activity.util.AppUtil;
import com.wtf.activity.util.ContentProviderUtil;
import com.wtf.activity.util.ToastUtil;

public class SplashActivity extends BaseActivity {

    private ImageView loadingImage;
    private CountDownTimer mTimer;
    private static final int SPLASH_MIN_MILLIS = 5000;
    private boolean networkConnected;
    private Tracker mTracker;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        InitAnalytic();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

            window.setStatusBarColor(this.getResources().getColor(R.color.wtf_dark_blue));
        }

        loadingImage = (ImageView)findViewById(R.id.loadingImage);

        Runnable r = new Runnable() {
            @Override
            public void run() {
                loadingImage.setVisibility(View.VISIBLE);
                YoYo.with(Techniques.RotateIn).duration(5000)
                        .playOn(loadingImage);
            }
        };
            Handler h = new Handler();
            h.postDelayed(r, 300);

        networkConnected = AppUtil.isNetworkAvailable(this);

        startCountDown();
//        for (int i = 0; i<5;i++){
//            Runnable r = new Runnable() {
//                @Override
//                public void run() {
//                    YoYo.with(Techniques.RotateIn).duration(1000).withListener(new MyAnimationListener() {
//                        @Override
//                        public void onAnimationEnd(Animator animation) {
//                            super.onAnimationEnd(animation);
//                            Toast.makeText(SplashActivity.this, "lol", Toast.LENGTH_LONG).show();
//
//                        }
//
//
//                    }).playOn(loadingImage);
//                }
//            };
//
//
//            Handler h = new Handler();
//            h.postDelayed(r, 1000);
//        }




//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                final Intent mainIntent = new Intent(SplashActivity.this, LoginActivity.class);
//                SplashActivity.this.startActivity(mainIntent);
//                SplashActivity.this.finish();
//            }
//        }, 5000);
    }

        private void startCountDown() {
            mTimer = new CountDownTimer(SPLASH_MIN_MILLIS, 1000) {

                @Override
                public void onTick(long millisUntilFinished) {
//                    Runnable r = new Runnable() {
//                @Override
//                public void run() {
//                    YoYo.with(Techniques.RotateIn).duration(1000)
//                            .playOn(loadingImage);
//                }
//            };
//
//
//            Handler h = new Handler();
//            h.postDelayed(r, 0);


                }

                @Override
                public void onFinish() {
                    if (networkConnected) {
                        final Intent mainIntent = new Intent(SplashActivity.this, LoginActivity.class);
                        SplashActivity.this.startActivity(mainIntent);
                        SplashActivity.this.finish();
                    }else {
                        showConnectionDialog();
                        ToastUtil.toaster(getResources().getString(R.string.please_check_your_internet), false);
                    }
                }
            };

            mTimer.start();
        }

    private void InitAnalytic(){
        MyApp application = (MyApp) getApplication();
        mTracker = application.getDefaultTracker();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mTracker.setScreenName("Splash screen");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    private void showConnectionDialog() {
        LayoutInflater factory = LayoutInflater.from(this);
        final View deleteDialogView = factory.inflate(
                R.layout.location_dialog, null);
        final AlertDialog deleteDialog = new AlertDialog.Builder(this).create();
        deleteDialog.setView(deleteDialogView);
        TextView textView2 = (TextView) deleteDialogView.findViewById(R.id.textView2);
        textView2.setText(getResources().getString(R.string.network_not_availible));
        deleteDialogView.findViewById(R.id.openSettingsB).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                ContentProviderUtil.openConnectionSettings(SplashActivity.this);

                //your business logic

            }
        });
        Button b = (Button) deleteDialogView.findViewById(R.id.okB);
        b.setText(getResources().getString(R.string.try_again));
        deleteDialogView.findViewById(R.id.okB).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                finish();
                Intent i = new Intent(SplashActivity.this,SplashActivity.class);
                startActivity(i);
                deleteDialog.dismiss();

            }
        });

        deleteDialog.show();

    }

}
