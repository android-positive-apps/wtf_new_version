package com.wtf.activity.main;

import android.os.Bundle;
import android.app.Activity;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.SwitchCompat;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.wtf.activity.R;
import com.wtf.activity.fragments.BusinessPageFragment;
import com.wtf.activity.fragments.ProfileFragment;
import com.wtf.activity.util.FragmentsUtil;
import com.wtf.activity.util.SwitchUtil;

public class BusinessActivity extends BaseActivity implements CompoundButton.OnCheckedChangeListener {

    public static DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private SwitchCompat incognitoSwitch;
    private TextView incoTv;
    private ImageView locationIconActionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_business);

        setupActionBar(R.id.app_actionbar, getString(R.string.app_name));

        setUpDrawerNavigation();

        findActionBarViews();

        if (savedInstanceState==null){
            FragmentsUtil.addFragment(getSupportFragmentManager(), BusinessPageFragment.newInstance(), R.id.businessContainer);
        }

    }

    private void findActionBarViews() {
        incoTv=(TextView)findViewById(R.id.incoTv);
        incognitoSwitch= (SwitchCompat) findViewById(R.id.incognitoSwitch);
        locationIconActionBar= (ImageView)findViewById(R.id.locationIconActionBar);
        incoTv.setVisibility(View.VISIBLE);
        incognitoSwitch.setVisibility(View.VISIBLE);
        locationIconActionBar.setVisibility(View.INVISIBLE);
        SwitchUtil.switchColor(MyApp.userManager.isIncognito(),incognitoSwitch,this);
        incognitoSwitch .setOnCheckedChangeListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        incognitoSwitch.setChecked(MyApp.userManager.isIncognito());


    }

    private void setUpDrawerNavigation() {
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerToggle = new ActionBarDrawerToggle
                (this, mDrawerLayout, getActionbarToolbar(), 0, 0) {

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }
        };
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        MyApp.appManager.setFromMap(false);
        MyApp.appManager.save();
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        MyApp.userManager.setIsIncognito(isChecked);
        MyApp.userManager.save();
        SwitchUtil.switchColor(isChecked,incognitoSwitch,this);
        if (!MyApp.appManager.isAnonymous())
            MapActivity.updatAppUserEnabledLoc();
    }
}
