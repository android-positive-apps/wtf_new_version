package com.wtf.activity.main;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.AppCompatCheckBox;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.LoginEvent;
import com.crashlytics.android.answers.SignUpEvent;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.sromku.simple.fb.Permission;
import com.sromku.simple.fb.SimpleFacebook;
import com.sromku.simple.fb.entities.Profile;
import com.sromku.simple.fb.listeners.OnLoginListener;
import com.sromku.simple.fb.listeners.OnProfileListener;
import com.wtf.activity.R;
import com.wtf.activity.network.NetworkCallback;
import com.wtf.activity.network.requests.ApplicationLoginRequest;
import com.wtf.activity.network.requests.UpdateAppuserRequest;
import com.wtf.activity.network.response.ApplicationLoginResponse;
import com.wtf.activity.network.response.ResponseObject;
import com.wtf.activity.network.response.UpdateAppuserResponse;
import com.wtf.activity.objects.MainDetails;
import com.wtf.activity.util.ToastUtil;

import java.util.List;

public class LoginActivity extends BaseActivity implements View.OnClickListener {

    private TextView notNow;
    private SimpleFacebook mSimpleFacebook;
    private LinearLayout LoginFacebookLinear;
    private final String TAG= "LoginActivity";
    private AppCompatCheckBox checkBox;
    private TextView termsTv;
    private TextView cookiesTv;
    private TextView policyTv;
    private Tracker mTracker;

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        if (MyApp.userManager.isLoggedIn()){
            finish();
            Intent i = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(i);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        InitAnalytic();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

            window.setStatusBarColor(this.getResources().getColor(R.color.wtf_dark_blue));
        }

        findViews();
        setListeners();

//        if (!MyApp.appManager.isAccaptedTerms()){
////            showTermsDialog();
//            showWebViewDialog(getResources().getString(R.string.terms_of_use),getResources().getString(R.string.terms_of_use_gowayu));
//
//        }

    }


    private void findViews() {
        LoginFacebookLinear = (LinearLayout) findViewById(R.id.LoginFacebookLinear);
        notNow=(TextView)findViewById(R.id.notNow);
        termsTv=(TextView)findViewById(R.id.termsTv);
        cookiesTv=(TextView)findViewById(R.id.cookiesTv);
        policyTv=(TextView)findViewById(R.id.policyTv);
        checkBox=(android.support.v7.widget.AppCompatCheckBox)findViewById(R.id.checkBox);
    }
    private void setListeners() {
        LoginFacebookLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(checkBox.isChecked())
                performFbLogin();
                else
                    ToastUtil.toaster(getResources().getString(R.string.agree_terms),false);
            }
        });
        notNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkBox.isChecked()) {

                    Intent i = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(i);
                    MyApp.userManager.setLoggedIn(false);
                    MyApp.appManager.setAnonymous(true);
//                MainDetails mainDetails = new MainDetails("0","0","0","0","0","0","0");
//                MyApp.appManager.getMainDetails().setFriends("0");
//                MyApp.appManager.getMainDetails().setPoints("0");
                    MainDetails mainDetails = MyApp.appManager.getMainDetails();
                    if (mainDetails != null) {
                        mainDetails.setPoints("0");
                        mainDetails.setFriends("0");
                        MyApp.appManager.setMainDetails(mainDetails);
                    }
                    MyApp.appManager.save();
                    MyApp.userManager.save();
                }else {
                    ToastUtil.toaster(getResources().getString(R.string.agree_terms),false);
                }
            }
        });
        termsTv.setOnClickListener(this);
        cookiesTv.setOnClickListener(this);
        policyTv.setOnClickListener(this);

        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    MyApp.appManager.setAccaptedTerms(true);
                    MyApp.appManager.save();
                }else {
                    MyApp.appManager.setAccaptedTerms(false);
                    MyApp.appManager.save();
                }
            }
        });
    }


    private void performLogin(final Profile profile) {
        String fbID = profile.getId();
        System.out.println("--->  login id= " + fbID);

//        if (MyApp.userManager.getAppuser()==null) {
            MyApp.networkManager.makeRequest(new ApplicationLoginRequest(fbID),
                    new NetworkCallback<ApplicationLoginResponse>() {
                        @Override
                        public void onResponse(boolean success, String errorDesc, ResponseObject<ApplicationLoginResponse> response) {
                            super.onResponse(success, errorDesc, response);
                            if (success) {
                                System.out.println("--->  login id= "+response.getData().getAppuser().getID());
                                MyApp.userManager.setLoggedIn(true);
                                MyApp.userManager.setAppuser(response.getData().getAppuser());
                                MyApp.userManager.setMyGifts(response.getData().getMyGifts());
                                MyApp.userManager.setReceivedGifts(response.getData().getReceivedGifts());
                                MyApp.userManager.setUpdateInterval(response.getData().getUpdateInterval());
                                MyApp.userManager.save();
                                MyApp.appManager.setAnonymous(false);
                                MyApp.appManager.save();

                                dismissProgressDialog();

                                Intent i = new Intent(LoginActivity.this, MainActivity.class);
                                startActivity(i);

                                Answers.getInstance().logLogin(new LoginEvent()
                                        .putMethod("Login")
                                        .putSuccess(true)
                                        .putCustomAttribute("Name",profile.getName()+"")
                                        .putCustomAttribute("FbID", profile.getId())
                                        .putCustomAttribute("AppUserID", response.getData().getAppuser().getID()));

                            } else {
                                Answers.getInstance().logLogin(new LoginEvent()
                                        .putMethod("Login")
                                        .putSuccess(false)
                                        .putCustomAttribute("Name",profile.getName()+"")
                                        .putCustomAttribute("FbID", profile.getId()));

                                updateAppUser(profile);
                            }
                        }
                    });
//        }else {
//            if (fbID.equals(MyApp.userManager.getAppuser().getFBID())) {
//                MyApp.userManager.setLoggedIn(true);
//                MyApp.appManager.setAnonymous(false);
//                MyApp.userManager.save();
//                MyApp.appManager.save();
//                dismissProgressDialog();
//
//                Intent i = new Intent(LoginActivity.this, MainActivity.class);
//                startActivity(i);
//            }else {
//                MyApp.userManager.setAppuser(null);
//                MyApp.userManager.save();
//                performLogin(fbID);
//            }
//        }
    }

    private void updateAppUser(final Profile profile) {
//        showProgressDialog();


        MyApp.networkManager.makeRequest(new UpdateAppuserRequest("0",profile.getFirstName(), profile.getLastName(), profile.getId(), profile.getEmail(), profile.getPicture(), this), new NetworkCallback<UpdateAppuserResponse>() {
            @Override
            public void onResponse(boolean success, String errorDesc, ResponseObject<UpdateAppuserResponse> response) {
                super.onResponse(success, errorDesc, response);
                if (success) {
//                    getStatistics();
                    int id = response.getData().getID();
                    performLogin(profile);
                    Log.d(TAG, "success UpdateAppuserRequest"+id);

                    Answers.getInstance().logSignUp(new SignUpEvent()
                            .putMethod("Register")
                            .putSuccess(true)
                            .putCustomAttribute("Name",profile.getName()+"")
                            .putCustomAttribute("FbID", profile.getId())
                            .putCustomAttribute("AppUserID", id));


                } else {
//                    dismissProgressDialog();

                }
                dismissProgressDialog();
            }
        });

    }
    private void InitAnalytic(){
        MyApp application = (MyApp) getApplication();
        mTracker = application.getDefaultTracker();
    }



    @Override
    public void onResume() {
        super.onResume();
        mTracker.setScreenName("Login screen");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        mSimpleFacebook = SimpleFacebook.getInstance(this);
        if (MyApp.userManager.isLoggedIn()){
            finish();
        }
        if (MyApp.appManager.isAccaptedTerms()){
            checkBox.setChecked(true);
        }else {
            checkBox.setChecked(false);
        }

    }

    private void performFbLogin() {

        final OnLoginListener onLoginListener = new OnLoginListener() {

            @Override
            public void onException(Throwable throwable) {

            }

            @Override
            public void onFail(String reason) {

            }

            @Override
            public void onLogin(String accessToken,
                                List<Permission> acceptedPermissions,
                                List<Permission> declinedPermissions) {
                MyApp.userManager.setFBaccessToken(accessToken);

                getFbProfile();
                MyApp.appManager.setAnonymous(false);
                MyApp.userManager.save();
            }

            @Override
            public void onCancel() {


            }
        };
        mSimpleFacebook.login(onLoginListener);


    }
    private void getFbProfile() {
        OnProfileListener onProfileListener = new OnProfileListener() {
            @Override
            public void onComplete(Profile profile) {
//                System.out.println("--->  profile!!!!"+profile.getFirstName()+" "+profile.getLastName()+" "+profile.getName()+" "+profile.getMiddleName()+" dd");

//                System.out.println("--->  My profile id = " + profile.getId()+profile.toString());

                showProgressDialog();
                performLogin(profile);
                MyApp.userManager.setUserProfile(profile);
                MyApp.userManager.save();



            }



        };
        mSimpleFacebook.getProfile(onProfileListener);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        mSimpleFacebook.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }


    private void showTermsDialog() {

        LayoutInflater factory = LayoutInflater.from(this);
        final View deleteDialogView = factory.inflate(
                R.layout.terms_dialog, null);
        final AlertDialog deleteDialog = new AlertDialog.Builder(this).create();
        deleteDialog.setView(deleteDialogView);
        deleteDialog.setCancelable(false);
        deleteDialog.setCanceledOnTouchOutside(false);
        deleteDialogView.findViewById(R.id.disagree).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                deleteDialog.setCancelable(true);
                finish();
                //your business logic
                deleteDialog.dismiss();

            }
        });
        deleteDialogView.findViewById(R.id.agree).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                deleteDialog.setCancelable(true);
                MyApp.appManager.setAccaptedTerms(true);
                MyApp.appManager.save();
                deleteDialog.dismiss();

            }
        });

        deleteDialog.show();

    }
    private  void showWebViewDialog(String title,String link){
        final AlertDialog.Builder alert;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            alert = new AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            alert = new AlertDialog.Builder(this);
        }
//        final AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle(title);
        alert.setCancelable(false);

        WebView wv = new WebView(this);
        wv.loadUrl(link);
        wv.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);

                return true;
            }

        });

        alert.setView(wv);

        alert.setNegativeButton(getResources().getString(R.string.disagree), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                alert.setCancelable(true);
                finish();
                dialog.dismiss();
            }
        });
        alert.setPositiveButton(getResources().getString(R.string.agree), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                alert.setCancelable(true);
                MyApp.appManager.setAccaptedTerms(true);
                MyApp.appManager.save();

                dialog.dismiss();
            }
        });
        alert.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.policyTv:
                showRegolarWebDialog(getResources().getString(R.string.privacy_policy),getResources().getString(R.string.privacy_policy_gowayu));
                break;
            case R.id.termsTv:
                showRegolarWebDialog(getResources().getString(R.string.terms_of_use),getResources().getString(R.string.terms_of_use_gowayu));
                break;
            case R.id.cookiesTv:
                showRegolarWebDialog(getResources().getString(R.string.about_the_app_text),getResources().getString(R.string.about_app_gowayu));
                break;
        }
    }

    private void showRegolarWebDialog(String title, String link) {
        final AlertDialog.Builder alert;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            alert = new AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            alert = new AlertDialog.Builder(this);
        }
        alert.setTitle(title);

        WebView wv = new WebView(this);
        wv.loadUrl(link);

        wv.setWebViewClient(new WebViewClient() {


            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);

                return true;
            }
        });

        alert.setView(wv);
        alert.setNegativeButton(getResources().getString(R.string.close), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        alert.show();
    }
}
