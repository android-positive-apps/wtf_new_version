package com.wtf.activity.main;

import android.app.AlertDialog;
import android.app.LoaderManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FilterQueryProvider;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RatingBar;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
//import com.google.maps.android.clustering.Cluster;
//import com.google.maps.android.clustering.ClusterManager;
//import com.google.maps.android.clustering.view.DefaultClusterRenderer;
//import com.google.maps.android.ui.IconGenerator;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.google.maps.android.ui.IconGenerator;
import com.squareup.picasso.Callback;
import com.sromku.simple.fb.SimpleFacebook;
import com.sromku.simple.fb.entities.Profile;
import com.sromku.simple.fb.listeners.OnFriendsListener;
import com.wtf.activity.R;
import com.wtf.activity.adapters.BusnissesMapListAdapter;
import com.wtf.activity.adapters.CheckinDialogAdapter;
import com.wtf.activity.adapters.FriendsListArrayAdapter;
import com.wtf.activity.adapters.FriendsMapListCursorAdapter;
import com.wtf.activity.animations.MyAnimationListener;
import com.wtf.activity.database.SelectionBuilder;
import com.wtf.activity.database.tables.TableFriends;
import com.wtf.activity.fragments.BusinessPageFragment;
import com.wtf.activity.network.NetworkCallback;
import com.wtf.activity.network.requests.AppuserCheckInRequest;
import com.wtf.activity.network.requests.GetBusinessV2Request;
import com.wtf.activity.network.requests.GetBusinessesByCategoryRequest;
import com.wtf.activity.network.requests.GetFriendsLocationV2Request;
import com.wtf.activity.network.requests.GetPeopleAroundRequest;
import com.wtf.activity.network.requests.SearchBusinessRequest;
import com.wtf.activity.network.requests.SearchRequest;
import com.wtf.activity.network.requests.UpdateAppuserEnableLocationRequest;
import com.wtf.activity.network.requests.UpdateAppuserRadiusRequest;
import com.wtf.activity.network.response.AppuserCheckInResponse;
import com.wtf.activity.network.response.GetBusinessV2Response;
import com.wtf.activity.network.response.GetBusinessesByCategoryResponse;
import com.wtf.activity.network.response.GetFriendsLocationV2Response;
import com.wtf.activity.network.response.GetPeopleAroundResponse;
import com.wtf.activity.network.response.ResponseObject;
import com.wtf.activity.network.response.ResponseObject2;
import com.wtf.activity.network.response.SearchBusinessResponse;
import com.wtf.activity.network.response.SearchResponse;
import com.wtf.activity.network.response.UpdateAppuserRadiusResponse;
import com.wtf.activity.network.response.UpdateAppuserResponse;
import com.wtf.activity.objects.Appuser;
import com.wtf.activity.objects.Business;
import com.wtf.activity.objects.Friend;
import com.wtf.activity.objects.PeopleLocation;
import com.wtf.activity.ui.MultiDrawable;
import com.wtf.activity.ui.RoundedImageView;
import com.wtf.activity.util.AppUtil;
import com.wtf.activity.util.BitmapUtil;
import com.wtf.activity.util.ContentProviderUtil;
import com.wtf.activity.util.DialogExample;
import com.wtf.activity.util.ImageLoader;
import com.wtf.activity.util.SwitchUtil;
import com.wtf.activity.util.ToastUtil;

import java.util.ArrayList;
import java.util.List;

public class MapActivity extends BaseActivity implements View.OnClickListener, OnMapReadyCallback, LoaderManager.LoaderCallbacks<Cursor>, GoogleMap.OnMarkerClickListener {
    public static final int TYPE_BUSINESS = 1;
    public static final int TYPE_FRIEND = 2;
    public static final String ACTION_RATE_PLACE = "actionRatePlace";
    public static final String ACTION_LOCATE_FRIEND = "actionLocateFriend";
    public static final String ACTION_PlACES= "action_places";
    public static DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private FrameLayout checkinMapButton;
    private FrameLayout placesMapButton;
    private ImageView locationIconActionBar;
    private LinearLayout actionBarLinear;
    private LinearLayout searchMenuLinearLayout;
    private EditText searchMenuEditText;
    private TextView cancelMenuSearch;
    private MenuItem searchMenuButton;
    private LinearLayout RadiusSearchLinear;
    private String searchString;
    private LinearLayout search_list_map_view;
    private Toolbar mToolbar;
    private LinearLayout mapPopUpButtons;
    private ImageView nightlife_button;
    private ImageView fashion_button;
    private ImageView food_button;
    private ImageView places_grey_button;
    private ImageView sport_button;
    private ImageView entertainment_button;
    private ImageView services_button3x;
    private EditText radiusEditText;
    private ImageView radiusMinusButton;
    private ImageView radiusPlusButton;
    private SwitchCompat incognitoSwitch;
    private ListView FriendsListMapSearch;
    private ListView FriendsListMapSearch2;
    private ListView placesListMapSearch;
    private SimpleFacebook mSimpleFacebook;
    private int dialogItemPosition = -1;
    private BusnissesMapListAdapter busnissesMapListAdapter;
    private ArrayList<Business> businessesFromSearch = new ArrayList<>();
    private ArrayList<Friend> friendsFromSearch = new ArrayList<>();
    private MapFragment mMap;
    private GoogleMap googleMap;

    //    ArrayList<ArrayList<Business>> allCategorys = new ArrayList<ArrayList<Business>>();
//    ArrayList<Business> fashion = new ArrayList<Business>();
//    ArrayList<Business> sport = new ArrayList<Business>();
//    ArrayList<Business> food = new ArrayList<Business>();
//    ArrayList<Business> nightLife = new ArrayList<Business>();
//    ArrayList<Business> entertainment = new ArrayList<Business>();
//    ArrayList<Business> hotel = new ArrayList<Business>();
    private FrameLayout frameContainer;
    private ArrayList<Friend> friendsLocation;
    private LatLng usrLatLng;
    private Handler handler;

    ArrayList<com.google.android.gms.maps.model.Marker> allMarkers;
    private ArrayList<Business> businesses = new ArrayList<>();
    private FriendsMapListCursorAdapter friendsMapListCursorAdapter;
    private boolean search_list_mapIsVisable;
    private ClusterManager<Friend> mClusterManager;
    private ArrayList<Friend> allFriends;
    private boolean anonymous = MyApp.appManager.isAnonymous();
    private final String TAG= "MapActivity";
    private ClusterManager<PeopleLocation> peopleClusterMangager;
    private FriendsListArrayAdapter friendsListArrayAdapter;
    private String serchString = "";
    private FrameLayout noPlacesFrame;
    private FrameLayout noFriendsFrame;
    private SwipeRefreshLayout swipe;
    private boolean fromProfile=false;
    private boolean fromTrendy=false;
    private Tracker mTracker;
    private ImageView refreshButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        InitAnalytic();

        setupActionBar(R.id.app_actionbar, getString(R.string.app_name));
        setUpDrawerNavigation();

        findViews();

        setListeners();
        setAdapters();
        setDetails();

        setUpMap();

    }

    private void setUpMap() {
        allMarkers = new ArrayList<>();
        mMap = (MapFragment) getFragmentManager()
                .findFragmentById(R.id.mapFragment);
        mMap.getMapAsync(this);
        googleMap = mMap.getMap();


    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        usrLatLng = MyApp.appManager.getLatLng();
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(usrLatLng, 14));
//        googleMap.moveCamera(CameraUpdateFactory.newLatLng(usrLatLng));
        System.out.println("--->  locationTest Map "+usrLatLng.latitude+"||"+usrLatLng.longitude);
        makeUserMarker();
        if (!anonymous)
        getFriendsLocation();
        googleMap.setOnMarkerClickListener(this);
        setCluster(googleMap);
        setPeopleAroundMarkers();
        getIntentAction();


    }


    public static void updatAppUserEnabledLoc() {
        MyApp.networkManager.makeRequest(new UpdateAppuserEnableLocationRequest(),new NetworkCallback<UpdateAppuserResponse>(){
            @Override
            public void onResponse(boolean success, String errorDesc, ResponseObject<UpdateAppuserResponse> response) {
                super.onResponse(success, errorDesc, response);

            }
        });
    }

    private void setPeopleAroundMarkers() {
        String radius = "30";
        MyApp.networkManager.makeRequest(new GetPeopleAroundRequest(radius), new NetworkCallback<GetPeopleAroundResponse>() {
            @Override
            public void onResponse(boolean success, String errorDesc, ResponseObject<GetPeopleAroundResponse> response) {
                if (success) {
                    Log.d(TAG, "success GetPeopleAroundRequest");
                    addPeopleAroundMarker(response.getData().getPeopleLocations());
                    peopleClusterMangager.cluster();

                }
            }
        });
    }

    private void addPeopleAroundMarker(final ArrayList<PeopleLocation> peopleLocations) {
        peopleClusterMangager.clearItems();
        if (peopleLocations.size()>0)
            for (PeopleLocation pl:peopleLocations ){
                LatLng latLng = new LatLng(pl.getLat(),pl.getLng());
                boolean isUser= false;
                if (latLng.latitude==usrLatLng.latitude&&latLng.longitude==usrLatLng.longitude) {
                    isUser = true;
                    frameContainer.removeAllViews();

                }
                boolean isFriend =false;
                if (allFriends!=null&&allFriends.size()>0)
                    for (Friend f : friendsLocation){
                        if(f.getLat().equals(latLng.latitude+"")&&
                                f.getLng().equals(latLng.longitude+"")){
                            isFriend=true;
                            System.out.println("--->  isrind: "+f.getLat()+"="+latLng.latitude+
                                    " --"+  f.getLng()+" ="+latLng.longitude);
                            frameContainer.removeAllViews();

                        }
                    }
                if (!isFriend&&!isUser) {
                    peopleClusterMangager.addItem(pl);

                }

        }

//        handler = new Handler();
//        final View view = getLayoutInflater().inflate(R.layout.stranger_marker, null);
//        final Bitmap bitmap = BitmapUtil.createBitmapFromView(frameContainer, view);
//
//        final Runnable r = new Runnable() {
//            public void run() {
//                if (peopleLocations!=null && peopleLocations.size()>0)
//                    for (PeopleLocation pl: peopleLocations){
//                        LatLng latLng = new LatLng(pl.getLat(),pl.getLng());
//                        boolean isUser= false;
//                        if (latLng.latitude==usrLatLng.latitude&&latLng.longitude==usrLatLng.longitude) {
//                            isUser = true;
//                            frameContainer.removeAllViews();
//
//                        }
//                        boolean isFriend =false;
//                        if (allFriends!=null&&allFriends.size()>0)
//                            for (Friend f : friendsLocation){
//                                    if(f.getLat().equals(latLng.latitude+"")&&
//                                            f.getLng().equals(latLng.longitude+"")){
//                                        isFriend=true;
//                                        System.out.println("--->  isrind: "+f.getLat()+"="+latLng.latitude+
//                                              " --"+  f.getLng()+" ="+latLng.longitude);
//                                        frameContainer.removeAllViews();
//
//                                    }
//                            }
//                        if (!isFriend&&!isUser) {
//                            Marker m = googleMap.addMarker(new MarkerOptions().position(latLng).
//                                    icon(BitmapDescriptorFactory.fromBitmap(bitmap)));
//                            frameContainer.removeAllViews();
//                            if (m != null)
//                                allMarkers.add(m);
//                        }
//                    }
//
//            }
//        };
//
//        handler.postDelayed(r, 0);
//



    }

    private void setCluster(GoogleMap map) {
//        LatLng location = new LatLng(usrLatLng.latitude, usrLatLng.longitude);
//        map.moveCamera(CameraUpdateFactory.newLatLngZoom(location, 9.5f));

        mClusterManager = new ClusterManager<Friend>(this, map);
        mClusterManager.setRenderer(new FriendRenderer(map));

//        map.setOnMarkerClickListener(mClusterManager);
        peopleClusterMangager = new ClusterManager<PeopleLocation>(this,map);
        peopleClusterMangager.setRenderer(new PeopleRenderer(map));
        map.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition cameraPosition) {
//                peopleClusterMangager.onCameraIdle();
                peopleClusterMangager.onCameraChange(cameraPosition);
                mClusterManager.onCameraChange(cameraPosition);
            }
        });

//        map.setOnInfoWindowClickListener(mClusterManager);
//        mClusterManager.setOnClusterClickListener(this);
//        mClusterManager.setOnClusterInfoWindowClickListener(this);
//        mClusterManager.setOnClusterItemClickListener(this);
//        mClusterManager.setOnClusterItemInfoWindowClickListener(this);

        peopleClusterMangager.cluster();
        mClusterManager.cluster();
    }
    private void addItems() {
//        ArrayList<Integer> dd= new ArrayList<>();
//        dd.add(R.drawable.white_facebook_button3x);
//        dd.add(R.drawable.search_radius_minus_button3x       );
//        dd.add(R.drawable.business_page_share_icon3x        );
//        dd.add(R.drawable.abc_btn_rating_star_on_mtrl_alpha );
//        dd.add(R.drawable.plus_button3x);
//        dd.add(R.drawable.x_icon);
//        int j=0;
//        //test:
//        Friend f = new Friend(0,"1","32.4337700","34.8415267","111","yosi","yosi",""+R.drawable.white_facebook_button3x,"s");
//        Friend f2 = new Friend(2,"123","32.4337700","34.8415267","111","meir","meir",""+R.drawable.search_radius_minus_button3x     ,"s");
//        Friend f3 = new Friend(2,"123","32.1337948","34.8415700","111","izit","dsad",""+R.drawable.business_page_share_icon3x       ,"s");
//        Friend f4 = new Friend(2,"123","32.9337544","34.9423333","111","a","alla",""+R.drawable.abc_btn_rating_star_on_mtrl_alpha,"s");
//        Friend f5 = new Friend(2,"123","32.8337544","34.8423333","111","s","alla",""+R.drawable.plus_button3x,"s");
//        Friend f6 = new Friend(2,"123","32.8337700","34.8423450","111","c","alla",""+R.drawable.x_icon,"s");
//        Friend f = new Friend(0,"10206136325629038","32.4337700","34.8415267","111","yosi","yosi","http://www.mishlohim.co.il/data//agadir1.jpg","s");
//        Friend f2 = new Friend(0,"10206136325629038","32.4337700","34.8415267","111","meir","meir","http://sfile.f-static.com/image/users/16584/ftp/my_files/vncurdr.jpg","s");
//        Friend f3 = new Friend(0,"10206136325629038","32.1337948","34.8415700","111","izit","dsad","http://www.mishlohim.co.il/data//agadir1.jpg","s");
//        allFriends.add(f);
//        allFriends.add(f2);
//        allFriends.add(f3);
//
//        BitmapUtil.saveImage(getApplicationContext(), b, f.getFBID(), BitmapUtil.IMAGE_EXTENATION);

        // Add ten cluster items in close proximity, for purposes of this example.
        if (allFriends!=null&&allFriends.size()>0)
        for (int i = 0; i <allFriends.size(); i++) {
            mClusterManager.addItem(allFriends.get(i));
        }
    }

//    @Override
//    public boolean onClusterItemClick(final Friend friend) {
//
//        googleMap.animateCamera(CameraUpdateFactory.newLatLng(friend.getPosition()), 150, new GoogleMap.CancelableCallback() {
//            @Override
//            public void onFinish() {
//                showInfoWindow(TYPE_FRIEND, friend);
//
//            }
//
//            @Override
//            public void onCancel() {
//
//            }
//        });
//        return true;
//    }

//    @Override
//    public boolean onClusterClick(Cluster<Friend> cluster) {
//        googleMap.getCameraPosition();
//        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(cluster.getPosition(), 12));
//
//        return true;
//    }

    private void makeUserMarker() {
//        googleMap.addMarker(new MarkerOptions().position(usrLatLng).title("Me"));
        final View view = getLayoutInflater().inflate(R.layout.marker_layout, null);
        RoundedImageView image = (RoundedImageView) view.findViewById(R.id.marker_image);
        if (!anonymous) {
            Bitmap bit = BitmapUtil.loadImageFromStorage(MyApp.appManager.getImagePath(), MyApp.userManager.getAppuser().getFBID());
            if (bit!=null) {
                image.setImageBitmap(bit);
                Bitmap bitmap = BitmapUtil.createBitmapFromView(frameContainer, view);


                Marker m = googleMap.addMarker(new MarkerOptions().position(usrLatLng).
                        icon(BitmapDescriptorFactory.fromBitmap(bitmap)).title(getString(R.string.me)));
                frameContainer.removeAllViews();
                if (m != null)
                    allMarkers.add(m);

                System.out.println("--->  userStroge PHOTO");
            } else {
                ImageLoader.ByUrl(
                        MyApp.userManager.getUserProfile().getPicture(), image, R.drawable.ic_launcher,
                        true, image.getWidth(), image.getHeight(), new Callback() {
                            @Override
                            public void onSuccess() {
//                            System.out.println("--->  " + MyApp.userManager.getUserProfile().getPicture());
//        BitmapDescriptor b = new BitmapDescriptor()
                                Bitmap bitmap = BitmapUtil.createBitmapFromView(frameContainer, view);


                                Marker m = googleMap.addMarker(new MarkerOptions().position(usrLatLng).
                                        icon(BitmapDescriptorFactory.fromBitmap(bitmap)).title(getString(R.string.me)));
                                frameContainer.removeAllViews();
                                if (m != null)
                                    allMarkers.add(m);
                            }

                            @Override
                            public void onError() {
                                Marker m = googleMap.addMarker(new MarkerOptions().position(usrLatLng)
                                        .title(getString(R.string.me)));
                                frameContainer.removeAllViews();
                                allMarkers.add(m);
                            }
                        });
            }}else {
            Marker m = googleMap.addMarker(new MarkerOptions().position(usrLatLng)
                    .title(getString(R.string.me)));
            frameContainer.removeAllViews();
            allMarkers.add(m);
        }

    }

    private void addFriendsMarker() {
        allFriends.clear();
        mClusterManager.clearItems();
        for (final Friend friend : friendsLocation) {
            String sb = new SelectionBuilder().where(TableFriends.TAG_FBID).equealTo(friend.getFBID()).build();
            ArrayList<Friend> friends = MyApp.tableFriends.read(sb, TableFriends.TAG_FBID +" asc");
            Friend f = null;
            if (friends.size() > 0) {
                f = friends.get(0);
                if (f != null) {
                    f.setLat(friend.getLat());
                    f.setLng(friend.getLng());
                    f.setID(friend.getID());
                }
                System.out.println("--->  addFriendsMarker "+f.getLat()+" "+f.getID());
                MyApp.tableFriends.insertOrUpdate(f);

            }
            if (f != null){
                if (friend.getUserActivity()!=null){
                    f.setUserActivity(friend.getUserActivity());
//                    if (friend.getUserActivity().getActivity().equals("CHECKIN")) {
//
//                    }else if (friend.getUserActivity().getActivity().equals("RANK")) {
//                    }
                }
                allFriends.add(f);

            }

//            double lat = Double.parseDouble(friend.getLat());
//            double lng = Double.parseDouble(friend.getLng());
//
//            final LatLng friendLAatLng = new LatLng(lat, lng);
//            final View view = getLayoutInflater().inflate(R.layout.marker_layout, null);
//            RoundedImageView image = (RoundedImageView) view.findViewById(R.id.marker_image);

            //                if (!f.getImage().equals("")) {
//                    final Friend finalF = f;
//                    ImageLoader.ByUrl(f.getImage(), image, R.drawable.ic_launcher, true, image.getWidth(), image.getHeight(), new Callback() {
//                        @Override
//                        public void onSuccess() {
////                            Bitmap bitmap = BitmapUtil.createBitmapFromView(frameContainer, view);
//
////
////                            Marker m = googleMap.addMarker(new MarkerOptions().position(friendLAatLng).
////                                    icon(BitmapDescriptorFactory.fromBitmap(bitmap)).title(finalF.getFirstName() + " " + finalF.getLastName()));
////                            frameContainer.removeAllViews();
////                            allMarkers.add(m);
////                            mClusterManager.addItem(finalF);
////                        System.out.println("--->  friend: "+friend.getID()+","+friend.getLat()+","+
////                                friend.getLng()+", "+friend.getFirstName() );
//                        }
//
//                        @Override
//                        public void onError() {
//                            Marker m = googleMap.addMarker(new MarkerOptions().position(friendLAatLng).
//                                    title(finalF.getFirstName() + " " + finalF.getLastName()));
//                            frameContainer.removeAllViews();
//                            allMarkers.add(m);
//                        }
//                    });
//                }


        }
        addItems();

    }


    private void getFriendsLocation() {
        allFriends = new ArrayList<>();
        //getFriendsLocation:/
        ///v2
        if (!MyApp.appManager.isAnonymous()) {
            MyApp.networkManager.makeRequest(new GetFriendsLocationV2Request(MyApp.userManager.getFbFriendsIDs()),new NetworkCallback<GetFriendsLocationV2Response>(){
                @Override
                public void onResponse(boolean success, String errorDesc, ResponseObject<GetFriendsLocationV2Response> response) {
                    super.onResponse(success, errorDesc, response);
                    if (success) {
                        if (friendsLocation != null)
                            friendsLocation.clear();
                        friendsLocation = response.getData().getFriends();
                        if (friendsLocation.size() > 0) {
                            addFriendsMarker();
                        }

                        mClusterManager.cluster();
                    } else {
                        ToastUtil.toaster(errorDesc, true);
                    }
                }
            });


        }

            ////
//          if (!MyApp.appManager.isAnonymous()) {
//
//              MyApp.networkManager.makeRequest(new GetAppuserFriendsLocationRequest(MyApp.userManager.getFbFriendsIDs()), new NetworkCallback<GetAppuserFriendsLocationResponse>() {
//                  @Override
//                  public void onResponse(boolean success, String errorDesc, ResponseObject<GetAppuserFriendsLocationResponse> response) {
//                      super.onResponse(success, errorDesc, response);
//                      if (success) {
//                          if (friendsLocation != null)
//                              friendsLocation.clear();
//                          friendsLocation = response.getData().getFriends();
//                          if (friendsLocation.size() > 0) {
//                              addFriendsMarker();
//                          }
//
//                      } else {
//                          ToastUtil.toaster(errorDesc, true);
//                      }
//                  }
//              });
//          }
    }


    private void setDetails() {
        Appuser appUser;
        if (!anonymous) {
            appUser = MyApp.userManager.getAppuser();
            radiusEditText.setText(appUser.getRadius() + "");
        }
        else {
            radiusEditText.setText(MyApp.userManager.getRadius());
        }

    }


    private void setAdapters() {
        //businesses adapter:
        busnissesMapListAdapter = new BusnissesMapListAdapter(this, businessesFromSearch);
        placesListMapSearch.setAdapter(busnissesMapListAdapter);
        placesListMapSearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Business b = businessesFromSearch.get(position);
                locateBusiness(b.getLat(), b.getLng(), b);
                onBackPressed();
            }
        });
        //friends cursor adapter:
        friendsMapListCursorAdapter = new FriendsMapListCursorAdapter(this, null);

        friendsMapListCursorAdapter.setFilterQueryProvider(new FilterQueryProvider() {
            @Override
            public Cursor runQuery(CharSequence constraint) {
                serchString = constraint.toString();
                getLoaderManager().restartLoader(0,null,MapActivity.this);
                return null;
            }
        });

        getLoaderManager().initLoader(0, null, this);
        FriendsListMapSearch.setAdapter(friendsMapListCursorAdapter);
        FriendsListMapSearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Friend friend = MyApp.tableFriends.readEntityByID(id);
                locateFriend(friend.getLat(), friend.getLng());
                onBackPressed();
            }
        });

//        friendsListArrayAdapter =new FriendsListArrayAdapter(this,friendsFromSearch);
//        FriendsListMapSearch2.setAdapter(friendsListArrayAdapter);
//        FriendsListMapSearch2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                Friend f = friendsFromSearch.get(position);
//                locateFriend(f.getLat(),f.getLng());
//                onBackPressed();
//            }
//        });
    }

    private void locateBusiness(String lat, String lng, final Business business) {

        LatLng latLng = null;
        try {
            latLng = new LatLng(Double.parseDouble(lat), Double.parseDouble(lng));

        } catch (NumberFormatException e) {
            e.printStackTrace();
            ToastUtil.toaster(getResources().getString(R.string.unable_locate_place), false);
        }
        boolean markerAlreadyOnMap = false;

        if (latLng != null) {

            if (latLng.latitude == 0 && latLng.longitude == 0) {
                ToastUtil.toaster(getString(R.string.unable_find_place_location), false);
                return;
            }
            googleMap.getCameraPosition();
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 14));

            for (Marker m : allMarkers) {
                if (m.getPosition() == latLng && m.getTitle().equals(business.getName())) {
                    markerAlreadyOnMap = true;
                }
            }
            if (!markerAlreadyOnMap) {
                final BitmapDescriptor bd = getMarkerIconByCategory(business.getCategory());
                handler = new Handler();

                final LatLng finalLatLng = latLng;
                final Runnable r = new Runnable() {
                    public void run() {

                        Marker m = googleMap.addMarker(new MarkerOptions().position(finalLatLng).
                                icon(bd).title(business.getName()));
                        allMarkers.add(m);
                        businesses.add(business);
                        onMarkerClick(m);


                    }
                };

                handler.postDelayed(r, 0);
//            googleMap.addMarker(new MarkerOptions().icon())
            }
        }
    }

    private BitmapDescriptor getMarkerIconByCategory(String category) {
        final View view = getLayoutInflater().inflate(R.layout.business_marker, null);
        ImageView categoryImage = (ImageView) view.findViewById(R.id.categoryImage);
        if (category.equals(Business.SPORT))
            categoryImage.setImageResource(R.drawable.map_pin_sport2x);
        else if (category.equals(Business.FOOD))
            categoryImage.setImageResource(R.drawable.map_pin_food2x);
        else if (category.equals(Business.FASHION))
            categoryImage.setImageResource(R.drawable.map_pin_fashion2x);
        else if (category.equals(Business.NIGHT))
            categoryImage.setImageResource(R.drawable.map_pin_nightlife2x);
        else if (category.equals(Business.ENTERTAINMENT))
            categoryImage.setImageResource(R.drawable.map_pin_entertainment2x);
        else
            categoryImage.setImageResource(R.drawable.map_pin_hotel2x);
        final Bitmap icon = BitmapUtil.createBitmapFromView(frameContainer, view);

        final BitmapDescriptor icon2 = BitmapDescriptorFactory.fromBitmap(icon);

        frameContainer.removeAllViews();
        return icon2;
    }

    private void locateFriend(String lat, String lng) {
        boolean isFound=false;
        LatLng latLng = null;
        try {
            latLng = new LatLng(Double.parseDouble(lat), Double.parseDouble(lng));
            googleMap.getCameraPosition();
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 14));
            isFound=true;
        } catch (NumberFormatException e) {
            e.printStackTrace();
            ToastUtil.toaster(getResources().getString(R.string.unable_locate_friend), false);
        }
        if (isFound){
            Marker marker = null;

//            Collection<Marker> friendArray = mClusterManager.getClusterMarkerCollection().getMarkers();
            for (Marker m : allMarkers){
                System.out.println("--->  locate "+m.getPosition().toString()+"&&& "+lat+" "+lng);

                if (m.getPosition().latitude==latLng.latitude&&m.getPosition().longitude==latLng.longitude){
                    marker=m;
                }
            }if (marker!=null)
            onMarkerClick(marker);
        }

    }

    private void getIntentAction() {
        Intent getI = getIntent();
        if (getI.getAction() != null) {
            if (getI.getAction().equals(MainActivity.ACTION_CHECK_IN)) {
                searchCloseBusinessesAndCheckIn();
            }else if (getI.getAction().equals(ACTION_RATE_PLACE)){
                String businessId = getI.getStringExtra("businessID");
                String businessExId = getI.getStringExtra("businessExID");
                getBusiness(businessId,businessExId);
                fromProfile=true;
            }else if (getI.getAction().equals(ACTION_LOCATE_FRIEND)){
                String lat = getI.getStringExtra("Lat");
                String lng = getI.getStringExtra("Lng");
                locateFriend(lat,lng);
            }else if (getI.getAction().equals(MainActivity.ACTION_TRENDY)){
                addTrendyMarkers();
                fromTrendy=true;
            }else if (getI.getAction().equals(ACTION_PlACES)){
                    openCategoryButtons();
            }
        }
    }

    private void getBusiness(String businessId, String businessExId) {
//        MyApp.networkManager.makeRequest(new GetBusinessRequest(businessId, businessExId), new NetworkCallback<GetBusinessResponse>() {
//            @Override
//            public void onResponse(boolean success, String errorDesc, ResponseObject<GetBusinessResponse> response) {
//                super.onResponse(success, errorDesc, response);
//                if (success) {
//                    Business b = response.getData().getBusiness();
//                    if (b != null)
//                        locateBusiness(b.getLat(), b.getLng(), b);
//                    else {
//                        ToastUtil.toaster(getString(R.string.unable_locate_place), false);
//                    }
//                }
//            }
//        });

        MyApp.networkManager.makeRequest(new GetBusinessV2Request(businessId, businessExId),new NetworkCallback<GetBusinessV2Response>(){
            @Override
            public void onResponse(boolean success, String errorDesc, ResponseObject<GetBusinessV2Response> response) {
                super.onResponse(success, errorDesc, response);
                if (success) {
                    Business b = response.getData().getBusiness();
                    if (b != null)
                        locateBusiness(b.getLat(), b.getLng(), b);
                    else {
                        ToastUtil.toaster(getString(R.string.unable_locate_place), false);
                    }
                }else {
                    ToastUtil.toaster(getString(R.string.unable_locate_place), false);
                }
            }
        });
    }

    private void searchCloseBusinessesAndCheckIn() {
        LatLng latLng = MyApp.appManager.getLatLng();

        //search for businesses close to the user
        MyApp.networkManager.makeRequest(new SearchBusinessRequest("", "", latLng.latitude + "", latLng.longitude + "", 15), new NetworkCallback<SearchBusinessResponse>() {
            @Override
            public void onResponse(boolean success, String errorDesc, ResponseObject<SearchBusinessResponse> response) {
                super.onResponse(success, errorDesc, response);
                if (success) {
                    ArrayList<Business> businesses = response.getData().getBusinesses();
                    showCheckInDialog(businesses);

                }
            }
        });
    }

    private void showCheckInDialog(final ArrayList<Business> businesses) {
        LayoutInflater factory = LayoutInflater.from(this);
        final View checkInDialogView = factory.inflate(
                R.layout.dialog_checkin, null);
        final AlertDialog checkinDialog = new AlertDialog.Builder(this).create();
        checkinDialog.setView(checkInDialogView);
        ListView checkinDialogListView = (ListView) checkInDialogView.findViewById(R.id.checkinDialogListView);
        CheckinDialogAdapter checkinDialogAdapter = new CheckinDialogAdapter(this, businesses);
        checkinDialogListView.setAdapter(checkinDialogAdapter);
        checkinDialogListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                dialogItemPosition = position;
            }
        });


        checkInDialogView.findViewById(R.id.checkinDialogImageView).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (dialogItemPosition != -1) {
                    checkinDialog.dismiss();
                    Business b = businesses.get(dialogItemPosition);
//                    appUserCheckin(checkinDialog, b);
                    MyApp.appManager.setTempBusiness(b);
                    MyApp.appManager.save();

                    Intent i = new Intent(MapActivity.this,BusinessActivity.class);
                    i.setAction(BusinessPageFragment.ACTION_MAP);
                    startActivity(i);

//                    String date = DateUtil.getDateAsStringByMilli(DateUtil.FORMAT_JUST_DATE,System.currentTimeMillis());
//                    Business toSave = new Business(b.getName(),b.getCategory(),b.getLat(),b.getLng(),b.getCity(),date,b.getID(),b.getExternalID());
//                    MyApp.tableCheckIns.insert(toSave);

                }
            }
        });
        checkInDialogView.findViewById(R.id.x_icon_checkin_dialog).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                checkinDialog.dismiss();
            }
        });


        checkinDialog.show();

    }

    private void appUserCheckin(final AlertDialog checkinDialog, final Business b) {

        if (!MyApp.userManager.isIncognito()) {

            String businessId = b.getID();
            MyApp.networkManager.makeRequest(new AppuserCheckInRequest(businessId), new NetworkCallback<AppuserCheckInResponse>() {
                @Override
                public void onResponse(boolean success, String errorDesc, ResponseObject<AppuserCheckInResponse> response) {
                    super.onResponse(success, errorDesc, response);
                    if (success) {
                        ToastUtil.toaster(getResources().getString(R.string.chacked_in_at) + b.getName(), false);

                        if (!response.getData().getGiftPoints().isEmpty()) {
                            if (!response.getData().getGiftPoints().equals("0")) {
//                            String received = getResources().getString(R.string.you_received) + " " + response.getData().getGiftPoints() + " " + getResources().getString(R.string.bonus_points) + "!";
                                String received = getResources().getString(R.string.well_done_you_go_another) + " " + response.getData().getGiftPoints() + " " + getResources().getString(R.string.points_) + "!";

                                ToastUtil.toaster(received, false);
                            }
                        }

                        checkinDialog.dismiss();

                    } else {

                        ToastUtil.toaster(response.getErrorDesc() + "", true);
                    }
                }
            });
        }else {
            showIncognitoDialog();
        }
    }
    private void showIncognitoDialog() {
        final AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle(getResources().getString(R.string.cant_checkin_oncognito));


        alert.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {

                dialog.dismiss();
            }
        });
        alert.setNeutralButton(getResources().getString(R.string.open_settings), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                Intent i = new Intent(MapActivity.this, SettingsActivity.class);
                startActivity(i);
                finish();

            }
        });
        alert.show();
    }
    private void setListeners() {
        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                search(searchMenuEditText.getText().toString());
            }
        });
        checkinMapButton.setOnClickListener(this);
        placesMapButton.setOnClickListener(this);
        cancelMenuSearch.setOnClickListener(this);
        radiusMinusButton.setOnClickListener(this);
        radiusPlusButton.setOnClickListener(this);
        searchMenuEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!search_list_mapIsVisable){
                    search_list_map_view.setVisibility(View.VISIBLE);
                    search_list_mapIsVisable = true;
                    mapPopUpButtons.setVisibility(View.GONE);
                }
            }
        });
        radiusEditText.setFilters(new InputFilter[]{new InputFilterMinMax("1", "1000000")});

        searchMenuEditText.addTextChangedListener(new SearchTextOnActionBar());
        searchMenuEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    search_list_map_view.setVisibility(View.VISIBLE);
                    mapPopUpButtons.setVisibility(View.GONE);
                    search_list_mapIsVisable = true;
                    busnissesMapListAdapter.clear();
                    search(searchMenuEditText.getText().toString());
                    View view = getCurrentFocus();
                    if (view != null) {
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    }
                    return true;
                }
                return false;
            }
        });

        nightlife_button.setOnClickListener(new MapPopUpButtonsListeners());
        fashion_button.setOnClickListener(new MapPopUpButtonsListeners());
        food_button.setOnClickListener(new MapPopUpButtonsListeners());
        places_grey_button.setOnClickListener(new MapPopUpButtonsListeners());
        sport_button.setOnClickListener(new MapPopUpButtonsListeners());
        entertainment_button.setOnClickListener(new MapPopUpButtonsListeners());
        services_button3x.setOnClickListener(new MapPopUpButtonsListeners());

        incognitoSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                MyApp.userManager.setIsIncognito(isChecked);
                MyApp.userManager.save();
                    if (!anonymous)
                        updatAppUserEnabledLoc();
                SwitchUtil.switchColor(isChecked,incognitoSwitch,MapActivity.this);

            }
        });

        setRadiusTextChangeListener();

        refreshButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Runnable r = new Runnable() {
                    @Override
                    public void run() {
                        YoYo.with(Techniques.RotateIn).duration(300).withListener(new MyAnimationListener() {
                            @Override
                            public void onAnimationEnd(Animation animation) {
                                super.onAnimationEnd(animation);
                            }

                        }).playOn(refreshButton);
                        refreshMap();
                    }
                };

                Handler h = new Handler();
                h.postDelayed(r, 5);
            }
        });
    }

    private void refreshMap() {
        ClearAllMarkers();
        googleMap.clear();

        usrLatLng = MyApp.appManager.getLatLng();
//        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(usrLatLng, 14));

        getFriendsLocation();
        makeUserMarker();
        setPeopleAroundMarkers();
        dissmissPopUpButtons();

//        peopleClusterMangager.cluster();
//        mClusterManager.cluster();

//        this.googleMap = googleMap;
//        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
//        usrLatLng = MyApp.appManager.getLatLng();
//        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(usrLatLng, 14));
////        googleMap.moveCamera(CameraUpdateFactory.newLatLng(usrLatLng));
//        System.out.println("--->  locationTest Map "+usrLatLng.latitude+"||"+usrLatLng.longitude);
//        makeUserMarker();
//        if (!anonymous)
//            getFriendsLocation();
//        googleMap.setOnMarkerClickListener(this);
//        setCluster(googleMap);
//        setPeopleAroundMarkers();
//        getIntentAction();
    }



    private void setRadiusTextChangeListener() {
        radiusEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!s.toString().isEmpty()) {
                    MyApp.userManager.setRadius(s.toString());
                    MyApp.userManager.save();
                    if (!anonymous)
                        updateUserRadius(s.toString());
                }else {
                    radiusEditText.setText("1");
                }
            }
        });
    }

    private void updateUserRadius(final String s) {
        int radius = -1;
        try {
            radius = Integer.parseInt(s);

        } catch (NumberFormatException e) {
            e.printStackTrace();
            radius = -1;
        }
        if (radius>0) {
            MyApp.networkManager.makeRequest2(new UpdateAppuserRadiusRequest(radius), new NetworkCallback<UpdateAppuserRadiusResponse>() {

                @Override
                public void onResponse2(boolean success, String errorDesc, ResponseObject2<UpdateAppuserRadiusResponse> response) {
                    super.onResponse2(success, errorDesc, response);
                    if (!success) {
                        ToastUtil.toaster(response.getErrorDesc(), true);
                        MyApp.userManager.getAppuser().setRadius(s);
                        MyApp.userManager.save();
                    }
                }
            });
        }
    }

    private void findViews() {
        checkinMapButton = (FrameLayout) findViewById(R.id.checkinMapButton);
        placesMapButton = (FrameLayout) findViewById(R.id.placesMapButton);
        locationIconActionBar = (ImageView) findViewById(R.id.locationIconActionBar);
        actionBarLinear = (LinearLayout) findViewById(R.id.actionBarLinear);


        //container:
        frameContainer = (FrameLayout) findViewById(R.id.frameContainer);

        //actionbar
        searchMenuLinearLayout = (LinearLayout) findViewById(R.id.searchMenuLinearLayout);
        searchMenuEditText = (EditText) findViewById(R.id.searchMenuEditText);
        cancelMenuSearch = (TextView) findViewById(R.id.cancelMenuSearch);
        mToolbar = getActionbarToolbar();
        incognitoSwitch = (SwitchCompat) findViewById(R.id.incognitoSwitch);

        TextView incoTv = (TextView) findViewById(R.id.incoTv);
        incoTv.setVisibility(View.VISIBLE);
        incognitoSwitch.setVisibility(View.VISIBLE);

        //radius:
        radiusEditText = (EditText) findViewById(R.id.radiusEditText);


        RadiusSearchLinear = (LinearLayout) findViewById(R.id.RadiusSearchLinear);
        radiusMinusButton = (ImageView) findViewById(R.id.radiusMinusButton);
        radiusPlusButton = (ImageView) findViewById(R.id.radiusPlusButton);
        //list
        search_list_map_view = (LinearLayout) findViewById(R.id.search_list_map_view);
        FriendsListMapSearch = (ListView) findViewById(R.id.FriendsListMapSearch);
        FriendsListMapSearch2 = (ListView) findViewById(R.id.FriendsListMapSearch2);
        placesListMapSearch = (ListView) findViewById(R.id.placesListMapSearch);

        noPlacesFrame = (FrameLayout)findViewById(R.id.noPlacesFrame);
        noFriendsFrame = (FrameLayout)findViewById(R.id.noFriendsFrame);
        //popup buttons
        mapPopUpButtons = (LinearLayout) findViewById(R.id.mapPopUpButtons);

        nightlife_button = (ImageView) findViewById(R.id.nightlife_button);
        fashion_button = (ImageView) findViewById(R.id.fashion_button);
        food_button = (ImageView) findViewById(R.id.food_button);
        places_grey_button = (ImageView) findViewById(R.id.places_grey_button);
        sport_button = (ImageView) findViewById(R.id.sport_button);
        entertainment_button = (ImageView) findViewById(R.id.entertainment_button);
        services_button3x = (ImageView) findViewById(R.id.services_button3x);

        //refresh:
        swipe = (SwipeRefreshLayout)findViewById(R.id.swipe);
        refreshButton=(ImageView) findViewById(R.id.refreshButton);
    }


    private void InitAnalytic(){
        MyApp application = (MyApp) getApplication();
        mTracker = application.getDefaultTracker();
    }



    @Override
    protected void onResume() {
        super.onResume();

        mTracker.setScreenName("Map screen");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        if (MyApp.appManager.isBackFromBusiness()){
            MyApp.appManager.setBackFromBusiness(false);
            MyApp.appManager.save();
            finish();
            return;
        }else if (MyApp.appManager.isBackFromBusinessForProfile()){
            MyApp.appManager.setBackFromBusinessForProfile(false);
            MyApp.appManager.save();
            Intent i = new Intent(MapActivity.this,ProfileActivity.class);
            startActivity(i);
            finish();
            return;
        }

        usrLatLng = MyApp.appManager.getLatLng();
        mSimpleFacebook = SimpleFacebook.getInstance(this);
        locationIconActionBar.setVisibility(View.GONE);
        moveMenuIconsLeft();
        if (!ContentProviderUtil.isGPSEnebled(this)) {
            DialogExample.openLocationDialog(this);

        }
        incognitoSwitch.setChecked(MyApp.userManager.isIncognito());
        SwitchUtil.switchColor(MyApp.userManager.isIncognito(),incognitoSwitch,this);

    }

    private void moveMenuIconsLeft() {
//        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(actionBarLinear.getWidth(),actionBarLinear.getHeight());
//        layoutParams.setMargins(0, 0, 50, 0);
//        actionBarLinear.setLayoutParams(layoutParams);
        actionBarLinear.setPadding(0, 0, 120, 0);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.checkinMapButton:
                if (!anonymous)
                searchCloseBusinessesAndCheckIn();
                else {
                    ToastUtil.toaster(getResources().getString(R.string.please_login_facebook_checkin),false);

                }
                break;
            case R.id.placesMapButton:
                openCategoryButtons();
                break;
            case R.id.cancelMenuSearch:
                searchMenuLinearLayout.setVisibility(View.GONE);
                actionBarLinear.setVisibility(View.VISIBLE);
                searchMenuButton.setVisible(true);
                getActionBarTextView().setVisibility(View.VISIBLE);
                RadiusSearchLinear.setVisibility(View.GONE);
                search_list_map_view.setVisibility(View.GONE);
                search_list_mapIsVisable = false;
                View view = this.getCurrentFocus();
                if (view != null) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }

                break;
            case R.id.radiusMinusButton:

                int minus1 = Integer.parseInt(radiusEditText.getText().toString()) - 1;

                radiusEditText.setText(minus1 + "");

                break;
            case R.id.radiusPlusButton:
                int plus1 = Integer.parseInt(radiusEditText.getText().toString()) + 1;

                radiusEditText.setText(plus1 + "");

                break;


        }
    }

    boolean isCategoryViewVisable = false;

    private void openCategoryButtons() {

        isCategoryViewVisable = true;
        mapPopUpButtons.setVisibility(View.VISIBLE);
        checkinMapButton.setVisibility(View.INVISIBLE);
        placesMapButton.setVisibility(View.INVISIBLE);

        Runnable r = new Runnable() {
            @Override
            public void run() {
                YoYo.with(Techniques.RotateIn).duration(300).withListener(new MyAnimationListener() {
                    @Override
                    public void onAnimationEnd(Animation animation) {
                        super.onAnimationEnd(animation);
                    }

                }).playOn(mapPopUpButtons);
            }
        };

        Handler h = new Handler();
        h.postDelayed(r, 5);


        //


    }

    @Override
    public void onBackPressed() {
        if (isCategoryViewVisable) {
            dissmissPopUpButtons();
        } else if (search_list_mapIsVisable) {
            search_list_map_view.setVisibility(View.GONE);
            search_list_mapIsVisable = false;
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_map, menu);
        searchMenuButton = menu.findItem(R.id.action_search);


        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        searchMenuEditText.setText("");
        RadiusSearchLinear.setVisibility(View.VISIBLE);
        searchMenuLinearLayout.setVisibility(View.VISIBLE);
        actionBarLinear.setVisibility(View.GONE);
        searchMenuButton.setVisible(false);
        getActionBarTextView().setVisibility(View.INVISIBLE);

        Runnable r = new Runnable() {
            @Override
            public void run() {
                RadiusSearchLinear.setVisibility(View.VISIBLE);
                YoYo.with(Techniques.BounceInDown).duration(300).playOn(RadiusSearchLinear);
            }
        };

        Handler h = new Handler();
        h.postDelayed(r, 50);

        return super.onOptionsItemSelected(item);

    }

    private void setUpDrawerNavigation() {
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerToggle = new ActionBarDrawerToggle
                (this, mDrawerLayout, getActionbarToolbar(), 0, 0) {

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }
        };
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public boolean onMarkerClick(final Marker marker) {
        if (marker.getTitle()!=null) {
            int type = 0;
            Business clickedBusiness = null;
            Friend clickedFriend = null;
            LatLng markerLatLng = marker.getPosition();

            String title = marker.getTitle().trim();
            System.out.println("--->  marker: " + markerLatLng + " ," + title);

            if (businesses != null)
                for (Business b : businesses) {
                    System.out.println("--->  marker: " + markerLatLng + " ," + title);
                    System.out.println("--->  type= " + type + " size" + businesses.size() + " lat= " + b.getLat() + " lng=" + b.getLng() + " name = " + b.getName());

                    LatLng latLng = null;
                    try {
                        latLng = new LatLng(Double.parseDouble(b.getLat()), Double.parseDouble(b.getLng()));

                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                        ToastUtil.toaster(getString(R.string.no_business_inf), false);
                    }
                    assert latLng != null;
                    if (markerLatLng.latitude == latLng.latitude && markerLatLng.longitude == latLng.longitude && title.equals(b.getName())) {
                        type = TYPE_BUSINESS;
                        clickedBusiness = b;
                        MyApp.appManager.setTempBusiness(b);
                        MyApp.appManager.save();

                    }
                }
//        }
            if (type == 0) {
                ArrayList<Friend> friends = MyApp.tableFriends.readAll(TableFriends.TAG_FIRST_NAME + " asc");
                if (friends != null)
                    for (Friend f : friends) {
                        System.out.println("--->  type= " + type + " size" + friends.size() + " lat= " + f.getLat() + " lng=" + f.getLng() + " name = " + f.getFirstName());
                        System.out.println("--->  marker: " + markerLatLng + " ," + title);

                        LatLng latLng = null;
                        try {
                            latLng = new LatLng(Double.parseDouble(f.getLat()), Double.parseDouble(f.getLng()));

                        } catch (NumberFormatException e) {
                            e.printStackTrace();
                        }
                        System.out.println("--->  lat=" + latLng);

                        if (latLng != null) {
                            if (markerLatLng.latitude == latLng.latitude &&
                                    markerLatLng.longitude == latLng.longitude && title.equals(f.getFirstName())) {
                                type = TYPE_FRIEND;
                                clickedFriend = f;
                                System.out.println("--->  type= " + type + " friend= " + f.getFirstName());
                                for (Friend friend : allFriends){
                                    if (f.getFBID().equals(friend.getFBID())){
                                        f.setUserActivity(friend.getUserActivity());
                                    }

                                }

                            }
                        }
                    }
            }
            System.out.println("--->  type= " + type);


            final int finalType = type;
            final Business finalClickedBusiness = clickedBusiness;
            final Friend finalClickedFriend = clickedFriend;
            googleMap.animateCamera(CameraUpdateFactory.newLatLng(marker.getPosition()), 150, new GoogleMap.CancelableCallback() {
                @Override
                public void onFinish() {
                    if (finalType == TYPE_BUSINESS)
                        showInfoWindow(finalType, finalClickedBusiness);
                    else if (finalType == TYPE_FRIEND)
                        showInfoWindow(finalType, finalClickedFriend);

                }

                @Override
                public void onCancel() {

                }
            });
            if (type == 0)
                return false;
            else
                return true;
        }else {
            googleMap.getCameraPosition();
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(marker.getPosition(), 12));
//
            return false;
        }
    }

    private void showInfoWindow(int type, Object obj) {
        switch (type) {
            case TYPE_BUSINESS:
                openCategoryInfoWindow(obj);

                break;
            case TYPE_FRIEND:
                Friend f = (Friend) obj;
                if (f.getUserActivity()!=null) {
                    if (f.getUserActivity().getActivity().equals("non")) {
                        openBasePopUpWindow(obj);

                    } else if (f.getUserActivity().getActivity().equals("RANK")) {
                        if (f.getUserActivity().getBusiness() != null)
                            openRatedFriendPopUpView(obj);
                        else
                            openBasePopUpWindow(obj);

                    } else if (f.getUserActivity().getActivity().equals("CHECKIN")) {
                        if (f.getUserActivity().getBusiness() != null)
                            openCheckinPopUpView(obj);
                        else
                            openBasePopUpWindow(obj);
                    }
                }else {
                    openBasePopUpWindow(obj);
                }
                break;
        }
    }

    private void openRatedFriendPopUpView(Object obj) {


        final Friend f = (Friend)obj;
        final Business finalBusiness = f.getUserActivity().getBusiness();

        handler = new Handler();
        final Runnable r = new Runnable() {
            public void run() {
                View popupView = LayoutInflater.from(MapActivity.this).inflate(R.layout.map_rated_info_window, null);

                int width = (int) AppUtil.convertDpToPixel(268, MapActivity.this);
                int height = (int) AppUtil.convertDpToPixel(147, MapActivity.this);
//                Log.i("MES", "showInfoWindow: w|h " + width + "|" + height);

                final PopupWindow popupWindow = new PopupWindow(popupView, width, height, true);

                popupWindow.setOutsideTouchable(true);
                popupWindow.setBackgroundDrawable(new BitmapDrawable());
                popupWindow.setTouchable(true);
                popupWindow.setFocusable(true);
                popupWindow.setAnimationStyle(R.style.AnimationInfoWindow);
                int HeightOffset = (int) AppUtil.convertDpToPixel(73+55, MapActivity.this);


                View v = findViewById(R.id.screen_content_layout);
                int windowWidthHalf= popupView.getWidth()/2;
                popupWindow.showAtLocation(v, Gravity.CENTER,-windowWidthHalf,- HeightOffset);
                //find views:
                TextView infoWindowTextView = (TextView) popupView.findViewById(R.id.ratedNameInfoWindowTextView);
                TextView ratedInInfoWindowTextView = (TextView) popupView.findViewById(R.id.ratedInInfoWindowTextView);
                ImageView infoWindowFacebook    = (ImageView) popupView.findViewById(R.id.checkinInfoWindowFacebook);
                ImageView infoWindowPhone       = (ImageView) popupView.findViewById(R.id.checkinInfoWindowPhone);
                ImageView infoWindowMessage  = (ImageView) popupView.findViewById(R.id.checkinInfoWindowMessage);
                ImageView infoWindowBusiness  = (ImageView) popupView.findViewById(R.id.infoWindowMessageBusinessImageView);
                RatingBar infoWindowRatingBar = (RatingBar) popupView.findViewById(R.id.infoWindowRatingBar);
                //set rating:
                if(!f.getUserActivity().getRank().equals("")&&!f.getUserActivity().getRank().equals("not implemented yet")){
                    float rate= Float.parseFloat(f.getUserActivity().getRank());
                    infoWindowRatingBar.setRating(rate);
                }


                infoWindowBusiness.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (finalBusiness != null) {
                            MyApp.appManager.setTempBusiness(finalBusiness);
                            MyApp.appManager.save();
//                            Intent businessIntent = new Intent(MapActivity.this, BusinessActivity.class);
//                            businessIntent.setAction(BusinessPageFragment.ACTION_MAP);
//                            startActivity(businessIntent);
                            openBusinessPage();
                        }
                    }
                });

                String name =f.getFirstName()+" "+f.getLastName();
                infoWindowTextView.setText(name);
//                infoWindowTextView.setText(f.getFirstName());
                if (finalBusiness!=null)
                    ratedInInfoWindowTextView.setText(getResources().getString(R.string.rated)+": "+ finalBusiness.getName());
                   infoWindowFacebook.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.facebook.com/" + f.getFBID()));
                            startActivity(intent);
                        } catch (Exception e) {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.facebook.com/" + f.getFirstName() + "." + f.getLastName())));
                        }
                    }
                });
                infoWindowPhone.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (f.getPhone().isEmpty())
                            ToastUtil.toaster(getString(R.string.phone_not_available), false);
                        else {
                            if (ContentProviderUtil.isTelephonyEnabled(MapActivity.this))
                                ContentProviderUtil.openDialer(MapActivity.this,f.getPhone());
                        }                         }
                });
                infoWindowMessage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (f.getPhone().isEmpty())
                            ToastUtil.toaster(getString(R.string.phone_not_available), false);
                        else {
                            if (ContentProviderUtil.isTelephonyEnabled(MapActivity.this))
                                ContentProviderUtil.openSMSScreen(MapActivity.this,"",f.getPhone());
                        }
                    }
                });


            }
        };

        handler.postDelayed(r, 200);

    }

    private void openCheckinPopUpView(Object obj) {
        final Friend f = (Friend)obj;
        handler = new Handler();
        final Runnable r = new Runnable() {
            public void run() {
                View popupView = LayoutInflater.from(MapActivity.this).inflate(R.layout.map_checki_info_window, null);

                int width = (int) AppUtil.convertDpToPixel(250, MapActivity.this);
                int height = (int) AppUtil.convertDpToPixel(125, MapActivity.this);
//                Log.i("MES", "showInfoWindow: w|h " + width + "|" + height);

                final PopupWindow popupWindow = new PopupWindow(popupView, width, height, true);

                popupWindow.setOutsideTouchable(true);
                popupWindow.setBackgroundDrawable(new BitmapDrawable());
                popupWindow.setTouchable(true);
                popupWindow.setFocusable(true);
                popupWindow.setAnimationStyle(R.style.AnimationInfoWindow);
                int HeightOffset = (int) AppUtil.convertDpToPixel(62+55, MapActivity.this);


                View v = findViewById(R.id.screen_content_layout);
                int windowWidthHalf= popupView.getWidth()/2;
                popupWindow.showAtLocation(v, Gravity.CENTER,-windowWidthHalf,- HeightOffset);
                //find views:
                TextView infoWindowTextView = (TextView) popupView.findViewById(R.id.checkInNameInfoWindowTextView);
                TextView checkInInfoWindowTextView = (TextView) popupView.findViewById(R.id.checkInInfoWindowTextView);
                ImageView infoWindowFacebook    = (ImageView) popupView.findViewById(R.id.checkinInfoWindowFacebook);
                ImageView infoWindowPhone       = (ImageView) popupView.findViewById(R.id.checkinInfoWindowPhone);
                ImageView infoWindowMessage  = (ImageView) popupView.findViewById(R.id.checkinInfoWindowMessage);
                ImageView infoWindowBusiness  = (ImageView) popupView.findViewById(R.id.infoWindowMessageBusinessImageView);
                infoWindowBusiness.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (f.getUserActivity().getBusiness() != null) {
                            MyApp.appManager.setTempBusiness(f.getUserActivity().getBusiness());
                            MyApp.appManager.save();
//                            Intent businessIntent = new Intent(MapActivity.this, BusinessActivity.class);
//                            businessIntent.setAction(BusinessPageFragment.ACTION_MAP);
//                            startActivity(businessIntent);
                            openBusinessPage();
                        }
                    }
                });

                String name =f.getFirstName()+" "+f.getLastName();
                infoWindowTextView.setText(name);

                if (f.getUserActivity().getBusiness()!=null)
                checkInInfoWindowTextView.setText(getResources().getString(R.string.checked_in)+ ": "+f.getUserActivity().getBusiness().getName());
                infoWindowFacebook.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.facebook.com/" + f.getFBID()));
                            startActivity(intent);
                        } catch (Exception e) {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.facebook.com/" + f.getFirstName() + "." + f.getLastName())));
                        }
                    }
                });
                infoWindowPhone.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (f.getPhone().isEmpty())
                            ToastUtil.toaster(getString(R.string.phone_not_available), false);
                        else {
                            if (ContentProviderUtil.isTelephonyEnabled(MapActivity.this))
                                ContentProviderUtil.openDialer(MapActivity.this,f.getPhone());
                        }                    }
                });
                infoWindowMessage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (f.getPhone().isEmpty())
                        ToastUtil.toaster(getString(R.string.phone_not_available), false);
                        else {
                            if (ContentProviderUtil.isTelephonyEnabled(MapActivity.this))
                                ContentProviderUtil.openSMSScreen(MapActivity.this,"",f.getPhone());
                        }
//                        Handler h = new Handler();
//                        Runnable runnable= new Runnable() {
//                            @Override
//                            public void run() {
//                                startActivity(new Intent(Intent.ACTION_VIEW,
//                                        Uri.parse("fb://messaging/" + f.getFBID())));
//                            }
//                        };
//                       h.postDelayed(runnable,0);
                    }
                });


            }
        };

        handler.postDelayed(r, 200);


    }


    private void openBusinessPage(){
        Intent businessIntent = new Intent(MapActivity.this, BusinessActivity.class);
        businessIntent.setAction(BusinessPageFragment.ACTION_MAP);
        startActivityForResult(businessIntent,1);


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode==1){
            String businessId = data.getStringExtra("businessID");
            String businessExId = data.getStringExtra("businessExID");
            getBusiness(businessId,businessExId);
        }
    }

    private void openCategoryInfoWindow(Object obj) {
        final Business b = (Business)obj;
        handler = new Handler();

        final Runnable r = new Runnable() {
            public void run() {
                View popupView = LayoutInflater.from(MapActivity.this).inflate(R.layout.map_place_info_window, null);
                int width = (int) AppUtil.convertDpToPixel(250, MapActivity.this);
                int height = (int) AppUtil.convertDpToPixel(122, MapActivity.this);
//                Log.i("MES", "showInfoWindow: w|h " + width + "|" + height);
                final PopupWindow popupWindow = new PopupWindow(popupView,width, height, true);

                popupWindow.setOutsideTouchable(true);
                popupWindow.setBackgroundDrawable(new BitmapDrawable());
                popupWindow.setTouchable(true);
                popupWindow.setFocusable(true);
                popupWindow.setAnimationStyle(R.style.AnimationInfoWindow);

                int HeightOffset = (int) AppUtil.convertDpToPixel(38+55, MapActivity.this);

                View v = findViewById(R.id.screen_content_layout);
                int windowWidthHalf= popupView.getWidth()/2;
                popupWindow.showAtLocation(v, Gravity.CENTER,-windowWidthHalf,- HeightOffset);
                //find views:
                TextView placeInfoWindowTextView = (TextView) popupView.findViewById(R.id.placeInfoWindowTextView);
                ImageView placeInfoWindowFacebook    = (ImageView) popupView.findViewById(R.id.placeInfoWindowFacebook);
                ImageView placeInfoWindowPhone       = (ImageView) popupView.findViewById(R.id.placeInfoWindowPhone);
                ImageView placeInfoWindowMessage  = (ImageView) popupView.findViewById(R.id.placeInfoWindowMessage);
                ImageView placeinfoWindowBusinessImageView  = (ImageView) popupView.findViewById(R.id.placeinfoWindowMessageBusinessImageView);
                RatingBar placeInfoWindowRatingBar = (RatingBar)popupView.findViewById(R.id.placeInfoWindowRatingBar);

                placeInfoWindowTextView.setText(b.getName());
//                if (fromProfile) {
                try {
                    placeInfoWindowRatingBar.setRating(Integer.parseInt(b.getAdminRank()) / 20f);
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                    placeInfoWindowRatingBar.setRating(b.getRank() / 20f);

                }

//                }
//                else


                placeInfoWindowFacebook.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(b.getFacebookLink()));
                            startActivity(intent);
                        } catch (Exception e) {
                            ToastUtil.toaster(getString(R.string.facebook_not_avalible), false);

                        }
                    }
                });
                placeInfoWindowPhone.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!b.getPhone().isEmpty()) {
                            if (ContentProviderUtil.isTelephonyEnabled(MapActivity.this))
                                ContentProviderUtil.openDialer(MapActivity.this, b.getPhone());
                            else {
                                ToastUtil.toaster(getResources().getString(R.string.device_not_support_dialer),false);
                            }
                        } else if (!b.getPhone2().isEmpty()) {
                            if (ContentProviderUtil.isTelephonyEnabled(MapActivity.this))
                                ContentProviderUtil.openDialer(MapActivity.this, b.getPhone2());
                            else {
                                ToastUtil.toaster(getResources().getString(R.string.device_not_support_dialer),false);
                            }
                        } else
                            ToastUtil.toaster(getString(R.string.phone_not_available), false);

                    }
                });
                placeInfoWindowMessage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!b.getWebsite().equals(""))
                            ContentProviderUtil.openBrowser(MapActivity.this, b.getWebsite());
                        else
                            ToastUtil.toaster(getString(R.string.error_getting_business_web), false);

                    }
                });

                placeinfoWindowBusinessImageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
//                        Intent businessIntent = new Intent(MapActivity.this, BusinessActivity.class);
//                        businessIntent.setAction(BusinessPageFragment.ACTION_MAP);
//                        startActivity(businessIntent);
                        openBusinessPage();
                        popupWindow.dismiss();

                    }
                });
            }
        };

        handler.postDelayed(r, 200);
    }

    private void openBasePopUpWindow(Object obj) {
        final Friend f = (Friend)obj;
        handler = new Handler();

        final Runnable r = new Runnable() {
            public void run() {
                View popupView = LayoutInflater.from(MapActivity.this).inflate(R.layout.map_base_info_window, null);

                int width = (int) AppUtil.convertDpToPixel(200, MapActivity.this);
                int height = (int) AppUtil.convertDpToPixel(100, MapActivity.this);
//                Log.i("MES", "showInfoWindow: w|h " + width + "|" + height);

                final PopupWindow popupWindow = new PopupWindow(popupView, width, height, true);

                popupWindow.setOutsideTouchable(true);
                popupWindow.setBackgroundDrawable(new BitmapDrawable());
                popupWindow.setTouchable(true);
                popupWindow.setFocusable(true);
                popupWindow.setAnimationStyle(R.style.AnimationInfoWindow);
                int HeightOffset = (int) AppUtil.convertDpToPixel(52+55, MapActivity.this);
//                int widthOffset = (int) AppUtil.convertDpToPixel(100, this);
//                Log.i("MES", "showInfoWindow: offset " + HeightOffset + "|" + widthOffset);

//                int x = MyApp.appManager.getScreenSize().getWidth() / 2  - width;
//                int y = MyApp.appManager.getScreenSize().getHeight() / 2-HeightOffset;
//                Log.i("MES", "showInfoWindow: x|y " + x + "|" + y);

                View v = findViewById(R.id.screen_content_layout);
                int windowWidthHalf= popupView.getWidth()/2;
//                MyApp.appManager.getScreenSize().getWidth() / 2-windowWidth
                popupWindow.showAtLocation(v, Gravity.CENTER,-windowWidthHalf,- HeightOffset);
                //find views:
                TextView infoWindowTextView = (TextView) popupView.findViewById(R.id.infoWindowTextView);
                ImageView infoWindowFacebook    = (ImageView) popupView.findViewById(R.id.infoWindowFacebook);
                ImageView infoWindowPhone       = (ImageView) popupView.findViewById(R.id.infoWindowPhone);
                ImageView infoWindowMessage  = (ImageView) popupView.findViewById(R.id.infoWindowMessage);

                String name =f.getFirstName()+" "+f.getLastName();
                infoWindowTextView.setText(name);
                infoWindowFacebook.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.facebook.com/"+f.getFBID()));
                            startActivity(intent);
                        } catch(Exception e) {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.facebook.com/"+f.getFirstName()+"."+f.getLastName())));
                        }
                    }
                });
                infoWindowPhone.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (f.getPhone().isEmpty())
                            ToastUtil.toaster(getString(R.string.phone_not_available), false);
                        else {
                            if (ContentProviderUtil.isTelephonyEnabled(MapActivity.this))
                                ContentProviderUtil.openDialer(MapActivity.this,f.getPhone());
                        }                           }
                });
                infoWindowMessage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (f.getPhone().isEmpty())
                            ToastUtil.toaster(getString(R.string.phone_not_available), false);
                        else {
                            if (ContentProviderUtil.isTelephonyEnabled(MapActivity.this))
                                ContentProviderUtil.openSMSScreen(MapActivity.this,"",f.getPhone());
                        }
//                        startActivity(new Intent(Intent.ACTION_VIEW,
//                                Uri.parse("fb://messaging/" + f.getFBID())));
                    }
                });


            }
        };

        handler.postDelayed(r, 200);




    }


    private class SearchTextOnActionBar implements TextWatcher {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//          openListPopUpView();
//          searchFriends("");
            busnissesMapListAdapter.clear();
//            friendsListArrayAdapter.clear();
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            searchString = s.toString();
            if (count >=1) {
//                FriendsListMapSearch2.setVisibility(View.VISIBLE);
//                FriendsListMapSearch.setVisibility(View.GONE);

                friendsMapListCursorAdapter.getFilter().filter(searchString);
                final int length =s.toString().length();
                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        if (searchString.length()==length)
                            search(searchString);
                    }
                }, 600);


            }else {
                friendsMapListCursorAdapter.getFilter().filter("");
//                noPlacesFrame.setVisibility(View.VISIBLE);
//                placesListMapSearch.setVisibility(View.GONE);
//                FriendsListMapSearch.setVisibility(View.VISIBLE);
//                FriendsListMapSearch2.setVisibility(View.GONE);
            }

        }

        @Override
        public void afterTextChanged(Editable s) {
//          search_list_map_view.setVisibility(View.VISIBLE);
            if (friendsMapListCursorAdapter.getCount()>0){
                noFriendsFrame.setVisibility(View.GONE);
                FriendsListMapSearch.setVisibility(View.VISIBLE);
            }else {
                FriendsListMapSearch.setVisibility(View.GONE);
                noFriendsFrame.setVisibility(View.VISIBLE);
            }
        }
    }

    private void search(String s){
//        if (!MyApp.appManager.isAnonymous()) {
        String userId;
        if (!MyApp.appManager.isAnonymous())
            userId= MyApp.userManager.getAppuser().getID();
        else {
            userId="";
        }

                swipe.setRefreshing(true);

            MyApp.networkManager.makeRequest(new SearchRequest(userId,s + "", MyApp.appManager.getLatLng().latitude + "", MyApp.appManager.getLatLng().longitude + "", Integer.parseInt(radiusEditText.getText().toString())), new NetworkCallback<SearchResponse>() {
                @Override
                public void onResponse(boolean success, String errorDesc, ResponseObject<SearchResponse> response) {
                    super.onResponse(success, errorDesc, response);
                    if (friendsMapListCursorAdapter.getCount()>0){
                        noFriendsFrame.setVisibility(View.GONE);
                        FriendsListMapSearch.setVisibility(View.VISIBLE);
                    }else {
                        FriendsListMapSearch.setVisibility(View.GONE);
                        noFriendsFrame.setVisibility(View.VISIBLE);
                    }
                    if (success) {
                        if (response.getData().getPlacesResults() != null) {
                            busnissesMapListAdapter.clear();
                            for (Business b : response.getData().getPlacesResults()) {
                                businessesFromSearch.add(b);
                                businesses.add(b);
                            }
                            busnissesMapListAdapter.notifyDataSetChanged();

                            if (response.getData().getPlacesResults().size()>0) {

                                noPlacesFrame.setVisibility(View.GONE);
                                swipe.setVisibility(View.VISIBLE);
                            } else {
                                noPlacesFrame.setVisibility(View.VISIBLE);
                                swipe.setVisibility(View.GONE);
                            }
                        }
                        swipe.setRefreshing(false);

                        /**
                         * original list from search
                         */
//                        if (response.getData().getFriendsResults() != null) {
//                            ArrayList<String> friendsIDs = MyApp.userManager.getFbFriendsIDs();
//                            for (Friend f : response.getData().getFriendsResults()) {
//                                if (friendsIDs.size() > 0) {
//                                    for (String id : friendsIDs) {
//                                        if (f.getID().equals(id)) {
//                                            friendsFromSearch.add(f);
//                                            allFriends.add(f);
//                                            friendsLocation.add(f);
//                                        }
//                                    }
//                                }
//
//                            }
//                            friendsListArrayAdapter.notifyDataSetChanged();
//                        }
                    }else {
                        swipe.setRefreshing(false);

                    }

                }
            });
//        }else {
//            searchPlace(s);
//        }
    }

    private void searchPlace(String s) {
//        LatLng latLng = MyApp.appManager.getLatLng();
        swipe.setRefreshing(true);

        MyApp.networkManager.makeRequest(new SearchBusinessRequest("", s, "", "", 299), new NetworkCallback<SearchBusinessResponse>() {
            @Override
            public void onResponse(boolean success, String errorDesc, ResponseObject<SearchBusinessResponse> response) {
                super.onResponse(success, errorDesc, response);
                if (success) {
                    if (response.getData().getBusinesses()!=null){
                    for (Business b : response.getData().getBusinesses()) {
                        businessesFromSearch.add(b);
                        businesses.add(b);


                    }
                        System.out.println("--->  size"+response.getData().getBusinesses().size());
                        if (response.getData().getBusinesses().size()>0) {

                            noPlacesFrame.setVisibility(View.GONE);
                            swipe.setVisibility(View.VISIBLE);
                        } else {
                            noPlacesFrame.setVisibility(View.VISIBLE);
                            swipe.setVisibility(View.GONE);
                        }
                    busnissesMapListAdapter.notifyDataSetChanged();
                }else {
                        noPlacesFrame.setVisibility(View.VISIBLE);
                        swipe.setVisibility(View.GONE);
                    }
                }else {
                    noPlacesFrame.setVisibility(View.VISIBLE);
                    swipe.setVisibility(View.GONE);
                }
                swipe.setRefreshing(false);

            }
        });
    }

    private void searchFriends(final String toSearch) {
        OnFriendsListener onFriendsListener = new OnFriendsListener() {
            @Override
            public void onComplete(List<Profile> friends) {
                for (int i = 0; i < friends.size(); i++) {
                    Profile profile = friends.get(i);
                    String name = profile.getFirstName();
                    if (name.startsWith(toSearch)) {
                        //todo add to list
                    }
                }
            }

    /*
     * You can override other methods here:
     * onThinking(), onFail(String reason), onException(Throwable throwable)
     */
        };

        mSimpleFacebook.getFriends(onFriendsListener);
    }


    private class MapPopUpButtonsListeners implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            googleMap.clear();
            String category = "";
            switch (v.getId()) {
                case R.id.nightlife_button:
                    category = Business.NIGHT;
                    break;
                case R.id.fashion_button:
                    category = Business.FASHION;
                    break;
                case R.id.food_button:
                    category = Business.FOOD;
                    break;
                case R.id.places_grey_button:

                    break;
                case R.id.sport_button:

                    category = Business.SPORT;
                    break;
                case R.id.entertainment_button:
                    category = Business.ENTERTAINMENT;
                    break;
                case R.id.services_button3x:
                    category=Business.SERVICES;
                    break;

            }
            ClearAllMarkers();
            getFriendsLocation();

            makeUserMarker();
            setPeopleAroundMarkers();
            dissmissPopUpButtons();
//            peopleClusterMangager.cluster();
//            mClusterManager.cluster();

            if (!category.equals(""))
                showBusinessesCategoryMarkers(category);





        }


    }

    private void ClearAllMarkers() {
//        googleMap.clear();
        System.out.println("--->  " + allMarkers.size());
        for (int i = 0; i < allMarkers.size(); i++) {
            Marker m = allMarkers.get(i);
            m.remove();
        }
        allMarkers.clear();
    }

    private void addTrendyMarkers(){
        ArrayList<Business> trendyBusiness= MyApp.appManager.getTrendyBusinesses();
        if (trendyBusiness.size()>0){
            for (final Business b : trendyBusiness){
                final BitmapDescriptor icon2 = getMarkerIconByCategory(b.getCategory());
                double lat = Double.parseDouble(b.getLat());
                double lng = Double.parseDouble(b.getLng());
                final LatLng latlng = new LatLng(lat, lng) ;
                handler = new Handler();

                final Runnable r = new Runnable() {
                    public void run() {
                        boolean exist = false;

                        for (Marker marker: allMarkers){
                            if (marker.getPosition()==latlng&&b.getName().equals(marker.getTitle())){
                                exist=true;
                            }
                        }

                        if (!exist){
                            Marker m = googleMap.addMarker(new MarkerOptions().position(latlng).
                                    icon(icon2).title(b.getName()));
                            allMarkers.add(m);
                            businesses.add(b);
                        }


                    }
                };

                handler.postDelayed(r, 0);
            }
        }
    }

    private void showBusinessesCategoryMarkers(String category) {
//        int rad = Integer.parseInt(MyApp.userManager.getAppuser().getRadius());
//        if (rad>100)
//            rad=100;
//        System.out.println("--->"+ rad+" "+ category+usrLatLng.latitude+""+usrLatLng.longitude+"");
        MyApp.networkManager.makeRequest2(new GetBusinessesByCategoryRequest(category, usrLatLng.latitude + "", usrLatLng.longitude + "", 20, 9), new NetworkCallback<GetBusinessesByCategoryResponse>() {
            @Override
            public void onResponse2(boolean success, String errorDesc, ResponseObject2<GetBusinessesByCategoryResponse> response) {
                super.onResponse2(success, errorDesc, response);
                if (success) {
                    if (response.getData().size() > 0) {
                        System.out.println("--->  " + response.getData().size());
                        businesses = new ArrayList<>();
                        for (final GetBusinessesByCategoryResponse b : response.getData()) {
                            final Business bn = castToBusinessAndAddToList(b);
                            double lat = Double.parseDouble(bn.getLat());
                            double lng = Double.parseDouble(bn.getLng());
                            final LatLng latlng = new LatLng(lat, lng);

//                            final View view = getLayoutInflater().inflate(R.layout.business_marker, null);
//                            ImageView categoryImage = (ImageView) view.findViewById(R.id.categoryImage);
//                            categoryImage.setImageResource(R.drawable.map_pin_food_2x);
//                            final Bitmap icon = BitmapUtil.createBitmapFromView(frameContainer, view);
//
                            final BitmapDescriptor icon2 = getMarkerIconByCategory(bn.getCategory());

                            handler = new Handler();

                            final Runnable r = new Runnable() {
                                public void run() {
                                    boolean exist = false;
                                    for (Marker marker: allMarkers){
                                        if (marker.getPosition()==latlng&&bn.getName().equals(marker.getTitle())){
                                            exist=true;
                                        }
                                    }
                                    if (!exist){
                                        Marker m = googleMap.addMarker(new MarkerOptions().position(latlng).
                                                icon(icon2).title(b.getName()));
                                        allMarkers.add(m);
                                    }
                                }
                            };

                            handler.postDelayed(r, 0);


                        }
                        frameContainer.removeAllViews();


                    }
                }
//                if (fromTrendy)
//                    addTrendyMarkers();
            }
        });

    }

    @NonNull
    private Business castToBusinessAndAddToList(GetBusinessesByCategoryResponse b) {
        Business bn = new Business(b.getCoupons(), b.getName(), b.getExternalID(), b.getID(), b.getPhone(), b.getPhone2(), b.getOpening(), b.getImage(), b.getMapImage(), b.getCheckIns(), b.getSpecialRank(), b.getNumGifts(), b.getCategory(), b.getFax(), b.getCountry(), b.getCity(),
                b.getStreet(), b.getHouseNum(), b.getWebsite(), b.getFacebookLink(), b.getBusinessDesc(), b.getRank(),
                b.getNumCoupons(), b.getStatus(), b.getEdited(), b.getLat(), b.getLng(), b.getAdminRank(), b.getShowAdminRank(), b.getCreated()
                , b.getNumRankers(), b.getSort(), b.getDistance());
        businesses.add(bn);
        return bn;
    }

    private void dissmissPopUpButtons() {
        isCategoryViewVisable = false;
        YoYo.with(Techniques.ZoomOut).duration(300).withListener(new MyAnimationListener() {
            @Override
            public void onAnimationEnd(Animation animation) {
                super.onAnimationEnd(animation);
            }

        }).playOn(mapPopUpButtons);

        Runnable r = new Runnable() {
            @Override
            public void run() {
                mapPopUpButtons.setVisibility(View.GONE);
                checkinMapButton.setVisibility(View.VISIBLE);
                placesMapButton.setVisibility(View.VISIBLE);
            }
        };

        Handler h = new Handler();
        h.postDelayed(r, 290);

    }

//        friendsMapListCursorAdapter.swapCursor(data);
//        friendsMapListCursorAdapter.swapCursor(null);
//        return MyApp.tableFriends.readLouderCursor(TableFriends.TAG_FIRST_NAME + " ASC");

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
//        String[] selecationArgs = {'%'+ searchMenuEditText.getText().toString() +'%'};
//        selection = new SelectionBuilder().where(TableVideos.COLUMN_VIDEO_TITLE).like().build();
//        return TubieApp.tableVideosManager.readLouderCursor(selection, selecationArgs, TableVideos.COLUMN_ID);
        String selectionBuilder= new SelectionBuilder().where(TableFriends.TAG_FIRST_NAME).like(serchString).or().where(TableFriends.TAG_LAST_NAME).like(serchString).build();
        return MyApp.tableFriends.readLouderCursor(selectionBuilder,TableFriends.TAG_FIRST_NAME + " ASC");
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        friendsMapListCursorAdapter.swapCursor(data);
    }


    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        friendsMapListCursorAdapter.swapCursor(null);
    }
    private class FriendRenderer extends DefaultClusterRenderer<Friend> {
        private final IconGenerator mIconGenerator = new IconGenerator(getApplicationContext());
        private final IconGenerator mIconGenerator2 = new IconGenerator(getApplicationContext());
        private final IconGenerator mClusterIconGenerator = new IconGenerator(getApplicationContext());
        private final ImageView mImageView;
        private final RoundedImageView mCircleClusterImageView;
        private final ImageView mClusterImageView;
        private final int mDimension;
        private final ImageView friend_locatorImgView;

        public FriendRenderer(GoogleMap map) {
            super(getApplicationContext(), map, mClusterManager);


            View multiProfile = getLayoutInflater().inflate(R.layout.multi_profile, null);
            mClusterIconGenerator.setContentView(multiProfile);
            mClusterImageView = (ImageView) multiProfile.findViewById(R.id.image);

            View multiProfile2 = getLayoutInflater().inflate(R.layout.marker_layout, null);
            mIconGenerator2.setContentView(multiProfile2);
            mCircleClusterImageView = (RoundedImageView) multiProfile2.findViewById(R.id.marker_image);
            friend_locatorImgView = (ImageView) multiProfile2.findViewById(R.id.friend_locatorImgView);


            mImageView = new ImageView(getApplicationContext());
            mDimension = (int) getResources().getDimension(R.dimen.custom_profile_image);
            mImageView.setLayoutParams(new ViewGroup.LayoutParams(mDimension, mDimension));
            int padding = (int) getResources().getDimension(R.dimen.custom_profile_padding);
            mImageView.setPadding(padding, padding, padding, padding);
            mIconGenerator.setContentView(mImageView);


        }

        @Override
        protected void onBeforeClusterItemRendered(Friend friend, MarkerOptions markerOptions) {
            // Draw a single person.
            // Set the info window to show their name.
//            mImageView.setImageDrawable(person.getProfileImageDrawable());
////            ImageLoader.ByUrl(person.getProfileImage(), mImageView);
//            Bitmap icon = mIconGenerator.makeIcon();
//            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon)).title(person.getUserName());

            if (!friend.getImage().equals("")) {

                Bitmap bit = BitmapUtil.loadImageFromStorage(MyApp.appManager.getImagePath(), friend.getFBID());
                mCircleClusterImageView.setImageBitmap(bit);
            }
//                ImageLoader.ByUrl(friend.getImage(), mCircleClusterImageView);
            if (friend.getUserActivity()!=null) {
                if (friend.getUserActivity().getActivity().equals("CHECKIN")) {
                    friend_locatorImgView.setImageResource(R.drawable.checkin_friend_locator3x);

                }else if (friend.getUserActivity().getActivity().equals("RANK")) {
                    friend_locatorImgView.setImageResource(R.drawable.rated_friend_locator3x);
                }else if (friend.getUserActivity().getActivity().equals("non")){
                    friend_locatorImgView.setImageResource(R.drawable.friend_locator3x);
                }
            }

//            mCircleClusterImageView.setImageDrawable(friend.getProfileImageDrawable());
            mIconGenerator2.setBackground(new ColorDrawable(Color.TRANSPARENT));
            Bitmap icon = mIconGenerator2.makeIcon();

            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon)).title(friend.getFirstName());


//            map.addMarker(new MarkerOptions().position(location).icon(BitmapDescriptorFactory.fromBitmap(icon)));
        }

        @Override
        protected void onBeforeClusterRendered(Cluster<Friend> cluster, MarkerOptions markerOptions) {
            // Draw multiple people.
            // Note: this method runs on the UI thread. Don't spend too much time in here (like in this example).
            List<Drawable> profilePhotos = new ArrayList<Drawable>(Math.min(4, cluster.getSize()));
            int width = mDimension;
            int height = mDimension;

            for (Friend f : cluster.getItems()) {

                // Draw 4 at most.
                if (profilePhotos.size() == 4) break;


//                Drawable drawable = getResources().getDrawable(R.drawable.avatar);
//                Drawable drawable = new BitmapDrawable(BitmapFactory.decodeByteArray(p.getProfileImageBlob(), 0, p.getProfileImageBlob().length));
                Drawable drawable = null;
               if (!f.getImage().equals("")) {
//                 Bitmap bit=  BitmapUtil.getImageBitmap(getApplicationContext(), f.getFBID(), ".jpg");
//               assert bit != null;
                   Bitmap bit=BitmapUtil.loadImageFromStorage(MyApp.appManager.getImagePath(),f.getFBID());
                 drawable = new BitmapDrawable(getResources(), bit);

//                    drawable = getResources().getDrawable(Integer.parseInt(f.getImage()));
               }
               else
               drawable = getResources().getDrawable(R.drawable.ic_launcher);



                drawable.setBounds(0, 0, width, height);
                profilePhotos.add(drawable);
            }
            MultiDrawable multiDrawable = new MultiDrawable(profilePhotos);
            multiDrawable.setBounds(0, 0, width, height);

            mClusterImageView.setImageDrawable(multiDrawable);
            Bitmap icon = mClusterIconGenerator.makeIcon(String.valueOf(cluster.getSize()));
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));


        }

        @Override
        protected boolean shouldRenderAsCluster(Cluster cluster) {
            // Always render clusters.
            return cluster.getSize() > 1;
        }

//        @Override
//        protected void onClusterRendered(Cluster<Friend> cluster, Marker marker) {
//            super.onClusterRendered(cluster, marker);
//            allMarkers.add(marker);
//        }

        @Override
        protected void onClusterItemRendered(Friend clusterItem, Marker marker) {
            super.onClusterItemRendered(clusterItem, marker);
            allMarkers.add(marker);

        }
    }
////////////////////////////////////////////////////////////////////////////////////////////




private class PeopleRenderer extends DefaultClusterRenderer<PeopleLocation> {
    private final IconGenerator mIconGenerator = new IconGenerator(getApplicationContext());
    private final IconGenerator mIconGenerator2 = new IconGenerator(getApplicationContext());
    private final IconGenerator mClusterIconGenerator = new IconGenerator(getApplicationContext());
    private final ImageView mImageView;
//    private final ImageView mClusterImageView;
    private final int mDimension;
    private final TextView strangersNumberTv;

    public PeopleRenderer(GoogleMap map) {
        super(getApplicationContext(), map, peopleClusterMangager);


        View multiProfile = getLayoutInflater().inflate(R.layout.number_cluster, null);
        mClusterIconGenerator.setContentView(multiProfile);
//        mClusterImageView = (ImageView) multiProfile.findViewById(R.id.image);
        strangersNumberTv = (TextView)multiProfile.findViewById(R.id.text);


        View multiProfile2 = getLayoutInflater().inflate(R.layout.stranger_marker, null);
        mIconGenerator2.setContentView(multiProfile2);


        mImageView = new ImageView(getApplicationContext());
        mDimension = (int) getResources().getDimension(R.dimen.custom_profile_image);
        mImageView.setLayoutParams(new ViewGroup.LayoutParams(mDimension, mDimension));
        int padding = (int) getResources().getDimension(R.dimen.custom_profile_padding);
        mImageView.setPadding(padding, padding, padding, padding);
        mIconGenerator.setContentView(mImageView);


    }

    @Override
    protected void onBeforeClusterItemRendered(PeopleLocation peopleLocation, MarkerOptions markerOptions) {
        // Draw a single person.
        // Set the info window to show their name.
//            mImageView.setImageDrawable(person.getProfileImageDrawable());
////            ImageLoader.ByUrl(person.getProfileImage(), mImageView);
//            Bitmap icon = mIconGenerator.makeIcon();
//            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon)).title(person.getUserName());


//            mCircleClusterImageView.setImageDrawable(friend.getProfileImageDrawable());
        mIconGenerator2.setBackground(new ColorDrawable(Color.TRANSPARENT));
        Bitmap icon = mIconGenerator2.makeIcon();

        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));


//            map.addMarker(new MarkerOptions().position(location).icon(BitmapDescriptorFactory.fromBitmap(icon)));
    }

    @Override
    protected void onBeforeClusterRendered(Cluster<PeopleLocation> cluster, MarkerOptions markerOptions) {
        // Draw multiple people.
        // Note: this method runs on the UI thread. Don't spend too much time in here (like in this example).
//        List<Drawable> profilePhotos = new ArrayList<Drawable>(Math.min(4, cluster.getSize()));
//        int width = mDimension;
//        int height = mDimension;
//
//        for (PeopleLocation f : cluster.getItems()) {
//
//            // Draw 4 at most.
//            if (profilePhotos.size() == 4) break;
//
//
////                Drawable drawable = getResources().getDrawable(R.drawable.avatar);
////                Drawable drawable = new BitmapDrawable(BitmapFactory.decodeByteArray(p.getProfileImageBlob(), 0, p.getProfileImageBlob().length));
//            Drawable drawable = null;
//
//                drawable = getResources().getDrawable(R.drawable.stranger_locator3x);
//
//
//
//            drawable.setBounds(0, 0, width, height);
//            profilePhotos.add(drawable);
//        }
//        MultiDrawable multiDrawable = new MultiDrawable(profilePhotos);
//        multiDrawable.setBounds(0, 0, width, height);
//
//        mClusterImageView.setImageDrawable(multiDrawable);
        Bitmap icon = mClusterIconGenerator.makeIcon(cluster.getSize()+"");
//        strangersNumberTv.setText(cluster.getSize()+"");
        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));


    }

    @Override
    protected boolean shouldRenderAsCluster(Cluster cluster) {
        // Always render clusters.
        return cluster.getSize() > 1;
    }

}
    public class InputFilterMinMax implements InputFilter {

        private int min, max;

        public InputFilterMinMax(int min, int max) {
            this.min = min;
            this.max = max;
        }

        public InputFilterMinMax(String min, String max) {
            this.min = Integer.parseInt(min);
            this.max = Integer.parseInt(max);
        }

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            try {
                // Remove the string out of destination that is to be replaced
                String newVal = dest.toString().substring(0, dstart) + dest.toString().substring(dend, dest.toString().length());
                // Add the new string in
                newVal = newVal.substring(0, dstart) + source.toString() + newVal.substring(dstart, newVal.length());
                int input = Integer.parseInt(newVal);
                if (isInRange(min, max, input))
                    return null;
            } catch (NumberFormatException nfe) { }
            return "";
        }

        private boolean isInRange(int a, int b, int c) {
            return b > a ? c >= a && c <= b : c >= b && c <= a;
        }
    }
}
