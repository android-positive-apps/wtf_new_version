package com.wtf.activity.main;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.facebook.FacebookSdk;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.sromku.simple.fb.Permission;
import com.sromku.simple.fb.SimpleFacebook;
import com.sromku.simple.fb.SimpleFacebookConfiguration;
import com.wtf.activity.R;
import com.wtf.activity.database.tables.TableCheckIns;
import com.wtf.activity.database.tables.TableObserver;
import com.wtf.activity.database.tables.TableFriends;
import com.wtf.activity.network.NetworkManager;
import com.wtf.activity.storage.AppPreference;
import com.wtf.activity.storage.AppSettings;
import com.wtf.activity.storage.StorageManager;
import com.wtf.activity.storage.UserSettings;
import com.wtf.activity.util.AppUtil;
import com.wtf.activity.util.DeviceUtil;
import com.wtf.activity.util.ToastUtil;

//import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.Fabric;
import java.util.ArrayList;


//// TODO: 5/10/2016
//    import com.google.android.gms.analytics.GoogleAnalytics;
//    import com.google.android.gms.analytics.Tracker;

/**
 * Created by natiapplications on 16/08/15.
 */
public class MyApp extends Application implements Application.ActivityLifecycleCallbacks {

    public static final String FACEBOOK_NAME_SPACE = "bmmapplication";

    private static final String TAG = "ApplicationStatus";
    public static BaseActivity mainInstance;


    public static Context appContext;
    public static String appVersion;




    public static AppPreference appPreference;
    public static AppPreference.PreferenceUserSettings userSettings;
    public static AppPreference.PreferenceGeneralSettings generalSettings;
    public static AppPreference.PreferenceUserProfil userProfile;


    public static BaseActivity currentActivity;


    //managers
    public static NetworkManager networkManager;
    public static StorageManager storageManager;

    public static ArrayList<String> currentPlaceIds = new ArrayList<String>();
    public static AppSettings appManager;
    public static UserSettings userManager;
    public static TableFriends tableFriends;
    public static TableCheckIns tableCheckIns;
    private Tracker mTracker;

    //// TODO: 5/10/2016
//    private Tracker mTracker;


    @Override
    public void onCreate() {
        // TODO Auto-generated method stub
        super.onCreate();
        FacebookSdk.sdkInitialize(this);

        Fabric.with(this, new Crashlytics());
//        Fabric.with(this, new Crashlytics());
        registerActivityLifecycleCallbacks(this);
        appContext = getApplicationContext();
        appVersion = DeviceUtil.getDeviceVersion();
        loadPreferences();
        loadDataBase();
        appManager = AppSettings.getInstance();
        userManager = UserSettings.getInstance();
        networkManager = NetworkManager.getInstance(appContext);
        storageManager = StorageManager.getInstance();
        cofigFB();

    }

    private void loadDataBase() {
        tableFriends =TableFriends.getInstance(appContext);
        tableCheckIns= TableCheckIns.getInstance(appContext);
    }

    public static void closeDatabase () {
        MyApp.tableFriends.close();
        MyApp.tableCheckIns.close();


    }

    public static void deleteDatabase () {
        MyApp.tableFriends.deleteAll();
        MyApp.tableCheckIns.deleteAll();

    }
    private void cofigFB() {
        Permission[] permissions = new Permission[] {
                Permission.USER_PHOTOS,
                Permission.EMAIL,
                Permission.USER_FRIENDS,

        };

        SimpleFacebookConfiguration configuration = new SimpleFacebookConfiguration.Builder()
                .setAppId(getResources().getString(R.string.app_id))
                .setNamespace("wtf")
//                .setNamespace(FACEBOOK_NAME_SPACE)
                .setPermissions(permissions)
                .setAskForAllPermissionsAtOnce(true)
                .build();

        SimpleFacebook.setConfiguration(configuration);

    }


    private void loadPreferences() {
        appPreference = AppPreference.getInstans(appContext);
        userSettings = appPreference.getUserSettings();
        userProfile = appPreference.getUserProfil();
        generalSettings = appPreference.getGeneralSettings();
    }


    public static void addTablesObservers (TableObserver observer){
       // MyApp.tableManager.addObserver(observer);

    }

    public static void removeTablesObservers (TableObserver observer){
       // MyApp.tableManager.removeObserver(observer);

    }

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
        if (activity instanceof MainActivity)
        currentActivity= (BaseActivity) activity;
    }

    @Override
    public void onActivityStarted(Activity activity) {
        if(activity instanceof LoginActivity || activity instanceof MainActivity){
            AppUtil.startLocationService(activity);
        }
    }

    @Override
    public void onActivityResumed(Activity activity) {
        Log.i(TAG, "onActivityResumed: " + activity);

    }

    @Override
    public void onActivityPaused(Activity activity) {
        Log.i(TAG, "onActivityPaused: " + activity);
        appManager.save();
    }

    @Override
    public void onActivityStopped(Activity activity) {

    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

    }

    @Override
    public void onActivityDestroyed(Activity activity) {
        if(activity instanceof MainActivity){
//            AppUtil.stopLocationService(activity);

        }
    }


    synchronized public Tracker getDefaultTracker() {
        if (mTracker == null) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
            mTracker = analytics.newTracker(R.xml.global_tracker);
        }
        return mTracker;
    }
}
