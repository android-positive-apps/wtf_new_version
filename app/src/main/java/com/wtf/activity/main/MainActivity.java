package com.wtf.activity.main;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.maps.model.LatLng;
import com.sromku.simple.fb.SimpleFacebook;
import com.sromku.simple.fb.entities.Profile;
import com.sromku.simple.fb.listeners.OnFriendsListener;
import com.sromku.simple.fb.listeners.OnProfileListener;
import com.sromku.simple.fb.utils.Attributes;
import com.sromku.simple.fb.utils.PictureAttributes;
import com.wtf.activity.R;
import com.wtf.activity.adapters.SelectBusinessToRateAdapter;
import com.wtf.activity.fragments.BusinessPageFragment;
import com.wtf.activity.network.NetworkCallback;
import com.wtf.activity.network.requests.GetAppuserIdRequest;
import com.wtf.activity.network.requests.GetAppuserRequest;
import com.wtf.activity.network.requests.GetBusinessesByCategoryV2Request;
import com.wtf.activity.network.requests.GetStatisticsRequest;
import com.wtf.activity.network.requests.SearchBusinessRequest;
import com.wtf.activity.network.requests.UpdateAppuserGCMRequest;
import com.wtf.activity.network.requests.UpdateAppuserRequest;
import com.wtf.activity.network.response.GetAppuserIdResponse;
import com.wtf.activity.network.response.GetAppuserResponse;
import com.wtf.activity.network.response.GetBusinessesByCategoryV2Response;
import com.wtf.activity.network.response.GetStatisticsResponse;
import com.wtf.activity.network.response.ResponseObject;
import com.wtf.activity.network.response.SearchBusinessResponse;
import com.wtf.activity.network.response.UpdateAppuserGCMResponse;
import com.wtf.activity.network.response.UpdateAppuserResponse;
import com.wtf.activity.objects.Appuser;
import com.wtf.activity.objects.Business;
import com.wtf.activity.objects.Friend;
import com.wtf.activity.objects.MainDetails;
import com.wtf.activity.util.AppUtil;
import com.wtf.activity.util.BitmapUtil;
import com.wtf.activity.util.ContentProviderUtil;
import com.wtf.activity.util.DialogExample;
import com.wtf.activity.util.SwitchUtil;
import com.wtf.activity.util.ToastUtil;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;


public class MainActivity extends BaseActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {



    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    String SENDER_ID = "221047633308";

    public static DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private LinearLayout friendsButton;
    private TextView friendsTextView;
    private LinearLayout trendyButton;
    private TextView trendyTextView;
    private LinearLayout pointsButton;
    private TextView pointsTextView;
    private LinearLayout businessesButton;
    private TextView BusinessesTextView;
    private FrameLayout checkinButton;
    private FrameLayout rateButton;
    private ImageView locationIconActionBar;
    private SimpleFacebook mSimpleFacebook;
    private SwitchCompat incognitoSwitch;
    private int dialogItemPosition=-1;

    public static final String ACTION_CHECK_IN= "action_check_in";
    public static final String ACTION_POINTS_OPEN= "points_action_open";
    private Bitmap bitmap;
    private boolean anonymous=true;
    private final String TAG= "Main Activity";
    private TextView usersAroundTextView;
    private TextView checkInsTextView;
    private TextView ratedPlacesTextView;
    private GoogleCloudMessaging gcm;
    private String gcmString;
    public static final String ACTION_TRENDY= "action_trendy";
    private Tracker mTracker;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_main);

        InitAnalytic();

        anonymous = MyApp.appManager.isAnonymous();
        AppUtil.saveScreenDimention(this);

        setupActionBar(R.id.app_actionbar, getString(R.string.app_name));
        setUpDrawerNavigation();

        findViews();
        setListeners();
        mSimpleFacebook = SimpleFacebook.getInstance(this);

        makeOrUpdateGcm();
        insertLastDeatils();
        getBusinessNumber();
        getStatistics();
        getTrendy10();


        if (!anonymous) {
            getFbFriends();
        }

        getIntentInOrderToOpenGiftsAct();


    }

    private void getIntentInOrderToOpenGiftsAct() {
        Intent i = getIntent();
        if (i!=null){
            if (i.getAction()!=null) {
                if (i.getAction().isEmpty())
                    return;
                if (i.getAction().equals(ACTION_POINTS_OPEN)) {
                    Intent openGifts = new Intent(MainActivity.this, GiftsActivity.class);
                    startActivity(openGifts);
                }
            }
        }
    }


    //    private void switchColor(boolean checked) {
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
////            incognitoSwitch.getThumbDrawable().setColorFilter(checked ? Color.BLACK : Color.WHITE, PorterDuff.Mode.MULTIPLY);
////            incognitoSwitch.getTrackDrawable().setColorFilter(!checked ? Color.BLACK : Color.WHITE, PorterDuff.Mode.MULTIPLY);
//            incognitoSwitch.getThumbDrawable().setColorFilter(Color.WHITE, PorterDuff.Mode.MULTIPLY);
//            incognitoSwitch.getTrackDrawable().setColorFilter(!checked ? Color.BLACK  : getResources().getColor(R.color.wtf_light_blue), PorterDuff.Mode.MULTIPLY);
//        }
//    }
    private void makeOrUpdateGcm() {
        if (!anonymous) {
            if (checkPlayServices()) {
                gcm = GoogleCloudMessaging.getInstance(this);
                gcmString = MyApp.appManager.getGcm();


//            sendRegistrationIdToBackend();
                if (gcmString.isEmpty()) {
                    registerInBackground();
                }else {
                    updateAppUserGcm(gcmString);
                }
            } else {
                Log.i(TAG, "No valid Google Play Services APK found.");
            }
        }
    }


    private void insertLastDeatils() {
        MainDetails mainDetails = MyApp.appManager.getMainDetails();
        if (mainDetails!=null){
            checkInsTextView.setText(mainDetails.getCheckIns());
            ratedPlacesTextView.setText(mainDetails.getRatedPlaces());
            usersAroundTextView.setText(mainDetails.getUserAround());
            BusinessesTextView.setText(mainDetails.getBusinesses());
            friendsTextView.setText(mainDetails.getFriends());
            pointsTextView.setText(mainDetails.getPoints());
            trendyTextView.setText("10");
//            trendyTextView.setText(mainDetails.getTrendySpots());
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        MainDetails mainDetails= new MainDetails(
                friendsTextView.getText().toString(),
                BusinessesTextView.getText().toString(),
                pointsTextView.getText().toString(),
                trendyTextView.getText().toString(),
                ratedPlacesTextView.getText().toString(),
                checkInsTextView.getText().toString(),
                usersAroundTextView.getText().toString());
        MyApp.appManager.setMainDetails(mainDetails);
        MyApp.appManager.save();
    }
    private void getTrendy10() {
        try {
            MyApp.networkManager.makeRequest(new GetBusinessesByCategoryV2Request("", MyApp.appManager.getLatLng().latitude + "",
                    MyApp.appManager.getLatLng().longitude + "", 10, 10, "Rank"), new NetworkCallback<GetBusinessesByCategoryV2Response>() {
                @Override
                public void onResponse(boolean success, String errorDesc, ResponseObject<GetBusinessesByCategoryV2Response> response) {
                    super.onResponse(success, errorDesc, response);
                    if (success) {
                        if (response.getData().getBusinesses() != null) {
                            String num = response.getData().getBusinesses().size() + "";
                            if (response.getData().getBusinesses().size() > 0) {
                                MyApp.appManager.setTrendyBusinesses( response.getData().getBusinesses());
                                MyApp.appManager.save();
                                trendyTextView.setText(num);
                            } else {
                                trendyTextView.setText("0");
                            }
                        }
                    }else{

                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();

        }

    }
    private void getBusinessNumber() {
        try {
            MyApp.networkManager.makeRequest(new GetBusinessesByCategoryV2Request("", MyApp.appManager.getLatLng().latitude + "",
                    MyApp.appManager.getLatLng().longitude + "", 25000, 99, ""), new NetworkCallback<GetBusinessesByCategoryV2Response>() {
                @Override
                public void onResponse(boolean success, String errorDesc, ResponseObject<GetBusinessesByCategoryV2Response> response) {
                    super.onResponse(success, errorDesc, response);
                    if (success) {
                        if (response.getData().getBusinesses() != null) {
                            String num = response.getData().getBusinesses().size() + "";
                            BusinessesTextView.setText(num);
                            if (response.getData().getBusinesses().size() > 0) {
                                int count = 0;
                                for (Business b : response.getData().getBusinesses()) {
                                    if (b.isTrendySpot())
                                        count++;
                                }
                                //todo for now 28.4, no set text
//                                trendyTextView.setText(count + "");
                            } else {
                                //todo for now 28.4, no set text
//                                trendyTextView.setText("0");
                            }
                        }
                    }else{
                        //todo for now 28.4, no set text
//                        trendyTextView.setText("0");
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();

        }

    }


    private void getUserProfile() {

            OnProfileListener onProfileListener = new OnProfileListener() {
                @Override
                public void onComplete(Profile profile) {
                    MyApp.userManager.setUserProfile(profile);
                    updateAppUser(profile);
                    MyApp.userManager.save();
                    saveProfilePictureToStorage(profile);
                }


    /*
     * You can override other methods here:
     * onThinking(), onFail(String reason), onException(Throwable throwable)
     */
            };
            PictureAttributes pictureAttributes = Attributes.createPictureAttributes();
            pictureAttributes.setHeight(500);
            pictureAttributes.setWidth(500);
            pictureAttributes.setType(PictureAttributes.PictureType.SQUARE);

        Profile.Properties properties = new Profile.Properties.Builder()
                .add(Profile.Properties.ID).add(Profile.Properties.FIRST_NAME)
                .add(Profile.Properties.LAST_NAME).add(Profile.Properties.EMAIL)
                .add(Profile.Properties.PICTURE, pictureAttributes).build();
//
//            Profile.Properties properties = new Profile.Properties.Builder()
//                    .add(Profile.Properties.PICTURE, pictureAttributes)
//                    .build();
            mSimpleFacebook.getProfile(properties, onProfileListener);


    }

    private void saveProfilePictureToStorage(final Profile profile) {
        Thread thread = new Thread(new Runnable(){
            @Override
            public void run() {

                Bitmap b = getBitmapFromURL(profile.getPicture());
                if (b != null) {
//            BitmapUtil.saveImage(getApplicationContext(),bitmap,f.getFBID(),".jpg");
                    String path = BitmapUtil.saveToInternalStorage(getApplicationContext(), profile.getId(), b);
                    MyApp.appManager.setImagePath(path);
                    MyApp.appManager.save();

                }
            }});
        thread.start();
    }

    private void updateAppUser(final Profile profile) {
//        showProgressDialog();
        MyApp.networkManager.makeRequest(new UpdateAppuserRequest(MyApp.userManager.getAppuser().getID(),profile.getFirstName(), profile.getLastName(), profile.getId(), profile.getEmail(), profile.getPicture(), this), new NetworkCallback<UpdateAppuserResponse>() {
            @Override
            public void onResponse(boolean success, String errorDesc, ResponseObject<UpdateAppuserResponse> response) {
                super.onResponse(success, errorDesc, response);
                if (success) {
                    InsertMainDetails();
//                    getStatistics();
                    Log.d(TAG, "success UpdateAppuserRequest");

                    Answers.getInstance().logCustom(new CustomEvent("UpdateAppUser")
                            .putCustomAttribute("Success","true")
                            .putCustomAttribute("FbID", profile.getId())
                            .putCustomAttribute("AppUserID", MyApp.userManager.getAppuser().getID()));

                } else {
//                    dismissProgressDialog();
                    InsertMainDetails();


                    Answers.getInstance().logCustom(new CustomEvent("UpdateAppUser")
                            .putCustomAttribute("Success","false")
                            .putCustomAttribute("FbID", profile.getId())
                            .putCustomAttribute("AppUserID", MyApp.userManager.getAppuser().getID())
                            .putCustomAttribute("Error", errorDesc+""));

                }
            }
        });
    }

    private void InitAnalytic(){
        MyApp application = (MyApp) getApplication();
        mTracker = application.getDefaultTracker();
    }


    @Override
    public void onResume() {
        super.onResume();

        mTracker.setScreenName("Main screen");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());


        checkPlayServices();
        anonymous = MyApp.appManager.isAnonymous();
//
        if (!anonymous) {
//            getFbFriends();
//            getUserProfile();
            getUserProfile();

        }

        mSimpleFacebook = SimpleFacebook.getInstance(this);
        locationIconActionBar.setVisibility(View.VISIBLE);
        if (!ContentProviderUtil.isGPSEnebled(this)){
            DialogExample.openLocationDialog(this);
            locationIconActionBar.setImageResource(R.drawable.location_grey_icon_3x);
        }else {
            locationIconActionBar.setImageResource(R.drawable.location_icon3x);

        }
        /**
         * 18.4.16 change, make incognito invisible
         */

        incognitoSwitch.setChecked(MyApp.userManager.isIncognito());
        SwitchUtil.switchColor(MyApp.userManager.isIncognito(),incognitoSwitch,this);


//        if (!anonymous)
//        InsertMainDetails();

    }

    private void getStatistics() {
        MyApp.networkManager.makeRequest(new GetStatisticsRequest(), new NetworkCallback<GetStatisticsResponse>() {
            @Override
            public void onResponse(boolean success, String errorDesc, ResponseObject<GetStatisticsResponse> response) {
                super.onResponse(success, errorDesc, response);
                if (success) {
                    Log.d(TAG, "success GetStatisticsRequest");
                    usersAroundTextView.setText(response.getData().getUsersAround() + "");
                    checkInsTextView.setText(response.getData().getCheckIns() + "");
                    ratedPlacesTextView.setText(response.getData().getRatedPlaces() + "");
                }

            }
        });
    }

//    private void updateAppUserLOcation() {
//        MyApp.networkManager.makeRequest2(new UpdateAppuserLocationRequest(this), new NetworkCallback<UpdateAppuserResponse>() {
//            @Override
//            public void onResponse2(boolean success, String errorDesc, ResponseObject2<UpdateAppuserResponse> response) {
//                super.onResponse2(success, errorDesc, response);
//                if (success) {
//                    Log.d(TAG, "success update location");
//                } else {
//                    ToastUtil.toaster(errorDesc, true);
//                }
//
//            }
//        });
//    }

    private void InsertMainDetails() {

        //get the appuser and insert to user settings
        MyApp.networkManager.makeRequest(new GetAppuserRequest(MyApp.userManager.getAppuser().getID()), new NetworkCallback<GetAppuserResponse>() {
            @Override
            public void onResponse(boolean success, String errorDesc, ResponseObject<GetAppuserResponse> response) {
                super.onResponse(success, errorDesc, response);
                if (success) {
                    MyApp.userManager.setAppuser(response.getData().getAppuser());
                    MyApp.userManager.save();
                    pointsTextView.setText(response.getData().getAppuser().getPoints()+"");
//                    updateAppUserLOcation();
//                    dismissProgressDialog();

                }else {
//                    dismissProgressDialog();

                }

            }
        });

        //get points:
        String points = MyApp.userManager.getAppuser().getPoints()+"";
        pointsTextView.setText(points);





    }


    private void getFbFriends() {

        OnFriendsListener onFriendsListener = new OnFriendsListener() {
            @Override
            public void onComplete(List<Profile> friends) {

                friendsTextView.setText(friends.size()+ "");

                ArrayList<String> fbFriends = new ArrayList<>();
                if (friends.size()>0) {

                    MyApp.tableFriends.deleteAll();

                    for (int i = 0; i < friends.size(); i++) {
                        Profile profile = friends.get(i);
                        fbFriends.add(profile.getId());

                        getFriendDetails(profile);

                    }
                    MyApp.userManager.setFbFriendsIDs(fbFriends);
                    MyApp.userManager.save();
                }
            }

            @Override
            public void onFail(String reason) {
                super.onFail(reason);

            }

            /*
     * You can override other methods here:
     * onThinking(), onFail(String reason), onException(Throwable throwable)
     */
        };

        mSimpleFacebook.getFriends(onFriendsListener);
    }

    private void getFriendDetails(Profile profile) {

        if (profile!=null) {
            System.out.println("--->  friends profile "+profile.toString());
            long id = Long.parseLong(profile.getId());
            final String image = "https://graph.facebook.com/"+profile.getId()+"/picture?type=normal";
//            String sb= new SelectionBuilder().where(TableFriends.TAG_FBID).equealTo(profile.getId()).build();
//            ArrayList<Friend> friends =  MyApp.tableFriends.read(sb, TableFriends.TAG_FBID+" ASC");
//            Friend f=null;
//            if (friends.size()>0) {
//               f = friends.get(0);
//            }
//            if (f!=null){
//                    f.setImage(image);
//            }else {
//                f = new Friend(0,profile.getId(),"","","",profile.getName(),"",image,"");
//            }
//            Drawable d = ImageLoader.drawableFromUrl(image);
//                  Bitmap b = drawableToBitmap(d);
            Friend f = new Friend(profile.getId(),"","","",profile.getName(),"",image,"","");

            getFriendAppuserIdAndInsertDetails(profile, image);
//            MyApp.tableFriends.insertOrUpdate(f);
            //
//            System.out.println("--->  db: "+ MyApp.tableFriends.readAll("").size());

            //

//            ImageView testImage = (ImageView)findViewById(R.id.testImage);
//            ImageLoader.ByUrl(image,testImage);


//            final URL finalU = u;
            final Friend finalF = f;
            Thread thread = new Thread(new Runnable(){
                @Override
                public void run() {


//            if (finalU !=null)
                    //  bitmap = BitmapFactory.decodeStream(finalU.openConnection().getInputStream());
                    bitmap = getBitmapFromURL(image);
                    if (bitmap!=null) {
//            BitmapUtil.saveImage(getApplicationContext(),bitmap,f.getFBID(),".jpg");
                        String path = BitmapUtil.saveToInternalStorage(getApplicationContext(), finalF.getFBID(), bitmap);
                        MyApp.appManager.setImagePath(path);
                        MyApp.appManager.save();

                    }
                }
                });

            thread.start();

//           Bitmap bit=  BitmapUtil.getImageBitmap(getApplicationContext(), f.getFBID(), BitmapUtil.IMAGE_EXTENATION);
////            assert bit != null;
//            Drawable dra = new BitmapDrawable(getResources(), bit);


//           Bitmap s=  BitmapUtil.loadImageFromStorage(path, f.getFBID());
//            System.out.println("--->  =file="+f1.getAbsolutePath()+f1.getName() );

        }
    }

    @NonNull
    private void getFriendAppuserIdAndInsertDetails(final Profile profile, final String image) {
        MyApp.networkManager.makeRequest(new GetAppuserIdRequest(profile.getId()),new NetworkCallback<GetAppuserIdResponse>(){
            @Override
            public void onResponse(boolean success, String errorDesc, ResponseObject<GetAppuserIdResponse> response) {
                super.onResponse(success, errorDesc, response);
                if (success){
                    if (response.getData().getAppuserID()!=null){
                        if (!response.getData().getAppuserID().isEmpty()){
                            getFriendDetailsFromServer(response.getData().getAppuserID(),profile,image);
                        }else {
                            Friend f = new Friend(profile.getId(),"","","",profile.getName(),"",image,"","");
                            System.out.println("--->  friend: " + f.getID());
                            MyApp.tableFriends.insert(f);
                        }
                    }else {
                        Friend f = new Friend(profile.getId(),"","","",profile.getName(),"",image,"","");
                        System.out.println("--->  friend: " + f.getID());
                        MyApp.tableFriends.insert(f);
                    }
                }else {
                    Friend f = new Friend(profile.getId(),"","","",profile.getName(),"",image,"","");
                    System.out.println("--->  friend: " + f.getID());
                    MyApp.tableFriends.insert(f);
                }
            }
        });




    }

    private void getFriendDetailsFromServer(String appuserID, final Profile profile, final String image) {
        MyApp.networkManager.makeRequest(new GetAppuserRequest(appuserID),new NetworkCallback<GetAppuserResponse>(){
            @Override
            public void onResponse(boolean success, String errorDesc, ResponseObject<GetAppuserResponse> response) {
                super.onResponse(success, errorDesc, response);
                if (success){
                    Appuser au =response.getData().getAppuser();
                    if (au!=null){

                            Friend f = new Friend(profile.getId(),"","",au.getID(),au.getFirstName(),au.getLastName(),image,"",au.getPhone());
                            System.out.println("--->  friend: " + au.getFirstName()+" "+f.getID());
                            MyApp.tableFriends.insert(f);

                    }else {
                        Friend f = new Friend(profile.getId(),"","","",profile.getName(),"",image,"","");
                        System.out.println("--->  friend: " + f.getID());
                        MyApp.tableFriends.insert(f);
                    }
                }else {
                    Friend f = new Friend(profile.getId(),"","","",profile.getName(),"",image,"","");
                    System.out.println("---> friend: " + f.getID());
                    MyApp.tableFriends.insert(f);
                }
            }
        });
    }

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil
                .isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }
    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
    public static Bitmap drawableToBitmap (Drawable drawable) {
        Bitmap bitmap = null;

        if (drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            if(bitmapDrawable.getBitmap() != null) {
                return bitmapDrawable.getBitmap();
            }
        }

        if(drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) {
            bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888); // Single color bitmap will be created of 1x1 pixel
        } else {
            bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        }

        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }


    private void setListeners() {
        friendsButton   .setOnClickListener(this);
        trendyButton    .setOnClickListener(this);
        pointsButton    .setOnClickListener(this);
        businessesButton.setOnClickListener(this);
        checkinButton   .setOnClickListener(this);
        rateButton      .setOnClickListener(this);
        incognitoSwitch .setOnCheckedChangeListener(this);
    }

    private void findViews() {
        friendsButton       =(LinearLayout)findViewById(R.id.friendsButton);
        trendyButton        =(LinearLayout)findViewById(R.id.trendyButton);
        pointsButton        =(LinearLayout)findViewById(R.id.pointsButton);
        businessesButton    = (LinearLayout) findViewById(R.id.businessesButton);

        friendsTextView     =(TextView)findViewById(R.id.friendsTextView);
        trendyTextView      =(TextView)findViewById(R.id.trendyTextView);
        pointsTextView      =(TextView)findViewById(R.id.pointsTextView);
        BusinessesTextView  = (TextView) findViewById(R.id.BusinessesTextView);

        usersAroundTextView= (TextView) findViewById(R.id.usersAroundTextView);
        checkInsTextView= (TextView) findViewById(R.id.checkInsTextView);
        ratedPlacesTextView= (TextView) findViewById(R.id.ratedPlacesTextView);

        checkinButton       =(FrameLayout)findViewById(R.id.checkinButton);
        rateButton          =(FrameLayout)findViewById(R.id.rateButton);

        locationIconActionBar=(ImageView)findViewById(R.id.locationIconActionBar);

         incognitoSwitch = (SwitchCompat) findViewById(R.id.incognitoSwitch);

    }



    private void setUpDrawerNavigation() {
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerToggle = new ActionBarDrawerToggle
                (this, mDrawerLayout,getActionbarToolbar(), 0, 0){

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }
        };
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }


    @Override
    public void onClick(View v) {
        Intent i = new Intent(MainActivity.this,MapActivity.class);
        switch (v.getId()){
            case R.id.checkinButton:
//                appUserCheckin();
                if (!anonymous) {
                    i.setAction(ACTION_CHECK_IN);
                    startActivity(i);
                }else {
                    ToastUtil.toaster(getResources().getString(R.string.please_login_facebook_checkin),false);
                }
                break;
            case R.id.friendsButton:
                startActivity(i);
                break;
            case R.id.businessesButton:
                i.setAction(MapActivity.ACTION_PlACES);
                startActivity(i);
                break;
            case R.id.rateButton:
                if (!anonymous) {
                    searchBusinessesClose();
                }else {
                    ToastUtil.toaster(getResources().getString(R.string.please_login_facebook_rate), false);
                }
                break;
            case R.id.pointsButton:
                if (!anonymous) {
                    Intent giftsIntent = new Intent(MainActivity.this, GiftsActivity.class);
                    startActivity(giftsIntent);
                }else {
                    ToastUtil.toaster(getResources().getString(R.string.login_facebook_coupons), false);

                }
                break;
            case R.id.trendyButton:
                i.setAction(ACTION_TRENDY);
                startActivity(i);
                break;
        }

    }

    private void searchBusinessesClose() {
        LatLng latLng = MyApp.appManager.getLatLng();
//
        //search for businesses close to the user
        MyApp.networkManager.makeRequest(new SearchBusinessRequest("", "", latLng.latitude + "", latLng.longitude + "", 10), new NetworkCallback<SearchBusinessResponse>() {
            @Override
            public void onResponse(boolean success, String errorDesc, ResponseObject<SearchBusinessResponse> response) {
                super.onResponse(success, errorDesc, response);
                if (success) {
                    ArrayList<Business> businesses = response.getData().getBusinesses();
                    showBusinessesToRateDialog(businesses);

                }
            }
        });
    }

    private void showBusinessesToRateDialog(final ArrayList<Business> businesses) {
        LayoutInflater factory = LayoutInflater.from(this);
        final View checkInDialogView = factory.inflate(
                R.layout.dialog_business_chooser, null);
        final AlertDialog chooseBusinessDialog = new AlertDialog.Builder(this).create();
        chooseBusinessDialog.setView(checkInDialogView);
        ListView businessesDialogListView = (ListView) checkInDialogView.findViewById(R.id.businessesDialogListView);
        SelectBusinessToRateAdapter businessesDialogAdapter = new SelectBusinessToRateAdapter(this, businesses);
        businessesDialogListView.setAdapter(businessesDialogAdapter);
        businessesDialogListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                dialogItemPosition = position;
            }
        });


        checkInDialogView.findViewById(R.id.businessesDialogImageView).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (dialogItemPosition != -1) {
                    Business b = businesses.get(dialogItemPosition);
                    MyApp.appManager.setTempBusiness(b);
                    MyApp.appManager.save();
                    chooseBusinessDialog.dismiss();
                    Intent BusinessPageIntent= new Intent(MainActivity.this,BusinessActivity.class);
                    BusinessPageIntent.setAction(BusinessPageFragment.ACTION_RATE);
                    startActivity(BusinessPageIntent);
                }
            }
        });
        checkInDialogView.findViewById(R.id.x_icon_businss_dialog).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                chooseBusinessDialog.dismiss();
            }
        });


        chooseBusinessDialog.show();

    }


    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        MyApp.userManager.setIsIncognito(isChecked);
        MyApp.userManager.save();
        SwitchUtil.switchColor(isChecked,incognitoSwitch,this);
        if (!anonymous)
            MapActivity.updatAppUserEnabledLoc();

    }
    private void registerInBackground() {

        new AsyncTask<Void, Void, String>() {

            @Override
            protected String doInBackground(Void... params) {
                String msg = "";
                try {
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging.getInstance(MainActivity.this);
                    }
                    gcmString = gcm.register(SENDER_ID);
                    msg = "Device registered, registration ID=" + gcmString;

                    // Persist the regID - no need to register again.
                   MyApp.appManager.setGcm(gcmString);
                    MyApp.appManager.save();

                    //todo update user dcm task
                    updateAppUserGcm(gcmString);
//                    sendRegistrationIdToBackend();

                } catch (IOException ex) {
                    msg = "Error :" + ex.getMessage();
                    // If there is an error, don't just keep trying to register.
                    // Require the user to click a button again, or perform
                    // exponential back-off.
                }
                return msg;
            }
        }.execute();
    }

    private void updateAppUserGcm(String gcm) {
        if (!anonymous){
        if (MyApp.userManager.getAppuser()!=null) {
            if (!MyApp.userManager.getAppuser().getID().equals("")) {
                MyApp.networkManager.makeRequest(new UpdateAppuserGCMRequest(gcm), new NetworkCallback<UpdateAppuserGCMResponse>() {
                    @Override
                    public void onResponse(boolean success, String errorDesc, ResponseObject<UpdateAppuserGCMResponse> response) {
                        super.onResponse(success, errorDesc, response);
                        if (success) {
//                    ToastUtil.toaster("gcmm="+response.getData().getGCM(),false);
                        }
                    }
                });
            }
        }
        }
    }

}
