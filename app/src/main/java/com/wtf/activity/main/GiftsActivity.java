package com.wtf.activity.main;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.SwitchCompat;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.ViewAnimator;
import android.widget.ViewFlipper;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.wtf.activity.R;
import com.wtf.activity.adapters.FriendsListArrayAdapter;
import com.wtf.activity.adapters.GiftsGridArrayAdapter;
//import com.wtf.activity.dialogs.AppDialog;
import com.wtf.activity.database.tables.TableFriends;
import com.wtf.activity.fragments.BusinessPageFragment;
import com.wtf.activity.network.NetworkCallback;
import com.wtf.activity.network.requests.GetAppuserGiftsRequest;
import com.wtf.activity.network.requests.GetAppuserIdRequest;
import com.wtf.activity.network.requests.GetAppuserReceivedGiftsRequest;
import com.wtf.activity.network.requests.GetCouponRequest;
import com.wtf.activity.network.requests.GetRouletteCouponsRequest;
import com.wtf.activity.network.requests.SetCouponUsedRequest;
import com.wtf.activity.network.requests.SetGiftUsedRequest;
import com.wtf.activity.network.requests.TransferGiftRequest2;
import com.wtf.activity.network.response.GetAppuserGiftsResponse;
import com.wtf.activity.network.response.GetAppuserIdResponse;
import com.wtf.activity.network.response.GetCouponResponse;
import com.wtf.activity.network.response.GetRouletteCouponsResponse;
import com.wtf.activity.network.response.ResponseObject;
import com.wtf.activity.network.response.ResponseObject2;
import com.wtf.activity.network.response.SetCouponUsedResponse;
import com.wtf.activity.network.response.SetGiftUsedResponse;
import com.wtf.activity.network.response.TransferGiftResponse;
import com.wtf.activity.objects.Coupon;
import com.wtf.activity.objects.Friend;
import com.wtf.activity.objects.Gift;
import com.wtf.activity.ui.TwoWayAdapterView;
import com.wtf.activity.ui.TwoWayGridView;
import com.wtf.activity.util.Animation3D;
import com.wtf.activity.util.ImageLoader;
import com.wtf.activity.util.SwitchUtil;
import com.wtf.activity.util.TextUtil;
import com.wtf.activity.util.ToastUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class GiftsActivity extends BaseActivity implements CompoundButton.OnCheckedChangeListener {

    private ViewFlipper view_flipper1;
    private ViewFlipper view_flipper2;
    private ViewFlipper view_flipper3;
    private ImageView handleBarImageView;

    private List<ImageView> mImageViews1;
    private List<ImageView> mImageViews2;
    private List<ImageView> mImageViews3;

    private int mSpeed;
    private int mFactor = 10;
    private int mCount1 = new Random().nextInt(10) + 10;
    private int mCount2 = new Random().nextInt(10) + 10;
    private int mCount3 = new Random().nextInt(10) + 10;
    private ArrayList<Gift> gifts= new ArrayList<>();
    private ArrayList<Gift> receivedGifts= new ArrayList<>();
    private GiftsGridArrayAdapter giftsGridArrayAdapter;
    private TwoWayGridView myGiftsGrid;
    private GiftsGridArrayAdapter receivedGiftsGridArrayAdapter;
    private TwoWayGridView receivedGiftsGrid;

    private boolean isRuning;
    private TextView points_bar;
    private TextView noGiftsTv;
    public static DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private TextView incoTv;
    private SwitchCompat incognitoSwitch;
    private ImageView locationIconActionBar;
    private TextView noReceivedGiftsTv;
    private Tracker mTracker;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gifts);
//        setupActionBar(R.id.app_actionbar2, actionbarName);
        setupActionBar(R.id.app_actionbar, getString(R.string.app_name));
        setUpDrawerNavigation();
        InitAnalytic();
        findViews();
        setListeners();
        setDetails();
        setAdapters();
        getCoupons();
        getUserGifts();
        getUserReceivedGifts();

    }

    private void InitAnalytic(){
        MyApp application = (MyApp) getApplication();
        mTracker = application.getDefaultTracker();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mTracker.setScreenName("Gifts screen");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    private void setUpDrawerNavigation() {
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerToggle = new ActionBarDrawerToggle
                (this, mDrawerLayout, getActionbarToolbar(), 0, 0) {

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }
        };
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }
    private void getUserReceivedGifts() {

        MyApp.networkManager.makeRequest(new GetAppuserReceivedGiftsRequest(1),new NetworkCallback<GetAppuserGiftsResponse>(){
            @Override
            public void onResponse(boolean success, String errorDesc, ResponseObject<GetAppuserGiftsResponse> response) {
                super.onResponse(success, errorDesc, response);
                if (success){
                    if (response.getData().getGifts()!=null){
                        receivedGifts.clear();
                        for (int i=0; i<response.getData().getGifts().size();i++){
                            receivedGifts.add(response.getData().getGifts().get(i));

                        }
                        if (receivedGifts.size()>0) {
                            noReceivedGiftsTv.setVisibility(View.GONE);
                            receivedGiftsGrid.setVisibility(View.VISIBLE);
                        }else {
                            noReceivedGiftsTv.setVisibility(View.VISIBLE);
                            receivedGiftsGrid.setVisibility(View.GONE);
                        }
                        receivedGiftsGridArrayAdapter.notifyDataSetChanged();
                    }
                }
            }
        });



    }

    private void setAdapters() {
        giftsGridArrayAdapter=new GiftsGridArrayAdapter(this,gifts);
        myGiftsGrid.setAdapter(giftsGridArrayAdapter);
        myGiftsGrid.setOnItemClickListener(new TwoWayAdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(TwoWayAdapterView<?> parent, View view, int position, long id) {
                Gift gift = gifts.get(position);
                showGiftDialog(gift);
            }
        });

        receivedGiftsGridArrayAdapter=new GiftsGridArrayAdapter(this,receivedGifts);
        receivedGiftsGrid.setAdapter(receivedGiftsGridArrayAdapter);
        receivedGiftsGrid.setOnItemClickListener(new TwoWayAdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(TwoWayAdapterView<?> parent, View view, int position, long id) {
                Gift gift = receivedGifts.get(position);
                showGiftDialog(gift);
            }
        });

    }

    private void showGiftDialog(final Gift gift) {
        LayoutInflater factory = LayoutInflater.from(this);
        final View deleteDialogView = factory.inflate(
                R.layout.gift_click_dialog, null);
        final AlertDialog deleteDialog = new AlertDialog.Builder(this).create();
        deleteDialog.setView(deleteDialogView);
        deleteDialogView.findViewById(R.id.sendGiftB).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {


                //your business logic
                deleteDialog.dismiss();
                openFriendsChooserDialog(gift);
            }
        });
        deleteDialogView.findViewById(R.id.useGiftB).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                showGiftDialogFragment(gift);


                deleteDialog.dismiss();


            }
        });

        deleteDialog.show();


    }

    private void showGiftDialogFragment(final Gift gift) {

        LayoutInflater factory = LayoutInflater.from(this);
        final View couponDialogView = factory.inflate(
                R.layout.gift_alert_dialog, null);
        final AlertDialog giftDialog = new AlertDialog.Builder(this).create();
        giftDialog.setView(couponDialogView);
        //find views and set details:
        TextView couponTitle = (TextView) couponDialogView.findViewById(R.id.couponTitle);
        TextView couponKind = (TextView) couponDialogView.findViewById(R.id.couponKind);
        TextView couponDes = (TextView) couponDialogView.findViewById(R.id.couponDes);
        TextView couponSmallLetters = (TextView) couponDialogView.findViewById(R.id.couponSmallLetters);
        ImageView couponImageView = (ImageView)couponDialogView.findViewById(R.id.couponImageView);

        if (gift.getImage().isEmpty()||gift.getImage().equals("http://www.mypushserver.com/dev/bmm/"))
            couponImageView.setVisibility(View.GONE);
        else {
            ImageLoader.ByUrl(gift.getImage(),couponImageView);
        }

        couponTitle.setText(gift.getBusinessName());
        couponKind.setText(gift.getTitle());
        couponDes.setText(gift.getDescription());
        couponSmallLetters.setText(gift.getSmallLetters());
//        itemsLeftTv.setText(gift.getit);
//        Date date= new Date(coupon.getExpiration());
//        String justDate=  DateUtil.getDateAsStringByMilli(DateUtil.FORMAT_JUST_DATE,date.getTime());
//        exTv.setText(justDate);


        if (gift.getDescription().isEmpty())
            couponDes.setVisibility(View.GONE);
        if (gift.getSmallLetters().isEmpty())
            couponSmallLetters.setVisibility(View.GONE);

        final LinearLayout couponCodeLinear = (LinearLayout) couponDialogView.findViewById(R.id.couponCodeLinear);
        final EditText couponEditText = (EditText) couponDialogView.findViewById(R.id.couponEditText);
        couponDialogView.findViewById(R.id.insertButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtil.baseFieldsValidation(couponEditText)){
                    couponCodeLinear.setVisibility(View.GONE);
                    setGiftUsed(gift,couponEditText.getText().toString(),giftDialog);

                }
            }
        });

        couponDialogView.findViewById(R.id.getItButton).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                couponCodeLinear.setVisibility(View.VISIBLE);


            }
        });
        couponDialogView.findViewById(R.id.cancelButton).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                giftDialog.dismiss();

            }
        });

        giftDialog.show();
        dismissProgressDialog();


    }

    private void setGiftUsed(Gift gift, String pass, final AlertDialog deleteDialog) {
        MyApp.networkManager.makeRequest2(new SetGiftUsedRequest(gift.getGiftID(),pass),new NetworkCallback<SetGiftUsedResponse>(){
            @Override
            public void onResponse2(boolean success, String errorDesc, ResponseObject2<SetGiftUsedResponse> response) {
                super.onResponse2(success, errorDesc, response);
                if (success){
                    ToastUtil.toaster("Gift used",false);
                    deleteDialog.dismiss();
                    getUserGifts();
                    getUserReceivedGifts();
                }
            }
        });
    }

    private void setCouonUsed(Coupon coupon, String pass, final AlertDialog dialog) {
        MyApp.networkManager.makeRequest2(new SetCouponUsedRequest(MyApp.userManager.getAppuser().getID(),
                MyApp.appManager.getLatLng().latitude+"",coupon.getID(),MyApp.appManager.getLatLng().longitude+"",pass),new NetworkCallback<SetCouponUsedResponse>(){
            @Override
            public void onResponse2(boolean success, String errorDesc, ResponseObject2<SetCouponUsedResponse> response) {
                super.onResponse2(success, errorDesc, response);
                if (success){
                    ToastUtil.toaster("Coupon used",false);
                    dialog.dismiss();
                }

            }
        });
    }
    private void openFriendsChooserDialog(final Gift gift) {
        ArrayList<Friend> friends = new ArrayList<>();
        friends =MyApp.tableFriends.readAll(TableFriends.TAG_FIRST_NAME+" asc");
        FriendsListArrayAdapter friendsListArrayAdapter = new FriendsListArrayAdapter(this,friends);

        LayoutInflater factory = LayoutInflater.from(this);
        final View deleteDialogView = factory.inflate(
                R.layout.friends_list_choose_dialog, null);
        final AlertDialog deleteDialog = new AlertDialog.Builder(this).setNegativeButton("cancel", null).create();
        deleteDialog.setView(deleteDialogView);
        ListView listView= (ListView)deleteDialogView.findViewById(R.id.friends_list) ;
        listView.setAdapter(friendsListArrayAdapter);
        final ArrayList<Friend> finalFriends = friends;
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Friend f = finalFriends.get(position);
                getFriendID(f.getFBID(),gift);


                deleteDialog.dismiss();

            }
        });


        deleteDialog.show();


    }

    private void getFriendID(final String fbid, final Gift gift) {
        MyApp.networkManager.makeRequest(new GetAppuserIdRequest(fbid), new NetworkCallback<GetAppuserIdResponse>() {
            @Override
            public void onResponse(boolean success, String errorDesc, ResponseObject<GetAppuserIdResponse> response) {
                super.onResponse(success, errorDesc, response);
                if (success) {
                    if (response.getData().getAppuserID() != null) {
                        String id = response.getData().getAppuserID();
                        transferGift(id,gift);
                    }
                }


            }
        });
    }

    private void transferGift(String toID,Gift gift){
        MyApp.networkManager.makeRequest2(new TransferGiftRequest2(MyApp.userManager.getAppuser().getID(),toID,
                gift.getGiftID()),new NetworkCallback<TransferGiftResponse>(){
            @Override
            public void onResponse2(boolean success, String errorDesc, ResponseObject2<TransferGiftResponse> response) {
                super.onResponse2(success, errorDesc, response);
                ToastUtil.toaster(getString(R.string.gift_sent),false);
            }
        });
    }

    private void getUserGifts() {
        MyApp.networkManager.makeRequest(new GetAppuserGiftsRequest(1),new NetworkCallback<GetAppuserGiftsResponse>(){
            @Override
            public void onResponse(boolean success, String errorDesc, ResponseObject<GetAppuserGiftsResponse> response) {
                super.onResponse(success, errorDesc, response);
                if (success){
                    if (response.getData().getGifts()!=null){
                        gifts.clear();
                        for (int i=0; i<response.getData().getGifts().size();i++){
                            gifts.add(response.getData().getGifts().get(i));

                        }
                        if (gifts.size()>0) {
                            noGiftsTv.setVisibility(View.GONE);
                            myGiftsGrid.setVisibility(View.VISIBLE);
                        }else {
                            noGiftsTv.setVisibility(View.VISIBLE);
                            myGiftsGrid.setVisibility(View.GONE);
                        }
                        giftsGridArrayAdapter.notifyDataSetChanged();
                    }
                }
            }
        });
    }

    private void setDetails() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

            window.setStatusBarColor(this.getResources().getColor(R.color.wtf_dark_blue));
        }


        String actionbarName= getString(R.string.earned_points_)+": "+MyApp.userManager.getAppuser().getPoints();
        points_bar.setText(actionbarName);

        mImageViews1 = new ArrayList<>();
        mImageViews2 = new ArrayList<>();
        mImageViews3 = new ArrayList<>();
    }

    private void getCoupons() {
        MyApp.networkManager.makeRequest(new GetRouletteCouponsRequest(6),new NetworkCallback<GetRouletteCouponsResponse>(){
            @Override
            public void onResponse(boolean success, String errorDesc, ResponseObject<GetRouletteCouponsResponse> response) {
                super.onResponse(success, errorDesc, response);
                if (success){
                    List<Coupon> list = response.getData().getCoupons();
                    System.out.println("--->  list"+list.size()+list.get(0).getImage());
                    initList(mImageViews1, list);
                    initList(mImageViews2 , list);
                    initList(mImageViews3 , list);
                    initFlipper(view_flipper1, mImageViews1);
                    initFlipper(view_flipper2, mImageViews2);
                    initFlipper(view_flipper3, mImageViews3);
                }
            }
        });
    }

    private void findViews() {
        incoTv=(TextView)findViewById(R.id.incoTv);
        incognitoSwitch= (SwitchCompat) findViewById(R.id.incognitoSwitch);
        locationIconActionBar= (ImageView)findViewById(R.id.locationIconActionBar);
        incoTv.setVisibility(View.VISIBLE);
        incognitoSwitch.setVisibility(View.VISIBLE);
        locationIconActionBar.setVisibility(View.INVISIBLE);
        SwitchUtil.switchColor(MyApp.userManager.isIncognito(),incognitoSwitch,this);

        points_bar= (TextView)findViewById(R.id.points_bar);
        noGiftsTv = (TextView)findViewById(R.id.noGiftsTv);
        noReceivedGiftsTv = (TextView)findViewById(R.id.noReceivedGiftsTv);

        view_flipper1 = (ViewFlipper) findViewById(R.id.view_flipper1);
        view_flipper2 = (ViewFlipper) findViewById(R.id.view_flipper2);
        view_flipper3 = (ViewFlipper) findViewById(R.id.view_flipper3);
        handleBarImageView= (ImageView)findViewById(R.id.handleBarImageView);

        myGiftsGrid=(TwoWayGridView)findViewById(R.id.myGiftsGrid);
        receivedGiftsGrid=(TwoWayGridView)findViewById(R.id.receivedGiftsGrid);
        ImageView locationIconActionBar = (ImageView) findViewById(R.id.locationIconActionBar);
        locationIconActionBar.setVisibility(View.GONE);
    }

    private void setListeners() {
        handleBarImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isRuning)
                animateHandlebar(v);
            }
        });
        incognitoSwitch .setOnCheckedChangeListener(this);
    }

    private void animateHandlebar(View view) {
        Animation animation = new Animation3D(0, -180, 0,
                view.getHeight() - 14, 0, true);
        animation.setDuration(700);
        handleBarImageView.startAnimation(animation);
        startSlotAnimation();
    }
    private void startSlotAnimation() {

        mSpeed = 150;
        Handler h = new Handler();
        h.postDelayed(r, mSpeed);
    }

    private Runnable r = new Runnable() {

        @Override
        public void run() {
            isRuning=true;
            down(mCount1 , view_flipper1);
            mCount1--;
            down(mCount2 , view_flipper2);
            mCount2--;
            down(mCount3 , view_flipper3);
            mCount3--;

            if (mCount1 < 1 && mCount2 < 1 && mCount3 < 1 ) {
                mCount1 = new Random().nextInt(10) + 10;
                mCount2 = new Random().nextInt(10) + 10;
                mCount3 = new Random().nextInt(10) + 10;
                isRuning= false;
                return;
            } else {
                Handler h = new Handler();
                h.postDelayed(r, mSpeed);
            }
        }
    };

    private void down(int count , ViewAnimator fliper) {
        if(count == 0 ){
            return;
        }
        mSpeed += mFactor;
        Animation outToBottom = new TranslateAnimation(
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 1.0f);
        outToBottom.setInterpolator(new AccelerateInterpolator());
        outToBottom.setDuration(mSpeed);
        Animation inFromTop = new TranslateAnimation(
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, -1.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f);
        inFromTop.setInterpolator(new AccelerateInterpolator());
        inFromTop.setDuration(mSpeed);
        fliper.clearAnimation();
        fliper.setInAnimation(inFromTop);
        fliper.setOutAnimation(outToBottom);



        if (fliper.getDisplayedChild() == 0) {
            fliper.setDisplayedChild(mImageViews1.size() - 1);
        } else {
            fliper.showPrevious();
        }

    }

    private void initList(List<ImageView> list, final List<Coupon> coupons) {

        ImageView imageView = null;
        for (int i = 0; i < coupons.size(); i++) {

            try {

                final Coupon currentCoupon = coupons.get(i);

                imageView = new ImageView(this);
                imageView.setLayoutParams(new ViewGroup.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                imageView.setScaleType(ImageView.ScaleType.FIT_XY);

				/*Picasso.with(getActivity()).load(coupons.get(i).getImgUrl())
						.fit().into(imageView);*/
//                imageView.setImageUrl(coupons.get(i).getImgUrl());
//                ImageLoader.ByUrl(coupons.get(i).getImage(), imageView,R.drawable.trans);
                ImageLoader.ByUrl(coupons.get(i).getImage(), imageView,R.mipmap.ic_launcher);

                imageView.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        showProgressDialog();
                        // get the full coupon and show coupon fragment
                        MyApp.networkManager.makeRequest(new GetCouponRequest(currentCoupon.getID()),new NetworkCallback<GetCouponResponse>(){
                            @Override
                            public void onResponse(boolean success, String errorDesc, ResponseObject<GetCouponResponse> response) {
                                super.onResponse(success, errorDesc, response);
                                if (success){
                                    Coupon coupon = response.getData().getCoupon();
                                    if (coupon!=null) {
                                        showCouponDialog(coupon);

                                    }else {
                                        dismissProgressDialog();
                                    }
                                }
                            }
                        });
                    }
                });
                list.add(imageView);
            } catch (Exception e) {

            }
        }

        Collections.shuffle(list);

    }

    private void showCouponDialog(final Coupon coupon) {

            LayoutInflater factory = LayoutInflater.from(this);
            final View couponDialogView = factory.inflate(
                    R.layout.coupon_dialog, null);
            final AlertDialog deleteDialog = new AlertDialog.Builder(this).create();
            deleteDialog.setView(couponDialogView);
            //find views and set details:
             setCouponDialogDetails(coupon, couponDialogView,deleteDialog);
        final LinearLayout couponCodeLinear = (LinearLayout) couponDialogView.findViewById(R.id.couponCodeLinear);
        final EditText couponEditText = (EditText) couponDialogView.findViewById(R.id.couponEditText);

        couponEditText.setOnEditorActionListener(
                new EditText.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        if (actionId == EditorInfo.IME_ACTION_DONE) {
                            if (TextUtil.baseFieldsValidation(couponEditText)){
                                setCouonUsed(coupon,couponEditText.getText().toString(),deleteDialog);
                                couponCodeLinear.setVisibility(View.GONE);
                                return false;
                            }

                        }
                        return false;
                    }
                });
        couponDialogView.findViewById(R.id.insertButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtil.baseFieldsValidation(couponEditText)){
                    setCouonUsed(coupon,couponEditText.getText().toString(),deleteDialog);
                    couponCodeLinear.setVisibility(View.GONE);
                }
            }
        });



//        couponDialogView.findViewById(R.id.insertButton).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (TextUtil.baseFieldsValidation(couponEditText)){
//                    setCouonUsed(coupon,couponEditText.getText().toString());
//                    couponCodeLinear.setVisibility(View.GONE);
//                }
//            }
//        });

            couponDialogView.findViewById(R.id.getItButton).setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    couponCodeLinear.setVisibility(View.VISIBLE);


                }
            });
            couponDialogView.findViewById(R.id.cancelButton).setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    deleteDialog.dismiss();

                }
            });

            deleteDialog.show();
        dismissProgressDialog();


    }

    private void setCouponDialogDetails(final Coupon coupon, View couponDialogView, final AlertDialog deleteDialog) {
        TextView couponTitle = (TextView) couponDialogView.findViewById(R.id.couponTitle);
        TextView couponKind = (TextView) couponDialogView.findViewById(R.id.couponKind);
        TextView couponDes = (TextView) couponDialogView.findViewById(R.id.couponDes);
        TextView couponSmallLetters = (TextView) couponDialogView.findViewById(R.id.couponSmallLetters);
        TextView itemsLeftTv = (TextView) couponDialogView.findViewById(R.id.itemsLeftTv);
        TextView exTv = (TextView) couponDialogView.findViewById(R.id.exTv);
        ImageView couponImageView = (ImageView)couponDialogView.findViewById(R.id.couponImageView);

        if (coupon.getImage().isEmpty()||coupon.getImage().equals("http://www.mypushserver.com/dev/bmm/"))
            couponImageView.setVisibility(View.GONE);
        else {
            ImageLoader.ByUrl(coupon.getImage(),couponImageView);
        }

        couponTitle.setText(coupon.getBusinessName());
        couponKind.setText(coupon.getTitle());
        couponDes.setText(coupon.getDescription());
        couponSmallLetters.setText(coupon.getSmallLetters());
        itemsLeftTv.setText(coupon.getItemsLeft());
//        Date date= new Date(coupon.getExpiration());
//        String justDate=  DateUtil.getDateAsStringByMilli(DateUtil.FORMAT_JUST_DATE,date.getTime());
//        exTv.setText(justDate);
        exTv.setText(coupon.getExpiration());

        if (coupon.getDescription().isEmpty())
            couponDes.setVisibility(View.GONE);
        if (coupon.getSmallLetters().isEmpty())
            couponSmallLetters.setVisibility(View.GONE);


        //setListeners
        couponTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteDialog.dismiss();

                openBusinessActivity(coupon.getBusinessID());
            }
        });
        couponImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteDialog.dismiss();

                openBusinessActivity(coupon.getBusinessID());

            }
        });
    }

    private void openBusinessActivity(String businessID) {
        finish();
        Intent i = new Intent(GiftsActivity.this,BusinessActivity.class);
        i.putExtra("businessID",businessID);
        i.setAction(BusinessPageFragment.ACTION_GIFTS);
        startActivity(i);
    }


    private void initFlipper(ViewFlipper viewFlipper, List<ImageView> imageViews) {

        for (int i = 0; i < imageViews.size(); i++) {
            viewFlipper.addView(imageViews.get(i));
        }
    }

    @Override
    public void onBackPressed() {

        if (mDrawerLayout.isDrawerOpen(Gravity.LEFT)){
            mDrawerLayout.closeDrawer(Gravity.LEFT);
        }else {
            super.onBackPressed();
        }

    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        MyApp.userManager.setIsIncognito(isChecked);
        MyApp.userManager.save();
        SwitchUtil.switchColor(isChecked,incognitoSwitch,this);
        if (!MyApp.appManager.isAnonymous())
            MapActivity.updatAppUserEnabledLoc();
    }
}
