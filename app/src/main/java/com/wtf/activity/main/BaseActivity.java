package com.wtf.activity.main;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wtf.activity.R;
import com.wtf.activity.database.tables.TableObserver;
import com.wtf.activity.fragments.BaseFragment;
import com.wtf.activity.util.AppUtil;
import com.wtf.activity.util.ProgressDialogUtil;



public  class BaseActivity extends ActionBarActivity implements  TableObserver {

	protected Menu menu;
	protected Toolbar mActionBarToolbar = null;
	protected LinearLayout actionbarShadow;


	protected boolean activityIsOn;
	protected ProgressDialog progressDialog;
	protected BaseFragment currentFragment;



	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		activityIsOn = true;
		MyApp.addTablesObservers(this);

	}


	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		View rootView = getWindow().getDecorView().findViewById(android.R.id.content);
		AppUtil.setTextFonts(this, rootView);
	}

	@Override
	protected void onResume() {
		super.onResume();
		MyApp.currentActivity = this;
	}


	@Override
	protected void onDestroy() {
		super.onDestroy();
		activityIsOn = false;
		MyApp.removeTablesObservers(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		this.menu = menu;
		return true;
	}



	public void hideOption(int id)
	{
		MenuItem item = menu.findItem(id);
		item.setVisible(false);
	}

	public void showOption(int id)
	{
		MenuItem item = menu.findItem(id);
		item.setVisible(true);
	}

	public void setOptionTitle(int id, String title)
	{
		MenuItem item = menu.findItem(id);
		item.setTitle(title);
	}

	public void setOptionIcon(int id, int iconRes)
	{
		MenuItem item = menu.findItem(id);
		item.setIcon(iconRes);
	}



	public void setupActionBar(int toolbarId) {

		Toolbar toolbar = (Toolbar) findViewById(toolbarId);
		if (toolbar != null) {
			mActionBarToolbar = toolbar;
			setSupportActionBar(toolbar);
			getSupportActionBar().setDisplayShowTitleEnabled(false);
			getSupportActionBar().setDisplayHomeAsUpEnabled(true);
			getSupportActionBar().setHomeButtonEnabled(true);
		}

	}


	public void setupActionBar(int toolbarId,String title) {

		Toolbar toolbar = (Toolbar) findViewById(toolbarId);
		if (toolbar != null) {
			mActionBarToolbar = toolbar;
			setSupportActionBar(toolbar);
			getSupportActionBar().setDisplayShowTitleEnabled(false);
			getSupportActionBar().setDisplayHomeAsUpEnabled(true);
			getSupportActionBar().setHomeButtonEnabled(true);
			actionbarDisplayTitle(title);
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			Window window = this.getWindow();
			window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
			window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

				window.setStatusBarColor(this.getResources().getColor(R.color.wtf_dark_blue));
			}
		}

	}

	public Toolbar getActionbarToolbar(){
		return mActionBarToolbar;
	}

	public void actionbarDisplayTitle(String title) {
		if (title == null){
			actionbarSetActionbarImageVisibility(View.VISIBLE);
			try {
				getActionBarTextView().setText("");
			} catch (Exception e) {}
		}else{
			actionbarSetActionbarImageVisibility(View.GONE);
			try {
				getActionBarTextView().setText(title);
			} catch (Exception e) {}
		}

	}

	public void actionbarSetActionbarImageVisibility (int visible){
		findViewById(R.id.actionbar_image).setVisibility(visible);
	}

	public ImageView actionbarGetActionbarImage () {
		return (ImageView)findViewById(R.id.actionbar_image);
	}

	public void actionbarDisplayLogo() {
		View logo = findViewById(R.id.app_logo);
		getSupportActionBar().setDisplayShowTitleEnabled(false);
		logo.setVisibility(View.VISIBLE);
	}

	public TextView getActionBarTextView() {

		return (TextView) findViewById(R.id.app_logo);
	}

	public void actionbarHideWithAnimation() {
		int toolbarHeight = mActionBarToolbar.getHeight();
		mActionBarToolbar.animate().translationY(toolbarHeight * (-1))
				.setDuration(300).start();
	}

	public void actionbarShowWithAnimation() {
		mActionBarToolbar.animate().translationY(0).setDuration(300).start();
	}



	public void actionbarAddView(View v) {
		LinearLayout linearActionbarView = (LinearLayout)
				findViewById(R.id.linearToolbarContainer);
		linearActionbarView.removeAllViews();
		linearActionbarView.addView(v);
	}


	public void actionbarRemoveView(View v) {
		LinearLayout linearActionbarView = (LinearLayout) findViewById(R.id.linearToolbarContainer);
		linearActionbarView.removeView(v);
	}

	public void actionbarRemoveAllViews() {
		LinearLayout linearActionbarView = (LinearLayout) findViewById(R.id.linearToolbarContainer);
		linearActionbarView.removeAllViews();
	}

	public void actionbarAddView(int layoutRecource) {
		LayoutInflater inflater = LayoutInflater.from(this);
		LinearLayout linearActionbarView = (LinearLayout) mActionBarToolbar.findViewById(R.id.linearToolbarContainer);
		View viewToAdd = inflater.inflate(layoutRecource, linearActionbarView, false);

		linearActionbarView.addView(viewToAdd);
	}


	public void actionbarSetAlpha(float alpha){
		actionbarShadow = (LinearLayout)findViewById(R.id.actionbar_shadow);
		try {
			if (alpha < 1){
				actionbarShadow.setVisibility(View.GONE);
			}else{
				actionbarShadow.setVisibility(View.VISIBLE);
			}
		} catch (Exception e) {}

		int originalColor = getResources().getColor(R.color.app_gray);
		int bgWithAlpha = getColorWithAlpha(originalColor, alpha);

		mActionBarToolbar.setBackgroundColor(bgWithAlpha);
	}

	public static int getColorWithAlpha( int baseColor, float alpha) {
		int a = Math.min(255, Math.max(0, (int) (alpha * 255))) << 24;
		int rgb = 0x00ffffff & baseColor;
		return a + rgb;
	}

	public int adjustAlpha(int color, float factor) {
		int alpha = Math.round(Color.alpha(color) * factor);
		int red = Color.red(color);
		int green = Color.green(color);
		int blue = Color.blue(color);
		return Color.argb(alpha, red, green, blue);
	}




	public void showProgressDialog(String message) {
		this.progressDialog =
				ProgressDialogUtil.showProgressDialog(this, message);
	}

	public void showProgressDialog() {
		try {
			this.progressDialog =
					ProgressDialogUtil.showProgressDialog(this,
							getString(R.string.deafult_dialog_messgae));


		} catch (Exception e) {}
	}

	public void dismissProgressDialog() {
		ProgressDialogUtil.dismisDialog(this.progressDialog);
	}

	public void setCurrentFragment (BaseFragment currentFragment){
		this.currentFragment = currentFragment;
	}

	public BaseFragment getCurrentFragment (){
		return this.currentFragment;
	}


	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		if (currentFragment != null){
			currentFragment.onNewIntent(intent);
		}
	}

	@Override
	public void onTableChanged(String tableName, int action) {

	}

}
