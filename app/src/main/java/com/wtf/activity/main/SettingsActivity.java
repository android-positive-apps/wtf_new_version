package com.wtf.activity.main;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.jakewharton.processphoenix.ProcessPhoenix;
import com.sromku.simple.fb.SimpleFacebook;
import com.sromku.simple.fb.listeners.OnLogoutListener;
import com.wtf.activity.R;
import com.wtf.activity.network.NetworkCallback;
import com.wtf.activity.network.requests.UpdateAppuserRadiusRequest;
import com.wtf.activity.network.response.ResponseObject2;
import com.wtf.activity.network.response.UpdateAppuserRadiusResponse;
import com.wtf.activity.objects.Appuser;
import com.wtf.activity.util.SwitchUtil;
import com.wtf.activity.util.ToastUtil;

import java.io.File;

public class SettingsActivity extends BaseActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener, TextWatcher {

    private TextView backSettings;
    private ImageView leftArrowSettings;
    private SwitchCompat settingsSwitch;
    private TextView logOutTextViewButton;
    private ImageView radiusMinusSettings;
    private ImageView radiusPlusSettings;

    private EditText radiusEditTextSettings;
    private LinearLayout aboutTheAppLinear;
    private LinearLayout privacyPolicyLinear;
    private LinearLayout TermsLinear;
    private SimpleFacebook mSimpleFacebook;
    private TextView facebookUnderText;
    private Tracker mTracker;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        InitAnalytic();
        findViews();
        setListeners();
        settingsSwitch.setChecked(MyApp.userManager.isIncognito());
        SwitchUtil.switchColor(MyApp.userManager.isIncognito(),settingsSwitch,this);

    }

    private void InitAnalytic(){
        MyApp application = (MyApp) getApplication();
        mTracker = application.getDefaultTracker();
    }


    @Override
    public void onResume() {
        super.onResume();
        mTracker.setScreenName("Settings screen");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        mSimpleFacebook = SimpleFacebook.getInstance(this);
        if (!MyApp.userManager.isLoggedIn()&&MyApp.appManager.isAnonymous()){
            logOutTextViewButton.setVisibility(View.GONE);
            facebookUnderText.setVisibility(View.GONE);
        }else {
            logOutTextViewButton.setVisibility(View.VISIBLE);
            facebookUnderText.setVisibility(View.VISIBLE);
        }
        if (!MyApp.appManager.isAnonymous()) {
            String radius = MyApp.userManager.getAppuser().getRadius();

            radiusEditTextSettings.setText(radius);
        }else {
                radiusEditTextSettings.setText(MyApp.userManager.getRadius());
            }


    }


    private void findViews() {
        backSettings         = (TextView) findViewById(R.id.backSettings);
        leftArrowSettings = (ImageView) findViewById(R.id.leftArrowSettings);

        settingsSwitch = (SwitchCompat) findViewById(R.id.settingsSwitch);
        radiusMinusSettings = (ImageView) findViewById(R.id.radiusMinusSettings);
        radiusPlusSettings = (ImageView) findViewById(R.id.radiusPlusSettings);
        radiusEditTextSettings = (EditText) findViewById(R.id.radiusEditTextSettings);

        aboutTheAppLinear = (LinearLayout) findViewById(R.id.aboutTheAppLinear);
        TermsLinear = (LinearLayout) findViewById(R.id.TermsLinear);
        privacyPolicyLinear = (LinearLayout) findViewById(R.id.privacyPolicyLinear);

        logOutTextViewButton = (TextView) findViewById(R.id.logOutTextViewButton);
        facebookUnderText= (TextView) findViewById(R.id.facebookUnderText);
    }

    private void setListeners() {
        backSettings.setOnClickListener(this);
        leftArrowSettings.setOnClickListener(this);
        radiusMinusSettings.setOnClickListener(this);
        radiusPlusSettings .setOnClickListener(this);
        aboutTheAppLinear .setOnClickListener(this);
        TermsLinear .setOnClickListener(this);
        privacyPolicyLinear .setOnClickListener(this);
        logOutTextViewButton .setOnClickListener(this);

        settingsSwitch.setOnCheckedChangeListener(this);
        radiusEditTextSettings.addTextChangedListener(this);
        radiusEditTextSettings.setFilters(new InputFilter[]{new InputFilterMinMax("1", "1000000")});
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.backSettings:
                finish();
                break;
            case R.id.leftArrowSettings:
                finish();
                break;
            case R.id.radiusMinusSettings:
                int num = Integer.parseInt(radiusEditTextSettings.getText().toString())-1;
                        radiusEditTextSettings.setText(num+"");
                break;
            case R.id.radiusPlusSettings:
                int num2 = Integer.parseInt(radiusEditTextSettings.getText().toString())+1;
                radiusEditTextSettings.setText(num2+"");
                break;
            case R.id.aboutTheAppLinear:
                showWebViewDialog(getResources().getString(R.string.about_the_app_text),getResources().getString(R.string.about_app_gowayu));
                break;
            case R.id.TermsLinear:
                showWebViewDialog(getResources().getString(R.string.terms_of_use),getResources().getString(R.string.terms_of_use_gowayu));

                break;
            case R.id.privacyPolicyLinear:
                showWebViewDialog(getResources().getString(R.string.privacy_policy),getResources().getString(R.string.privacy_policy_gowayu));

                break;
            case R.id.logOutTextViewButton:
                performLogOutFromFb();

                break;
        }

    }

    private  void showWebViewDialog(String title,String link){
        final AlertDialog.Builder alert;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            alert = new AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            alert = new AlertDialog.Builder(this);
        }
        alert.setTitle(title);

        WebView wv = new WebView(this);
        wv.loadUrl(link);

        wv.setWebViewClient(new WebViewClient() {


            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);

                return true;
            }
        });

        alert.setView(wv);
        alert.setNegativeButton(getResources().getString(R.string.close), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        alert.show();

    }

    private void performLogOutFromFb() {
        showLogOutDialog();

    }

    private void showLogOutDialog() {
        LayoutInflater factory = LayoutInflater.from(this);
        final View deleteDialogView = factory.inflate(
                R.layout.location_dialog, null);
        final AlertDialog deleteDialog = new AlertDialog.Builder(this).create();
        deleteDialog.setView(deleteDialogView);
        TextView textView2= (TextView)deleteDialogView.findViewById(R.id.textView2);
        textView2.setText(getResources().getString(R.string.logout_dialog_text));
        Button cancel = (Button) deleteDialogView.findViewById(R.id.openSettingsB);
        cancel.setText(getResources().getString(R.string.cancel));
        cancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                deleteDialog.dismiss();
            }
        });
        Button ok = (Button)deleteDialogView.findViewById(R.id.okB);
        ok.setText(getResources().getString(R.string.log_out));
        ok.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                OnLogoutListener onLogoutListener = new OnLogoutListener() {

                    @Override
                    public void onLogout() {
                        System.out.println("log out success");
                        MyApp.userManager.setLoggedIn(false);
                        MyApp.appManager.setAnonymous(true);
                        MyApp.userManager.setFBaccessToken("");
                        MyApp.userManager.setAppuser(new Appuser("","16"));
                        MyApp.userManager.save();
                        MyApp.tableFriends.deleteAll();
//                        clearApplicationData();
                        ProcessPhoenix.triggerRebirth(SettingsActivity.this);
//                        Intent intent = new Intent(SettingsActivity.this, LoginActivity.class);
//                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                        startActivity(intent);
//                        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(SettingsActivity.this);
//                        preferences.edit().clear().apply();
//                        SharedPreferences.Editor.clear().apply();
//                        MyApp.userManager.setFBaccessToken("");
//                        MyApp.userManager.setFbFriendsIDs(null);
//                        MyApp.userManager.set

//                getActivity().finish();

//                        Intent mStartActivity = new Intent(SettingsActivity.this,  LoginActivity.class);
//                        int mPendingIntentId = 123456;
//                        PendingIntent mPendingIntent = PendingIntent.getActivity(SettingsActivity.this, mPendingIntentId,    mStartActivity, PendingIntent.FLAG_CANCEL_CURRENT);
//                        AlarmManager mgr = (AlarmManager)SettingsActivity.this.getSystemService(Context.ALARM_SERVICE);
//                        mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 100, mPendingIntent);
//                        System.exit(0);

                    }

                };
                mSimpleFacebook.logout(onLogoutListener);

                deleteDialog.dismiss();

            }
        });

        deleteDialog.show();

    }
    public void clearApplicationData() {
        File cache = this.getCacheDir();
        File appDir = new File(cache.getParent());
        if (appDir.exists()) {
            String[] children = appDir.list();
            for (String s : children) {
                if (!s.equals("lib")) {
                    deleteDir(new File(appDir, s));
                    Log.i("TAG", "**************** File /data/data/APP_PACKAGE/" + s + " DELETED *******************");
                }
            }
        }
    }
    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }

        return dir.delete();
    }
    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            MyApp.userManager.setIsIncognito(isChecked);
        MyApp.userManager.save();
        SwitchUtil.switchColor(isChecked,settingsSwitch,SettingsActivity.this);
        if (!MyApp.appManager.isAnonymous()){
                MapActivity.updatAppUserEnabledLoc();
        }

    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (!s.toString().isEmpty()) {
            MyApp.userManager.setRadius(s.toString());
            if (!MyApp.appManager.isAnonymous()) {
                MyApp.userManager.getAppuser().setRadius(s.toString());
            }
            MyApp.userManager.save();
            updateUserRadius(s.toString());
        }else {
            radiusEditTextSettings.setText("1");
        }
    }

    @Override
    public void afterTextChanged(Editable s) {
    }

    private void updateUserRadius(final String s) {
        int radius = -1;
        try {
            radius = Integer.parseInt(s);

        } catch (NumberFormatException e) {
            e.printStackTrace();
            radius = -1;
        }
        if (radius>0&&!MyApp.appManager.isAnonymous()) {
            MyApp.networkManager.makeRequest2(new UpdateAppuserRadiusRequest(radius), new NetworkCallback<UpdateAppuserRadiusResponse>() {

                @Override
                public void onResponse2(boolean success, String errorDesc, ResponseObject2<UpdateAppuserRadiusResponse> response) {
                    super.onResponse2(success, errorDesc, response);
                    if (!success) {
                        ToastUtil.toaster(response.getErrorDesc(), true);
                        MyApp.userManager.getAppuser().setRadius(s);
                        MyApp.userManager.save();
                    }
                }
            });
        }
    }

    public class InputFilterMinMax implements InputFilter {

        private int min, max;

        public InputFilterMinMax(int min, int max) {
            this.min = min;
            this.max = max;
        }

        public InputFilterMinMax(String min, String max) {
            this.min = Integer.parseInt(min);
            this.max = Integer.parseInt(max);
        }

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            try {
                // Remove the string out of destination that is to be replaced
                String newVal = dest.toString().substring(0, dstart) + dest.toString().substring(dend, dest.toString().length());
                // Add the new string in
                newVal = newVal.substring(0, dstart) + source.toString() + newVal.substring(dstart, newVal.length());
                int input = Integer.parseInt(newVal);
                if (isInRange(min, max, input))
                    return null;
            } catch (NumberFormatException nfe) { }
            return "";
        }

        private boolean isInRange(int a, int b, int c) {
            return b > a ? c >= a && c <= b : c >= b && c <= a;
        }
    }

}
