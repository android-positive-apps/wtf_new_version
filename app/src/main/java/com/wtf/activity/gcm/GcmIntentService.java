/**
 * 
 */
package com.wtf.activity.gcm;

import java.util.Set;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;



/**
 * @author Nati Application
 *
 */
public class GcmIntentService extends IntentService {
	
	
	public static final int NOTIFICATION_ID = 1;
	
	public static final String TAG = "pushNotificationTag";
	
	

	
	
	NotificationCompat.Builder builder;
	
	

	public GcmIntentService() {
		super("GcmIntentService");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		Bundle extras = intent.getExtras();
		GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
		String messageType = gcm.getMessageType(intent);
		
		Log.e(GcmIntentService.TAG,"**********************");
		Log.e(GcmIntentService.TAG,"new push notification");
		
		if (extras != null){
			handelNotification(extras);
		}else{
			Log.i(GcmIntentService.TAG,"no extras");
		}
		
		// Release the wake lock provided by the WakefulBroadcastReceiver.
		GcmBroadcastReceiver.completeWakefulIntent(intent);
	}

	private void handelNotification (Bundle extras){
		
		Log.d(GcmIntentService.TAG,"Extras = \n" + describePushContent(extras));
		
		int pushType = dettectPushType (extras);
		
		Log.d(GcmIntentService.TAG,"Code = " + pushType );

		Log.e(GcmIntentService.TAG,"**********************");
		
	}

	/**
	 * @param extras
	 * @return
	 */
	private int dettectPushType(Bundle extras) {
		String code = extras.getString("code");
		try {
			return Integer.parseInt(code);
		} catch (Exception e) {
			int intCode = extras.getInt("code");
			return intCode;
		}
	}
	
	private String describePushContent(Bundle extras) {
		StringBuilder builder = new StringBuilder();

		Set<String> keys = extras.keySet();
		for (String key : keys) {
			builder.append(key + " --- " + extras.get(key));
			builder.append("\n");
			
		}
		return builder.toString();
	}

	

}
