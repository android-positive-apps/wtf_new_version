package com.wtf.activity.network.requests;

import com.wtf.activity.main.MyApp;
import com.wtf.activity.network.ApiInterface;
import com.wtf.activity.network.response.ResponseObject;
import com.wtf.activity.network.response.UpdateAppuserGCMResponse;

import retrofit.Callback;

/**
 * Created by Niv on 3/6/2016.
 */
public class UpdateAppuserGCMRequest extends RequestObject<UpdateAppuserGCMResponse> {

    private String id= MyApp.userManager.getAppuser().getID();
    private String GCM;

    public UpdateAppuserGCMRequest(String GCM) {
        this.GCM = GCM;
    }

    @Override
    protected void execute(ApiInterface apiInterface, Callback<ResponseObject<UpdateAppuserGCMResponse>> callback) {
        apiInterface.updateAppuserGCM(id,GCM,callback);
    }

    @Override
    protected boolean showDefaultToastsOnError() {
        return true;
    }
}
