package com.wtf.activity.network.requests;

import com.wtf.activity.main.MyApp;
import com.wtf.activity.network.ApiInterface;
import com.wtf.activity.network.response.ApplicationLoginResponse;
import com.wtf.activity.network.response.AppuserCheckInResponse;
import com.wtf.activity.network.response.ResponseObject;
import com.wtf.activity.network.response.ResponseObject2;
import com.wtf.activity.util.DeviceUtil;

import retrofit.Callback;

/**
 * Created by Niv on 3/3/2016.
 */
public class AppuserCheckInRequest extends RequestObject<AppuserCheckInResponse> {

    private String AppuserID= MyApp.userManager.getAppuser().getID();
    private String BusinessID;
    private String Lat = String.valueOf(MyApp.appManager.getLatLng().latitude);
    private String Lng = String.valueOf(MyApp.appManager.getLatLng().longitude);

    public AppuserCheckInRequest(String businessID) {
        BusinessID = businessID;
    }

    @Override
    protected void execute(ApiInterface apiInterface, Callback<ResponseObject<AppuserCheckInResponse>> callback) {
        apiInterface.appuserCheckIn(AppuserID,BusinessID,Lat,Lng,callback);
    }

    @Override
    protected boolean showDefaultToastsOnError() {
        return false;
    }
}
