package com.wtf.activity.network.requests;

import com.wtf.activity.main.MyApp;
import com.wtf.activity.network.ApiInterface;
import com.wtf.activity.network.response.GetCouponResponse;
import com.wtf.activity.network.response.ResponseObject;

import retrofit.Callback;

/**
 * Created by Niv on 3/7/2016.
 */
public class GetCouponRequest extends RequestObject<GetCouponResponse> {

    private String AppuserID = MyApp.userManager.getAppuser().getID();
    private String CouponID;

    public GetCouponRequest(String couponID) {
        CouponID = couponID;
    }

    @Override
    protected void execute(ApiInterface apiInterface, Callback<ResponseObject<GetCouponResponse>> callback) {
        apiInterface.getCoupon(AppuserID,CouponID,callback);
    }

    @Override
    protected boolean showDefaultToastsOnError() {
        return true;
    }
}
