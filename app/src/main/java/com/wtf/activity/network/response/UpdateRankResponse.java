package com.wtf.activity.network.response;

/**
 * Created by Niv on 3/23/2016.
 */
public class UpdateRankResponse {
    private String GiftPoints="";
    private String TotalPoints="";

    public String getGiftPoints() {
        return GiftPoints;
    }

    public String getTotalPoints() {
        return TotalPoints;
    }
}
