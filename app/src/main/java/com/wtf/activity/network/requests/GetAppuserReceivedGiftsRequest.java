package com.wtf.activity.network.requests;

import com.wtf.activity.main.MyApp;
import com.wtf.activity.network.ApiInterface;
import com.wtf.activity.network.response.GetAppuserGiftsResponse;
import com.wtf.activity.network.response.ResponseObject;

import retrofit.Callback;

/**
 * Created by Niv on 3/6/2016.
 */
public class GetAppuserReceivedGiftsRequest extends RequestObject<GetAppuserGiftsResponse> {


    private String AppuserId= MyApp.userManager.getAppuser().getID();
    private int Unused;
    //int bool 0/1 , 1= unused gifts only


    public GetAppuserReceivedGiftsRequest( int unused) {
        Unused = unused;
    }

    @Override
    protected void execute(ApiInterface apiInterface, Callback<ResponseObject<GetAppuserGiftsResponse>> callback) {
        apiInterface.getAppuserReceivedGifts(AppuserId,Unused,callback);
    }

    @Override
    protected boolean showDefaultToastsOnError() {
        return true;
    }
}