package com.wtf.activity.network.requests;

import com.wtf.activity.main.MyApp;
import com.wtf.activity.network.ApiInterface;
import com.wtf.activity.network.response.ResponseObject;
import com.wtf.activity.network.response.ApplicationLoginResponse;
import com.wtf.activity.util.DeviceUtil;

import retrofit.Callback;

/**
 * Created by Niv on 3/2/2016.
 */
public class ApplicationLoginRequest extends RequestObject<ApplicationLoginResponse>{

    private String FacebookID;
    private String UDID= DeviceUtil.getDeviceUDID();
    private String Lat;
    private String Lng;

    public ApplicationLoginRequest(String facebookID) {
//        FacebookID = "["+facebookID+"]";
        FacebookID = facebookID;
//        this.UDID = UDID;
//        Lat = lat;
//        Lng = lng;
        try {
            Lat = MyApp.appManager.getLatLng().latitude+"";
            Lng = MyApp.appManager.getLatLng().longitude+"";
        } catch (Exception e) {
            e.printStackTrace();
            Lat ="";
            Lng ="";
        }
    }

    @Override
    protected void execute(ApiInterface apiInterface, Callback<ResponseObject<ApplicationLoginResponse>> callback) {
        apiInterface.applicationLogin(FacebookID, UDID, Lat, Lng, callback);
    }

    @Override
    protected boolean showDefaultToastsOnError() {
        return false;
    }
}
