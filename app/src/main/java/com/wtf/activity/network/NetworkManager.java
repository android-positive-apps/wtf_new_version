package com.wtf.activity.network;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;


import com.wtf.activity.main.MyApp;
import com.wtf.activity.network.requests.RequestObject;
import com.wtf.activity.network.requests.RequestObject2;

import retrofit.RestAdapter;


/**
 * Created by natiapplications on 16/08/15.
 */
public class NetworkManager {

    public static final String BASE_URL = "http://*";

    public static final int REMOTE_CONT_TYPE_Gateway = 0;
    public static final int REMOTE_CONT_TYPE_CLOUD = 1;


    private Context context;
    private RestAdapter restAdapter;
    private ApiInterface apiInterface;
    // connectivity manager to check network state before send the requests
    private ConnectivityManager connectivityManager;

    private NetworkManager(Context context) {
        this.context = context;
    }

    private static NetworkManager instance;

    public static NetworkManager getInstance (Context context) {
        if (instance == null){
            instance = new NetworkManager(context);
        }
        instance.initRestAdapter();
        return instance;
    }

    public ApiInterface initRestAdapter (){

       /* OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(15, TimeUnit.SECONDS); // connect timeout
        client.setReadTimeout(15, TimeUnit.SECONDS);
        OkClient okClient = new OkClient(client);*/



        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(BASE_URL.replace("*","www.mypushserver.com/dev/bmm"))
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();

        apiInterface = restAdapter.create(ApiInterface.class);

        return  apiInterface;

    }


    public void makeRequest (RequestObject request,NetworkCallback<?> callback){
        if (request != null){
            request.request(this.apiInterface,callback);
        }
    }

    public void makeRequest2 (RequestObject2 request2,NetworkCallback<?> callback2){
        if (request2 != null){
            request2.request(this.apiInterface,callback2);
        }
    }





    public static boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager =
                (ConnectivityManager) MyApp.appContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        } else {
            return false;
        }

    }

    public static boolean isWifiAvailable () {
        ConnectivityManager connManager = (ConnectivityManager)MyApp.appContext.
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        if (mWifi.isConnected()) {
            return true;
        }
        return false;
    }









}
