package com.wtf.activity.network.requests;

import android.content.Context;

import com.wtf.activity.main.MyApp;
import com.wtf.activity.network.ApiInterface;
import com.wtf.activity.network.response.GetAppuserFriendsLocationResponse;
import com.wtf.activity.network.response.ResponseObject;
import com.wtf.activity.network.response.UpdateAppuserResponse;
import com.wtf.activity.util.ContentProviderUtil;
import com.wtf.activity.util.DeviceUtil;

import retrofit.Callback;

/**
 * Created by Niv on 3/6/2016.
 */
public class UpdateAppuserRequest extends RequestObject<UpdateAppuserResponse> {


//    private String ID= MyApp.userManager.getAppuser().getID();
    private String ID;
    private String UDID= DeviceUtil.getDeviceUDID();
    private String FirstName="";
    private String LastName="";
    private String isLocationEnabled="1";
    private String FBid;
    private String email="";
    private String fbAvatar="";

    public UpdateAppuserRequest(String Id, String firstName, String lastName, String FBid, String email, String fbAvatar,Context context) {
        FirstName = firstName;
        LastName = lastName;
        this.FBid = FBid;
//        this.email = email;
        this.fbAvatar = fbAvatar;
        isLocationEnabled = MyApp.userManager.isIncognito()?"0":"1";
        ID= Id;
    }

    //    public UpdateAppuserRequest(String firstName, String lastName,String FBid,) {
//        FirstName = firstName;
//        LastName = lastName;
////        isLocationEnabled = !ContentProviderUtil.isGPSEnebled(context)?"1":"0";
//        this.FBid=FBid;
//    }

    @Override
    protected void execute(ApiInterface apiInterface, Callback<ResponseObject<UpdateAppuserResponse>> callback) {
        apiInterface.updateAppuser(ID,FirstName,LastName,FBid,UDID,email,fbAvatar,isLocationEnabled,callback);
    }

    @Override
    protected boolean showDefaultToastsOnError() {
        return false;
    }
}
