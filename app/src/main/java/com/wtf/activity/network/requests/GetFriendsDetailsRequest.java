package com.wtf.activity.network.requests;

import com.wtf.activity.main.MyApp;
import com.wtf.activity.network.ApiInterface;
import com.wtf.activity.network.response.GetFriendsDetailsResponse;
import com.wtf.activity.network.response.ResponseObject;

import retrofit.Callback;

/**
 * Created by Niv on 3/27/2016.
 */
public class GetFriendsDetailsRequest extends RequestObject<GetFriendsDetailsResponse> {

    private String AppuserID;

    public GetFriendsDetailsRequest(String appuserID) {
        AppuserID = appuserID;
    }

    @Override
    protected void execute(ApiInterface apiInterface, Callback<ResponseObject<GetFriendsDetailsResponse>> callback) {
        apiInterface.getFriendsDetails(AppuserID,callback);
    }

    @Override
    protected boolean showDefaultToastsOnError() {
        return true;
    }
}
