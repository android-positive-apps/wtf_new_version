package com.wtf.activity.network.response;

/**
 * Created by Niv on 3/27/2016.
 */
public class GetStatisticsResponse {
    private long UsersAround;
    private long CheckIns;
    private long RatedPlaces;

    public long getUsersAround() {
        return UsersAround;
    }

    public long getCheckIns() {
        return CheckIns;
    }

    public long getRatedPlaces() {
        return RatedPlaces;
    }
}
