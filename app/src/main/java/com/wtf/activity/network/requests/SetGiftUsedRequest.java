package com.wtf.activity.network.requests;

import com.wtf.activity.main.MyApp;
import com.wtf.activity.network.ApiInterface;
import com.wtf.activity.network.response.ResponseObject;
import com.wtf.activity.network.response.ResponseObject2;
import com.wtf.activity.network.response.SetCouponUsedResponse;
import com.wtf.activity.network.response.SetGiftUsedResponse;

import retrofit.Callback;

/**
 * Created by Niv on 4/14/2016.
 */
public class SetGiftUsedRequest extends RequestObject2<SetGiftUsedResponse> {

    private String AppuserID= MyApp.userManager.getAppuser().getID();
    private String Lat= MyApp.appManager.getLatLng().latitude+"";
    private String GiftID;
    private String Lng =MyApp.appManager.getLatLng().longitude+"";
    private String Password;

    public SetGiftUsedRequest( String giftID, String password) {
        GiftID = giftID;
        Password = password;
    }

    @Override
    protected void execute(ApiInterface apiInterface, Callback<ResponseObject2<SetGiftUsedResponse>> callback) {
        apiInterface.setGiftUsed(AppuserID,GiftID,Lat,Lng,Password,callback);
    }

    @Override
    protected boolean showDefaultToastsOnError() {
        return true;
    }
}
