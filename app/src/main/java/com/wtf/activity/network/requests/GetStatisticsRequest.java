package com.wtf.activity.network.requests;

import com.wtf.activity.network.ApiInterface;
import com.wtf.activity.network.response.GetStatisticsResponse;
import com.wtf.activity.network.response.ResponseObject;

import retrofit.Callback;

/**
 * Created by Niv on 3/27/2016.
 */
public class GetStatisticsRequest extends RequestObject<GetStatisticsResponse> {
    public GetStatisticsRequest() {
    }

    @Override
    protected void execute(ApiInterface apiInterface, Callback<ResponseObject<GetStatisticsResponse>> callback) {
        apiInterface.getStatistics(callback);
    }

    @Override
    protected boolean showDefaultToastsOnError() {
        return true;
    }
}
