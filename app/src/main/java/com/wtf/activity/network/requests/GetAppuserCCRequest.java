package com.wtf.activity.network.requests;

import com.wtf.activity.main.MyApp;
import com.wtf.activity.network.ApiInterface;
import com.wtf.activity.network.response.GetAppuserCCResponse;
import com.wtf.activity.network.response.ResponseObject;

import retrofit.Callback;

/**
 * Created by Niv on 3/7/2016.
 */
public class GetAppuserCCRequest extends RequestObject<GetAppuserCCResponse> {
    private String AppuserID = MyApp.userManager.getAppuser().getID();

    public GetAppuserCCRequest() {
    }

    @Override
    protected void execute(ApiInterface apiInterface, Callback<ResponseObject<GetAppuserCCResponse>> callback) {
        apiInterface.getAppuserCC(AppuserID,callback);
    }

    @Override
    protected boolean showDefaultToastsOnError() {
        return false;
    }
}
