package com.wtf.activity.network.requests;

import com.wtf.activity.network.ApiInterface;
import com.wtf.activity.network.response.GetBusinessesByCategoryV2Response;
import com.wtf.activity.network.response.ResponseObject;

import retrofit.Callback;

/**
 * Created by Niv on 3/28/2016.
 */
public class GetBusinessesByCategoryV2Request extends RequestObject<GetBusinessesByCategoryV2Response> {

    private String Category;
    private String Lat;
    private String Lng;
    private int Radius;
    private int numResults;
    private String sort;

    public GetBusinessesByCategoryV2Request(String category, String lat, String lng, int radius, int numResults, String sort) {
        Category = category;
        Lat = lat;
        Lng = lng;
        Radius = radius;
        this.numResults = numResults;
        this.sort = sort;
    }

    @Override
    protected void execute(ApiInterface apiInterface, Callback<ResponseObject<GetBusinessesByCategoryV2Response>> callback) {
        apiInterface.getBusinessesByCategoryV2(Category,Lat,Lng,Radius,numResults,sort,callback);
    }

    @Override
    protected boolean showDefaultToastsOnError() {
        return true;
    }
}
