package com.wtf.activity.network.response;

/**
 * Created by Niv on 3/6/2016.
 */
public class GetAppuserGCMResponse {
    private String GCM;

    public String getGCM() {
        return GCM;
    }

    public void setGCM(String GCM) {
        this.GCM = GCM;
    }
}
