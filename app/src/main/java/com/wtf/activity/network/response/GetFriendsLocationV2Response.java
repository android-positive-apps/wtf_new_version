package com.wtf.activity.network.response;

import com.wtf.activity.objects.Friend;

import java.util.ArrayList;

/**
 * Created by Niv on 4/7/2016.
 */
public class GetFriendsLocationV2Response {
    private ArrayList<Friend>Friends;

    public ArrayList<Friend> getFriends() {
        return Friends;
    }
}
