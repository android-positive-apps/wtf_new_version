package com.wtf.activity.network.requests;

import com.wtf.activity.main.MyApp;
import com.wtf.activity.network.ApiInterface;
import com.wtf.activity.network.response.ResponseObject;
import com.wtf.activity.network.response.ResponseObject2;
import com.wtf.activity.network.response.UpdateAppuserRadiusResponse;

import retrofit.Callback;

/**
 * Created by Niv on 3/7/2016.
 */
public class UpdateAppuserRadiusRequest extends RequestObject2<UpdateAppuserRadiusResponse> {
    private String AppuserID= MyApp.userManager.getAppuser().getID();
    private int Radius;
    public UpdateAppuserRadiusRequest(int radius) {
        Radius = radius;
    }


    @Override
    protected void execute(ApiInterface apiInterface, Callback<ResponseObject2<UpdateAppuserRadiusResponse>> callback) {
        apiInterface.updateAppuserRadius(AppuserID,Radius,callback);
    }

    @Override
    protected boolean showDefaultToastsOnError() {
        return true;
    }
}
