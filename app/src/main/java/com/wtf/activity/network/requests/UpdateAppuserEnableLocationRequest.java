package com.wtf.activity.network.requests;

import android.content.Context;

import com.wtf.activity.main.MyApp;
import com.wtf.activity.network.ApiInterface;
import com.wtf.activity.network.response.ResponseObject;
import com.wtf.activity.network.response.UpdateAppuserResponse;
import com.wtf.activity.util.DeviceUtil;

import retrofit.Callback;

/**
 * Created by Niv on 3/6/2016.
 */
public class UpdateAppuserEnableLocationRequest extends RequestObject<UpdateAppuserResponse> {


    private String ID= MyApp.userManager.getAppuser().getID();
    private String isLocationEnabled="1";


    public UpdateAppuserEnableLocationRequest() {

        isLocationEnabled = MyApp.userManager.isIncognito()?"0":"1";

    }

    //    public UpdateAppuserRequest(String firstName, String lastName,String FBid,) {
//        FirstName = firstName;
//        LastName = lastName;
////        isLocationEnabled = !ContentProviderUtil.isGPSEnebled(context)?"1":"0";
//        this.FBid=FBid;
//    }

    @Override
    protected void execute(ApiInterface apiInterface, Callback<ResponseObject<UpdateAppuserResponse>> callback) {
        apiInterface.updateAppuserEnabledLocation(ID,isLocationEnabled,callback);
    }

    @Override
    protected boolean showDefaultToastsOnError() {
        return true;
    }
}
