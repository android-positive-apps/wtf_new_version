package com.wtf.activity.network.requests;

import com.wtf.activity.main.MyApp;
import com.wtf.activity.network.ApiInterface;
import com.wtf.activity.network.response.GetAppuserGCMResponse;
import com.wtf.activity.network.response.ResponseObject;

import retrofit.Callback;

/**
 * Created by Niv on 3/6/2016.
 */
public class GetAppuserGcmRequest extends RequestObject<GetAppuserGCMResponse> {


    private String ID= MyApp.userManager.getAppuser().getID();

    public GetAppuserGcmRequest() {
    }

    @Override
    protected void execute(ApiInterface apiInterface, Callback<ResponseObject<GetAppuserGCMResponse>> callback) {
        apiInterface.getAppuserGCM(ID,callback);
    }

    @Override
    protected boolean showDefaultToastsOnError() {
        return true;
    }


}
