package com.wtf.activity.network.requests;

import com.wtf.activity.main.MyApp;
import com.wtf.activity.network.ApiInterface;
import com.wtf.activity.network.response.GetBusinessResponse;
import com.wtf.activity.network.response.ResponseObject;

import retrofit.Callback;

/**
 * Created by Niv on 3/7/2016.
 */
public class GetBusinessRequest extends RequestObject<GetBusinessResponse> {

    private String appuserid = MyApp.userManager.getAppuser().getID();
    private String businessid;
    private String business_ext_id;

    public GetBusinessRequest(String businessid, String business_ext_id) {
        this.businessid = businessid;
        this.business_ext_id = business_ext_id;
    }

    @Override
    protected void execute(ApiInterface apiInterface, Callback<ResponseObject<GetBusinessResponse>> callback) {
        apiInterface.getBusiness(appuserid,businessid,business_ext_id,callback);
    }

    @Override
    protected boolean showDefaultToastsOnError() {
        return true;
    }
}
