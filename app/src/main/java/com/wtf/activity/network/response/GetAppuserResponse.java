package com.wtf.activity.network.response;

import com.google.gson.annotations.SerializedName;
import com.wtf.activity.objects.Appuser;

/**
 * Created by Niv on 3/6/2016.
 */
public class GetAppuserResponse {

    @SerializedName("Appuser")
    private Appuser appuser;

    public Appuser getAppuser() {
        return appuser;
    }

    public void setAppuser(Appuser appuser) {
        this.appuser = appuser;
    }
}
