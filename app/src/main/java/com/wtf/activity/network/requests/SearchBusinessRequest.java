package com.wtf.activity.network.requests;

import com.wtf.activity.network.ApiInterface;
import com.wtf.activity.network.response.ResponseObject;
import com.wtf.activity.network.response.SearchBusinessResponse;

import retrofit.Callback;

/**
 * Created by Niv on 3/7/2016.
 */
public class SearchBusinessRequest extends RequestObject<SearchBusinessResponse> {

    private String Category;
    private String name;
    private String Lat;
    private String Lng;
    private int numResults;

    public SearchBusinessRequest(String category, String name, String lat, String lng, int numResults) {
        Category = category;
        this.name = name;
        Lat = lat;
        Lng = lng;
        this.numResults = numResults;
    }

    @Override
    protected void execute(ApiInterface apiInterface, Callback<ResponseObject<SearchBusinessResponse>> callback) {
        apiInterface.searchBusiness(name,Category,Lat,Lng,numResults,callback);
    }

    @Override
    protected boolean showDefaultToastsOnError() {
        return true;
    }
}
