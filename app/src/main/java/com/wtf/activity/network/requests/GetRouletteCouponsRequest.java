package com.wtf.activity.network.requests;

import com.wtf.activity.main.MyApp;
import com.wtf.activity.network.ApiInterface;
import com.wtf.activity.network.response.GetRouletteCouponsResponse;
import com.wtf.activity.network.response.ResponseObject;

import retrofit.Callback;

/**
 * Created by Niv on 3/7/2016.
 */
public class GetRouletteCouponsRequest extends RequestObject<GetRouletteCouponsResponse> {

    private String AppuserID= MyApp.userManager.getAppuser().getID();
    private int Amount ;

    public GetRouletteCouponsRequest(int amount) {
        Amount = amount;
    }

    @Override
    protected void execute(ApiInterface apiInterface, Callback<ResponseObject<GetRouletteCouponsResponse>> callback) {
        apiInterface.getRouletteCoupons(AppuserID,Amount,callback);
    }

    @Override
    protected boolean showDefaultToastsOnError() {
        return false;
    }
}
