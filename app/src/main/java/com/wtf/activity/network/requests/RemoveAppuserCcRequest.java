package com.wtf.activity.network.requests;

import android.app.DownloadManager;

import com.wtf.activity.main.MyApp;
import com.wtf.activity.network.ApiInterface;
import com.wtf.activity.network.response.AddAppuserCCResponse;
import com.wtf.activity.network.response.RemoveAppuserCCResponse;
import com.wtf.activity.network.response.ResponseObject;
import com.wtf.activity.network.response.ResponseObject2;

import retrofit.Callback;

/**
 * Created by Niv on 3/6/2016.
 */
public class RemoveAppuserCcRequest extends RequestObject2<RemoveAppuserCCResponse> {
    private String AppuserID= MyApp.userManager.getAppuser().getID();
    private String CCID;

    public RemoveAppuserCcRequest( String CCID) {
        this.CCID = CCID;
        //1-4 ?
        //todo check ccid properties
    }

    @Override
    protected void execute(ApiInterface apiInterface, Callback<ResponseObject2<RemoveAppuserCCResponse>> callback) {
        apiInterface.removeAppuserCC(AppuserID, CCID, callback);
    }

    @Override
    protected boolean showDefaultToastsOnError() {
        return true;
    }
}
