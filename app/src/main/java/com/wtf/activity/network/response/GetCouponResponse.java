package com.wtf.activity.network.response;

import com.google.gson.annotations.SerializedName;
import com.wtf.activity.objects.Coupon;

/**
 * Created by Niv on 3/7/2016.
 */
public class GetCouponResponse {

    @SerializedName("Coupon")
    private Coupon coupon;

    public Coupon getCoupon() {
        return coupon;
    }
}
