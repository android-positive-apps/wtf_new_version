package com.wtf.activity.network.response;

import com.wtf.activity.objects.Business;
import com.wtf.activity.objects.Friend;

import java.util.ArrayList;

/**
 * Created by Niv on 3/27/2016.
 */
public class SearchResponse  {

    private ArrayList<Business> PlacesResults;
    private ArrayList<Friend> FriendsResults;


    public ArrayList<Business> getPlacesResults() {
        return PlacesResults;
    }

    public ArrayList<Friend> getFriendsResults() {
        return FriendsResults;
    }
}
