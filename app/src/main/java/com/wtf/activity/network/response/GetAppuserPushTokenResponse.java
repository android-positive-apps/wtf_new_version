package com.wtf.activity.network.response;

import org.w3c.dom.ProcessingInstruction;

/**
 * Created by Niv on 3/6/2016.
 */
public class GetAppuserPushTokenResponse {
    private String PushToken;

    public String getPushToken() {
        return PushToken;
    }

    public void setPushToken(String pushToken) {
        PushToken = pushToken;
    }
}
