package com.wtf.activity.network.response;

import com.wtf.activity.objects.Friend;
import com.wtf.activity.objects.Gift;

import java.util.ArrayList;

/**
 * Created by Niv on 3/2/2016.
 */
public class GetAppuserFriendsLocationResponse {
    private ArrayList<Friend> Friends;

    public ArrayList<Friend> getFriends() {
        return Friends;
    }

    public void setFriends(ArrayList<Friend> friends) {
        Friends = friends;
    }
}
