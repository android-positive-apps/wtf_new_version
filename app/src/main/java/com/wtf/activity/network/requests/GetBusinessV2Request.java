package com.wtf.activity.network.requests;

import com.wtf.activity.main.MyApp;
import com.wtf.activity.network.ApiInterface;
import com.wtf.activity.network.response.GetBusinessV2Response;
import com.wtf.activity.network.response.ResponseObject;

import retrofit.Callback;

/**
 * Created by Niv on 3/28/2016.
 */
public class GetBusinessV2Request extends RequestObject<GetBusinessV2Response>{

    private String appuserid = MyApp.appManager.isAnonymous()? "-1":MyApp.userManager.getAppuser().getID();
    private String businessid;
    private String business_ext_id;

    public GetBusinessV2Request(String businessid, String business_ext_id) {
        this.businessid = businessid;
        this.business_ext_id = business_ext_id;
    }
    @Override
    protected void execute(ApiInterface apiInterface, Callback<ResponseObject<GetBusinessV2Response>> callback) {
        apiInterface.getBusinessV2(appuserid,businessid,business_ext_id,callback);
    }

    @Override
    protected boolean showDefaultToastsOnError() {
        return false;
    }
}
