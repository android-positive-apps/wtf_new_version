package com.wtf.activity.network.requests;

import com.wtf.activity.main.MyApp;
import com.wtf.activity.network.ApiInterface;
import com.wtf.activity.network.response.GetAppuserResponse;
import com.wtf.activity.network.response.ResponseObject;

import retrofit.Callback;

/**
 * Created by Niv on 3/6/2016.
 */
public class GetAppuserRequest extends RequestObject<GetAppuserResponse> {

    private String AppuserID;

    public GetAppuserRequest(String appuserID) {
        AppuserID = appuserID;
    }

    @Override
    protected void execute(ApiInterface apiInterface, Callback<ResponseObject<GetAppuserResponse>> callback) {
        apiInterface.getAppuser(AppuserID,callback);
    }

    @Override
    protected boolean showDefaultToastsOnError() {
        return false;
    }
}
