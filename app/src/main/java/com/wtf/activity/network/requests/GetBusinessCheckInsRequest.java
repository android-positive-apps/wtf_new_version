package com.wtf.activity.network.requests;

import com.wtf.activity.network.ApiInterface;
import com.wtf.activity.network.response.GetBusinessCheckInsResponse;
import com.wtf.activity.network.response.ResponseObject;

import org.json.JSONArray;

import java.util.ArrayList;

import retrofit.Callback;

/**
 * Created by Niv on 3/6/2016.
 */
public class GetBusinessCheckInsRequest extends RequestObject<GetBusinessCheckInsResponse> {

    private String BusinessID;
    private ArrayList<String> FbIdS;

    public GetBusinessCheckInsRequest(String businessID, ArrayList<String> fbIdS) {
        BusinessID = businessID;
        FbIdS = fbIdS;
    }

    @Override
    protected void execute(ApiInterface apiInterface, Callback<ResponseObject<GetBusinessCheckInsResponse>> callback) {
        JSONArray friendsJsonArray = new JSONArray(FbIdS);
//        System.out.println("--->  "+BusinessID+" "+ FbIdS+" "+friendsJsonArray);
        apiInterface.getBusinessCheckIns(friendsJsonArray,BusinessID,callback);

    }

    @Override
    protected boolean showDefaultToastsOnError() {
        return true;
    }
}
