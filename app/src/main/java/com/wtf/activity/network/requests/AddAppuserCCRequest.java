package com.wtf.activity.network.requests;

import com.wtf.activity.main.MyApp;
import com.wtf.activity.network.ApiInterface;
import com.wtf.activity.network.response.AddAppuserCCResponse;
import com.wtf.activity.network.response.ResponseObject;
import com.wtf.activity.network.response.ResponseObject2;

import retrofit.Callback;

/**
 * Created by Niv on 3/6/2016.
 */
public class AddAppuserCCRequest extends RequestObject2<AddAppuserCCResponse> {

    private String AppuserID= MyApp.userManager.getAppuser().getID();
    private String CCID;

    public AddAppuserCCRequest(String CCID) {
        this.CCID = CCID;
        //1-4 ?
        // ccid properties:
        /* 1=mastercard
            2= visa
            3= hever
            4=yours
         */

    }

    @Override
    protected void execute(ApiInterface apiInterface, Callback<ResponseObject2<AddAppuserCCResponse>> callback) {
        apiInterface.addAppuserCC(AppuserID,CCID,callback);
    }

    @Override
    protected boolean showDefaultToastsOnError() {
        return true;
    }
}
