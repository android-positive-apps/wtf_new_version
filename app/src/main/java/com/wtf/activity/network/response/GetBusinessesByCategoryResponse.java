package com.wtf.activity.network.response;

import com.wtf.activity.objects.Business;
import com.wtf.activity.objects.Coupon;

import java.util.ArrayList;

/**
 * Created by Niv on 3/7/2016.
 */
public class GetBusinessesByCategoryResponse {

    ArrayList<Coupon> Coupons;


    private String Name;
    private String ExternalID;
    private String ID;
    private String externalID;
    private String Phone;
    private String Phone2;
    private String Opening;
    private String Image;
    private String MapImage;
    private int CheckIns;
    private float SpecialRank;
    private int NumGifts;
    private String Category;
    private String Fax;
    private String Country;
    private String City;
    private String Street;
    private String HouseNum;
    private String Website;
    private String FacebookLink;
    private String BusinessDesc;
    private int Rank;
    private int NumCoupons;
    private String Status;
    private String Edited;
    private String Lat;
    private String Lng;
    private String AdminRank;
    private String ShowAdminRank;
    private String Created;
    private String NumRankers;
    private String Sort;
    private String Distance;

    public ArrayList<Coupon> getCoupons() {
        return Coupons;
    }

    public String getName() {
        return Name;
    }

    public String getExternalID() {
        return ExternalID;
    }

    public String getPhone() {
        return Phone;
    }

    public String getPhone2() {
        return Phone2;
    }

    public String getOpening() {
        return Opening;
    }

    public String getImage() {
        return Image;
    }

    public String getMapImage() {
        return MapImage;
    }

    public int getCheckIns() {
        return CheckIns;
    }

    public float getSpecialRank() {
        return SpecialRank;
    }

    public int getNumGifts() {
        return NumGifts;
    }

    public String getCategory() {
        return Category;
    }

    public String getFax() {
        return Fax;
    }

    public String getCountry() {
        return Country;
    }

    public String getCity() {
        return City;
    }

    public String getStreet() {
        return Street;
    }

    public String getHouseNum() {
        return HouseNum;
    }

    public String getWebsite() {
        return Website;
    }

    public String getFacebookLink() {
        return FacebookLink;
    }

    public String getBusinessDesc() {
        return BusinessDesc;
    }

    public int getRank() {
        return Rank;
    }

    public int getNumCoupons() {
        return NumCoupons;
    }

    public String getStatus() {
        return Status;
    }

    public String getEdited() {
        return Edited;
    }

    public String getLat() {
        return Lat;
    }

    public String getLng() {
        return Lng;
    }

    public String getAdminRank() {
        return AdminRank;
    }

    public String getShowAdminRank() {
        return ShowAdminRank;
    }

    public String getCreated() {
        return Created;
    }

    public String getNumRankers() {
        return NumRankers;
    }

    public String getSort() {
        return Sort;
    }

    public String getDistance() {
        return Distance;
    }

    public String getID() {
        return ID;
    }
}
