package com.wtf.activity.network.requests;

import com.wtf.activity.main.MyApp;
import com.wtf.activity.network.ApiInterface;
import com.wtf.activity.network.response.ResponseObject;
import com.wtf.activity.network.response.ResponseObject2;
import com.wtf.activity.network.response.UpdateRankResponse;

import retrofit.Callback;

/**
 * Created by Niv on 3/23/2016.
 */
public class UpdateRankRequest extends RequestObject<UpdateRankResponse> {

    private String userID = MyApp.userManager.getAppuser().getID();
    private String businessID;
    private int q1;
    private int q2;
    private int q3;
    private int q4;

    public UpdateRankRequest(String businessID, int q1, int q2, int q3, int q4) {

        this.businessID = businessID;
        this.q1 = q1;
        this.q2 = q2;
        this.q3 = q3;
        this.q4 = q4;
    }

    @Override
    protected void execute(ApiInterface apiInterface, Callback<ResponseObject<UpdateRankResponse>> callback) {
        apiInterface.updateRank(userID,businessID,q1,q2,q3,q4,callback);
    }

    @Override
    protected boolean showDefaultToastsOnError() {
        return true;
    }
}
