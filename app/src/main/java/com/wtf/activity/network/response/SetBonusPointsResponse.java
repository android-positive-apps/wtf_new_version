package com.wtf.activity.network.response;

/**
 * Created by Niv on 3/7/2016.
 */
public class SetBonusPointsResponse {
    private String Points;

    public String getPoints() {
        return Points;
    }

    public void setPoints(String points) {
        Points = points;
    }
}
