package com.wtf.activity.network.response;

import com.wtf.activity.objects.Friend;

import java.util.ArrayList;

/**
 * Created by Niv on 3/3/2016.
 */
public class AppuserCheckInResponse {

   private String GiftPoints="";
   private String TotalPoints="";

    public String getGiftPoints() {
        return GiftPoints;
    }

    public String getTotalPoints() {
        return TotalPoints;
    }
}
