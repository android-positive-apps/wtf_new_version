package com.wtf.activity.network.requests;

import com.wtf.activity.main.MapActivity;
import com.wtf.activity.main.MyApp;
import com.wtf.activity.network.ApiInterface;
import com.wtf.activity.network.response.GetPeopleAroundResponse;
import com.wtf.activity.network.response.ResponseObject;

import retrofit.Callback;

/**
 * Created by Niv on 3/27/2016.
 */
public class GetPeopleAroundRequest extends RequestObject<GetPeopleAroundResponse> {

    private String Latitude = MyApp.appManager.getLatLng().latitude+"";
    private String Longitude=MyApp.appManager.getLatLng().longitude+"";
    private String radius;

    public GetPeopleAroundRequest(String radius) {
        this.radius = radius;
    }

    @Override
    protected void execute(ApiInterface apiInterface, Callback<ResponseObject<GetPeopleAroundResponse>> callback) {
            apiInterface.getPeopleAround(Latitude,Longitude,radius,callback);
    }

    @Override
    protected boolean showDefaultToastsOnError() {
        return true;
    }
}
