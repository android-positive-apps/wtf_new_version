package com.wtf.activity.network.requests;

import com.wtf.activity.network.ApiInterface;
import com.wtf.activity.network.response.ApplicationLoginResponse;
import com.wtf.activity.network.response.GetAppuserFriendsLocationResponse;
import com.wtf.activity.network.response.ResponseObject;

import org.json.JSONArray;

import java.util.ArrayList;

import retrofit.Callback;

/**
 * Created by Niv on 3/2/2016.
 */
public class GetAppuserFriendsLocationRequest extends RequestObject<GetAppuserFriendsLocationResponse> {

    private ArrayList<String> FbIdS;

    public GetAppuserFriendsLocationRequest(ArrayList<String> fbIdS) {
        FbIdS = fbIdS;

    }

    @Override
    protected void execute(ApiInterface apiInterface, Callback<ResponseObject<GetAppuserFriendsLocationResponse>> callback) {
        JSONArray friendsJsonArray = new JSONArray(FbIdS);
        apiInterface.getAppuserFriendsLocation(friendsJsonArray,callback);
    }

    @Override
    protected boolean showDefaultToastsOnError() {
        return true;
    }
}
