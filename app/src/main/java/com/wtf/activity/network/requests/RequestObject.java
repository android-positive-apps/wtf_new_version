package com.wtf.activity.network.requests;

import com.wtf.activity.network.ApiInterface;
import com.wtf.activity.network.NetworkCallback;
import com.wtf.activity.network.NetworkManager;
import com.wtf.activity.network.response.ResponseObject;
import com.wtf.activity.network.response.ResponseObject2;
import com.wtf.activity.util.ToastUtil;


import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by natiapplications on 16/08/15.
 */
public abstract class RequestObject<T> implements Callback<ResponseObject<T>>{


    protected ApiInterface apiInterface;


    protected NetworkCallback<T> networkCallback;
    protected int remoteControllerType;

    protected abstract void execute(ApiInterface apiInterface, Callback<ResponseObject<T>> callback);
    protected abstract boolean showDefaultToastsOnError();


    public void request (ApiInterface apiInterface, final NetworkCallback<T> networkCallback){
        this.apiInterface = apiInterface;
        this.networkCallback = networkCallback;


        if (NetworkManager.isNetworkAvailable()){
            execute(apiInterface, this);
        }else{
            finishOnError("Network is no available");
        }


    }

    @Override
    public void success(ResponseObject<T> responseObject, Response response) {

        if (responseObject == null){
            finishOnError("error = " + "responseIsNull");
            return;
        }

        if (!responseObject.isHasError()) {
            try {
                String responseString =  new String(((TypedByteArray)response.getBody()).getBytes());
                responseObject.setResponseBody(responseString);
            }catch (Exception e) {
                e.printStackTrace();
            }

            if (networkCallback != null) {
                networkCallback.onResponse(true, null, responseObject);
            }
        } else {
            finishOnError("error = " + responseObject.getErrorDesc());
        }
    }

    @Override
    public void failure(RetrofitError error) {
        finishOnError("error = " + error.getMessage());
    }



    protected  void finishOnError (String error){
        showErrorToast(error);
        if (networkCallback != null) {
            networkCallback.onResponse(false, error,null);
        }
    }


    public void showErrorToast (String errorMessage){
        if (showDefaultToastsOnError()){
            ToastUtil.toaster(errorMessage, false);
        }
    }



}
