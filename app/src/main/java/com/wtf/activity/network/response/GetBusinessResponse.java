package com.wtf.activity.network.response;

import com.google.gson.annotations.SerializedName;
import com.wtf.activity.objects.Business;

/**
 * Created by Niv on 3/7/2016.
 */
public class GetBusinessResponse {

    @SerializedName("Business")
    private Business business;

    public Business getBusiness() {
        return business;
    }
}
