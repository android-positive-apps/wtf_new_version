package com.wtf.activity.network.response;

import com.wtf.activity.objects.PeopleLocation;

import java.util.ArrayList;

/**
 * Created by Niv on 3/27/2016.
 */
public class GetPeopleAroundResponse {

    private ArrayList<PeopleLocation> PeopleLocations;

    public ArrayList<PeopleLocation> getPeopleLocations() {
        return PeopleLocations;
    }
}
