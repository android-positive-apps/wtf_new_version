/**
 * 
 */
package com.wtf.activity.network.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * @author natiapplications
 *
 */
public class ResponseObject<T> implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private T data;
	@SerializedName("error")
	private int errorCode;
	@SerializedName("errdesc")
	private String errorDesc;

	private String responseBody;


	public ResponseObject() {
		super();

	}


	/**
	 * @return the errorCode
	 */
	public int getErrorCode() {
		return errorCode;
	}

	/**
	 * @param errorCode the errorCode to set
	 */
	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}

	/**
	 * @return the data
	 */
	public T getData() {
		return data;
	}

	/**
	 * @param data the data to set
	 */
	public void setData(T data) {
		this.data = data;
	}

	/**
	 * @return the isHasError
	 */
	public boolean isHasError() {
		return errorCode > 0 ? true:false;
	}



	/**
	 * @return the errorDesc
	 */
	public String getErrorDesc() {
		return errorDesc;
	}

	/**
	 * @param errorDesc the errorDesc to set
	 */
	public void setErrorDesc(String errorDesc) {
		this.errorDesc = errorDesc;
	}


	public void setResponseBody (String body){
		this.responseBody = body;
	}

	public String getResponseBody() {
		return responseBody;
	}


}
