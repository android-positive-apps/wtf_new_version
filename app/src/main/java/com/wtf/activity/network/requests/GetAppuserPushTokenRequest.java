package com.wtf.activity.network.requests;

import com.wtf.activity.network.ApiInterface;
import com.wtf.activity.network.response.GetAppuserPushTokenResponse;
import com.wtf.activity.network.response.ResponseObject;

import retrofit.Callback;

/**
 * Created by Niv on 3/6/2016.
 */
public class GetAppuserPushTokenRequest extends RequestObject<GetAppuserPushTokenResponse> {

    private String ID;

    public GetAppuserPushTokenRequest(String ID) {
        this.ID = ID;
    }

    @Override
    protected void execute(ApiInterface apiInterface, Callback<ResponseObject<GetAppuserPushTokenResponse>> callback) {
        apiInterface.getAppuserPushToken(ID,callback);
    }

    @Override
    protected boolean showDefaultToastsOnError() {
        return true;
    }
}
