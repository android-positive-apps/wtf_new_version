package com.wtf.activity.network.requests;

import com.wtf.activity.main.MyApp;
import com.wtf.activity.network.ApiInterface;
import com.wtf.activity.network.response.ResponseObject;
import com.wtf.activity.network.response.UpdateAppuserResponse;

import retrofit.Callback;

/**
 * Created by Niv on 3/6/2016.
 */
public class UpdateAppuserPhoneRequest extends RequestObject<UpdateAppuserResponse> {


    private String ID= MyApp.userManager.getAppuser().getID();
    private String phone;


    public UpdateAppuserPhoneRequest(String Phone) {

        phone = Phone;

    }



    @Override
    protected void execute(ApiInterface apiInterface, Callback<ResponseObject<UpdateAppuserResponse>> callback) {
        apiInterface.updateAppuserPhone(ID,phone,callback);
    }

    @Override
    protected boolean showDefaultToastsOnError() {
        return true;
    }
}
