package com.wtf.activity.network.requests;

import com.wtf.activity.main.MyApp;
import com.wtf.activity.network.ApiInterface;
import com.wtf.activity.network.response.ResponseObject;
import com.wtf.activity.network.response.SearchResponse;

import retrofit.Callback;

/**
 * Created by Niv on 3/27/2016.
 */
public class SearchRequest extends RequestObject<SearchResponse> {

//    private String AppuserID = MyApp.userManager.getAppuser().getID();
    private String AppuserID;
    private String SearchString;
    private String Lat;
    private String Lng;
    private int Radius;

    public SearchRequest(String AppuserID, String searchString, String lat, String lng, int radius) {
        SearchString = searchString;
        Lat = lat;
        Lng = lng;
        Radius = radius;
        this.AppuserID=AppuserID;
    }

    @Override
    protected void execute(ApiInterface apiInterface, Callback<ResponseObject<SearchResponse>> callback) {

        apiInterface.search(AppuserID,SearchString,Lat,Lng,Radius,callback);

    }

    @Override
    protected boolean showDefaultToastsOnError() {
        return false;
    }
}
