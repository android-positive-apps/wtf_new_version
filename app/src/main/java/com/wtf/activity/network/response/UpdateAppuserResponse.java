package com.wtf.activity.network.response;

/**
 * Created by Niv on 3/6/2016.
 */
public class UpdateAppuserResponse {
    private int ID;

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }
}
