package com.wtf.activity.network.requests;

import com.wtf.activity.network.ApiInterface;
import com.wtf.activity.network.response.ResponseObject;
import com.wtf.activity.network.response.ResponseObject2;
import com.wtf.activity.network.response.TransferGiftResponse;

import retrofit.Callback;

/**
 * Created by Niv on 4/13/2016.
 */
public class TransferGiftRequest2 extends RequestObject2<TransferGiftResponse> {
    private String fromAppUserId;
    private String toAppUserId;
    private String giftId;

    public TransferGiftRequest2(String fromAppUserId, String toAppUserId, String giftId) {
        this.fromAppUserId = fromAppUserId;
        this.toAppUserId = toAppUserId;
        this.giftId = giftId;
    }


    @Override
    protected void execute(ApiInterface apiInterface, Callback<ResponseObject2<TransferGiftResponse>> callback) {
        apiInterface.transferGift(fromAppUserId,toAppUserId,giftId,callback);

    }

    @Override
    protected boolean showDefaultToastsOnError() {
        return true;
    }
}

