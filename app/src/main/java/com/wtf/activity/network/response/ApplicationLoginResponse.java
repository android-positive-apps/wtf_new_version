package com.wtf.activity.network.response;

import com.wtf.activity.objects.Action;
import com.wtf.activity.objects.Appuser;
import com.wtf.activity.objects.Gift;
import com.wtf.activity.objects.ReceivedGifts;

import java.util.ArrayList;

/**
 * Created by Niv on 3/2/2016.
 */
public class ApplicationLoginResponse {

    private String AppVersion;
    private String UpdateInterval;
    private ArrayList<Gift> ReceivedGifts;
    private ArrayList<Gift> MyGifts;
    private Appuser Appuser;
    private ArrayList<Action> ActionsList;

    public String getAppVersion() {
        return AppVersion;
    }

    public void setAppVersion(String appVersion) {
        AppVersion = appVersion;
    }

    public String getUpdateInterval() {
        return UpdateInterval;
    }

    public void setUpdateInterval(String updateInterval) {
        UpdateInterval = updateInterval;
    }

    public ArrayList<Gift> getReceivedGifts() {
        return ReceivedGifts;
    }

    public void setReceivedGifts(ArrayList<Gift> receivedGifts) {
        ReceivedGifts = receivedGifts;
    }

    public ArrayList<Gift> getMyGifts() {
        return MyGifts;
    }

    public void setMyGifts(ArrayList<Gift> myGifts) {
        MyGifts = myGifts;
    }

    public com.wtf.activity.objects.Appuser getAppuser() {
        return Appuser;
    }

    public void setAppuser(com.wtf.activity.objects.Appuser appuser) {
        Appuser = appuser;
    }

    public ArrayList<Action> getActionsList() {
        return ActionsList;
    }

    public void setActionsList(ArrayList<Action> actionsList) {
        ActionsList = actionsList;
    }
}
