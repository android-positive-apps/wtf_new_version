package com.wtf.activity.network;

import com.wtf.activity.network.response.AddAppuserCCResponse;
import com.wtf.activity.network.response.AppuserCheckInResponse;
import com.wtf.activity.network.response.GetAppuserCCResponse;
import com.wtf.activity.network.response.GetAppuserFriendsLocationResponse;
import com.wtf.activity.network.response.GetAppuserGCMResponse;
import com.wtf.activity.network.response.GetAppuserGiftsResponse;
import com.wtf.activity.network.response.GetAppuserIdResponse;
import com.wtf.activity.network.response.GetAppuserPushTokenResponse;
import com.wtf.activity.network.response.GetAppuserResponse;
import com.wtf.activity.network.response.GetBusinessCheckInsResponse;
import com.wtf.activity.network.response.GetBusinessGiftsAndCouponsResponse;
import com.wtf.activity.network.response.GetBusinessResponse;
import com.wtf.activity.network.response.GetBusinessV2Response;
import com.wtf.activity.network.response.GetBusinessesByCategoryResponse;
import com.wtf.activity.network.response.GetBusinessesByCategoryV2Response;
import com.wtf.activity.network.response.GetCouponResponse;
import com.wtf.activity.network.response.GetFriendsDetailsResponse;
import com.wtf.activity.network.response.GetFriendsLocationV2Response;
import com.wtf.activity.network.response.GetPeopleAroundResponse;
import com.wtf.activity.network.response.GetRouletteCouponsResponse;
import com.wtf.activity.network.response.GetStatisticsResponse;
import com.wtf.activity.network.response.RemoveAppuserCCResponse;
import com.wtf.activity.network.response.ResponseObject;
import com.wtf.activity.network.response.ApplicationLoginResponse;
import com.wtf.activity.network.response.ResponseObject2;
import com.wtf.activity.network.response.SearchBusinessResponse;
import com.wtf.activity.network.response.SearchResponse;
import com.wtf.activity.network.response.SetBonusPointsResponse;
import com.wtf.activity.network.response.SetCouponUsedResponse;
import com.wtf.activity.network.response.SetGiftUsedResponse;
import com.wtf.activity.network.response.TransferGiftResponse;
import com.wtf.activity.network.response.UpdateAppuserGCMResponse;
import com.wtf.activity.network.response.UpdateAppuserLocationResponse;
import com.wtf.activity.network.response.UpdateAppuserRadiusResponse;
import com.wtf.activity.network.response.UpdateAppuserResponse;
import com.wtf.activity.network.response.UpdateRankResponse;

import org.json.JSONArray;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;

/**
 * Created by natiapplications on 16/08/15.
 */
public interface ApiInterface {

   String APLICATION_LOGIN= "/api/applicationLogin";
   String GET_APPUSER_ID= "/api/getAppuserID";
   String GET_APPUSER_FRIENDS_LOCATION= "/api/getAppuserFriendsLocation";
   String APPUSER_CHECKIN= "/api/appuserCheckIn";
   String UPDATE_APPUSER= "/api/updateAppuser";
   String UPDATE_APPUSER_GCM= "/api/updateAppuserGCM";
   String GET_APPUSER_PUSH_TOKEN= "/api/getAppuserPushToken";
   String GET_APPUSER_GCM= "/api/getAppuserGCM";
   String GET_APPUSER= "/api/getAppuser";
   String GET_APPUSER_GIFTS= "/api/getAppuserGifts";
   String GET_APPUSER_RECEIVED_GIFTS= "/api/getAppuserReceivedGifts";
   String GET_BUSINESS_CHECK_INS= "/api/getBusinessCheckIns";
   String ADD_APPUSER_CC = "/api/addAppuserCC";
   String REMOVE_APPUSER_CC = "/api/removeAppuserCC";
   String UPDATE_APP_USER_LOCATION = "/api/updateAppuserLocation";
   String SET_BONUS_POINTS = "/api/setBonusPoints";
   String UPDATE_APPUSER_RADIUS = "/api/updateAppuserRadius";
   String GET_ROULETTE_COUPONS = "/api/getRouletteCoupons";
   String GET_APPUSER_CC = "/api/getAppuserCC";
   String GET_COUPON = "/api/getCoupon";
   String GET_BUSINESS = "/api/getBusiness";
   String GET_BUSINESS_GIFTS_AND_COUPONS = "/api/getBusinessGiftsAndCoupons";
   String GET_BUSINESS_BY_CATEGORY = "/api/getBusinessesByCategory";
   String SEARCH_BUSINESS = "/api/searchBusiness";
   String UPDATE_RANK = "/api/updateRank";
   String SET_COUPON_USED = "/api/setCouponUsed";
   String TRANSFER_GIFT = "/api/transferGift";
   String SET_GIFT_USED = "/api/setGiftUsed";

    //V2
   String GET_STATITISTICS = "/papps-api/v2/general/getStatistics";
   String GET_PEOPLE_AROUND = "/papps-api/v2/general/getPeopleAround";
   String SEARCH = "/papps-api/v2/general/Search";
   String GET_FRIEND_DEATILS = "/papps-api/v2/general/getFriendDetailsl";
   String GET_BUSINESS_BY_CATEGORY_V2 = "/papps-api/v2/general/getBusinessesByCategory";
   String GET_BUSINESS_V2 = "/papps-api/v2/general/getBusiness";
    String GET_APPUSER_FRIENDS_LOCATION_V2= "/papps-api/v2/general/getAppuserFriendsLocation";

    //applicationLogin:
    String FACEBOOK_ID = "FacebookID";
    String UDID = "UDID";
    String LAT = "Lat";
    String LNG = "Lng";
    //updateUser
    String ID = "ID";
    String FIRST_NAME = "FirstName";
    String LAST_NAME= "LastName";
    String ENABLE_LOCATION= "EnableLocation";
    String FB_ID= "FBID";
    //FBIDs
    String FBIDs = "FBIDs";
    //CHECKIN:
    String APPUSER_ID = "AppuserID";
    //update user gcm:
    String GCM = "GCM";
    //appuser
    String APP_USER_ID = "AppuserID";
    //GIFTS:
    String UN_USED= "Unused";
    //BUSINEESS:
    String BUSINESS_ID="BusinessID";
    String BUSINESS_EXT_ID="BusinessExtID";
    String NAME="Name";

    //CC
    String CC_ID= "CCID";
    //APPUSER LOCATION
    String VERSION= "Version";
    //bonus
    String POINTS= "Points";
    String ACTION_NAME= "ActionName";
    //RADIUS:
    String RADIUS= "Radius";
    //ROULLETE
    String AMOUNT= "Amount";
    String COUPON_ID= "CouponID";
    //categories:
    String CATEGORY = "Category";
    String NUM_RESULTS = "NumResults";
    String SORT = "Sort";
    //coupon:
    String PASSWORD= "Password";

    @FormUrlEncoded
    @POST(APLICATION_LOGIN)
     void applicationLogin (@Field(FACEBOOK_ID)String FacebookID,
                                  @Field(UDID)String udid,
                                  @Field(LAT)String Lat,
                                  @Field(LNG)String Lng,
                                  Callback<ResponseObject<ApplicationLoginResponse>> retroCallback);
    @FormUrlEncoded
    @POST(GET_APPUSER_ID)
     void getAppuserID(@Field(FACEBOOK_ID)String FacebookID,
        Callback<ResponseObject<GetAppuserIdResponse>> retroCallback);

    @FormUrlEncoded
    @POST(GET_APPUSER_FRIENDS_LOCATION)
     void getAppuserFriendsLocation(@Field(FBIDs)JSONArray FacebookIDs,
                             Callback<ResponseObject<GetAppuserFriendsLocationResponse>> retroCallback);

    @FormUrlEncoded
    @POST(APPUSER_CHECKIN)
     void appuserCheckIn(@Field(APPUSER_ID)String AppUserId,
                               @Field(BUSINESS_ID)String BusinessId,
                               @Field(LAT)String Latitude,
                               @Field(LNG)String Longitude,
                                          Callback<ResponseObject<AppuserCheckInResponse>> retroCallback);


    @FormUrlEncoded
    @POST(UPDATE_APPUSER)
     void updateAppuser(@Field(ID) String Id,
                        @Field(FIRST_NAME) String FirstName,
                        @Field(LAST_NAME) String LastName,
                        @Field(FB_ID) String FbId,
                        @Field(UDID) String udid,
                        @Field("Email") String email,
                        @Field("FBAvatar") String fbAvatar,
                        @Field(ENABLE_LOCATION) String locationEnabled,
                        Callback<ResponseObject<UpdateAppuserResponse>> retroCallback);
    @FormUrlEncoded
    @POST(UPDATE_APPUSER)
    void updateAppuserEnabledLocation(
                        @Field(ID) String Id,
                        @Field(ENABLE_LOCATION) String locationEnabled,
                       Callback<ResponseObject<UpdateAppuserResponse>> retroCallback);

    @FormUrlEncoded
    @POST(UPDATE_APPUSER)
    void updateAppuserPhone(
            @Field(ID) String Id,
            @Field("Phone") String phoneNumber,
            Callback<ResponseObject<UpdateAppuserResponse>> retroCallback);

    @FormUrlEncoded
    @POST(UPDATE_APPUSER_GCM)
     void updateAppuserGCM(@Field(ID)String Id,
                                 @Field(GCM)String Gcm,
                             Callback<ResponseObject<UpdateAppuserGCMResponse>> retroCallback);

    @FormUrlEncoded
    @POST(GET_APPUSER_PUSH_TOKEN)
     void getAppuserPushToken(@Field(ID)String Id,
                             Callback<ResponseObject<GetAppuserPushTokenResponse>> retroCallback);



    @FormUrlEncoded
    @POST(GET_APPUSER_GCM)
     void getAppuserGCM(@Field(ID)String Id,
                                    Callback<ResponseObject<GetAppuserGCMResponse>> retroCallback);


    @FormUrlEncoded
    @POST(GET_APPUSER)
     void getAppuser(@Field(APP_USER_ID)String UserId,
                                    Callback<ResponseObject<GetAppuserResponse>> retroCallback);



    @FormUrlEncoded
    @POST(GET_APPUSER_GIFTS)
     void getAppuserGifts(@Field(APP_USER_ID)String UserId,
                                @Field(UN_USED)int unUsed,
                                    Callback<ResponseObject<GetAppuserGiftsResponse>> retroCallback);
  @FormUrlEncoded
    @POST(GET_APPUSER_RECEIVED_GIFTS)
     void getAppuserReceivedGifts
                                (@Field(APP_USER_ID)String UserId,
                                @Field(UN_USED)int unUsed,
                                    Callback<ResponseObject<GetAppuserGiftsResponse>> retroCallback);
    @FormUrlEncoded
    @POST(GET_BUSINESS_CHECK_INS)
     void getBusinessCheckIns
            (@Field(FBIDs)JSONArray FbIds,
             @Field(BUSINESS_ID)String businessId,
             Callback<ResponseObject<GetBusinessCheckInsResponse>> retroCallback);

    @FormUrlEncoded
    @POST(ADD_APPUSER_CC)
     void addAppuserCC
            (       @Field(APPUSER_ID)String appuserId,
                    @Field(CC_ID) String CCID,

             Callback<ResponseObject2<AddAppuserCCResponse>> retroCallback);

    @FormUrlEncoded
    @POST(REMOVE_APPUSER_CC)
     void removeAppuserCC
            (       @Field(APPUSER_ID)String appuserId,
                    @Field(CC_ID) String CCID,

                    Callback<ResponseObject2<RemoveAppuserCCResponse>> retroCallback);



    @FormUrlEncoded
    @POST(UPDATE_APP_USER_LOCATION)
     void updateAppuserLocation
                              (@Field(APPUSER_ID)String AppUserId,
                               @Field(LAT)String Latitude,
                               @Field(LNG)String Longitude,
                               @Field(VERSION)String version,
                               Callback<ResponseObject2<UpdateAppuserLocationResponse>> retroCallback);


    @FormUrlEncoded
    @POST(SET_BONUS_POINTS)
     void setBonusPoints (@Field(APPUSER_ID)String AppuserId,
                          @Field(POINTS)int points,
                          @Field(ACTION_NAME)String ActionName,
                          Callback<ResponseObject<SetBonusPointsResponse>> retroCallback);
/*
    WITH DATA AS ARRAY RESPONSE
 */
    @FormUrlEncoded
    @POST(UPDATE_APPUSER_RADIUS)
    void updateAppuserRadius
                        (@Field(APPUSER_ID)String AppuserId,
                         @Field(RADIUS)int Radius,
                         Callback<ResponseObject2<UpdateAppuserRadiusResponse>> retroCallback);


    @FormUrlEncoded
    @POST(GET_ROULETTE_COUPONS)
    void getRouletteCoupons
                        (@Field(APPUSER_ID)String AppuserId,
                         @Field(AMOUNT)int amount,
                         Callback<ResponseObject<GetRouletteCouponsResponse>> retroCallback);

    @FormUrlEncoded
    @POST(GET_APPUSER_CC)
    void getAppuserCC
                        (@Field(APPUSER_ID)String AppuserId,
                         Callback<ResponseObject<GetAppuserCCResponse>> retroCallback);


    @FormUrlEncoded
    @POST(GET_COUPON)
    void getCoupon
                        (@Field (APPUSER_ID)String AppuserId,
                         @Field (COUPON_ID) String CouponId,
                         Callback<ResponseObject<GetCouponResponse>> retroCallback);

    @FormUrlEncoded
    @POST(GET_BUSINESS)
       void getBusiness
                        (@Field (APPUSER_ID)String AppuserId,
                         @Field (BUSINESS_ID) String BusinessId,
                         @Field (BUSINESS_EXT_ID) String BusinessExtId,
                         Callback<ResponseObject<GetBusinessResponse>> retroCallback);

    @FormUrlEncoded
    @POST(GET_BUSINESS_GIFTS_AND_COUPONS)
    void getBusinessGiftsAndCoupons
            (@Field (APPUSER_ID)String AppuserId,
             @Field (BUSINESS_ID) String BusinessId,
             Callback<ResponseObject<GetBusinessGiftsAndCouponsResponse>> retroCallback);

    @FormUrlEncoded
    @POST(GET_BUSINESS_BY_CATEGORY)
    void getBusinessesByCategory
            (@Field (CATEGORY)String category,
             @Field (LAT) String lat,
             @Field (LNG) String lng,
             @Field (RADIUS) int radius,
             @Field (NUM_RESULTS) int numResults,
             @Field ("Sort") String sort,
             Callback<ResponseObject2<GetBusinessesByCategoryResponse>> retroCallback);

    @FormUrlEncoded
    @POST(SEARCH_BUSINESS)
    void searchBusiness
            (@Field (NAME)String Name,
             @Field (CATEGORY)String category,
             @Field (LAT) String lat,
             @Field (LNG) String lng,
             @Field (NUM_RESULTS) int numResults,
             Callback<ResponseObject<SearchBusinessResponse>> retroCallback);

    @FormUrlEncoded
    @POST(SET_COUPON_USED)
    void setCouponUsed
            (@Field (APPUSER_ID)String userID,
             @Field (COUPON_ID)String couponID,
             @Field (LAT) String lat,
             @Field (LNG) String lng,
             @Field (PASSWORD) String pass,
             Callback<ResponseObject2<SetCouponUsedResponse>> retroCallback);

   @FormUrlEncoded
    @POST(TRANSFER_GIFT)
    void transferGift
            (@Field ("FromAppuserID")String fromUserID,
             @Field ("ToAppuserID")String toUserID,
             @Field ("GiftID") String giftId,
             Callback<ResponseObject2<TransferGiftResponse>> retroCallback);

    @FormUrlEncoded
    @POST(SET_GIFT_USED)
    void setGiftUsed
            (@Field (APPUSER_ID)String userID,
             @Field ("GiftID")String giftID,
             @Field (LAT) String lat,
             @Field (LNG) String lng,
             @Field (PASSWORD) String pass,
             Callback<ResponseObject2<SetGiftUsedResponse>> retroCallback);

    //V2
    @FormUrlEncoded
    @POST(SEARCH)
    void search
            (@Field (APPUSER_ID)String AppuserID,
             @Field ("SearchString")String SearchString,
             @Field (LAT) String lat,
             @Field (LNG) String lng,
             @Field (RADIUS) int radius,
             Callback<ResponseObject<SearchResponse>> retroCallback);

    @FormUrlEncoded
    @POST(UPDATE_RANK)
    void updateRank
            (@Field(APPUSER_ID)String AppuserId,
             @Field(BUSINESS_ID)String BusinessId,
             @Field("Q1")int q1,
             @Field("Q2")int q2,
             @Field("Q3")int q3,
             @Field("Q4")int q4,
             Callback<ResponseObject<UpdateRankResponse>> retroCallback);

    @FormUrlEncoded
    @POST(GET_PEOPLE_AROUND)
    void getPeopleAround
            (@Field (LAT)String lat,
             @Field (LNG) String lng,
             @Field (RADIUS) String radius,
             Callback<ResponseObject<GetPeopleAroundResponse>> retroCallback);

    @FormUrlEncoded
    @POST(GET_FRIEND_DEATILS)
    void getFriendsDetails
            (@Field (APPUSER_ID)String id,
             Callback<ResponseObject<GetFriendsDetailsResponse>> retroCallback);


    @POST(GET_STATITISTICS)
    void getStatistics
            (
             Callback<ResponseObject<GetStatisticsResponse>> retroCallback);


    @FormUrlEncoded
    @POST(GET_BUSINESS_BY_CATEGORY_V2)
    void getBusinessesByCategoryV2
            (@Field (CATEGORY)String category,
             @Field (LAT) String lat,
             @Field (LNG) String lng,
             @Field (RADIUS) int radius,
             @Field (NUM_RESULTS) int numResults,
             @Field ("Sort") String sort,
             Callback<ResponseObject<GetBusinessesByCategoryV2Response>> retroCallback);


    @FormUrlEncoded
    @POST(GET_BUSINESS_V2)
    void getBusinessV2
            (@Field (APPUSER_ID)String AppuserId,
             @Field (BUSINESS_ID) String BusinessId,
             @Field (BUSINESS_EXT_ID) String BusinessExtId,
             Callback<ResponseObject<GetBusinessV2Response>> retroCallback);

    @FormUrlEncoded
    @POST(GET_APPUSER_FRIENDS_LOCATION_V2)
    void getFriendsLocationV2
            (@Field (FBIDs)JSONArray FacebookIDs,
             Callback<ResponseObject<GetFriendsLocationV2Response>> retroCallback);

//    @FormUrlEncoded
//    @POST(GET_APPUSER_FRIENDS_LOCATION_V2)
//    void getFriendsLocationV2
//            (@Field (FBIDs)JSONArray FacebookIDs,
//             Callback<ResponseObject<GetFriendsLocationV2Response>> retroCallback);
}
