package com.wtf.activity.network.requests;

import com.wtf.activity.network.ApiInterface;
import com.wtf.activity.network.response.ApplicationLoginResponse;
import com.wtf.activity.network.response.GetAppuserIdResponse;
import com.wtf.activity.network.response.ResponseObject;

import retrofit.Callback;

/**
 * Created by Niv on 3/2/2016.
 */
public class GetAppuserIdRequest extends RequestObject<GetAppuserIdResponse> {
    private String FacebookID;

    public GetAppuserIdRequest(String facebookID) {
        FacebookID = facebookID;
    }

    @Override
    protected void execute(ApiInterface apiInterface,
                           Callback<ResponseObject<GetAppuserIdResponse>> callback) {
        apiInterface.getAppuserID(FacebookID,callback);

    }

    @Override
    protected boolean showDefaultToastsOnError() {
        return false;
    }
}
