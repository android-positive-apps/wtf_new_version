package com.wtf.activity.network.response;

import com.wtf.activity.objects.Coupon;
import com.wtf.activity.objects.Gift;

import java.util.ArrayList;

/**
 * Created by Niv on 3/7/2016.
 */
public class GetBusinessGiftsAndCouponsResponse {
    ArrayList<Gift> Gifts;
    ArrayList<Coupon> Coupons;

    public ArrayList<Gift> getGifts() {
        return Gifts;
    }

    public ArrayList<Coupon> getCoupons() {
        return Coupons;
    }
}
