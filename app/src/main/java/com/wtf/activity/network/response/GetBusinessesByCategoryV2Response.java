package com.wtf.activity.network.response;

import com.wtf.activity.objects.Business;

import java.util.ArrayList;

/**
 * Created by Niv on 3/28/2016.
 */
public class GetBusinessesByCategoryV2Response {

    ArrayList<Business> Businesses;

    public ArrayList<Business> getBusinesses() {
        return Businesses;
    }
}
