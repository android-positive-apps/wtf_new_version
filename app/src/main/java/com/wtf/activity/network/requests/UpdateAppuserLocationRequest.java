package com.wtf.activity.network.requests;

import android.content.Context;

import com.wtf.activity.main.MyApp;
import com.wtf.activity.network.ApiInterface;
import com.wtf.activity.network.response.ResponseObject;
import com.wtf.activity.network.response.ResponseObject2;
import com.wtf.activity.network.response.UpdateAppuserLocationResponse;
import com.wtf.activity.util.AppUtil;

import retrofit.Callback;
import retrofit.http.Field;

/**
 * Created by Niv on 3/6/2016.
 */
public class UpdateAppuserLocationRequest extends RequestObject2<UpdateAppuserLocationResponse> {

//    (@Field(APPUSER_ID)String AppUserId,
//    @Field(LAT)String Latitude,
//    @Field(LNG)String Longitude,
//    @Field(VERSION)String version,

    private String AppUserId= MyApp.userManager.getAppuser().getID();
    private String Latitude =MyApp.appManager.getLatLng().latitude+"";
    private String Longitude=MyApp.appManager.getLatLng().longitude+"";
    private String version;

    public UpdateAppuserLocationRequest(Context context) {

        version = AppUtil.getApplicationVersion(context);
    }


    @Override
    protected void execute(ApiInterface apiInterface, Callback<ResponseObject2<UpdateAppuserLocationResponse>> callback) {
        apiInterface.updateAppuserLocation(AppUserId,Latitude,Longitude,version,callback);

    }

    @Override
    protected boolean showDefaultToastsOnError() {
        return false;
    }
}
