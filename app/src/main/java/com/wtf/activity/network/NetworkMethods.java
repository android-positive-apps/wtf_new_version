package com.wtf.activity.network;

/**
 * Created by Niv on 3/7/2016.
 * all network methods for easy use in the future
 */
public class NetworkMethods {
    /*
           //getAppUserID:
//        MyApp.networkManager.makeRequest(new GetAppuserIdRequest("10209284450852634"),
//                new NetworkCallback<GetAppuserIdResponse>(){
//                    @Override
//                    public void onResponse(boolean success, String errorDesc, ResponseObject<GetAppuserIdResponse> response) {
//                        super.onResponse(success, errorDesc, response);
//                        if(success){
//                            ToastUtil.toaster("success",true);
//
//                        }else {
//                            ToastUtil.toaster(errorDesc,true);
//                        }
//                    }
//                });

        //-------------------------------------------------------------

        System.out.println(DeviceUtil.getDeviceUDID()+"");
        //getFriendsLocation:
//        ArrayList<String> fbIds= new ArrayList<>();
//        fbIds.add("10209284450852634");
//        ArrayList<String> friendsIDs = new ArrayList<String>();
//        friendsIDs.add("10209284450852634");
//        MyApp.networkManager.makeRequest(new GetAppuserFriendsLocationRequest(friendsIDs),new NetworkCallback<GetAppuserFriendsLocationResponse>(){
//            @Override
//            public void onResponse(boolean success, String errorDesc, ResponseObject<GetAppuserFriendsLocationResponse> response) {
//                super.onResponse(success, errorDesc, response);
//                if(success){
//                    ToastUtil.toaster("success",true);
//                }else {
//                    ToastUtil.toaster(errorDesc,true);
//                }
//            }
//        });

        //-------------------------------------------------------------------
        //appuserCheckIn:
        //todo check this response:
//        MyApp.networkManager.makeRequest(new AppuserCheckInRequest("540","1"),new NetworkCallback<AppuserCheckInResponse>(){
//            @Override
//            public void onResponse(boolean success, String errorDesc, ResponseObject<AppuserCheckInResponse> response) {
//                super.onResponse(success, errorDesc, response);
//                if(success){
//                    ToastUtil.toaster("success",true);
//                }else {
//                    ToastUtil.toaster(errorDesc,true);
//                }
//
//            }
//
//        });
//---------------------------------------------------------------------

//            MyApp.networkManager.makeRequest(new UpdateAppuserRequest("540","Niv","Angert","10209284450852634",this),new NetworkCallback<UpdateAppuserResponse>(){
//                @Override
//                public void onResponse(boolean success, String errorDesc, ResponseObject<UpdateAppuserResponse> response) {
//                    super.onResponse(success, errorDesc, response);
//                    if(success){
//                        ToastUtil.toaster("success"+response.getData().getID(),true);
//                    }else {
//                        ToastUtil.toaster(errorDesc,true);
//                    }
//                }
//            });
//---------------------------------------------------------------------

//        //updateAppuserGCM:
//
//        MyApp.networkManager.makeRequest(new UpdateAppuserGCMRequest("540","RG34nkjh3434SDF345243rjh23w4kjsd"),new NetworkCallback<UpdateAppuserGCMResponse>(){
//            @Override
//            public void onResponse(boolean success, String errorDesc, ResponseObject<UpdateAppuserGCMResponse> response) {
//                super.onResponse(success, errorDesc, response);
//                if(success){
//                    ToastUtil.toaster("success"+response.getData().getGCM(),true);
//                }else {
//                    ToastUtil.toaster(errorDesc,true);
//                }
//
//            }
//        });
//-----------------------------------------------------------------------------------
        //getappuser push token

//        MyApp.networkManager.makeRequest(new GetAppuserPushTokenRequest("540"),new NetworkCallback<GetAppuserPushTokenResponse>(){
//            @Override
//            public void onResponse(boolean success, String errorDesc, ResponseObject<GetAppuserPushTokenResponse> response) {
//                super.onResponse(success, errorDesc, response);
//                if(success && response.getData().getPushToken()!=null ){
//                    ToastUtil.toaster("success"+response.getData().getPushToken(),true);
//                }else {
//                    ToastUtil.toaster(errorDesc,true);
//                }
//            }
//        });
//----------------------------------------------------------------------------
//        //get_appuser_GCM
//        MyApp.networkManager.makeRequest(new GetAppuserGcmRequest("540"),new NetworkCallback<GetAppuserGCMResponse>(){
//            @Override
//            public void onResponse(boolean success, String errorDesc, ResponseObject<GetAppuserGCMResponse> response) {
//                super.onResponse(success, errorDesc, response);
//                if(success && response.getData().getGCM()!=null ){
//                    ToastUtil.toaster("success"+response.getData().getGCM(),true);
//                }else {
//                    ToastUtil.toaster(errorDesc,true);
//                }
//
//            }
//        });
//--------------------------------///////////////////////////////////////////////////////
        //getAppuser:
//        MyApp.networkManager.makeRequest(new GetAppuserRequest("540"),new NetworkCallback<GetAppuserResponse>(){
//            @Override
//            public void onResponse(boolean success, String errorDesc, ResponseObject<GetAppuserResponse> response) {
//                super.onResponse(success, errorDesc, response);
//                if(success  ){
//                    ToastUtil.toaster("success"+response.getData().getAppuser().toString(),true);
//                }else {
//                    ToastUtil.toaster(errorDesc,true);
//                }
//            }
//        });
//-----------------------------------------------------------------------------------

        //get appuser gifts
//        MyApp.networkManager.makeRequest(new GetAppuserGiftsRequest("540",1),new NetworkCallback<GetAppuserGiftsResponse>(){
//            @Override
//            public void onResponse(boolean success, String errorDesc, ResponseObject<GetAppuserGiftsResponse> response) {
//                super.onResponse(success, errorDesc, response);
//                if(success  ){
//                    ToastUtil.toaster("success"+response.getData().getGifts().toString(),true);
//                }else {
//                    ToastUtil.toaster(errorDesc,true);
//                }
//            }
//        });
        ///-----------------------------------------------------------------------------
        //get appuser received gifts
//        MyApp.networkManager.makeRequest(new GetAppuserReceivedGiftsRequest("540",1),new NetworkCallback<GetAppuserGiftsResponse>(){
//            @Override
//            public void onResponse(boolean success, String errorDesc, ResponseObject<GetAppuserGiftsResponse> response) {
//                super.onResponse(success, errorDesc, response);
//                if(success  ){
//                    ToastUtil.toaster("success"+response.getData().getGifts().toString(),true);
//                }else {
//                    ToastUtil.toaster(errorDesc,true);
//                }
//            }
//        });
        //-------------------------------------------------------------------------
        //getBusinessCheckIns
//        ArrayList<String> fbIds = new ArrayList<>();
//        fbIds.add("10209284450852634");
//        MyApp.networkManager.makeRequest(new GetBusinessCheckInsRequest("1",fbIds),new NetworkCallback<GetBusinessCheckInsResponse>(){
//            @Override
//            public void onResponse(boolean success, String errorDesc, ResponseObject<GetBusinessCheckInsResponse> response) {
//                super.onResponse(success, errorDesc, response);
//                if(success  ){
//                    ToastUtil.toaster("success"+response.getData().getCheckIns().toString(),true);
//                }else {
//                    ToastUtil.toaster(errorDesc,true);
//                }
//            }
//        });

        //--------------------------------------------------------------------------------
        //AddAppuserCCRequest:
//
//
//        MyApp.networkManager.makeRequest(new AddAppuserCCRequest("540","2"),new NetworkCallback<AddAppuserCCResponse>(){
//            @Override
//            public void onResponse(boolean success, String errorDesc, ResponseObject<AddAppuserCCResponse> response) {
//                super.onResponse(success, errorDesc, response);
//                if (success){
//                    ToastUtil.toaster("success",true);
//                }else {
//                    ToastUtil.toaster(errorDesc,true);
//                }
//
//            }
//        });
//------------------------------------------------------------------------------------------

        //remove appusr cc:
//                MyApp.networkManager.makeRequest(new RemoveAppuserCcRequest("540","4"),new NetworkCallback<AddAppuserCCResponse>(){
//            @Override
//            public void onResponse(boolean success, String errorDesc, ResponseObject<AddAppuserCCResponse> response) {
//                super.onResponse(success, errorDesc, response);
//                if (success){
//                    ToastUtil.toaster("success",true);
//                }else {
//                    ToastUtil.toaster(errorDesc,true);
//                }
//
//            }
//        });

//------------------------------------------------------------------------------------------
        //update appuser location:
//        MyApp.networkManager.makeRequest(new UpdateAppuserLocationRequest("540",
//                MyApp.appManager.getLatLng().latitude+"",
//                MyApp.appManager.getLatLng().longitude+"",
//                this),new NetworkCallback<UpdateAppuserLocationResponse>(){
//            @Override
//            public void onResponse(boolean success, String errorDesc, ResponseObject<UpdateAppuserLocationResponse> response) {
//                super.onResponse(success, errorDesc, response);
//                if (success){
//                    ToastUtil.toaster("success",true);
//                }else {
//                    ToastUtil.toaster(errorDesc,true);
//                    System.out.println("retro "+errorDesc);
//                }
//
//            }
//        });

        //----------------------------------------------------------------------------------
        //set bonus points
//        MyApp.networkManager.makeRequest(new SetBonusPointsRequest(this,"540", GiftUtils.SHARE_POINT.actionName,20),new NetworkCallback<SetBonusPointsResponse>(){
//            @Override
//            public void onResponse(boolean success, String errorDesc, ResponseObject<SetBonusPointsResponse> response) {
//                super.onResponse(success, errorDesc, response);
//                if (success){
//                    ToastUtil.toaster("success "+response.getData().getPoints(),true);
//                }else {
//                    ToastUtil.toaster(errorDesc,true);
//                    System.out.println("retro "+errorDesc);
//                }
//            }
//        });
        //----------------------------------------------------------------------------------

//        //update appuser radius
//        MyApp.networkManager.makeRequest2(new UpdateAppuserRadiusRequest(2), new NetworkCallback<UpdateAppuserRadiusResponse>() {
//
//            @Override
//            public void onResponse2(boolean success, String errorDesc, ResponseObject2<UpdateAppuserRadiusResponse> response) {
//                super.onResponse2(success, errorDesc, response);
//                if (success) {
//                    ToastUtil.toaster("success", true);
//                } else {
//                    ToastUtil.toaster(response.getErrorDesc(), true);
//                }
//            }
//        });

//-------------------------------------------------------------------------------------
        //getRouletteCoupons
//        MyApp.networkManager.makeRequest(new GetRouletteCouponsRequest(20),new NetworkCallback<GetRouletteCouponsResponse>(){
//            @Override
//            public void onResponse(boolean success, String errorDesc, ResponseObject<GetRouletteCouponsResponse> response) {
//                super.onResponse(success, errorDesc, response);
//                if (success){
//                    ToastUtil.toaster("success ",true);
//                }else {
//                    ToastUtil.toaster(errorDesc,true);
//                    System.out.println("retro "+errorDesc);
//                }
//            }
//        });
//-------------------------------------------------------------------------------------
// get appuser cc
//        MyApp.networkManager.makeRequest(new GetAppuserCCRequest(),new NetworkCallback<GetAppuserCCResponse>(){
//            @Override
//            public void onResponse(boolean success, String errorDesc, ResponseObject<GetAppuserCCResponse> response) {
//                super.onResponse(success, errorDesc, response);
//                if (success){
//                  ToastUtil.toaster("success "+response.getData().getCreditCards().size(),true);
//
//                }
//
//            }
//        });
        //-----------------------------------------------------------------------------------------
//            //get coupon:
//        MyApp.networkManager.makeRequest(new GetCouponRequest("1"),new NetworkCallback<GetCouponResponse>(){
//            @Override
//            public void onResponse(boolean success, String errorDesc, ResponseObject<GetCouponResponse> response) {
//                super.onResponse(success, errorDesc, response);
//                if (success){
//                    ToastUtil.toaster("success ",true);
//                }
//            }
//        });
        //-----------------------------------------------------------------------------------------

//        //GetBusiness:
//        MyApp.networkManager.makeRequest(new GetBusinessRequest("1","1"),new NetworkCallback<GetBusinessResponse>(){
//            @Override
//            public void onResponse(boolean success, String errorDesc, ResponseObject<GetBusinessResponse> response) {
//                super.onResponse(success, errorDesc, response);
//                if (success){
//                    ToastUtil.toaster("success "+response.getData().getBusiness().getID(),true);
//                }
//            }
//        });
        //-----------------------------------------------------------------------------------------
//        //getBusinessGiftsAndCoupons:
//        MyApp.networkManager.makeRequest(new GetBusinessGiftsAndCouponsRequest("1"),new NetworkCallback<GetBusinessGiftsAndCouponsResponse>(){
//            @Override
//            public void onResponse(boolean success, String errorDesc, ResponseObject<GetBusinessGiftsAndCouponsResponse> response) {
//                super.onResponse(success, errorDesc, response);
//                if (success){
//                ToastUtil.toaster("success "+response.getData().getCoupons().size(),true);
//
//                }
//            }
//        });
        //---------------------------------------------------------------------------------------
//        //getBusinessesByCategory:
//        MyApp.networkManager.makeRequest2(new GetBusinessesByCategoryRequest(Business.FOOD, "32.085300", "34.781768", 1, 10), new NetworkCallback<GetBusinessesByCategoryResponse>(){
//            @Override
//            public void onResponse2(boolean success, String errorDesc, ResponseObject2<GetBusinessesByCategoryResponse> response) {
//                super.onResponse2(success, errorDesc, response);
//                if (success){
//                    ToastUtil.toaster("success "+response.getData().get(0).getID(),true);
//                }
//            }
//        });
        ///=------------------------------------------------------------------------------------
        //search business
//        MyApp.networkManager.makeRequest(new SearchBusinessRequest(Business.FOOD,"pizza","32.085300", "34.781768",10),new NetworkCallback<SearchBusinessResponse>(){
//            @Override
//            public void onResponse(boolean success, String errorDesc, ResponseObject<SearchBusinessResponse> response) {
//                super.onResponse(success, errorDesc, response);
//                if (success){
//                    ToastUtil.toaster("success"+response.getData().getBusinesses().get(0).getID(),true);
//                }
//            }
//        });
//----------------------------------------------------------------------------------
  //applogin
       MyApp.networkManager.makeRequest(new ApplicationLoginRequest("10209284450852634"),
               new NetworkCallback<ApplicationLoginResponse>() {
                   @Override
                   public void onResponse(boolean success, String errorDesc, ResponseObject<ApplicationLoginResponse> response) {
                       super.onResponse(success, errorDesc, response);
                       if(success){
                           MyApp.userManager.setAppuser(response.getData().getAppuser());
                           MyApp.userManager.save();


                       }else {
                           ToastUtil.toaster(errorDesc,true);
                       }
                   }
               });
     */
}
