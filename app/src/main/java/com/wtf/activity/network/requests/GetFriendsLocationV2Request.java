package com.wtf.activity.network.requests;

import com.wtf.activity.network.ApiInterface;
import com.wtf.activity.network.response.GetAppuserFriendsLocationResponse;
import com.wtf.activity.network.response.GetFriendsLocationV2Response;
import com.wtf.activity.network.response.ResponseObject;

import org.json.JSONArray;

import java.util.ArrayList;

import retrofit.Callback;

/**
 * Created by Niv on 4/7/2016.
 */
public class GetFriendsLocationV2Request extends RequestObject<GetFriendsLocationV2Response> {

    private ArrayList<String> FbIdS;
//    private ArrayList<Long> longFbIdS=new ArrayList<>();

    public GetFriendsLocationV2Request(ArrayList<String> fbIdS) {
        FbIdS = fbIdS;
//        for (String id:fbIdS){
//            long numId = Long.parseLong(id);
//            longFbIdS.add(numId);
//        }

    }

    @Override
    protected void execute(ApiInterface apiInterface, Callback<ResponseObject<GetFriendsLocationV2Response>> callback) {
        JSONArray friendsJsonArray = new JSONArray(FbIdS);
//        JSONArray friendsJsonArray = new JSONArray(FbIdS);
        System.out.println("--->  "+friendsJsonArray);
            apiInterface.getFriendsLocationV2(friendsJsonArray,callback);
    }

    @Override
    protected boolean showDefaultToastsOnError() {
        return true;
    }
}
