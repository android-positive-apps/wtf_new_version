package com.wtf.activity.network.requests;

import com.wtf.activity.network.ApiInterface;
import com.wtf.activity.network.response.ResponseObject;
import com.wtf.activity.network.response.ResponseObject2;
import com.wtf.activity.network.response.SetCouponUsedResponse;

import retrofit.Callback;

/**
 * Created by Niv on 4/12/2016.
 */
public class SetCouponUsedRequest extends RequestObject2<SetCouponUsedResponse> {

    private String AppuserID;
    private String Lat;
    private String CouponID;
    private String Lng;
    private String Password;


    public SetCouponUsedRequest(String appuserID, String lat, String couponID, String lng, String password) {
        AppuserID = appuserID;
        Lat = lat;
        CouponID = couponID;
        Lng = lng;
        Password = password;
    }

    @Override
    protected void execute(ApiInterface apiInterface, Callback<ResponseObject2<SetCouponUsedResponse>> callback) {
        apiInterface.setCouponUsed(AppuserID,CouponID,Lat,Lng,Password,callback);
    }

    @Override
    protected boolean showDefaultToastsOnError() {
        return true;
    }
}
