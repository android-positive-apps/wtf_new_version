package com.wtf.activity.network.response;

import com.wtf.activity.objects.CreditCard;

import java.util.ArrayList;

/**
 * Created by Niv on 3/7/2016.
 */
public class GetAppuserCCResponse {

    ArrayList<CreditCard> CreditCards;

    public ArrayList<CreditCard> getCreditCards() {
        return CreditCards;
    }
}
