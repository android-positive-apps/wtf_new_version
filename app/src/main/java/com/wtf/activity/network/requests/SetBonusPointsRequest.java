package com.wtf.activity.network.requests;

import android.content.Context;

import com.wtf.activity.main.MyApp;
import com.wtf.activity.network.ApiInterface;
import com.wtf.activity.network.response.ResponseObject;
import com.wtf.activity.network.response.SetBonusPointsResponse;

import retrofit.Callback;

/**
 * Created by Niv on 3/7/2016.
 */
public class SetBonusPointsRequest extends RequestObject<SetBonusPointsResponse>{

    private String appUserId= MyApp.userManager.getAppuser().getID();
    private String actionName;
    private int points;

    public SetBonusPointsRequest(String actionName, int points) {
        this.actionName = actionName;
        this.points = points;
    }

    @Override
    protected void execute(ApiInterface apiInterface, Callback<ResponseObject<SetBonusPointsResponse>> callback) {
        apiInterface.setBonusPoints(appUserId,points,actionName,callback);
    }

    @Override
    protected boolean showDefaultToastsOnError() {
        return true;
    }
}
