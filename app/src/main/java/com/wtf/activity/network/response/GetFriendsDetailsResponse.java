package com.wtf.activity.network.response;

import com.google.gson.annotations.SerializedName;
import com.wtf.activity.objects.Friend;

/**
 * Created by Niv on 3/27/2016.
 */
public class GetFriendsDetailsResponse {

    @SerializedName("Friend")
    private Friend friend;

    public Friend getFriend() {
        return friend;
    }
}
