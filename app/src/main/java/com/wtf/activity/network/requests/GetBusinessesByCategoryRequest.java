package com.wtf.activity.network.requests;

import com.wtf.activity.network.ApiInterface;
import com.wtf.activity.network.response.GetBusinessesByCategoryResponse;
import com.wtf.activity.network.response.ResponseObject2;

import retrofit.Callback;

/**
 * Created by Niv on 3/7/2016.
 */
public class GetBusinessesByCategoryRequest extends RequestObject2<GetBusinessesByCategoryResponse> {




    private String Category;
    private String Lat;
    private String Lng;
    private int Radius;
    private int numResults;

    public GetBusinessesByCategoryRequest(String category, String lat, String lng, int radius, int numResults) {
        Category = category;
        Lat = lat;
        Lng = lng;
        Radius = radius;
        this.numResults = numResults;
    }

    @Override
    protected void execute(ApiInterface apiInterface, Callback<ResponseObject2<GetBusinessesByCategoryResponse>> callback) {
        apiInterface.getBusinessesByCategory(Category,Lat,Lng,Radius,numResults,"Distance",callback);
    }

    @Override
    protected boolean showDefaultToastsOnError() {
        return true;
    }
}
