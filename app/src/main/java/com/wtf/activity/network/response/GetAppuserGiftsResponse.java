package com.wtf.activity.network.response;

import com.wtf.activity.objects.Appuser;
import com.wtf.activity.objects.AppuserGift;
import com.wtf.activity.objects.Gift;

import java.util.ArrayList;

/**
 * Created by Niv on 3/6/2016.
 */
public class GetAppuserGiftsResponse {

    private ArrayList<Gift> Gifts;

    public ArrayList<Gift> getGifts() {
        return Gifts;
    }
}
