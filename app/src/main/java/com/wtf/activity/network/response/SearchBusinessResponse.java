package com.wtf.activity.network.response;

import com.wtf.activity.objects.Business;

import java.util.ArrayList;

/**
 * Created by Niv on 3/7/2016.
 */
public class SearchBusinessResponse {
    ArrayList<Business> Businesses;

    public ArrayList<Business> getBusinesses() {
        return Businesses;
    }
}
