package com.wtf.activity.network.requests;

import com.wtf.activity.main.MyApp;
import com.wtf.activity.network.ApiInterface;
import com.wtf.activity.network.response.GetBusinessGiftsAndCouponsResponse;
import com.wtf.activity.network.response.ResponseObject;

import retrofit.Callback;

/**
 * Created by Niv on 3/7/2016.
 */
public class GetBusinessGiftsAndCouponsRequest extends RequestObject<GetBusinessGiftsAndCouponsResponse> {

    private String appuserid = MyApp.userManager.getAppuser().getID();
    private String businessid;

    public GetBusinessGiftsAndCouponsRequest(String businessid) {
        this.businessid = businessid;
    }

    @Override
    protected void execute(ApiInterface apiInterface, Callback<ResponseObject<GetBusinessGiftsAndCouponsResponse>> callback) {
            apiInterface.getBusinessGiftsAndCoupons(appuserid,businessid,callback);
    }

    @Override
    protected boolean showDefaultToastsOnError() {
        return true;
    }
}
