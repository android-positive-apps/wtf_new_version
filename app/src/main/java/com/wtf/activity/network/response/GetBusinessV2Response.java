package com.wtf.activity.network.response;

import com.google.gson.annotations.SerializedName;
import com.wtf.activity.objects.Business;
import com.wtf.activity.objects.CheckIn;
import com.wtf.activity.objects.Rating;
import com.wtf.activity.objects.SpecialOffer;

import java.util.ArrayList;

/**
 * Created by Niv on 3/28/2016.
 */
public class GetBusinessV2Response {

    @SerializedName("Business")
    private Business business;

    @SerializedName("Rating")
    private Rating rating;

    private ArrayList<CheckIn> CheckIns;
    private ArrayList<SpecialOffer> SpecialOffers;

    public Business getBusiness() {
        return business;
    }

    public ArrayList<CheckIn> getCheckIns() {
        return CheckIns;
    }

    public ArrayList<SpecialOffer> getSpecialOffers() {
        return SpecialOffers;
    }

    public Rating getRating() {
        return rating;
    }
}
