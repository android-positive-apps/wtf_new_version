/**
 * 
 */
package com.wtf.activity.fragments;



import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;

import com.wtf.activity.main.BaseActivity;
import com.wtf.activity.main.MyApp;
import com.wtf.activity.R;
import com.wtf.activity.database.tables.TableObserver;
import com.wtf.activity.util.AppUtil;
import com.wtf.activity.util.BackStackUtil;
import com.wtf.activity.util.ProgressDialogUtil;


/**
 * @author Nati Gabay
 *
 */
public class BaseFragment extends Fragment implements OnGlobalLayoutListener,TableObserver {

	protected String TAG = "lypeCycleLog";

	protected ProgressDialog progressDialog;
	public boolean fragIsOn;
	protected String screenName;

	public boolean onBackPressed () {
		return false;
	}



	public void showProgressDialog(String message) {
		this.progressDialog =
				ProgressDialogUtil.showProgressDialog(getActivity(), message);
	}

	public void showProgressDialog() {
		try {
			this.progressDialog =
					ProgressDialogUtil.showProgressDialog(getActivity(),
							getString(R.string.deafult_dialog_messgae));
		} catch (Exception e) {}
	}

	public void dismisProgressDialog() {
		ProgressDialogUtil.dismisDialog(this.progressDialog);
	}


	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onViewCreated(view, savedInstanceState);
		//TAG = getClass().getSimpleName() + "Log";
		Log.d(TAG, "ovViewCreated");
		view.getViewTreeObserver().addOnGlobalLayoutListener(this);
		fragIsOn = true;
		;
		BackStackUtil.addToBackStack(getClass().getSimpleName());
		AppUtil.setTextFonts(getActivity(), view);
		if (getBaseActivity() != null){
			getBaseActivity().setCurrentFragment(this);
		}

		MyApp.addTablesObservers(this);


	}


	@Override
	public View onCreateView(LayoutInflater inflater,
							 @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		Log.e(TAG, getClass().getSimpleName() + " onCreateView");
		return super.onCreateView(inflater, container, savedInstanceState);
	}


	@Override
	public void onResume() {
		super.onResume();
		Log.i(TAG, getClass().getSimpleName() + " onResume");
	}


	@Override
	public void onStart() {
		super.onStart();
		Log.i(TAG, getClass().getSimpleName() + " onStart");
	}




	@Override
	public void onDestroy() {
		super.onDestroy();
		Log.e("onDestroyLog", "onDestroy");
		fragIsOn = false;
		BackStackUtil.removeFromBackStack(getClass().getSimpleName());
		MyApp.removeTablesObservers(this);
	}



	@SuppressLint("NewApi")
	@Override
	public void onGlobalLayout() {
		try {
			getView().getViewTreeObserver().removeOnGlobalLayoutListener(this);
			onViewRendered(getView());
		} catch (Exception e) {} catch (Error e){}

	}

	public void onViewRendered(View view){
		Log.d(TAG, getClass().getSimpleName() + " onViewRendered");
	}


	public BaseActivity getBaseActivity() {
		BaseActivity activity = null;

		try {
			activity = (BaseActivity) getActivity();
		} catch (ClassCastException e) {
			e.printStackTrace();
		}

		return activity;
	}

	public void onNewIntent(Intent intent){

	}

	@Override
	public void onTableChanged(String tableName, int action) {

	}
	
}