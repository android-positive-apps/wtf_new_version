package com.wtf.activity.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.share.model.AppInviteContent;
import com.facebook.share.widget.AppInviteDialog;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.sromku.simple.fb.SimpleFacebook;
import com.sromku.simple.fb.entities.Profile;
import com.sromku.simple.fb.listeners.OnInviteListener;
import com.sromku.simple.fb.listeners.OnProfileListener;
import com.sromku.simple.fb.utils.Attributes;
import com.sromku.simple.fb.utils.PictureAttributes;
import com.wtf.activity.R;
import com.wtf.activity.adapters.CheckedInPlaceArrayAdapter;
import com.wtf.activity.adapters.CreditCardsArrayAdapter;
import com.wtf.activity.adapters.FriendsGridArrayAdapter;
import com.wtf.activity.adapters.FriendsGridCursorAdapter;
import com.wtf.activity.adapters.RatedPlacesAdapter;
import com.wtf.activity.database.tables.TableCheckIns;
import com.wtf.activity.database.tables.TableFriends;
import com.wtf.activity.main.GiftsActivity;
import com.wtf.activity.main.MapActivity;
import com.wtf.activity.main.MyApp;
import com.wtf.activity.main.ProfileActivity;
import com.wtf.activity.network.NetworkCallback;
import com.wtf.activity.network.requests.AddAppuserCCRequest;
import com.wtf.activity.network.requests.GetAppuserCCRequest;
import com.wtf.activity.network.requests.RemoveAppuserCcRequest;
import com.wtf.activity.network.requests.UpdateAppuserPhoneRequest;
import com.wtf.activity.network.requests.UpdateAppuserRequest;
import com.wtf.activity.network.response.AddAppuserCCResponse;
import com.wtf.activity.network.response.GetAppuserCCResponse;
import com.wtf.activity.network.response.RemoveAppuserCCResponse;
import com.wtf.activity.network.response.ResponseObject;
import com.wtf.activity.network.response.ResponseObject2;
import com.wtf.activity.network.response.UpdateAppuserResponse;
import com.wtf.activity.objects.Appuser;
import com.wtf.activity.objects.Business;
import com.wtf.activity.objects.CheckIn;
import com.wtf.activity.objects.CreditCard;
import com.wtf.activity.objects.Friend;
import com.wtf.activity.ui.TwoWayAdapterView;
import com.wtf.activity.util.AppUtil;
import com.wtf.activity.util.BitmapUtil;
import com.wtf.activity.util.DateUtil;
import com.wtf.activity.util.FragmentsUtil;
import com.wtf.activity.util.ImageLoader;
import com.wtf.activity.util.TextUtil;
import com.wtf.activity.util.ToastUtil;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.IllegalFormatCodePointException;
import java.util.List;
import java.util.Set;
import java.util.TimerTask;
import java.util.regex.Pattern;

import bolts.AppLinks;

/**
 * Created by Niv on 2/29/2016.
 */
public class ProfileFragment extends BaseFragment implements View.OnClickListener {
    private TextView friendsTextView;
    private TextView ratedPlacesTextView;
    private ImageView profileImageView;
    private TextView profileNameTv;
    private TextView dateRegTv;
    private TextView earnedPointsTv;
    private ImageView inviteFriendsPlusButton;
    private GridView friendsGrid;
    private ListView checkedInListProfile;
    private TextView myCheckInsTextView;
    private ListView ratedPlacesListProfile;
    private TextView EditDetailsTv;
    private TextView creditKindTv;
    private ImageView addCreditCardButton;
    private SimpleFacebook mSimpleFacebook;
    private ImageView showMoreFriendsImage;
    private FriendsGridCursorAdapter friendsGridCursorAdapter;
    private TextView moreTv;
    private LinearLayout moreLinear;
    private ArrayList<CreditCard> allCreditCards;
    private GridView cCgrid;
    private ArrayList<CreditCard> ccs = new ArrayList<>();
    private CreditCardsArrayAdapter creditCardsArrayAdapter;
    private TextView phoneTv;
    private TextView editTv;
    private LinearLayout phoneLinear;
    private EditText phoneEt;
    private Button insertPhoneB;
    private CallbackManager sCallbackManager;
    private Tracker mTracker;
    private LinearLayout pointsLinear;

    public ProfileFragment() {
        // TODO Auto-generated constructor stub
    }

    public static ProfileFragment newInstance() {
        ProfileFragment instance = new ProfileFragment();
        return instance;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        int layoutRes = R.layout.fragment_profile;
        View rootView = inflater.inflate(layoutRes, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        InitAnalytic();
        findViews(view);
        setListeners();
        setDetails();
        setAdapters();

    }

    private void setAdapters() {
        setRatedPlacesAdapter();
        setFriendsAdapter();
        setCheckinsAdapter();
        setCreditCardsAdapter();
    }

    private void setCreditCardsAdapter() {

        getAppuserCcs();
//        ArrayList<CreditCard> ccs= MyApp.userManager.getCreditCards();

    }

    private void getAppuserCcs() {
        cCgrid.setFocusable(false);
        MyApp.networkManager.makeRequest(new GetAppuserCCRequest(), new NetworkCallback<GetAppuserCCResponse>() {
            @Override
            public void onResponse(boolean success, String errorDesc, ResponseObject<GetAppuserCCResponse> response) {
                super.onResponse(success, errorDesc, response);
                if (success) {
                        if (isAdded() && getActivity()!=null) {
                            ccs = response.getData().getCreditCards();
                            if (ccs != null) {
                                ccs = removeReapitedItems(ccs);
                                creditCardsArrayAdapter = new CreditCardsArrayAdapter(getActivity(), ccs);
                                cCgrid.setAdapter(creditCardsArrayAdapter);
                                creditCardsArrayAdapter.notifyDataSetChanged();

                            }
                        }
                }
            }
        });
    }

//    @Override
//    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
////        super.onCreateContextMenu(menu, v, menuInfo);
//        if (v.getId() == R.id.cCgrid) {
//            GridView gv = (GridView) v;
//            AdapterView.AdapterContextMenuInfo acmi = (AdapterView.AdapterContextMenuInfo) menuInfo;
//            CreditCard cc = (CreditCard) gv.getItemAtPosition(acmi.position);
//
//            menu.add(Menu.NONE,1,Menu.NONE,getActivity().getResources().getString(R.string.remove_cc));
//            removeCC(cc);
//        }
//    }
//
//    @Override
//    public boolean onContextItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//            case 1:
//                AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
//                Log.d(TAG, "removing item pos=" + info.position);
//
//                return true;
//            default:
//                return super.onContextItemSelected(item);
//        }
//    }
            @Override
            public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
                super.onCreateContextMenu(menu, v, menuInfo);

                //inflate the context menu
                getActivity().getMenuInflater().inflate(R.menu.menu_cc,menu);
            }

                @Override
                public boolean onContextItemSelected(MenuItem item) {

                    AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
//                    long id = info.id;

                    // which action?
                    switch (item.getItemId()) {
                        case R.id.action_remove:
                            removeCC(ccs.get(info.position));
                            return true;

                        default:
                            return super.onContextItemSelected(item);
                    }
                }


    private void removeCC(final CreditCard cc) {
        MyApp.networkManager.makeRequest2(new RemoveAppuserCcRequest(cc.getID()),new NetworkCallback<RemoveAppuserCCResponse>(){
            @Override
            public void onResponse2(boolean success, String errorDesc, ResponseObject2<RemoveAppuserCCResponse> response) {
                super.onResponse2(success, errorDesc, response);
                if (success){
//                    creditCardsArrayAdapter.clear();
//                    setCreditCardsAdapter();

                    ccs.remove(cc);
                    creditCardsArrayAdapter.notifyDataSetChanged();
                }
            }
        });
    }

    private ArrayList<CreditCard> removeReapitedItems(ArrayList<CreditCard> ccs) {
        final ArrayList<CreditCard> newList= new ArrayList<>();

        Set<String> ids = new HashSet<String>();

        for( CreditCard item : ccs ) {
            if( ids.add( item.getName() )) {
                newList.add( item );
            }
        }
        return newList;
    }

    private void setCheckinsAdapter() {
        checkedInListProfile.setFocusable(false);
        ArrayList<Business> businesses = MyApp.tableCheckIns.readAll(TableCheckIns.TAG_ID+" desc");
        if (businesses!=null) {
            ArrayList<Business> fewBusinesses = new ArrayList<>();
            if (businesses.size() >= 4) {
                for (int i = 0; i < 4; i++) {
                    fewBusinesses.add(businesses.get(i));
                }
            } else if (businesses.size() > 0) {
                fewBusinesses = businesses;
            }
            resizeList(fewBusinesses.size(),checkedInListProfile);
            CheckedInPlaceArrayAdapter checkedInPlaceArrayAdapter = new CheckedInPlaceArrayAdapter(getActivity(), fewBusinesses);
            checkedInListProfile.setAdapter(checkedInPlaceArrayAdapter);

        }else {
            resizeList(0,checkedInListProfile);
        }
    }

    private void resizeList(int size, ListView listView) {
        float num = 0;

        switch (size) {
            case 0:
                num = 0;
                break;
            case 1:
                num = AppUtil.convertDpToPixel(100, getActivity());
                break;
            case 2:
                num = AppUtil.convertDpToPixel(200, getActivity());
                break;
            case 3:
                num = AppUtil.convertDpToPixel(300, getActivity());
                break;
            case 4:
                num = AppUtil.convertDpToPixel(400, getActivity());
                break;
        }
            ViewGroup.LayoutParams params = listView.getLayoutParams();
            params.height = (int) num;
            listView.setLayoutParams(params);

    }

    private void setFriendsAdapter() {
        //friends grid:
        friendsGrid.setFocusable(false);

//        friendsGridCursorAdapter = new FriendsGridCursorAdapter(getActivity(),null);
        ArrayList<Friend> friends = MyApp.tableFriends.readAll(TableFriends.TAG_FIRST_NAME+" asc");
        int size = friends.size();
        ArrayList<Friend> list = new ArrayList<>();
        if (friends.size()>8) {
            for (int i = 0; i < 7; i++) {
                list.add(friends.get(i));

            }
            list.add(new Friend("-1","show more",friends.size()-7+""));
        }else if (friends.size()==8){
            for (int i = 0; i < 8; i++) {
                list.add(friends.get(i));

            }
        }
        else if (friends.size()>0){
            list=friends;
        }

        FriendsGridArrayAdapter friendsGridArrayAdapter =
                new FriendsGridArrayAdapter(getActivity(),list);

        friendsGrid.setAdapter(friendsGridArrayAdapter);
        String friendsCount;
        if (size==1)
            friendsCount = size +" "+ getResources().getString(R.string.friend);
        else
            friendsCount = size +" "+ getResources().getString(R.string.friends);

        friendsTextView.setText(friendsCount);
        final ArrayList<Friend> finalList = list;
        friendsGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Friend friend = finalList.get(position);
                if (friend.getFBID().equals("-1")){
                    FragmentsUtil.openFragmentRghitToLeft(getFragmentManager(),FriendsListFragment.newInstance(),R.id.profileContainer);
                }else {

                     if (!friend.getLat().equals("")&&!friend.getLng().equals("")) {
                        Intent i = new Intent(getActivity(), MapActivity.class);
                        i.setAction(MapActivity.ACTION_LOCATE_FRIEND);
                        i.putExtra("Lat", friend.getLat());
                        i.putExtra("Lng", friend.getLng());
                        getActivity().startActivity(i);
                        getActivity().finish();
                    }else {
                        ToastUtil.toaster(getActivity().getResources().getString(R.string.unable_locate_friend), false);
                    }
                    /**
                     * 3.5.16 change:
                     **/
//                    try {
//                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.facebook.com/"+friend.getFBID()));
//                        startActivity(intent);
//                    } catch(Exception e) {
////                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.facebook.com/"+friend.getFirstName()+"."+friend.getLastName())));
//                    ToastUtil.toaster(getActivity().getResources().getString(R.string.unable_locate_friend), false);
//
//                    }


                }
            }
        });
//        getActivity().getLoaderManager().initLoader(0, null, this);
//        Cursor c = MyApp.tableFriends.readCursor()
    }

    private void setRatedPlacesAdapter() {
        ratedPlacesListProfile.setFocusable(false);
        final ArrayList<Business> ratedPlaces = MyApp.userManager.getRatedBusiness();
        if (ratedPlaces!=null){

            if (ratedPlaces.size()>1)
            Collections.reverse(ratedPlaces);

            RatedPlacesAdapter ratedPlacesAdapter = new RatedPlacesAdapter(getActivity(),ratedPlaces);
            ratedPlacesListProfile.setAdapter(ratedPlacesAdapter);
            resizeList(ratedPlaces.size(),ratedPlacesListProfile);


//            ratedPlacesListProfile.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//                @Override
//                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                    Business b = ratedPlaces.get(position);
//                    openBusinessActivity(b);
//                }
//            });

        }else {
            resizeList(0,ratedPlacesListProfile);
        }
    }

    private void openBusinessActivity(Business b) {
        MyApp.appManager.setTempBusiness(b);
        Intent i = new Intent(getActivity(),MapActivity.class);
        i.setAction(BusinessPageFragment.ACTION_PROFILE);
        startActivity(i);
    }

    private void setDetails() {
        Appuser appuser = MyApp.userManager.getAppuser();
        String name= appuser.getFirstName()+" "+appuser.getLastName();
        profileNameTv.setText(name);
        earnedPointsTv.setText(appuser.getPoints() + "");
//        dateRegTv.setText(appuser.getCreated());
        String date= appuser.getCreated();
//        DateUtil.getDateDescription()
        try {
            String[] splited = date.split("\\s+");
            String[] dateSplit = splited[0].split(Pattern.quote("-"));
            String newDate = dateSplit[2]+"/"+dateSplit[1]+"/"+dateSplit[0];
            dateRegTv.setText(newDate);
        } catch (Exception e) {
            e.printStackTrace();
            dateRegTv.setText(date);
        }

//        System.out.println("--->  time: "+ DateUtil.getDateAsStringByMilli(DateUtil.FORMAT_JUST_DATE,d.getMillis()));
//        System.out.println("--->  time: "+d.getYear()+"/"+d.monthOfYear());


        int size = MyApp.tableFriends.readAll("").size();
//        if (size>7)
//            moreLinear.setVisibility(View.VISIBLE);

        String sizeMore = size-7+" "+getResources().getString(R.string.more);
        moreTv.setText(sizeMore);
        phoneTv.setText(appuser.getPhone());
//        profileImageView.setImageResource(MyApp.userManager.getUserProfile().getPicture());
        //creditCards:
//        allCreditCards= new ArrayList<>();
//        for (int i=1;i<=6;i++){
//            CreditCard c = new CreditCard(i,"","");
//        }
    }

    private void getProfilePhoto() {

        Bitmap bit = BitmapUtil.loadImageFromStorage(MyApp.appManager.getImagePath(), MyApp.userManager.getAppuser().getFBID());
        if (bit!=null)
        profileImageView.setImageBitmap(bit);
        else {
            ImageLoader.ByUrl(MyApp.userManager.getUserProfile().getPicture(), profileImageView);
        }
//        OnProfileListener onProfileListener = new OnProfileListener() {
//            @Override
//            public void onComplete(Profile profile) {
//                Log.i(TAG, "My profile id = " + profile.getId());
//
//                MyApp.userManager.setUserProfile(profile);
//                MyApp.userManager.save();
//                ImageLoader.ByUrl(profile.getPicture(), profileImageView);
//                System.out.println("--->  "+profile.getPicture());
//
//            }
//
//
//    /*
//     * You can override other methods here:
//     * onThinking(), onFail(String reason), onException(Throwable throwable)
//     */
//        };
//        PictureAttributes pictureAttributes = Attributes.createPictureAttributes();
//        pictureAttributes.setHeight(500);
//        pictureAttributes.setWidth(500);
//        pictureAttributes.setType(PictureAttributes.PictureType.SQUARE);
//
//        Profile.Properties properties = new Profile.Properties.Builder()
//                .add(Profile.Properties.PICTURE, pictureAttributes)
//                .build();
//        mSimpleFacebook.getProfile(properties, onProfileListener);

    }

    @Override
    public void onResume() {
        super.onResume();
        mSimpleFacebook = SimpleFacebook.getInstance(getActivity());
        getProfilePhoto();
        mTracker.setScreenName("Profile screen");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());


    }

    private void InitAnalytic(){
        MyApp application = (MyApp) getActivity().getApplication();
        mTracker = application.getDefaultTracker();
    }


    private void findViews(View view) {
        //text
        profileNameTv=(TextView)view.findViewById(R.id.profileNameTv);
        friendsTextView=(TextView)view.findViewById(R.id.friendsTextView);
        ratedPlacesTextView=(TextView)view.findViewById(R.id.ratedPlacesTextView);
        dateRegTv=(TextView)view.findViewById(R.id.dateRegTv);
        earnedPointsTv=(TextView)view.findViewById(R.id.earnedPointsTv);
        myCheckInsTextView=(TextView)view.findViewById(R.id.myCheckInsTextView);
        editTv  =(TextView)view.findViewById(R.id.editTv);
        phoneTv =(TextView)view.findViewById(R.id.phoneTv);
//        EditDetailsTv=(TextView)view.findViewById(R.id.EditDetailsTv);
//        creditKindTv=(TextView)view.findViewById(R.id.creditKindTv);
        moreTv=(TextView)view.findViewById(R.id.moreTv);
        //linear:
        phoneLinear=(LinearLayout)view.findViewById(R.id.phoneLinear);
        pointsLinear=(LinearLayout)view.findViewById(R.id.pointsLinear);

        //editText:
        phoneEt=(EditText)view.findViewById(R.id.phoneEt);

        //button:
        insertPhoneB = (Button)view.findViewById(R.id.insertPhoneB);
        //image
        profileImageView=(ImageView)view.findViewById(R.id.profileImageView);
        inviteFriendsPlusButton=(ImageView)view.findViewById(R.id.inviteFriendsPlusButton);
        addCreditCardButton=(ImageView)view.findViewById(R.id.addCreditCardButton);
        showMoreFriendsImage=(ImageView)view.findViewById(R.id.showMoreFriendsImage);

        //grid
        friendsGrid=(GridView)view.findViewById(R.id.friendsGrid);
        cCgrid=(GridView)view.findViewById(R.id.cCgrid);
        //list
        checkedInListProfile=(ListView)view.findViewById(R.id.checkedInListProfile);
        ratedPlacesListProfile=(ListView)view.findViewById(R.id.ratedPlacesListProfile);

        //friends:
        showMoreFriendsImage = (ImageView)view.findViewById(R.id.showMoreFriendsImage);
        moreLinear = (LinearLayout)view.findViewById(R.id.moreLinear);

    }
    private void setListeners() {
        friendsTextView.setOnClickListener(this);
        ratedPlacesTextView.setOnClickListener(this);
//        EditDetailsTv.setOnClickListener(this);
        profileImageView.setOnClickListener(this);
        inviteFriendsPlusButton.setOnClickListener(this);
        addCreditCardButton.setOnClickListener(this);
        myCheckInsTextView.setOnClickListener(this);
        showMoreFriendsImage.setOnClickListener(this);
        editTv.setOnClickListener(this);
        insertPhoneB.setOnClickListener(this);
//        earnedPointsTv.setOnClickListener(this);
        pointsLinear.setOnClickListener(this);
        registerForContextMenu(cCgrid);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.showMoreFriendsImage:
                FragmentsUtil.openFragmentRghitToLeft(getFragmentManager(), FriendsListFragment.newInstance(), R.id.profileContainer);
                break;
            case R.id.myCheckInsTextView:
                if (MyApp.tableCheckIns.readAll("").size()>0)
                openCheckInsListDialog();
                else{
                    ToastUtil.toaster(getResources().getString(R.string.no_check_ins),false);
                }
                break;
            case R.id.inviteFriendsPlusButton:
//                inviteFrineds();
                openDialogInvite(getActivity());
                break;
            case R.id.addCreditCardButton:
                openCreditCardDialog();
                break;
            case R.id.friendsTextView:
                FragmentsUtil.openFragmentRghitToLeft(getFragmentManager(), FriendsListFragment.newInstance(), R.id.profileContainer);

                break;
            case R.id.editTv:
                Handler h =new Handler();

                Runnable r = new Runnable() {
                    @Override
                    public void run() {
                        phoneLinear.setVisibility(View.VISIBLE);
                        YoYo.with(Techniques.Shake).duration(800).delay(50).playOn(phoneLinear);

                    }
                };
                h.postDelayed(r,50);
                break;
            case R.id.insertPhoneB:
                if (TextUtil.baseFieldsValidation(phoneEt)){

                    MyApp.networkManager.makeRequest(new UpdateAppuserPhoneRequest(phoneEt.getText().toString()),new NetworkCallback<UpdateAppuserResponse>(){
                        @Override
                        public void onResponse(boolean success, String errorDesc, ResponseObject<UpdateAppuserResponse> response) {
                            super.onResponse(success, errorDesc, response);
                            if (success){
                                phoneTv.setText(phoneEt.getText().toString());
                                MyApp.userManager.getAppuser().setPhone(phoneEt.getText().toString());
                                MyApp.userManager.save();
                            }
                            Handler h2 =new Handler();
                            YoYo.with(Techniques.FadeOutUp).duration(800).playOn(phoneLinear);

                            Runnable r2 = new Runnable() {
                                @Override
                                public void run() {
                                    phoneLinear.setVisibility(View.GONE);


                                }
                            };
                            h2.postDelayed(r2,600);


                        }
                    });



                }
                break;
            case R.id.earnedPointsTv:
//                getActivity().finish();
//                Intent i = new Intent(getActivity(), GiftsActivity.class);
//                startActivity(i);

                break;
            case R.id.pointsLinear:
                getActivity().finish();
                Intent i = new Intent(getActivity(), GiftsActivity.class);
                startActivity(i);
                break;



        }
    }
//    private void inviteFrineds() {
//
//
//
//        OnInviteListener onInviteListener = new OnInviteListener() {
//            @Override
//            public void onException(Throwable throwable) {
//
//            }
//
//            @Override
//            public void onFail(String reason) {
//
//            }
//
//            @Override
//            public void onComplete(List<String> invitedFriends, String requestId) {
//                Log.i(TAG, "Invitation was sent to " + invitedFriends.size() + " users with request id " + requestId);
//            }
//
//            @Override
//            public void onCancel() {
//                Log.i(TAG, "Canceled the dialog");
//            }
//
//    /*
//     * You can override other methods here:
//     * onFail(String reason), onException(Throwable throwable)
//     */
//        };
//
////        mSimpleFacebook.invite(getActivity().getString(R.string.invite_messege), onInviteListener, "https://play.google.com/store/apps/details?id=com.wtf.activity");
//
//
//
//        String appLinkUrl, previewImageUrl;
//
//        appLinkUrl = "https://fb.me/1187254571298955";
//        previewImageUrl = "http://www.mypushserver.com/dev/bmm/images/logo.png";
//
//        if (AppInviteDialog.canShow()) {
//            AppInviteContent content = new AppInviteContent.Builder()
//                    .setApplinkUrl(appLinkUrl)
//                    .setPreviewImageUrl(previewImageUrl)
//                    .build();
//            AppInviteDialog.show(this, content);
//
//        }
//
//
////        mSimpleFacebook.invite();
//
//    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        sCallbackManager.onActivityResult(requestCode, resultCode, data);
    }

    public void openDialogInvite(final Activity activity) {
        String AppURl = "https://fb.me/1188990067792072";  //Generated from //fb developers

        String previewImageUrl = "http://www.mypushserver.com/dev/bmm/images/logo.png";

        sCallbackManager = CallbackManager.Factory.create();

        if (AppInviteDialog.canShow()) {
            AppInviteContent content = new AppInviteContent.Builder()
                    .setApplinkUrl(AppURl)
                    .setPreviewImageUrl(previewImageUrl)
                    .build();

            AppInviteDialog appInviteDialog = new AppInviteDialog(this);
            appInviteDialog.registerCallback(sCallbackManager,
                    new FacebookCallback<AppInviteDialog.Result>() {

                        @Override
                        public void onSuccess(AppInviteDialog.Result result) {
                            System.out.println("Invitation onSuccess");
                        }

                        @Override
                        public void onCancel() {
                            System.out.println("Invitation on cancel");

                        }

                        @Override
                        public void onError(FacebookException e) {
                            System.out.println("Invitation Error"+e.getMessage());

                        }
                    });

            appInviteDialog.show(this, content);
        }

    }
    private void openCheckInsListDialog() {
        LayoutInflater factory = LayoutInflater.from(getContext());
        final View deleteDialogView = factory.inflate(
                R.layout.user_check_ins_view_dialog, null);
        final AlertDialog deleteDialog = new AlertDialog.Builder(getContext()).create();
        deleteDialog.setView(deleteDialogView);
        ListView checkinsListViewDialog = (ListView) deleteDialogView.findViewById(R.id.checkinsListViewDialog);

        deleteDialogView.findViewById(R.id.x_iconMycheckin_dialog).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteDialog.dismiss();
            }
        });
        ArrayList<Business> businesses = MyApp.tableCheckIns.readAll(TableCheckIns.TAG_ID+" desc");
        if (businesses.size()>0){
            CheckedInPlaceArrayAdapter checkedInPlaceArrayAdapter = new CheckedInPlaceArrayAdapter(getActivity(), businesses);

            checkinsListViewDialog.setAdapter(checkedInPlaceArrayAdapter);
        }




        deleteDialog.show();
    }

    private void openCreditCardDialog() {
        LayoutInflater factory = LayoutInflater.from(getContext());
        final View deleteDialogView = factory.inflate(
                R.layout.credit_card_dialog, null);
        final AlertDialog deleteDialog = new AlertDialog.Builder(getContext()).create();
        deleteDialog.setView(deleteDialogView);
        ImageView credit1 = (ImageView)deleteDialogView.findViewById(R.id.credit1);
        ImageView credit2 = (ImageView)deleteDialogView.findViewById(R.id.credit2);
        ImageView credit3 = (ImageView)deleteDialogView.findViewById(R.id.credit3);
        ImageView credit4 = (ImageView)deleteDialogView.findViewById(R.id.credit4);
        ImageView credit5 = (ImageView)deleteDialogView.findViewById(R.id.credit5);
        ImageView credit6 = (ImageView)deleteDialogView.findViewById(R.id.credit6);

        credit1.setOnClickListener(new creditCardClickListener(deleteDialog));
        credit2.setOnClickListener(new creditCardClickListener(deleteDialog));
        credit3.setOnClickListener(new creditCardClickListener(deleteDialog));
//        credit4.setOnClickListener(new creditCardClickListener(deleteDialog));
//        credit5.setOnClickListener(new creditCardClickListener(deleteDialog));
//        credit6.setOnClickListener(new creditCardClickListener(deleteDialog));



        deleteDialog.show();
    }

    private class  creditCardClickListener implements View.OnClickListener {
        private AlertDialog dialog;
        public creditCardClickListener(AlertDialog Dialog) {
            this.dialog=Dialog;
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.credit1:
                    makeAddCCrequest(1+"");
                    dialog.dismiss();
                    break;
                case R.id.credit2:
                    makeAddCCrequest(2+"");
                    dialog.dismiss();

                    break;
                case R.id.credit3:
                    makeAddCCrequest(3+"");
                    dialog.dismiss();

                    break;
                case R.id.credit4:
//                    makeAddCCrequest(4+"");

                    break;
                case R.id.credit5:
                    break;
                case R.id.credit6:
                    break;
            }
        }
    }

    private void makeAddCCrequest(final String s) {
        MyApp.networkManager.makeRequest2(new AddAppuserCCRequest(s),new NetworkCallback<AddAppuserCCResponse>(){
            @Override
            public void onResponse2(boolean success, String errorDesc, ResponseObject2<AddAppuserCCResponse> response) {
                super.onResponse2(success, errorDesc, response);
                if (success) {
                    MyApp.userManager.getCreditCards().add(new CreditCard(Integer.parseInt(s),"",""));
                    MyApp.userManager.save();
                    getAppuserCcs();
                }
            }
        });
    }
}