package com.wtf.activity.fragments;

import android.app.LoaderManager;
//import android.app.LoaderManager;
import android.content.Context;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.content.Loader;

//import android.support.v4.app.LoaderManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.FilterQueryProvider;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
//import android.database.Cursor;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.sromku.simple.fb.SimpleFacebook;
import com.sromku.simple.fb.listeners.OnInviteListener;
import com.wtf.activity.R;
import com.wtf.activity.adapters.FriendListFragmentCursorAdapter;
import com.wtf.activity.database.SelectionBuilder;
import com.wtf.activity.database.tables.TableFriends;
import com.wtf.activity.main.MapActivity;
import com.wtf.activity.main.MyApp;
import com.wtf.activity.main.ProfileActivity;
import com.wtf.activity.objects.Friend;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Niv on 2/29/2016.
 */
public class FriendsListFragment extends BaseFragment implements View.OnClickListener, LoaderManager.LoaderCallbacks<Cursor>, TextWatcher {

    private EditText searchFriendsEditText;
    private TextView searchFriendsTextView;
    private ListView friendsList;
    private ImageView addFriendsButton;
    private FriendListFragmentCursorAdapter adapter;
    private String serchString="";
    private SimpleFacebook mSimpleFacebook;
    private Tracker mTracker;

    public FriendsListFragment() {
        // TODO Auto-generated constructor stub
    }

    public static FriendsListFragment newInstance() {
        FriendsListFragment instance = new FriendsListFragment();
        return instance;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        int layoutRes = R.layout.fragment_friends_list;
        View rootView = inflater.inflate(layoutRes, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        InitAnalytic();
        findViews(view);
        setListeners();
        setAdapter();
        setDetails();
    }


    @Override
    public void onResume() {
        super.onResume();
        mSimpleFacebook = SimpleFacebook.getInstance(getActivity());
        mTracker.setScreenName(/*"Image~" +*/ "FriendsList screen");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

    }
    private void InitAnalytic(){
        MyApp application = (MyApp) getActivity().getApplication();
        mTracker = application.getDefaultTracker();
    }


    private void findViews(View view) {
        searchFriendsEditText= (EditText)view.findViewById(R.id.searchFriendsEditText);
        searchFriendsTextView= (TextView)view.findViewById(R.id.searchFriendsTextView);
        friendsList= (ListView)view.findViewById(R.id.friendsList);
        addFriendsButton= (ImageView)view.findViewById(R.id.addFriendsButton);
    }
    private void setListeners() {
        searchFriendsTextView.setOnClickListener(this);
        addFriendsButton.setOnClickListener(this);
        searchFriendsEditText.addTextChangedListener(this);
        searchFriendsEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {

                    View view = getActivity().getCurrentFocus();
                    if (view != null) {
                        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    }
                    return true;
                }
                return false;
            }
        });

    }
    private void setAdapter() {
         adapter = new FriendListFragmentCursorAdapter(getActivity(),null);
        friendsList.setAdapter(adapter);
        getActivity().getLoaderManager().initLoader(0, null, this);
        adapter.setFilterQueryProvider(new FilterQueryProvider() {
            @Override
            public Cursor runQuery(CharSequence constraint) {
                serchString = constraint.toString();
                getActivity().getLoaderManager().restartLoader(0, null, FriendsListFragment.this);
                return null;
            }
        });
    }

    private void setDetails() {
        ArrayList<Friend> friends = MyApp.tableFriends.readAll(TableFriends.TAG_FIRST_NAME+" asc");
        int size = friends.size();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.addFriendsButton:

                inviteFrineds();
                break;
            case R.id.searchFriendsTextView:
                View view = getActivity().getCurrentFocus();
                if (view != null) {
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }

                break;

        }
    }

    private void inviteFrineds() {
        OnInviteListener onInviteListener = new OnInviteListener() {
            @Override
            public void onException(Throwable throwable) {

            }

            @Override
            public void onFail(String reason) {

            }

            @Override
            public void onComplete(List<String> invitedFriends, String requestId) {
                Log.i(TAG, "Invitation was sent to " + invitedFriends.size() + " users with request id " + requestId);
            }

            @Override
            public void onCancel() {
                Log.i(TAG, "Canceled the dialog");
            }

    /*
     * You can override other methods here:
     * onFail(String reason), onException(Throwable throwable)
     */
        };

        mSimpleFacebook.invite(getActivity().getResources().getString(R.string.invite_messege), onInviteListener,"https://play.google.com/store/apps/details?id=com.wtf.activity");

    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        String selectionBuilder= new SelectionBuilder().where(TableFriends.TAG_FIRST_NAME).like(serchString).build();
        return MyApp.tableFriends.readLouderCursor(selectionBuilder,TableFriends.TAG_FIRST_NAME + " ASC");
    }


    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        adapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        adapter.swapCursor(null);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        serchString=s.toString();
        if (count>0){
            adapter.getFilter().filter(serchString);

        }else {
            adapter.getFilter().filter("");

        }
    }

    @Override
    public void afterTextChanged(Editable s) {

    }
}
