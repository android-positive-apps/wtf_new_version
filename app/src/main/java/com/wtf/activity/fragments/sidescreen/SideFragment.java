package com.wtf.activity.fragments.sidescreen;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.sromku.simple.fb.SimpleFacebook;
import com.wtf.activity.fragments.BaseFragment;
import com.wtf.activity.R;
import com.wtf.activity.main.BusinessActivity;
import com.wtf.activity.main.GiftsActivity;
import com.wtf.activity.main.MainActivity;
import com.wtf.activity.main.MapActivity;
import com.wtf.activity.main.MyApp;
import com.wtf.activity.main.ProfileActivity;
import com.wtf.activity.main.SettingsActivity;
import com.wtf.activity.objects.Appuser;
import com.wtf.activity.objects.Business;
import com.wtf.activity.util.AppUtil;
import com.wtf.activity.util.ContentProviderUtil;
import com.wtf.activity.util.ToastUtil;


/**
 * Created by Izakos on 11/10/2015.
 */
public class SideFragment extends BaseFragment implements View.OnClickListener {

    private ListView SideListView;
    private int positionClicked;
    private ImageView x_icon_side;
    private TextView homeScreenTextView;
    private TextView mapTextView;
    private TextView profileTextView;
    private TextView settingsTextView;
    private TextView suggestAbusinessTextView;
    private boolean main;
    private FragmentActivity a;
    private TextView gowayuTextView;
    private TextView virsion;
    private ImageView powerdLogo;
    private TextView pointsTextView;
    private TextView registerBusiness;

    public SideFragment() {
        // TODO Auto-generated constructor stub
    }

    public static SideFragment newInstance() {
        SideFragment instance = new SideFragment();
        return instance;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        int layoutRes = R.layout.fragment_side;
        View rootView = inflater.inflate(layoutRes, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        findViews(view);
        setListeners();

        virsion.setText("Version: "+AppUtil.getApplicationVersion(getActivity()));
//        a= getActivity();


//        SideListView = (ListView) view.findViewById(R.id.SideListView);
//        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, getResources().getStringArray(R.array.sideMenu));
//        SideListView.setAdapter(adapter);

//        SideListView.setOnItemClickListener(this);
    }

    private void setListeners() {
        x_icon_side.setOnClickListener(this);
        homeScreenTextView       .setOnClickListener(this);
        mapTextView              .setOnClickListener(this);
        profileTextView          .setOnClickListener(this);
        settingsTextView         .setOnClickListener(this);
        suggestAbusinessTextView .setOnClickListener(this);
        gowayuTextView.setOnClickListener(this);
        pointsTextView.setOnClickListener(this);
        registerBusiness.setOnClickListener(this);
    }

    private void findViews(View view) {
        x_icon_side= (ImageView)view.findViewById(R.id.x_icon_side);

        homeScreenTextView       =(TextView)view.findViewById(R.id.homeScreenTextView);
        mapTextView              =(TextView)view.findViewById(R.id.mapTextView);
        profileTextView          =(TextView)view.findViewById(R.id.profileTextView);
        settingsTextView         =(TextView)view.findViewById(R.id.settingsTextView);
        suggestAbusinessTextView = (TextView) view.findViewById(R.id.suggestAbusinessTextView);
        gowayuTextView = (TextView) view.findViewById(R.id.gowayuTextView);
        virsion = (TextView) view.findViewById(R.id.virsion);
        pointsTextView = (TextView) view.findViewById(R.id.pointsTextView);
        registerBusiness = (TextView) view.findViewById(R.id.registerBusiness);

        powerdLogo= (ImageView) view.findViewById(R.id.powerdLogo);
        powerdLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ContentProviderUtil.openBrowser(getActivity(), "http://www.positive-apps.com/en");
            }
        });
    }


    @Override
    public void onClick(View v) {
//        if (getActivity() instanceof MapActivity){
//            main= false;
//        }else {
//
//            main= true;
//
//        }
        switch (v.getId()){
            case R.id.homeScreenTextView:
                if (getActivity() instanceof MainActivity){
                    MainActivity.mDrawerLayout.closeDrawers();


                }else if (getActivity() instanceof MapActivity){
                   getActivity().finish();
                }else if (getActivity() instanceof BusinessActivity){
                    if (MyApp.appManager.isFromMap()){
                        MyApp.appManager.setBackFromBusiness(true);
                        MyApp.appManager.setFromMap(false);
                        MyApp.appManager.save();
                        getActivity().finish();
                    }else {
                        getActivity().finish();
                    }
                }else if (getActivity() instanceof GiftsActivity){
                    getActivity().finish();
                }


                break;
            case R.id.mapTextView:
                if (getActivity() instanceof MainActivity){
                    MainActivity.mDrawerLayout.closeDrawers();

                    Intent i = new Intent(getActivity(),MapActivity.class);
                    startActivity(i);
                }else if (getActivity() instanceof MapActivity){
                    MapActivity.mDrawerLayout.closeDrawers();
                }else if (getActivity() instanceof GiftsActivity){
                    Intent i = new Intent(getActivity(),MapActivity.class);
                    startActivity(i);
                    getActivity().finish();
                }else if (getActivity() instanceof BusinessActivity){
                    if (MyApp.appManager.isFromMap()){
                        MyApp.appManager.setBackFromBusiness(false);
                        MyApp.appManager.save();
                        getActivity().finish();
                    }else if (MyApp.appManager.isFromProfile()){
                        MyApp.appManager.setBackFromBusiness(true);
                        MyApp.appManager.setFromProfile(false);
                        MyApp.appManager.save();
                        getActivity().finish();
                    }else {
                        Intent i = new Intent(getActivity(), MapActivity.class);
                        startActivity(i);
                        getActivity().finish();
                    }
                }
                break;
            case R.id.profileTextView:
                if (!MyApp.appManager.isAnonymous()) {
                    if (!(getActivity() instanceof BusinessActivity)) {
                        Intent profileIntent = new Intent(getActivity(), ProfileActivity.class);
                        startActivity(profileIntent);
                        if (getActivity() instanceof MapActivity || (getActivity() instanceof GiftsActivity)) {
                            getActivity().finish();
                        }
                    }else {
                        if (MyApp.appManager.isFromProfile()){
                            MyApp.appManager.setFromProfile(false);
                            MyApp.appManager.save();
                            getActivity().finish();
                        }else if (MyApp.appManager.isFromMap()){
                            MyApp.appManager.setBackFromBusinessForProfile(true);
                            MyApp.appManager.setFromMap(false);
                            MyApp.appManager.save();
                            getActivity().finish();
                        }else {
                            Intent profileIntent = new Intent(getActivity(), ProfileActivity.class);
                            startActivity(profileIntent);
                            getActivity().finish();
                        }
                    }
                }else {
                    ToastUtil.toaster(getResources().getString(R.string.please_login_facebook_profile),false);
                }
                break;
            case R.id.settingsTextView:
                Intent settIntent = new Intent(getActivity(),SettingsActivity.class);
                startActivity(settIntent);
                break;
            case R.id.suggestAbusinessTextView:
                ContentProviderUtil.openBrowser(getActivity(),"http://www.gowayu.com/contact");
                break;
            case R.id.x_icon_side:
                 if (getActivity() instanceof BusinessActivity){
                     BusinessActivity.mDrawerLayout.closeDrawers();

                }else if (getActivity() instanceof MapActivity){
                    MapActivity.mDrawerLayout.closeDrawers();
                }
                 else if (getActivity() instanceof MainActivity){
                     MainActivity.mDrawerLayout.closeDrawers();
                 }else if (getActivity() instanceof GiftsActivity){
                     GiftsActivity.mDrawerLayout.closeDrawers();
                 }
                break;
            case R.id.gowayuTextView:
                ContentProviderUtil.openBrowser(getActivity(),"http://www.gowayu.com");
                break;
            case R.id.pointsTextView:
//                ActivityCompat.finishAffinity(getActivity());
                if (!MyApp.appManager.isAnonymous()) {
                    if (!(getActivity() instanceof MainActivity) && !(getActivity() instanceof GiftsActivity)) {
                        Intent intent = new Intent(getActivity(), MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.setAction(MainActivity.ACTION_POINTS_OPEN);
                        startActivity(intent);
                    } else if (getActivity() instanceof MainActivity) {
                        Intent intent = new Intent(getActivity(), GiftsActivity.class);
                        startActivity(intent);
                    } else if (getActivity() instanceof GiftsActivity) {
                        GiftsActivity.mDrawerLayout.closeDrawers();
                    }
                }else {
                    ToastUtil.toaster(getResources().getString(R.string.login_facebook_coupons), false);

                }
                    break;
            case R.id.registerBusiness:
                ContentProviderUtil.openBrowser(getActivity(),"http://gowayu.com/join");
                break;

        }

    }

//    @Override
//    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//        MainActivity.mDrawerLayout.closeDrawers();
//        switch (i){
//            case 2:
//                FragmentsUtil.openFragment(getFragmentManager(), ContactsFragment.newInstance(), R.id.main_container);
//                break;
//            case 3:
//                FragmentsUtil.openFragment(getFragmentManager(), UniversityFragment.newInstance(Static.URL_TUTORIAL_VID), R.id.main_container);
//                break;
//            case 4:
//                FragmentsUtil.openFragment(getFragmentManager(), ProfileFragment.newInstance(), R.id.main_container);
//                break;
//            case 5:
//                getActivity().startActivity(new Intent(getActivity(), LoginActivity.class));
//                break;
//            default:
//                break;
//        }
//    }


}
