package com.wtf.activity.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.daimajia.easing.BaseEasingMethod;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.maps.model.LatLng;
import com.wtf.activity.R;
import com.wtf.activity.adapters.CheckInsArrayAdapter;
import com.wtf.activity.adapters.CheckinDialogAdapter;
import com.wtf.activity.main.BusinessActivity;
import com.wtf.activity.main.MapActivity;
import com.wtf.activity.main.MyApp;
import com.wtf.activity.main.SettingsActivity;
import com.wtf.activity.network.NetworkCallback;
import com.wtf.activity.network.requests.AppuserCheckInRequest;
import com.wtf.activity.network.requests.GetBusinessGiftsAndCouponsRequest;
import com.wtf.activity.network.requests.GetBusinessV2Request;
import com.wtf.activity.network.requests.GetCouponRequest;
import com.wtf.activity.network.requests.SearchBusinessRequest;
import com.wtf.activity.network.requests.SetCouponUsedRequest;
import com.wtf.activity.network.requests.UpdateRankRequest;
import com.wtf.activity.network.response.AppuserCheckInResponse;
import com.wtf.activity.network.response.GetBusinessGiftsAndCouponsResponse;
import com.wtf.activity.network.response.GetBusinessV2Response;
import com.wtf.activity.network.response.GetCouponResponse;
import com.wtf.activity.network.response.ResponseObject;
import com.wtf.activity.network.response.ResponseObject2;
import com.wtf.activity.network.response.SearchBusinessResponse;
import com.wtf.activity.network.response.SetBonusPointsResponse;
import com.wtf.activity.network.response.SetCouponUsedResponse;
import com.wtf.activity.network.response.UpdateRankResponse;
import com.wtf.activity.objects.Business;
import com.wtf.activity.objects.CheckIn;
import com.wtf.activity.objects.Coupon;
import com.wtf.activity.objects.Rating;
import com.wtf.activity.objects.SpecialOffer;
import com.wtf.activity.util.ContentProviderUtil;
import com.wtf.activity.util.DateUtil;
import com.wtf.activity.util.DistanceDescriptor;
import com.wtf.activity.util.GiftUtils;
import com.wtf.activity.util.ImageLoader;
import com.wtf.activity.util.TextUtil;
import com.wtf.activity.util.ToastUtil;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.IllegalFormatCodePointException;
import java.util.Set;
import java.util.logging.LogRecord;
import java.util.prefs.Preferences;

/**
 * Created by Niv on 2/29/2016.
 */
public class BusinessPageFragment extends BaseFragment implements View.OnClickListener {
    public static final String ACTION_MAP= "action_map";
    public static final String ACTION_RATE= "action_rate";
    public static final String ACTION_PROFILE= "action_profile";
    public static final String ACTION_GIFTS= "action_gifts";
    private Business business;
    private TextView BusinessNameTextView;
    private TextView businessDistanceTextView;
    private TextView businessAddressTextView;
    private TextView businessPhoneTextView;
    private TextView businessOpenHoursTextView;
    private FrameLayout imageFrame;
    private ImageView shareOfferImageView;
    private TextView offerCostTextView;
    private TextView offerDetailsTextView;
    private TextView descriptionTv;
    private TextView offerNameTextView;
    private ImageView mainBusinessImage;
    private RatingBar businessRatingBar;
    private LinearLayout checkInButtonBusinessPage;
    private LinearLayout rateButtonBusinessPage;
    private LinearLayout shareButtonBusinessPage;
    private int dialogItemPosition=-1;
    private ImageView RateImageView;
    private ImageView callImageView;
    private ArrayList<CheckIn> checkIns =new ArrayList<>();
    private ArrayList<SpecialOffer> specailOfers= new ArrayList<>();
            ;
    private GridView chekcInsGridB;
    private LinearLayout moreLinearB;
    private ImageView showMoreImageB;
    private TextView moreTvB;
    private TextView checkinsTextView;
    private CheckInsArrayAdapter checkInsArrayAdapter;
    private boolean isAnonymous;
    private ImageView checkInImageView;
    private Rating rating;
    private RatingBar serviceRatingBar;
    private RatingBar ambianceRatingBar;
    private RatingBar priceRatingBar;
    private RatingBar overallRatingBar;
    private TextView ratingTextView;
    private ArrayList<CheckIn> list;
    private CheckInsArrayAdapter checkInsArrayAdapterDialog;
    private ArrayList<CheckIn> sevenChecks;
    private ArrayList<Coupon> coupons=new ArrayList<>();
    private ImageView offerImage;
    private ImageView rightArrowOffersImageView;
    private ImageView leftArrowOffersImageView;
    private int counter=0;
    private LinearLayout offersLinear;
    private boolean fromMap=false;
    private TextView visitWeb;
    private TextView visitFacebook;
    private FrameLayout checkInsFrame;
    private int couponPosition =-1;
    private LinearLayout couponPurpleLinear;
    private Tracker mTracker;

    public BusinessPageFragment() {
    }

    public static BusinessPageFragment newInstance() {
        BusinessPageFragment instance = new BusinessPageFragment();
        return instance;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        int layoutRes = R.layout.fragment_business_page;
        View rootView = inflater.inflate(layoutRes, container, false);

        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        InitAnalytic();

        findViews(view);
        setListeners();
        setAdaptersV2();
//        setDetails();
        isAnonymous= MyApp.appManager.isAnonymous();
        Intent i = getActivity().getIntent();
        if (i!=null&&i.getAction()!=null){
            if (i.getAction().equals(ACTION_MAP)){
                business= MyApp.appManager.getTempBusiness();
                fromMap=true;
                MyApp.appManager.setFromMap(true);
                MyApp.appManager.save();

                    getbusinessV2();
            }else if (i.getAction().equals(ACTION_RATE)){
                business= MyApp.appManager.getTempBusiness();
                openRateDialog();
                getbusinessV2();
            }else if (i.getAction().equals(ACTION_PROFILE)){
                MyApp.appManager.setFromProfile(true);
                MyApp.appManager.save();
                business= MyApp.appManager.getTempBusiness();
                getbusinessV2();
            }else if (i.getAction().equals(ACTION_GIFTS)){
                String businessID = i.getStringExtra("businessID");
                if (!businessID.isEmpty()){
                    getBusinessById(businessID);
                }
            }
        }

//        setDetails();

//        final ScrollView scrollView = (ScrollView) view.findViewById(R.id.scrollView);
//        scrollView.postDelayed(new Runnable() {
//
//            @Override
//            public void run() {
//                scrollView.fullScroll(ScrollView.FOCUS_UP);
//            }
//        },200);
    }
    private void InitAnalytic(){
        MyApp application = (MyApp) getActivity().getApplication();
        mTracker = application.getDefaultTracker();
    }


    @Override
    public void onResume() {
        super.onResume();
        mTracker.setScreenName(/*"Image~" +*/ "Business screen");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

    }
    private void getBusinessById(String businessID) {
        MyApp.networkManager.makeRequest(new GetBusinessV2Request(businessID, ""), new NetworkCallback<GetBusinessV2Response>() {
            @Override
            public void onResponse(boolean success, String errorDesc, ResponseObject<GetBusinessV2Response> response) {
                super.onResponse(success, errorDesc, response);
                if (isAdded() && getActivity() != null){
                    if (success) {
                        business = response.getData().getBusiness();
                        if (business != null) {
                            checkIns = response.getData().getCheckIns();
                            specailOfers = response.getData().getSpecialOffers();
                            rating = response.getData().getRating();
                            setDetails();
//                    setAdapters();


                            setRatings();
                            if (checkIns != null)
                                handleCheckIns();
                            else {
                                checkInsFrame.setVisibility(View.GONE);
                                System.out.println("--->  " + checkIns.size());
                            }
                            if (!isAnonymous) {
                                getGiftsAndCoupons();
                            }
                        }
                    } else {
                        if (business != null) {
                            setDetails();
                            if (!isAnonymous) {
                                getGiftsAndCoupons();
                            }
                        }
                    }

            }
        }});
    }

    private void getGiftsAndCoupons() {
        MyApp.networkManager.makeRequest(new GetBusinessGiftsAndCouponsRequest(business.getID()),new NetworkCallback<GetBusinessGiftsAndCouponsResponse>(){
            @Override
            public void onResponse(boolean success, String errorDesc, ResponseObject<GetBusinessGiftsAndCouponsResponse> response) {
                super.onResponse(success, errorDesc, response);
                if (success){
                    if (response.getData().getCoupons()!=null){
                        if (response.getData().getCoupons().size()>0){
                            coupons=response.getData().getCoupons();
                            offersLinear.setVisibility(View.VISIBLE);

                            //
//                            coupons.add(coupons.get(0));
//                            coupons.add(coupons.get(0));
//                            coupons.add(coupons.get(0));
//                            coupons.add(coupons.get(0));
                            //
                            setCouponDetails(0,1);

                        }else {
                            offersLinear.setVisibility(View.GONE);
                        }
                    }
                    else {
                        offersLinear.setVisibility(View.GONE);
                    }
                }
            }
        });
    }

    private void setCouponDetails(final int position, final int side) {
        Handler handler = new Handler() {};
        Runnable r = new Runnable() {
            @Override
            public void run() {
                if (side==1)
                YoYo.with(Techniques.BounceInRight).duration(400).playOn(offerImage);
                else
                    YoYo.with(Techniques.BounceInLeft).duration(400).playOn(offerImage);

                System.out.println("--->  pos"+position);
                //firstSet
                Coupon firstCoupon= coupons.get(position);
                offerNameTextView.setText(firstCoupon.getTitle());
                offerDetailsTextView.setText(firstCoupon.getDescription());
                offerCostTextView.setText(firstCoupon.getSmallLetters());
                ImageLoader.ByUrl(firstCoupon.getImage(),offerImage);
                couponPosition=position;


                if (coupons.size()>position+1){
                    rightArrowOffersImageView.setVisibility(View.VISIBLE);
                }else {
                    rightArrowOffersImageView.setVisibility(View.INVISIBLE);
                }
                if (position==0){
                    leftArrowOffersImageView.setVisibility(View.INVISIBLE);
                }else {
                    leftArrowOffersImageView.setVisibility(View.VISIBLE);

                }
            }
        };
        handler.postDelayed(r,0);
//        System.out.println("--->  pos"+position);
//        //firstSet
//        Coupon firstCoupon= coupons.get(position);
//        offerNameTextView.setText(firstCoupon.getTitle());
//        offerDetailsTextView.setText(firstCoupon.getDescription());
//        offerCostTextView.setText(firstCoupon.getSmallLetters());
//        ImageLoader.ByUrl(firstCoupon.getImage(),offerImage);
//
//        if (coupons.size()>position+1){
//            rightArrowOffersImageView.setVisibility(View.VISIBLE);
//        }else {
//            rightArrowOffersImageView.setVisibility(View.INVISIBLE);
//        }
//        if (position==0){
//            leftArrowOffersImageView.setVisibility(View.INVISIBLE);
//        }else {
//            leftArrowOffersImageView.setVisibility(View.VISIBLE);
//
//        }
    }



//    private void getBusinessUpdated(String id, String externalID) {
//        MyApp.networkManager.makeRequest(new GetBusinessRequest(id, externalID), new NetworkCallback<GetBusinessResponse>() {
//            @Override
//            public void onResponse(boolean success, String errorDesc, ResponseObject<GetBusinessResponse> response) {
//                super.onResponse(success, errorDesc, response);
//                if (success) {
//                    Business b = response.getData().getBusiness();
//                    if (b != null)
//                        business = b;
//                    setDetails();
//
//                }
//
//            }
//        });
//    }


    private void findViews(View view) {
        BusinessNameTextView = (TextView)view.findViewById(R.id.BusinessNameTextView);
        businessDistanceTextView = (TextView)view.findViewById(R.id.businessDistanceTextView);
        businessAddressTextView = (TextView)view.findViewById(R.id.businessAddressTextView);
        visitWeb = (TextView)view.findViewById(R.id.visitWeb);
        visitFacebook = (TextView)view.findViewById(R.id.visitFacebook);

        businessPhoneTextView = (TextView)view.findViewById(R.id.businessPhoneTextView);
        businessOpenHoursTextView = (TextView)view.findViewById(R.id.businessOpenHoursTextView);
        descriptionTv = (TextView)view.findViewById(R.id.descriptionTv);
        offerNameTextView = (TextView)view.findViewById(R.id.offerNameTextView);
        offerDetailsTextView = (TextView)view.findViewById(R.id.offerDetailsTextView);
        offerCostTextView = (TextView)view.findViewById(R.id.offerCostTextView);
        shareOfferImageView = (ImageView)view.findViewById(R.id.shareOfferImageView);
        imageFrame = (FrameLayout)view.findViewById(R.id.imageFrame);
        mainBusinessImage = (ImageView)view.findViewById(R.id.mainBusinessImage);
        businessRatingBar=(RatingBar)view.findViewById(R.id.businessRatingBar);
        offerImage =(ImageView)view.findViewById(R.id.offerImage);


        rightArrowOffersImageView=(ImageView)view.findViewById(R.id.rightArrowOffersImageView);
        leftArrowOffersImageView=(ImageView)view.findViewById(R.id.leftArrowOffersImageView);
        offersLinear=(LinearLayout)view.findViewById(R.id.offersLinear);
        //buttons:
        checkInButtonBusinessPage= (LinearLayout)view.findViewById(R.id.checkInButtonBusinessPage);
        rateButtonBusinessPage= (LinearLayout)view.findViewById(R.id.rateButtonBusinessPage);
        shareButtonBusinessPage= (LinearLayout)view.findViewById(R.id.shareButtonBusinessPage);
        RateImageView= (ImageView)view.findViewById(R.id.RateImageView);
        callImageView= (ImageView)view.findViewById(R.id.callImageView);

        //CHECKIN
        chekcInsGridB =(GridView)view.findViewById(R.id.friendsGridB);
        moreLinearB= (LinearLayout)view.findViewById(R.id.moreLinearB);
        couponPurpleLinear= (LinearLayout)view.findViewById(R.id.couponPurpleLinear);
        showMoreImageB=(ImageView)view.findViewById(R.id.showMoreImageB);
        checkInImageView=(ImageView)view.findViewById(R.id.checkInImageView);
        moreTvB= (TextView)view.findViewById(R.id.moreTvB);
        checkinsTextView =(TextView)view.findViewById(R.id.checkinsTextView);
        checkInsFrame=(FrameLayout)view.findViewById(R.id.checkInsFrame);

        //rating
        serviceRatingBar = (RatingBar)view.findViewById(R.id.serviceRatingBar);
        ambianceRatingBar= (RatingBar)view.findViewById(R.id.ambianceRatingBar);
        priceRatingBar   = (RatingBar)view.findViewById(R.id.priceRatingBar);
        overallRatingBar = (RatingBar)view.findViewById(R.id.overallRatingBar);
        ratingTextView =(TextView)view.findViewById(R.id.ratingTextView);

    }

    private void setListeners() {

        checkInButtonBusinessPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isAnonymous)
                    makeCheckIn(business.getName(), business.getCategory());
                else
                    ToastUtil.toaster(getResources().getString(R.string.please_login_facebook_checkin),false);

            }
        });
        rateButtonBusinessPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openRateDialog();
            }
        });
        RateImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openRateDialog();
            }
        });
        shareButtonBusinessPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareBusiness();
            }
        });
        businessPhoneTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ContentProviderUtil.isTelephonyEnabled(getActivity()))
                    openDialer();
                else {
                    ToastUtil.toaster(getResources().getString(R.string.device_not_support_dialer), false);
                }
            }
        });
        callImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ContentProviderUtil.isTelephonyEnabled(getActivity()))
                    openDialer();
                else {
                    ToastUtil.toaster(getResources().getString(R.string.device_not_support_dialer), false);
                }
            }
        });
        checkInImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isAnonymous)
                    makeCheckIn(business.getName(), business.getCategory());
                else
                    ToastUtil.toaster(getResources().getString(R.string.please_login_facebook_checkin), false);

            }
        });
//        showMoreImageB.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                openCheckInList();
//            }
//        });
        rightArrowOffersImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                counter++;
                setCouponDetails(counter,1);
            }
        });
        leftArrowOffersImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                counter--;
                setCouponDetails(counter,0);
            }
        });
        shareOfferImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ContentProviderUtil.luanchShareChooser(getContext(),getActivity().getResources().getString(R.string.share_offer),coupons.get(counter).getBusinessName()+", "+coupons.get(counter).getTitle()+" - "+coupons.get(counter).getSmallLetters()+" "+coupons.get(counter).getDescription()+".");

            }
        });

        businessAddressTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAddressDialog();

            }
        });

        visitWeb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!business.getWebsite().equals(""))
                    ContentProviderUtil.openBrowser(getActivity(), business.getWebsite());
                else
                    ToastUtil.toaster(getString(R.string.error_getting_business_web), false);
            }
        });

        visitFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(business.getFacebookLink()));
                    startActivity(intent);
                } catch (Exception e) {
                    ToastUtil.toaster(getString(R.string.facebook_not_avalible), false);

                }
            }
        });

        couponPurpleLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (coupons!=null){
                    if (coupons.size()>0){
                        showProgressDialog();
                        Coupon coupon= coupons.get(couponPosition);

                        MyApp.networkManager.makeRequest(new GetCouponRequest(coupon.getID()),new NetworkCallback<GetCouponResponse>(){
                            @Override
                            public void onResponse(boolean success, String errorDesc, ResponseObject<GetCouponResponse> response) {
                                super.onResponse(success, errorDesc, response);
                                if (success){
                                    Coupon coupon = response.getData().getCoupon();
                                    if (coupon!=null) {
                                        showCouponDialog(coupon);
                                        dismisProgressDialog();
                                    }else {
                                        dismisProgressDialog();
                                    }
                                }
                            }
                        });




                    }
                }
            }
        });
    }

    private void showAddressDialog() {
        final AlertDialog.Builder alert;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            alert = new AlertDialog.Builder(getActivity(), android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            alert = new AlertDialog.Builder(getActivity());
        }
        alert.setTitle(getActivity().getResources().getString(R.string.show_business_on_map));


        alert.setNeutralButton(getResources().getString(R.string.open_nav), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                alert.setCancelable(true);
                Uri uri = Uri.parse("geo:0,0?q=" + businessAddressTextView.getText().toString().replace(" ", "+"));
                ContentProviderUtil.showMap(uri,getActivity());
                dialog.dismiss();
            }
        });
        alert.setPositiveButton(getResources().getString(R.string.app_name)+" "+getResources().getString(R.string.map_), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent i = new Intent(getActivity(), MapActivity.class);
                i.putExtra("businessID",business.getID());
                i.putExtra("businessExID",business.getExternalID());
                    if (fromMap){

                        getActivity().setResult(1,i);
                        getActivity().finish();
                    }else {
                        getActivity().finish();
                        i.setAction(MapActivity.ACTION_RATE_PLACE);
                        startActivity(i);
                    }
                dialog.dismiss();
            }
        });
        alert.show();
    }

    private void setDetails() {
        if (business!=null) {
            BusinessNameTextView.setText(business.getName());
            double distanceFromCLC= DistanceDescriptor.getDistancelATlNG(
                    MyApp.appManager.getLatLng().latitude, MyApp.appManager.getLatLng().longitude,
                    Double.parseDouble(business.getLat()), Double.parseDouble(business.getLng()));

                    String dis = getShortDistance(distanceFromCLC+"");

            businessDistanceTextView.setText(dis);
            String address = business.getCity()+" "+business.getStreet()+" "+business.getHouseNum();
            businessAddressTextView.setText(address);
            String phone = null;
            try {
                phone = !business.getPhone().equals("")?  business.getPhone():
                        (business.getPhone2()!=null?business.getPhone2()+"":"");
            } catch (Exception e) {
                e.printStackTrace();
            }
            businessPhoneTextView.setText(phone);
            businessOpenHoursTextView.setText(business.getOpening());
            descriptionTv.setText(business.getBusinessDesc());
            ImageLoader.ByUrl(business.getImage(), mainBusinessImage,R.drawable.trans,mainBusinessImage.getWidth(),mainBusinessImage.getHeight());
            int rank= 0;
            float rate=0;
            try {
                rank = Integer.parseInt(business.getAdminRank());
                rate = rank/20f;
            } catch (NumberFormatException e) {
                rate = business.getRank()/20f;
                e.printStackTrace();
            }
//            float rate = business.getRank()/20f;
            if (rate!=0)
            businessRatingBar.setRating(rate);

        }
    }

    private void getbusinessV2(){
        MyApp.networkManager.makeRequest(new GetBusinessV2Request(business.getID(), business.getExternalID()), new NetworkCallback<GetBusinessV2Response>() {
            @Override
            public void onResponse(boolean success, String errorDesc, ResponseObject<GetBusinessV2Response> response) {
                super.onResponse(success, errorDesc, response);
                if (isAdded() && getActivity() != null){
                    if (success) {
                        business = response.getData().getBusiness();
                        checkIns = response.getData().getCheckIns();
                        specailOfers = response.getData().getSpecialOffers();
                        rating = response.getData().getRating();
                        setDetails();
//                    setAdapters();


                        setRatings();

                        if (checkIns != null)
                            handleCheckIns();
                        else {
                            checkInsFrame.setVisibility(View.GONE);
                            System.out.println("--->  " + checkIns.size());
                        }

                    } else {
                        setDetails();
                    }
                if (!isAnonymous) {
                    getGiftsAndCoupons();
                }
            }
        }
        });
    }

    private void handleCheckIns() {
        sevenChecks.clear();

//        if (checkIns.size()>0){
//            for (int num=0;num<checkIns.size();num++){
//                CheckIn c = checkIns.get(num);
//                if (c==null){
//                    checkIns.remove(num);
//                }else if (c.getFBID()==null||c.getAppuserID()==0||c.getUserName()==null){
//                    checkIns.remove(num);
//
//                }
//
//            }
//        }
        if (checkIns.size()==0){
            checkInsFrame.setVisibility(View.GONE);
            System.out.println("--->  "+checkIns.size());
        }else {
            checkInsFrame.setVisibility(View.VISIBLE);
        }



            if (checkIns.size() > 8) {
                for (int i = 1; i < 8; i++) {
                    sevenChecks.add(checkIns.get(i));
                }
                sevenChecks.add(new CheckIn("-1", checkIns.size() - 7 + ""));
            }else {
                for (CheckIn cI:checkIns){
                    sevenChecks.add(cI);
                }
            }
            checkInsArrayAdapter.notifyDataSetChanged();

        String Count = getResources().getString(R.string.check_ins)+" ("+checkIns.size()+")";
        checkinsTextView.setText(Count);

    }

    private void setAdaptersV2() {
        sevenChecks= new ArrayList<CheckIn>();
        chekcInsGridB.setFocusable(false);
        checkInsArrayAdapter= new CheckInsArrayAdapter(getActivity(),sevenChecks);
        chekcInsGridB.setAdapter(checkInsArrayAdapter);


        chekcInsGridB.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                CheckIn checkIn = sevenChecks.get(position);
                if (checkIn!=null){
                    if (checkIn.getFBID()!=null) {
                        if (checkIn.getFBID().equals("-1")) {
                            openCheckInList(checkIns);

                        }else {
                            openFacebookPage(checkIn.getFBID());
                        }
                    }
                }
            }
        });
    }

    private void openFacebookPage(String fbid) {
        try {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.facebook.com/"+fbid));
            startActivity(intent);
        } catch(Exception e) {
            ToastUtil.toaster(getActivity().getResources().getString(R.string.facebook_not_avalible),false);
        }
    }


//    private void setAdapters() {
//        final ArrayList<CheckIn> newCheckiList = setUpCheckinListAndRemoveNullChecks();
//        System.out.print("niv newCheckiList size:"+newCheckiList.size());
//        checkInsArrayAdapter= new CheckInsArrayAdapter(getActivity(),newCheckiList);
//        chekcInsGridB.setAdapter(checkInsArrayAdapter);
//
//        String Count = getResources().getString(R.string.check_ins)+" ("+list.size()+")";
//        checkinsTextView.setText(Count);
////        if (list.size()>8)
////            moreLinearB.setVisibility(View.VISIBLE);
//
//        String sizeMore = "("+(list.size()-7)+")";
//        moreTvB.setText(sizeMore);
//
//
//        final ArrayList<CheckIn> newCheckinArray = makeSureItemsNotRepeated();
//
//        chekcInsGridB.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                CheckIn checkIn = newCheckiList.get(position);
//                if (checkIn!=null){
//                    if (checkIn.getFBID()!=null)
//                    if (checkIn.getFBID().equals("-1")){
//                        openCheckInList(newCheckinArray);
//
//                    }
//                }
//            }
//        });
//    }

//    @NonNull
//    private ArrayList<CheckIn> makeSureItemsNotRepeated() {
//        final ArrayList<CheckIn> newCheckinArray= new ArrayList<>();
//
//        Set<String> titles = new HashSet<String>();
//
//        for( CheckIn item : checkIns ) {
//            if( titles.add( item.getFBID() )) {
//                newCheckinArray.add( item );
//            }
//        }
//        System.out.print("niv checkIns:"+newCheckinArray.size());
//
//        return newCheckinArray;
//    }

//    @NonNull
//    private ArrayList<CheckIn> setUpCheckinListAndRemoveNullChecks() {
//        // set grid and remove all null checkins from the list:
//        int size = checkIns.size();
//        System.out.print("niv checkIns:"+checkIns.size());
//        list = new ArrayList<>();
//        if (size>8) {
//            for (int i = 0; i < 7; i++) {
//                if (checkIns.get(i).getFBID()!=null&&checkIns.get(i).getAppuserID()!=0)
//                list.add(checkIns.get(i));
//
//            }
////            if (list.size()==7)
////            list.add(new CheckIn("-1",size-7+""));
//        }else if (checkIns.size()==8){
//            for (int i = 0; i < 8; i++) {
//                if (checkIns.get(i).getFBID()!=null&&checkIns.get(i).getAppuserID()!=0)
//                    list.add(checkIns.get(i));
//
//            }
//        }else if (checkIns.size()>0){
//            for (int i=0;i<checkIns.size();i++){
//                if (checkIns.get(i).getFBID()!=null&&checkIns.get(i).getAppuserID()!=0)
//                    list=checkIns;
//            }
//        }
//        final ArrayList<CheckIn> newCheckiList= new ArrayList<>();
//
//        Set<String> ids = new HashSet<String>();
//
//        for( CheckIn item : list ) {
//            if( ids.add( item.getFBID() )) {
//                newCheckiList.add( item );
//            }
//        }
//        if (newCheckiList.size()>8){
//            newCheckiList.add(7,new CheckIn("-1", newCheckiList.size() - 7 + ""));
//        }
//        return newCheckiList;
//    }

    private void setRatings() {
        if (rating!=null) {
            try {
                serviceRatingBar.setRating(rating.getServiceRate());
                ambianceRatingBar.setRating(rating.getAmbienceRate());
                priceRatingBar.setRating(rating.getPriceRate());
                overallRatingBar.setRating(rating.getOverallRate());
//                String ratingText = getResources().getString(R.string.rating) + "(" +(int)rating.getNumOfRates() + ")";
                String ratingText = getResources().getString(R.string.rating) + "(" +business.getNumRankers() + ")";
                ratingTextView.setText(ratingText);
            } catch (Resources.NotFoundException e) {
                e.printStackTrace();
            }
        }else {
            try {
                serviceRatingBar.setRating(0);
                ambianceRatingBar.setRating(0);
                priceRatingBar.setRating(0);
                overallRatingBar.setRating(0);
                String ratingText = getResources().getString(R.string.rating) + "(" + 0 + ")";
                ratingTextView.setText(ratingText);
            } catch (Resources.NotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    private void openCheckInList(final ArrayList<CheckIn> checkIns) {
        LayoutInflater factory = LayoutInflater.from(getContext());
        final View deleteDialogView = factory.inflate(
                R.layout.checkin_grid_dialog, null);
        final AlertDialog deleteDialog = new AlertDialog.Builder(getContext()).create();
        deleteDialog.setView(deleteDialogView);
        deleteDialogView.findViewById(R.id.x_icon_checkin_dialog).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteDialog.dismiss();
            }
        });
//        ArrayList<CheckIn> checkIns1= new ArrayList<>();
//        for (CheckIn c: checkIns){
//            if (c.getFBID()!=null&&c.getAppuserID()!=0)
//                checkIns1.add(c);
//        }


        checkInsArrayAdapterDialog = new CheckInsArrayAdapter(getActivity(), checkIns);
        GridView g = (GridView) deleteDialogView.findViewById(R.id.gridViewDialog);
        g.setAdapter(checkInsArrayAdapterDialog);

        g.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                CheckIn checkIn = checkIns.get(position);
                openFacebookPage(checkIn.getFBID());
            }
        });


                deleteDialog.show();
    }

    private String getShortDistance(String distance) {
        double dis = 0;
        if (distance != "")
            try {
                dis = Double.parseDouble(distance);
                String s = ((int) (dis * 100)) / 100f + "";
                s = s + " " + getResources().getString(R.string.km);
                return s;
            } catch (Exception e) {
                e.printStackTrace();
            }

        return "";

    }

    private void openDialer() {
        if (!business.getPhone().isEmpty())
            ContentProviderUtil.openDialer(getActivity(),business.getPhone());
        else if (!business.getPhone2().isEmpty())
            ContentProviderUtil.openDialer(getActivity(),business.getPhone2());
        else
            ToastUtil.toaster(getActivity().getResources().getString(R.string.phone_na),false);
    }

    private void shareBusiness() {
        ContentProviderUtil.luanchShareChooser(getContext(),getActivity().getResources().getString(R.string.share_place),business.getName()+", "+business.getCity()+" "+business.getStreet()+" "+business.getHouseNum()+". "+getActivity().getResources().getString(R.string.app_name)+" - "+getActivity().getResources().getString(R.string.people_and_places)+" "+"https://play.google.com/store/apps/details?id=com.wtf.activity");
    }

    private void openRateDialog() {
        if (!MyApp.appManager.isAnonymous()) {
            LayoutInflater factory = LayoutInflater.from(getContext());
            final View checkInDialogView = factory.inflate(
                    R.layout.dialog_businesses_rate, null);
            final AlertDialog chooseBusinessDialog = new AlertDialog.Builder(getContext()).create();
            chooseBusinessDialog.setView(checkInDialogView);

            TextView rateTitleDialogTextView = (TextView) checkInDialogView.findViewById(R.id.rateTitleDialogTextView);
            String title = getActivity().getResources().getString(R.string.rate) + " " + business.getName();
            rateTitleDialogTextView.setText(title);


            final RatingBar serviceRatingBarDialog = (RatingBar) checkInDialogView.findViewById(R.id.serviceRatingBarDialog);
            final RatingBar ambianceRatingBarDialog = (RatingBar) checkInDialogView.findViewById(R.id.ambianceRatingBarDialog);
            final RatingBar priceRatingBarDialog = (RatingBar) checkInDialogView.findViewById(R.id.priceRatingBarDialog);
            final RatingBar overallRatingBarDialog = (RatingBar) checkInDialogView.findViewById(R.id.overallRatingBarDialog);
            checkInDialogView.findViewById(R.id.sendImageView).setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {


                    makeRateRequest((int) serviceRatingBarDialog.getRating(), (int) ambianceRatingBarDialog.getRating(), (int) priceRatingBarDialog.getRating(), (int) overallRatingBarDialog.getRating());
                    chooseBusinessDialog.dismiss();
                }
            });
            checkInDialogView.findViewById(R.id.x_icon_rate_dialog).setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    chooseBusinessDialog.dismiss();
                }
            });


            chooseBusinessDialog.show();
        }else {
            ToastUtil.toaster(getResources().getString(R.string.please_login_facebook_rate), true);
        }
    }


    ArrayList<Business> businesses= null;
    private void makeRateRequest(int rating, int rating2, int rating3, int rating4) {
        if (rating==0&&rating2==0&&rating3==0&&rating4==0){
            Toast.makeText(getActivity(),
                    getResources().getString(R.string.cant_rate_zero),
                    Toast.LENGTH_LONG).show();
            return;
        }

        if (!checkIfUserCanRate()){
            Toast.makeText(getActivity(),
                    getResources().getString(R.string.you_need_to_wait_rate),
                    Toast.LENGTH_LONG).show();
            return;
        }

        MyApp.networkManager.makeRequest(new UpdateRankRequest(business.getID(), rating, rating2, rating3, rating4), new NetworkCallback<UpdateRankResponse>() {
            @Override
            public void onResponse(boolean success, String errorDesc, ResponseObject<UpdateRankResponse> response) {
                super.onResponse(success, errorDesc, response);
                if (success) {
                    ToastUtil.toaster(getActivity().getResources().getString(R.string.rate_sent), false);
                    handleRatePlacesList();
                    addBonusPoints(response);
                    //refresh details:
                    Handler h = new Handler();
                    Runnable r = new Runnable() {
                        @Override
                        public void run() {
                            getbusinessV2();
                        }
                    };
                    h.postDelayed(r,1000);
                }
            }
        });
    }

    private void handleRatePlacesList() {
        businesses = MyApp.userManager.getRatedBusiness();
        if (businesses != null) {
            int placeRated = checkIfRated(businesses);
            if (placeRated != -1) {
                businesses.remove(placeRated);
            }
            if (businesses.size() > 3) {
                businesses.remove(0);
            }
            businesses.add(business);
            MyApp.userManager.setRatedBusiness(businesses);
            MyApp.userManager.save();
        } else {
            businesses = new ArrayList<>();
            businesses.add(business);
            MyApp.userManager.setRatedBusiness(businesses);
            MyApp.userManager.save();

        }
    }

    private void addBonusPoints(ResponseObject<UpdateRankResponse> response) {
        if (!response.getData().getGiftPoints().isEmpty()){
            if (!response.getData().getGiftPoints().equals("0")) {
//                String received = getActivity().getResources().getString(R.string.you_received) + " " + response.getData().getGiftPoints() + " " + getActivity().getResources().getString(R.string.bonus_points) + "!";
                String received = getActivity().getResources().getString(R.string.well_done_you_go_another) + " " + response.getData().getGiftPoints() + " " + getActivity().getResources().getString(R.string.points_) + "!";
                ToastUtil.toaster(received, false);
            }
        }

//        MyApp.networkManager.makeRequest(new SetBonusPointsRequest(GiftUtils.RANK_POINT.actionName,GiftUtils.RANK_POINT.value),new NetworkCallback<SetBonusPointsResponse>(){
//            @Override
//            public void onResponse(boolean success, String errorDesc, ResponseObject<SetBonusPointsResponse> response) {
//                super.onResponse(success, errorDesc, response);
//                if (success){
//                    MyApp.userManager.getAppuser().setPoints( MyApp.userManager.getAppuser().getPoints()+GiftUtils.RANK_POINT.value);
//                    MyApp.userManager.save();
//                    ToastUtil.toaster(getResources().getString(R.string.you_received_2_points),false);
//
//                }
//            }
//        });
    }

    private boolean checkIfUserCanRate () {
        try {
            final long queterHour = 15*60*1000;
            long lastCheckIn = MyApp.appManager.getLastRate();
            if (lastCheckIn > 0){
                long currentTime = System.currentTimeMillis();
                if (lastCheckIn + queterHour < currentTime ){
                    MyApp.appManager.setLastRate(currentTime);

                    return true;
                }else{
                    return false;
                }
            }else{
                MyApp.appManager.setLastRate(System.currentTimeMillis());
                MyApp.appManager.save();
                return true;
            }
        } catch (Exception e) {
            return true;
        }


    }

    private int checkIfRated(ArrayList<Business> businesses) {
        for (int i=0;i<businesses.size();i++){
            Business b = businesses.get(i);
            if (b.getID().equals(business.getID())){
                return i;
            }
        }
        return -1;
    }


    private void makeCheckIn(String name,String category) {
        LatLng latLng = MyApp.appManager.getLatLng();

        //search for businesses close to the user
//        MyApp.networkManager.makeRequest(new SearchBusinessRequest(""+category,name+"", latLng.latitude + "", latLng.longitude + "", 3), new NetworkCallback<SearchBusinessResponse>() {
//            @Override
//            public void onResponse(boolean success, String errorDesc, ResponseObject<SearchBusinessResponse> response) {
//                super.onResponse(success, errorDesc, response);
//                if (success) {
//                    ArrayList<Business> businesses = response.getData().getBusinesses();
                    ArrayList<Business> businesses = new ArrayList<>();
                    businesses.add(business);
                    showCheckInDialog(businesses);

//                }
//            }
//        });
    }


    @Override
    public void onClick(View v) {

    }
    boolean clicked=false;
    private void showCheckInDialog(final ArrayList<Business> businesses) {
        LayoutInflater factory = LayoutInflater.from(getActivity());
        final View checkInDialogView = factory.inflate(
                R.layout.dialog_checkin, null);
        final AlertDialog checkinDialog = new AlertDialog.Builder(getActivity()).create();
        checkinDialog.setView(checkInDialogView);

        clicked=false;

        ListView checkinDialogListView = (ListView) checkInDialogView.findViewById(R.id.checkinDialogListView);
        CheckinDialogAdapter checkinDialogAdapter = new CheckinDialogAdapter(getActivity(), businesses);
        checkinDialogListView.setAdapter(checkinDialogAdapter);
        checkinDialogListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {


            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                dialogItemPosition = position;
                clicked=true;
            }
        });


        checkInDialogView.findViewById(R.id.checkinDialogImageView).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (clicked) {
                    if (dialogItemPosition != -1) {
                        Business b = businesses.get(dialogItemPosition);
                        appUserCheckin(checkinDialog, b);
                        String date = DateUtil.getDateAsStringByMilli(DateUtil.FORMAT_JUST_DATE, System.currentTimeMillis());
                        Business toSave = new Business(b.getName(), b.getCategory(), b.getLat(), b.getLng(), b.getCity(), date, b.getID(), b.getExternalID(),b.getImage());
                        MyApp.tableCheckIns.insert(toSave);
                    }
                }else {
                    ToastUtil.toaster(getResources().getString(R.string.please_choose_business),false);
                }
            }
        });
        checkInDialogView.findViewById(R.id.x_icon_checkin_dialog).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                checkinDialog.dismiss();
            }
        });


        checkinDialog.show();

    }
    private void appUserCheckin(final AlertDialog checkinDialog, final Business b) {
        if (!MyApp.userManager.isIncognito()){
        String businessId = b.getID();
        MyApp.networkManager.makeRequest(new AppuserCheckInRequest(businessId), new NetworkCallback<AppuserCheckInResponse>() {
            @Override
            public void onResponse(boolean success, String errorDesc, ResponseObject<AppuserCheckInResponse> response) {
                super.onResponse(success, errorDesc, response);
                if (success) {
                    ToastUtil.toaster(getActivity().getResources().getString(R.string.chacked_in_at)+business.getName(), false);

                    if (!response.getData().getGiftPoints().isEmpty()){
                        if (!response.getData().getGiftPoints().equals("0")) {
//                            String received = getActivity().getResources().getString(R.string.you_received) + " " + response.getData().getGiftPoints() + " " + getActivity().getResources().getString(R.string.bonus_points) + "!";
                            String received = getActivity().getResources().getString(R.string.well_done_you_go_another) + " " + response.getData().getGiftPoints() + " " + getActivity().getResources().getString(R.string.points_) + "!";
                            ToastUtil.toaster(received, false);
                        }
                    }

                    checkinDialog.dismiss();
                    Handler h = new Handler();
                    Runnable r = new Runnable() {
                        @Override
                        public void run() {
                            getbusinessV2();
                        }
                    };
                  h.postDelayed(r,1000);


                } else {

                    try {
                        ToastUtil.toaster(response.getErrorDesc() + "", true);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }else {
           showIncognitoDialog();
        }

    }

    private void showIncognitoDialog() {
        final AlertDialog.Builder alert;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            alert = new AlertDialog.Builder(getActivity(), android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            alert = new AlertDialog.Builder(getActivity());
        }
        alert.setTitle(getActivity().getResources().getString(R.string.cant_checkin_oncognito));


        alert.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {

                dialog.dismiss();
            }
        });
        alert.setNeutralButton(getResources().getString(R.string.open_settings), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                Intent i = new Intent(getActivity(), SettingsActivity.class);
                startActivity(i);
                getActivity().finish();

            }
        });
        alert.show();
    }

    private void showCouponDialog(final Coupon coupon) {

        LayoutInflater factory = LayoutInflater.from(getActivity());
        final View couponDialogView = factory.inflate(
                R.layout.coupon_dialog, null);
        final AlertDialog deleteDialog = new AlertDialog.Builder(getActivity()).create();
        deleteDialog.setView(couponDialogView);
        //find views and set details:
        setCouponDialogDetails(coupon, couponDialogView,deleteDialog);
        final LinearLayout couponCodeLinear = (LinearLayout) couponDialogView.findViewById(R.id.couponCodeLinear);
        final EditText couponEditText = (EditText) couponDialogView.findViewById(R.id.couponEditText);
//        couponEditText.setImeOptions(EditorInfo.IME_ACTION_DONE);
        couponEditText.setOnEditorActionListener(
                new EditText.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        if (actionId ==EditorInfo.IME_ACTION_DONE) {
                            if (TextUtil.baseFieldsValidation(couponEditText)){
                                setCouonUsed(coupon,couponEditText.getText().toString(),deleteDialog);
                                couponCodeLinear.setVisibility(View.GONE);
                                return false;
                            }

                        }
                        return false;
                    }
                });
        couponDialogView.findViewById(R.id.insertButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtil.baseFieldsValidation(couponEditText)){
                    setCouonUsed(coupon,couponEditText.getText().toString(),deleteDialog);
                    couponCodeLinear.setVisibility(View.GONE);
                }
            }
        });

        couponDialogView.findViewById(R.id.getItButton).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (!isAnonymous) {
                    couponCodeLinear.setVisibility(View.VISIBLE);
                }else {
                    ToastUtil.toaster(getResources().getString(R.string.please_login_coupon),false);
                }

            }
        });
        couponDialogView.findViewById(R.id.cancelButton).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                deleteDialog.dismiss();

            }
        });

        deleteDialog.show();




    }
    private void setCouponDialogDetails(final Coupon coupon, View couponDialogView, final AlertDialog deleteDialog) {
        TextView couponTitle = (TextView) couponDialogView.findViewById(R.id.couponTitle);
        TextView couponKind = (TextView) couponDialogView.findViewById(R.id.couponKind);
        TextView couponDes = (TextView) couponDialogView.findViewById(R.id.couponDes);
        TextView couponSmallLetters = (TextView) couponDialogView.findViewById(R.id.couponSmallLetters);
        TextView itemsLeftTv = (TextView) couponDialogView.findViewById(R.id.itemsLeftTv);
        TextView exTv = (TextView) couponDialogView.findViewById(R.id.exTv);
        ImageView couponImageView = (ImageView)couponDialogView.findViewById(R.id.couponImageView);

        if (coupon.getImage().isEmpty()||coupon.getImage().equals("http://www.mypushserver.com/dev/bmm/"))
            couponImageView.setVisibility(View.GONE);
        else {
            ImageLoader.ByUrl(coupon.getImage(),couponImageView);
        }

        couponTitle.setText(coupon.getBusinessName());
        couponKind.setText(coupon.getTitle());
        couponDes.setText(coupon.getDescription());
        couponSmallLetters.setText(coupon.getSmallLetters());
        itemsLeftTv.setText(coupon.getItemsLeft());
//        Date date= new Date(coupon.getExpiration());
//        String justDate=  DateUtil.getDateAsStringByMilli(DateUtil.FORMAT_JUST_DATE,date.getTime());
//        exTv.setText(justDate);
        exTv.setText(coupon.getExpiration());

        if (coupon.getDescription().isEmpty())
            couponDes.setVisibility(View.GONE);
        if (coupon.getSmallLetters().isEmpty())
            couponSmallLetters.setVisibility(View.GONE);


        //setListeners
        couponTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteDialog.dismiss();

            }
        });
        couponImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteDialog.dismiss();


            }
        });
    }
    private void setCouonUsed(Coupon coupon, String pass, final AlertDialog deleteDialog) {
        MyApp.networkManager.makeRequest2(new SetCouponUsedRequest(MyApp.userManager.getAppuser().getID(),
                MyApp.appManager.getLatLng().latitude+"",coupon.getID(),MyApp.appManager.getLatLng().longitude+"",pass),new NetworkCallback<SetCouponUsedResponse>(){
            @Override
            public void onResponse2(boolean success, String errorDesc, ResponseObject2<SetCouponUsedResponse> response) {
                super.onResponse2(success, errorDesc, response);
                if (success){
                    ToastUtil.toaster("Coupon used",true);
                    deleteDialog.dismiss();
                }
            }
        });
    }
}