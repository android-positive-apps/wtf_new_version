package com.wtf.activity.storage;

import android.util.Log;

import com.google.gson.Gson;
import com.sromku.simple.fb.entities.Profile;
import com.wtf.activity.main.MyApp;
import com.wtf.activity.objects.Appuser;
import com.wtf.activity.objects.Business;
import com.wtf.activity.objects.CreditCard;
import com.wtf.activity.objects.Gift;

import java.util.ArrayList;

/**
 * Created by Niv on 2/29/2016.
 */
public class UserSettings {

    private String name;
    private String radius="16";
    private String UpdateInterval;
    private String FBaccessToken;
    private boolean loggedIn;
    private boolean isIncognito=false;
    private Appuser appuser;
    private ArrayList<Business> ratedBusiness;

    private ArrayList<Gift> ReceivedGifts;
    private ArrayList<Gift> MyGifts;
    private ArrayList<String> fbFriendsIDs;
    private ArrayList<CreditCard> creditCards= new ArrayList<>();
    private com.sromku.simple.fb.entities.Profile userProfile;


    public static UserSettings instance;

    public UserSettings() {
    }


    public static UserSettings getInstance (){
        if (instance == null){
            Gson gson = new Gson();
            UserSettings UserSettings = gson.fromJson(MyApp.userSettings.getUserSetting(), UserSettings.class);
            instance = UserSettings == null ? new UserSettings() : UserSettings;
        }
        return instance;
    }
    public void save(){
        Gson gson = new Gson();
        String json = gson.toJson(UserSettings.this);
        Log.i("UserSettings", "save: " + json.toString());
        MyApp.userSettings.setUserSetting(json);
    }

    @Override
    public String toString() {
        try{
            return new Gson().toJson(this).toString();
        }catch (Exception e){}catch (Error r){}

        return "";
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFBaccessToken() {
        return FBaccessToken;
    }

    public void setFBaccessToken(String FBaccessToken) {
        this.FBaccessToken = FBaccessToken;
    }

    public boolean isLoggedIn() {
        return loggedIn;
    }

    public void setLoggedIn(boolean loggedIn) {
        this.loggedIn = loggedIn;
    }

    public Appuser getAppuser() {
        return appuser;
    }

    public void setAppuser(Appuser appuser) {
        this.appuser = appuser;
    }

    public String getUpdateInterval() {
        return UpdateInterval;
    }

    public void setUpdateInterval(String updateInterval) {
        UpdateInterval = updateInterval;
    }

    public ArrayList<Gift> getMyGifts() {
        return MyGifts;
    }

    public void setMyGifts(ArrayList<Gift> myGifts) {
        MyGifts = myGifts;
    }

    public ArrayList<Gift> getReceivedGifts() {
        return ReceivedGifts;
    }

    public void setReceivedGifts(ArrayList<Gift> receivedGifts) {
        ReceivedGifts = receivedGifts;
    }

    public boolean isIncognito() {
        return isIncognito;
    }

    public void setIsIncognito(boolean isIncognito) {
        this.isIncognito = isIncognito;
    }

    public ArrayList<String> getFbFriendsIDs() {
        return fbFriendsIDs;
    }

    public void setFbFriendsIDs(ArrayList<String> fbFriendsIDs) {
        this.fbFriendsIDs = fbFriendsIDs;
    }

    public Profile getUserProfile() {
        return userProfile;
    }

    public void setUserProfile(Profile userProfile) {
        this.userProfile = userProfile;
    }

    public ArrayList<Business> getRatedBusiness() {
        return ratedBusiness;
    }

    public void setRatedBusiness(ArrayList<Business> ratedBusiness) {
        this.ratedBusiness = ratedBusiness;
    }

    public ArrayList<CreditCard> getCreditCards() {
        return creditCards;
    }

    public void setCreditCards(ArrayList<CreditCard> creditCards) {
        this.creditCards = creditCards;
    }

    public String getRadius() {
        return radius;
    }

    public void setRadius(String radius) {
        this.radius = radius;
    }
}
