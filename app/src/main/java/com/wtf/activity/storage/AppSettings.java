package com.wtf.activity.storage;

import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.wtf.activity.main.MyApp;
import com.wtf.activity.objects.Business;
import com.wtf.activity.objects.MainDetails;
import com.wtf.activity.objects.Size;

import java.util.ArrayList;

/**
 * Created by Niv on 2/29/2016.
 */
public class AppSettings {

    private LatLng latLng= new LatLng(32.085300,34.781768);
    private String gcmKey;
    private Size screenSize;
    private String imagePath;
    private Business tempBusiness;
    private boolean anonymous = true;
    private MainDetails mainDetails;
    private long lastRate=0;
    private String gcm="";
    private boolean accaptedTerms;
    private ArrayList<Business> trendyBusinesses= new ArrayList<>();
    private boolean fromMap= false;
    private boolean fromProfile= false;
    private boolean backFromBusiness=false;
    private boolean backFromBusinessForProfile=false;


    public static AppSettings instance;

    public AppSettings() {
    }

    public static AppSettings getInstance() {
        if (instance == null) {
            Gson gson = new Gson();
            AppSettings AppSettings = gson.fromJson(MyApp.generalSettings.getSetting(), AppSettings.class);
            instance = AppSettings == null ? new AppSettings() : AppSettings;
        }
        return instance;
    }



    ///////////////////////////////////////////////////


    public boolean isBackFromBusinessForProfile() {
        return backFromBusinessForProfile;
    }

    public void setBackFromBusinessForProfile(boolean backFromBusinessForProfile) {
        this.backFromBusinessForProfile = backFromBusinessForProfile;
    }

    public boolean isFromProfile() {
        return fromProfile;
    }

    public void setFromProfile(boolean fromProfile) {
        this.fromProfile = fromProfile;
    }

    public boolean isBackFromBusiness() {
        return backFromBusiness;
    }

    public void setBackFromBusiness(boolean backFromBusiness) {
        this.backFromBusiness = backFromBusiness;
    }

    public boolean isFromMap() {
        return fromMap;
    }

    public void setFromMap(boolean fromMap) {
        this.fromMap = fromMap;
    }

    public ArrayList<Business> getTrendyBusinesses() {
        return trendyBusinesses;
    }

    public void setTrendyBusinesses(ArrayList<Business> trendyBusinesses) {
        this.trendyBusinesses = trendyBusinesses;
    }

    public boolean isAccaptedTerms() {
        return accaptedTerms;
    }

    public void setAccaptedTerms(boolean accaptedTerms) {
        this.accaptedTerms = accaptedTerms;
    }

    public boolean isAnonymous() {
        return anonymous;
    }

    public void setAnonymous(boolean anonymous) {
        this.anonymous = anonymous;
    }

    public LatLng getLatLng() {
        return latLng;
    }

    public void setLatLng(LatLng latLng) {
        this.latLng = latLng;
    }

    public String getGcmKey() {
        return gcmKey;
    }

    public void setGcmKey(String gcmKey) {
        this.gcmKey = gcmKey;
    }

    public Size getScreenSize() {
        return screenSize;
    }

    public void setScreenSize(Size screenSize) {
        this.screenSize = screenSize;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public Business getTempBusiness() {
        return tempBusiness;
    }

    public void setTempBusiness(Business tempBusiness) {
        this.tempBusiness = tempBusiness;
    }

    public MainDetails getMainDetails() {
        return mainDetails;
    }

    public void setMainDetails(MainDetails mainDetails) {
        this.mainDetails = mainDetails;
    }

    public static void setInstance(AppSettings instance) {
        AppSettings.instance = instance;
    }

    public long getLastRate() {
        return lastRate;
    }

    public void setLastRate(long lastRate) {
        this.lastRate = lastRate;
    }


    public String getGcm() {
        return gcm;
    }

    public void setGcm(String gcm) {
        this.gcm = gcm;
    }

    public void save() {
        Gson gson = new Gson();
        String json = gson.toJson(AppSettings.this);
        Log.i("AppSettings", "save: " + json.toString());
        MyApp.generalSettings.setSetting(json);
    }

    @Override
    public String toString() {
        try {
            return new Gson().toJson(this).toString();
        } catch (Exception e) {
        } catch (Error r) {
        }

        return "";
    }


}
