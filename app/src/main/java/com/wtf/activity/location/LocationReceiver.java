package com.wtf.activity.location; /**
 *
 */


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Message;
import android.util.Log;

import com.wtf.activity.main.MyApp;
import com.wtf.activity.network.NetworkCallback;
import com.wtf.activity.network.requests.UpdateAppuserLocationRequest;
import com.wtf.activity.network.response.ResponseObject2;
import com.wtf.activity.network.response.UpdateAppuserResponse;
import com.wtf.activity.util.ToastUtil;

/**
 * @author natiapplications
 *
 */
public class LocationReceiver extends BroadcastReceiver {

    double latitude, longitude;

    @Override
    public void onReceive(final Context context, final Intent calledIntent)
    {
        Log.d("LOC_RECEIVER", "Location RECEIVED!");

        updateUserLocation(context);

    }


    private synchronized void updateUserLocation (Context context){
        //TODO send user location to the remote server
        Coordinates coordinates = new Coordinates(MyApp.appManager.getLatLng().latitude,MyApp.appManager.getLatLng().longitude);
        Message message = new Message();
        message.what = 1;
        message.obj = coordinates;
//        MainActivity.MAIN_HANDLER.dispatchMessage(message);

        if (!MyApp.appManager.isAnonymous())
            updateAppUserLOcation(context);

    }

    private void updateAppUserLOcation(Context context) {
        if (MyApp.userManager.isLoggedIn()) {
            if (MyApp.userManager.getAppuser()!=null) {
                if (!MyApp.userManager.getAppuser().getID().isEmpty()) {
                    MyApp.networkManager.makeRequest2(new UpdateAppuserLocationRequest(context), new NetworkCallback<UpdateAppuserResponse>() {
                        @Override
                        public void onResponse2(boolean success, String errorDesc, ResponseObject2<UpdateAppuserResponse> response) {
                            super.onResponse2(success, errorDesc, response);


                        }
                    });
                }

            }
        }
    }
}
