/**
 * 
 */
package com.wtf.activity.location;

import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Looper;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.wtf.activity.main.MyApp;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

/**
 * @author natiapplications
 *
 */
public class LocationService extends Service{

public static final long LOCATION_UPDATE_INTERVAL = 240 * 1000;
public static final long LOCATION_MAX_UPDATE_INTERVAL = 240 * 1000;
//	public static final long LOCATION_UPDATE_INTERVAL = 10 * 1000;
//	public static final long LOCATION_MAX_UPDATE_INTERVAL = 10 * 1000;

private static final String TAG = "GoogleApiLocationLog";

private GoogleApiClient mGoogleApiClient;
private MyLocationListener myLocationListener;
private LocationRequest mLocationRequest;

private boolean isLocationServiceActive;

		@Override
		public void onCreate() {
			super.onCreate();
			Log.d(TAG, "on location service created");
			buildGoogleApiClient();
		}


		@Override
		public IBinder onBind(Intent intent) {
			return null;
		}


		@Override
		public int onStartCommand(Intent intent, int flags, int startId) {
			// TODO Auto-generated method stub
			super.onStartCommand(intent, flags, startId);
			Log.d(TAG, "on location service start command");
			buildGoogleApiClient();
			return START_STICKY;
		}

		protected synchronized void buildGoogleApiClient() {

			if (myLocationListener == null || mGoogleApiClient == null){
				Log.d(TAG, "build google play service connection");
				myLocationListener = new MyLocationListener();
				mGoogleApiClient = new GoogleApiClient.Builder(this)
						.addConnectionCallbacks(myLocationListener)
						.addOnConnectionFailedListener(myLocationListener)
						.addApi(LocationServices.API)
						.build();
				createLocationRequest();
				mGoogleApiClient.connect();

			}else{
				Log.d(TAG, "connection is allray connencted " +
						" my location listener = " + (myLocationListener == null) +
						" apiClinte = " + (mGoogleApiClient == null));
			}
		}


		protected void createLocationRequest() {
			Log.d(TAG, "create location request");
			mLocationRequest = new LocationRequest();
			mLocationRequest.setInterval(LOCATION_UPDATE_INTERVAL);
			mLocationRequest.setFastestInterval(LOCATION_MAX_UPDATE_INTERVAL);
			mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
		}




		public void startLocationUpdates() {
			if (mGoogleApiClient != null &&
					mGoogleApiClient.isConnected()) {
				Log.d(TAG, "start location updates");
				LocationServices.FusedLocationApi.requestLocationUpdates(
						mGoogleApiClient, mLocationRequest, myLocationListener);
			}else{
				Log.d(TAG, "start updates fialed ~ "  +
						"google client is null ? " + (mGoogleApiClient == null));
			}
		}


		private void addLocationListener()
		{
			new Thread(new Runnable(){
				public void run(){
					try{
						Looper.prepare();

						while (true){
							Log.e(TAG, "trying to start location services");

							if(isLocationServiceActive){
								Log.e(TAG, "location found !");
								break;
							}else{
								Log.e(TAG, "location not found !");
								buildGoogleApiClient();
							}
							Thread.sleep(30000);
						}
						Looper.loop();
					}catch(Exception ex){
						ex.printStackTrace();
					}
					Log.e(TAG, "add listenr thread finished");
				}
			}, "LocationThread").start();

		}

		public  void updateLocation(Location location)
		{

			Log.e(TAG, "update location!");
			if (location == null){
				Log.e(TAG, "location is null ~!");
				return;
			}
			Log.e(TAG, "location : " + location.getLatitude() + ", " + location.getLongitude());

			double latitude, longitude;
			latitude = location.getLatitude();
			longitude = location.getLongitude();
			MyApp.appManager.setLatLng(new LatLng(latitude, longitude));
			MyApp.appManager.save();
			updateUserLocation();
			System.out.println("--->  locationTest "+latitude+"||"+longitude);
		}


		private synchronized void updateUserLocation (){
			Log.e(TAG, "update location on server");
			Intent filterRes = new Intent();
			filterRes.setAction("com.positiveapps.bmm.intent.action.LOCATION");
			MyApp.appContext.sendBroadcast(filterRes);
		}


		@Override
		public void onDestroy() {
			// TODO Auto-generated method stub
			super.onDestroy();
			Log.e(TAG, "location service stoped");
			try {
				stopLocationUpdates();
			} catch (Exception e) {}
		}


		protected void stopLocationUpdates() {
			try {
				LocationServices.FusedLocationApi.removeLocationUpdates(
						mGoogleApiClient, myLocationListener);
			} catch (Exception e) {}
			myLocationListener = null;
			mGoogleApiClient = null;
			isLocationServiceActive = false;
		}


class MyLocationListener implements ConnectionCallbacks, OnConnectionFailedListener ,
		LocationListener{


	@Override
	public void onConnectionFailed(ConnectionResult arg0) {
		Log.e(TAG, "on connection failed! " + arg0.getErrorCode());
		isLocationServiceActive = false;
		addLocationListener();
	}


	@Override
	public void onConnected(Bundle arg0) {
		Log.e(TAG, "on connection success! ");
		if (mGoogleApiClient == null){
			try {
				stopSelf();
			} catch (Exception e) {}
			return;
		}
		isLocationServiceActive = true;
		Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
				mGoogleApiClient);
		updateLocation(mLastLocation);
		startLocationUpdates();
	}

	@Override
	public void onConnectionSuspended(int arg0) {
		Log.e(TAG, "on connection suspended! " + arg0);
	}

	@Override
	public void onLocationChanged(Location location) {
		Log.e(TAG, "on Location changed");
		updateLocation(location);
	}


}



}
