/**
 * 
 */
package com.wtf.activity.location;

import java.io.Serializable;

import org.json.JSONArray;
import org.json.JSONObject;

import android.location.Location;

import com.google.android.gms.maps.model.LatLng;


/**
 * @author natiapplications
 *
 */
public class Coordinates implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private final String LAT = "Lat";
	private final String LON = "Lng";
	
	public double lat;
	public double lon;
	
	
	public Coordinates() {
		super();
		this.lat = 0.0;
		this.lon = 0.0;
	}
	
	public Coordinates(double lat, double lon) {
		super();
		this.lat = lat;
		this.lon = lon;
	}
	
	public Coordinates(String lat, String lon) {
		super();
		this.lat = Double.parseDouble(lat);
		this.lon = Double.parseDouble(lon);
	}
	
	
	public Coordinates(String toParse) {
		super();
		try {
			String[] coordi = toParse.split(",");
			this.lat = Double.parseDouble(coordi[0]);
			this.lon = Double.parseDouble(coordi[1]);
		} catch (Exception e) {
			this.lat = 0.0;
			this.lon = 0.0;
		}
		
	}
	
	public Coordinates(LatLng latLng) {
		super();
		this.lat = latLng.latitude;
		this.lon = latLng.longitude;
	}
	
	public Coordinates(Location location) {
		super();
		this.lat = location.getLatitude();
		this.lon = location.getLongitude();
	}
	
	public Coordinates(JSONObject toParse) {
		super();
		this.lat = toParse.optDouble(LAT,0.0);
		this.lon = toParse.optDouble(LON,0.0);
	}
	
	public Coordinates(JSONArray toParse) {
		super();
		if (toParse.length() >=1 ){
			this.lat = toParse.optDouble(0,0.0);
			this.lon = toParse.optDouble(1,0.0);
		}else{
			this.lat = 0.0;
			this.lon = 0.0;
		}
		
	}


	/**
	 * @return the lat
	 */
	public String getLatAsString() {
		if (lat == 0)
			lat = 0.0;
		return String.valueOf(lat);
	}

	/**
	 * @return the lon
	 */
	public String getLonAsString() {
		if (lon == 0)
			lon = 0.0;
		return String.valueOf(lon);
	}
	
	
	public String getCoordinatesAsString () {
		return getLatAsString() + "," + getLonAsString();
	}

	
	
	public LatLng toLatLag (){
		LatLng result = new LatLng(lat, lon);
		return result;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Coordinates [lat=" + lat + ", lon=" + lon + "]";
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(lat);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(lon);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Coordinates other = (Coordinates) obj;
		if (Double.doubleToLongBits(lat) != Double.doubleToLongBits(other.lat))
			return false;
		if (Double.doubleToLongBits(lon) != Double.doubleToLongBits(other.lon))
			return false;
		return true;
	}

	
	public boolean isValid () {
		if (lat == 0 || lon == 0){
			return false;
		}
		return true;
	}
	
	
	
	

}
