/**
 * 
 */
package com.wtf.activity.location;

/**
 * @author natiapplications
 *
 */
public class StaticMarker {
	
	public static final String BIG = "big";
	public static final String MID = "mid";
	public static final String SMALL = "small";
	
	public static final String BLACK = "black";
	public static final String BROWN = "brown";
	public static final String RED = "red";
	public static final String BLUE = "blue";
	public static final String GREEN = "green";
	public static final String PAPER = "purple";
	public static final String GRAY = "gray";
	public static final String YELLOW = "yellow";
	public static final String ORANGE = "orange";
	public static final String WHITE = "white";
	
	
	public double lat,lon;
	public String lable;
	public String color;
	public String size;
	
	
	public StaticMarker() {
		super();
		// TODO Auto-generated constructor stub
	}


	public StaticMarker(double lat, double lon, String lable, String color,
			String size) {
		super();
		this.lat = lat;
		this.lon = lon;
		this.lable = lable;
		this.color = color;
		this.size = size;
	}
	
	

}
