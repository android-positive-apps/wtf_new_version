package com.wtf.activity.location;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.wtf.activity.main.MainActivity;
import com.wtf.activity.main.MapActivity;
import com.wtf.activity.main.MyApp;
import com.wtf.activity.util.AppUtil;
import com.wtf.activity.util.ContentProviderUtil;

/**
 * Created by Niv on 6/20/2016.
 */
public class GpsLocationReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().matches("android.location.PROVIDERS_CHANGED")) {
            if (ContentProviderUtil.isGPSEnebled(context))
            if (MyApp.currentActivity!=null)
            AppUtil.startLocationService(MyApp.currentActivity);

        }
    }
}