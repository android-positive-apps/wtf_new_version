///**
// *
// */
//package com.wtf.activity.dialogs;
//
//
//
//import android.graphics.Color;
//import android.graphics.drawable.ColorDrawable;
//import android.os.Bundle;
//import android.support.v4.app.Fragment;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.view.Window;
//import android.view.View.OnClickListener;
//import android.widget.Button;
//import android.widget.TextView;
//
//import com.wtf.activity.R;
//
//
///**
// * @author natiapplications
// *
// */
//public class ErrorDialog extends BaseDialogFragment implements OnClickListener{
//
//	public static final String DIALOG_NAME = "ErrorDialog";
//
//
//	private Fragment parent;
//
//
//	private TextView subTitle;
//	private Button dissmisBtn;
//	private String titleString;
//
//	private DialogCallback callback;
//
//	/**
//	 *
//	 * @param callback
//	 */
//	public ErrorDialog(Fragment parent,String title,DialogCallback callback){
//		//this.mDescription = description;
//		this.callback = callback;
//		this.parent = parent;
//		this.titleString = title;
//	}
//
//	public ErrorDialog(String title,DialogCallback callback){
//		//this.mDescription = description;
//		this.callback = callback;
//		this.titleString = title;
//	}
//
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container,
//            Bundle savedInstanceState) {
//        View view = inflater.inflate(R.layout.dialog_error, container, false);
//
//        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
//        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//
//        subTitle = (TextView)view.findViewById(R.id.sub_title);
//        dissmisBtn = (Button)view.findViewById(R.id.btn_dissmis);
//        subTitle.setText(titleString);
//
//        dissmisBtn.setOnClickListener(this);
//
//        return view;
//    }
//
//
//
//
//    private void dismissDialog(){
//
//    	this.dismiss();
//    }
//
//
//
//	@Override
//	public void onClick(View v) {
//
//    	dismissDialog();
//
//    	if(callback != null){
//    		callback.onDialogButtonPressed(v.getId(), this);
//    	}
//	}
//
//
//
//
//
//}
//
