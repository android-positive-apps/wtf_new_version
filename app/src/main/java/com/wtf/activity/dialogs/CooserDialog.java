///**
// *
// */
//package com.wtf.activity.dialogs;
//
//
//
//import android.content.Context;
//import android.graphics.Color;
//import android.graphics.drawable.ColorDrawable;
//import android.os.Bundle;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.View.OnClickListener;
//import android.view.ViewGroup;
//import android.view.Window;
//import android.widget.AdapterView;
//import android.widget.AdapterView.OnItemClickListener;
//import android.widget.ArrayAdapter;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.ListView;
//import android.widget.TextView;
//
//import com.wtf.activity.main.MyApp;
//import com.wtf.activity.R;
//import com.wtf.activity.util.AppUtil;
//
//
///**
// * @author natiapplications
// *
// */
//public class CooserDialog extends BaseDialogFragment implements OnClickListener,OnItemClickListener{
//
//	public static final String DIALOG_NAME = "CooserDialog";
//
//
//	private DialogCallback callback;
//
//	private View footerView;
//	private String title;
//
//	private TextView titleTxt;
//	private LinearLayout footerContainer;
//	private ListView options;
//	private ChooserRow[] optionsRows;
//
//	/**
//	 *
//	 * @param callback
//	 */
//	public CooserDialog(String title,ChooserRow[] optionsRows,View footerView,DialogCallback callback){
//		this.title = title;
//		this.footerView = footerView;
//		this.callback = callback;
//		this.optionsRows = optionsRows;
//	}
//
//
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container,
//            Bundle savedInstanceState) {
//        View view = inflater.inflate(R.layout.dialog_chooser, container, false);
//
//        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
//        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//
//        titleTxt = (TextView)view.findViewById(R.id.chooser_title);
//        footerContainer = (LinearLayout)view.findViewById(R.id.footer_container);
//        options = (ListView)view.findViewById(R.id.option_list);
//
//        if (this.title != null&&!this.title.isEmpty()){
//        	titleTxt.setText(title);
//        }else{
//        	titleTxt.setVisibility(View.GONE);
//        }
//
//
//        options.setAdapter(new optionsAdapter(getActivity(), 0, 0, optionsRows));
//        options.setOnItemClickListener(this);
//
//        /*WindowManager.LayoutParams wmlp = getDialog().getWindow().getAttributes();
//
//        wmlp.gravity = Gravity.BOTTOM | Gravity.CENTER;
//
//        getDialog().getWindow().getAttributes().windowAnimations = R.style.CustomAlertDialogStyle;
//        Animation bottomUp = AnimationUtils.loadAnimation(getActivity(),
//                R.anim.anim_in_dialog);
//
//        view.startAnimation(bottomUp);*/
//
//        if (footerView != null){
//        	footerContainer.addView(footerView);
//        }else{
//        	footerContainer.setVisibility(View.GONE);
//        }
//
//        return view;
//    }
//
//
//
//
//    private void dismissDialog(){
//
//    	this.dismiss();
//    }
//
//
//
//	@Override
//	public void onClick(View v) {
//
//    	dismissDialog();
//
//    	if(callback != null){
//    		callback.onDialogButtonPressed(v.getId(), this);
//    	}
//	}
//
//
//
//	@Override
//	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
//		dismissDialog();
//		if (callback != null){
//			callback.onDialogOptionPressed(arg2, this);
//		}
//
//	}
//
//	class optionsAdapter extends ArrayAdapter<ChooserRow> {
//
//		ChooserRow[] options;
//		/**
//		 * @param context
//		 * @param resource
//		 * @param textViewResourceId
//		 * @param objects
//		 */
//		public optionsAdapter(Context context, int resource,
//				int textViewResourceId, ChooserRow[] objects) {
//			super(context, resource, textViewResourceId, objects);
//			this.options = objects;
//		}
//
//
//		@Override
//		public View getView(int position, View convertView, ViewGroup parent) {
//			convertView = getActivity().getLayoutInflater().inflate(R.layout.item_list_chooser, null);
//			((TextView)convertView.findViewById(R.id.text)).setText(options[position].desc);
//			((ImageView)convertView.findViewById(R.id.icon)).setImageResource(options[position].icon);
//			AppUtil.setTextFonts(MyApp.appContext, convertView);
//			if (position == getCount() -1){
//				convertView.findViewById(R.id.divider).setVisibility(View.GONE);
//			}else{
//				convertView.findViewById(R.id.divider).setVisibility(View.VISIBLE);
//			}
//			return convertView;
//		}
//
//	}
//
//
//
//
//}
