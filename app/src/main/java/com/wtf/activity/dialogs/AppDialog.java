///**
// *
// */
//package com.wtf.activity.dialogs;
//
//
//
//import android.graphics.Color;
//import android.graphics.drawable.ColorDrawable;
//import android.os.Bundle;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.view.Window;
//import android.view.View.OnClickListener;
//import android.widget.Button;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.RelativeLayout;
//import android.widget.TextView;
//
//import com.wtf.activity.R;
//
//
///**
// * @author natiapplications
// *
// */
//public class AppDialog extends BaseDialogFragment implements OnClickListener{
//
//	public static final String DIALOG_NAME = "app_dialog";
//
//
//
//
//	private TextView titleTv;
//	private TextView messageTv;
//	private RelativeLayout iconFrame;
//	private ImageView iconIv;
//	private LinearLayout buttonsContainer;
//
//	private String title;
//	private String message;
//	private int iconBg;
//	private int icon;
//	private AppDialogButton[] buttons;
//
//	private DialogCallback callback;
//
//	/**
//	 *
//	 * @param callback
//	 */
//	public AppDialog(String title,
//			String message,int iconBg, int icon,AppDialogButton[] buttons,
//			DialogCallback callback){
//
//		this.callback = callback;
//		this.title = title;
//		this.message = message;
//		this.iconBg = iconBg;
//		this.icon = icon;
//		this.buttons = buttons;
//	}
//
//	public AppDialog () {
//
//	}
//
//
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container,
//            Bundle savedInstanceState) {
//        View view = inflater.inflate(R.layout.dialog_app, container, false);
//
//        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
//        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//
//        titleTv = (TextView)view.findViewById(R.id.title);
//        messageTv = (TextView)view.findViewById(R.id.sub_title);
//        iconFrame = (RelativeLayout)view.findViewById(R.id.icon_frame);
//        iconIv = (ImageView)view.findViewById(R.id.icon);
//        buttonsContainer = (LinearLayout)view.findViewById(R.id.buttons);
//
//
//        titleTv.setText(title);
//        messageTv.setText(message);
//        iconFrame.setBackgroundResource(iconBg);
//        iconIv.setImageResource(icon);
//
//    	for (int i = 0; i < buttons.length; i++) {
//			View v =  getActivity().getLayoutInflater().inflate(R.layout.dialog_btn, null);
//			Button button = (Button) v.findViewById(R.id.button);
//			button.setId(buttons[i].id);
//			button.setBackgroundResource(buttons[i].color);
//			button.setText(buttons[i].text);
//			button.setOnClickListener(this);
//			buttonsContainer.addView(v);
//		}
//
//        return view;
//    }
//
//
//
//
//    private void dismissDialog(){
//
//    	this.dismiss();
//    }
//
//
//
//	@Override
//	public void onClick(View v) {
//
//    	dismissDialog();
//
//    	if(callback != null){
//    		callback.onDialogButtonPressed(v.getId(), this);
//    	}
//	}
//
//
//}
//
//
