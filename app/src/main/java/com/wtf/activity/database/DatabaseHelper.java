/**
 * 
 */
package com.wtf.activity.database;



import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.wtf.activity.database.tables.TableCheckIns;
import com.wtf.activity.database.tables.TableFriends;

/**
 * @author natiapplications
 * 
 */
public class DatabaseHelper extends SQLiteOpenHelper {

	// database name
	private static final String DATABASE_NAME = "wtf.db";
	// database version
	private static final int DATABASE_VERSION = 8;

	

	public DatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase database) {
		TableFriends.onCreate(database);
		TableCheckIns.onCreate(database);

	}

	@Override
	public void onUpgrade(SQLiteDatabase database, int oldVersion,
			int newVersion) {
		TableFriends.onUpgrade(database, oldVersion, newVersion);
		TableCheckIns.onUpgrade(database, oldVersion, newVersion);

	}

}
