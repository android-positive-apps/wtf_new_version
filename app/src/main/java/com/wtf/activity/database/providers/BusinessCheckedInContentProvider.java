package com.wtf.activity.database.providers;

import android.database.sqlite.SQLiteOpenHelper;

import com.wtf.activity.database.BaseContentProvider;
import com.wtf.activity.database.DatabaseHelper;
import com.wtf.activity.database.tables.TableCheckIns;
import com.wtf.activity.database.tables.TableFriends;

/**
 * Created by NIVO on 30/12/2015.
 */
public class BusinessCheckedInContentProvider extends BaseContentProvider {

    @Override
    protected String provideAuthority() {
        // TODO Auto-generated method stub
        return "com.wtf.activity.database.providers.BusinessCheckedInContentProvider";
    }


    @Override
    protected String provideBasePath() {
        // TODO Auto-generated method stub
        return "checkins";
    }


    @Override
    protected String provideTableName() {
        // TODO Auto-generated method stub
        return TableCheckIns.TABLE_PROFILE;
    }


    @Override
    protected String provideColumnID() {
        // TODO Auto-generated method stub
        return TableCheckIns.TAG_ID;
    }


    @Override
    protected String[] provideAllColumns() {
        // TODO Auto-generated method stub
        return TableCheckIns.ALL_COLUMNS;
    }


    @Override
    protected SQLiteOpenHelper provideSQLiteHalper() {
        // TODO Auto-generated method stub
        return new DatabaseHelper(getContext());
    }


}
