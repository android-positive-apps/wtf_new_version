package com.wtf.activity.database.providers;

import android.database.sqlite.SQLiteOpenHelper;


import com.wtf.activity.database.BaseContentProvider;
import com.wtf.activity.database.DatabaseHelper;
import com.wtf.activity.database.tables.TableFriends;

/**
 * Created by NIVO on 30/12/2015.
 */
public class FriendContentProvider extends BaseContentProvider {

    @Override
    protected String provideAuthority() {
        // TODO Auto-generated method stub
        return "com.wtf.activity.database.providers.FriendContentProvider";
    }


    @Override
    protected String provideBasePath() {
        // TODO Auto-generated method stub
        return "friend";
    }


    @Override
    protected String provideTableName() {
        // TODO Auto-generated method stub
        return TableFriends.TABLE_PROFILE;
    }


    @Override
    protected String provideColumnID() {
        // TODO Auto-generated method stub
        return TableFriends.TAG_ID;
    }


    @Override
    protected String[] provideAllColumns() {
        // TODO Auto-generated method stub
        return TableFriends.ALL_COLUMNS;
    }


    @Override
    protected SQLiteOpenHelper provideSQLiteHalper() {
        // TODO Auto-generated method stub
        return new DatabaseHelper(getContext());
    }


}
