/**
 * 
 */
package com.wtf.activity.database;

import java.io.Serializable;

/**
 * @author natiapplications
 *
 */
public class SelectionBuilder implements Serializable{
	
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private final String AND = " and ";
	private final String OR = " or ";
	private final String EQUEAL = " = ";
	private final String BIG_THEN = " > ";
	private final String SMALL_THEN = " < ";
	private final String BIG_OR_EQUEAL = " >= ";
	private final String SMALL_OR_EQUEAL = " <= ";
	private final String NO_EQUEAL = " != ";
	private final String BRACKET_LEFT = " ( ";
	private final String BRACKET_RIGHT = " ) ";
	private final String LIKE = " LIKE ";
	
	private StringBuilder stringBuilder = new StringBuilder();
	
	
	public SelectionBuilder (){
		
	}
	
	public SelectionBuilder where(String s){
		stringBuilder.append(s);
		return this;
	}
	
	public SelectionBuilder equealTo (String s){
		
		stringBuilder.append(EQUEAL);
		stringBuilder.append("\"");
		stringBuilder.append(s);
		stringBuilder.append("\"");
		return this;
	}
	
	public SelectionBuilder equealTo (int i){
		stringBuilder.append(EQUEAL);
		stringBuilder.append(String.valueOf(i));
		return this;
	}
	
	public SelectionBuilder equealTo (double d){
		stringBuilder.append(EQUEAL);
		stringBuilder.append(String.valueOf(d));
		return this;
	}
	
	public SelectionBuilder equealTo (long l){
		stringBuilder.append(EQUEAL);
		stringBuilder.append(String.valueOf(l));
		return this;
	}
	
	
	public SelectionBuilder equealTo (float f){
		stringBuilder.append(EQUEAL);
		stringBuilder.append(String.valueOf(f));
		return this;
	}
	
	public SelectionBuilder noEquealTo (String s){
		
		stringBuilder.append(NO_EQUEAL);
		stringBuilder.append("\"");
		stringBuilder.append(s);
		stringBuilder.append("\"");
		return this;
	}
	
	public SelectionBuilder noEquealTo (int i){
		stringBuilder.append(NO_EQUEAL);
		stringBuilder.append(String.valueOf(i));
		return this;
	}
	
	public SelectionBuilder noEquealTo (double d){
		stringBuilder.append(NO_EQUEAL);
		stringBuilder.append(String.valueOf(d));
		return this;
	}
	
	
	public SelectionBuilder noEquealTo (long l){
		stringBuilder.append(NO_EQUEAL);
		stringBuilder.append(String.valueOf(l));
		return this;
	}
	
	public SelectionBuilder noEquealTo (float f){
		stringBuilder.append(NO_EQUEAL);
		stringBuilder.append(String.valueOf(f));
		return this;
	}
	public SelectionBuilder bigThen (int i){
		stringBuilder.append(BIG_THEN);
		stringBuilder.append(String.valueOf(i));
		return this;
	}
	
	public SelectionBuilder smallThen (int i){
		stringBuilder.append(SMALL_THEN);
		stringBuilder.append(String.valueOf(i));
		return this;
	}
	
	public SelectionBuilder bigOrEquealTo (int i){
		stringBuilder.append(BIG_OR_EQUEAL);
		stringBuilder.append(String.valueOf(i));
		return this;
	}
	
	public SelectionBuilder smallOrEquealTo (int i){
		stringBuilder.append(SMALL_OR_EQUEAL);
		stringBuilder.append(String.valueOf(i));
		return this;
	}
	
	
	public SelectionBuilder bigThen (double d){
		stringBuilder.append(BIG_THEN);
		stringBuilder.append(String.valueOf(d));
		return this;
	}
	
	public SelectionBuilder smallThen (double d){
		stringBuilder.append(SMALL_THEN);
		stringBuilder.append(String.valueOf(d));
		return this;
	}
	
	public SelectionBuilder bigOrEquealTo (double d){
		stringBuilder.append(BIG_OR_EQUEAL);
		stringBuilder.append(String.valueOf(d));
		return this;
	}
	
	public SelectionBuilder smallOrEquealTo (double d){
		stringBuilder.append(SMALL_OR_EQUEAL);
		stringBuilder.append(String.valueOf(d));
		return this;
	}
	
	
	public SelectionBuilder bigThen (long l){
		stringBuilder.append(BIG_THEN);
		stringBuilder.append(String.valueOf(l));
		return this;
	}
	
	public SelectionBuilder smallThen (long l){
		stringBuilder.append(SMALL_THEN);
		stringBuilder.append(String.valueOf(l));
		return this;
	}
	
	public SelectionBuilder bigOrEquealTo (long l){
		stringBuilder.append(BIG_OR_EQUEAL);
		stringBuilder.append(String.valueOf(l));
		return this;
	}
	
	public SelectionBuilder smallOrEquealTo (long l){
		stringBuilder.append(SMALL_OR_EQUEAL);
		stringBuilder.append(String.valueOf(l));
		return this;
	}
	
	public SelectionBuilder bigThen (float f){
		stringBuilder.append(BIG_THEN);
		stringBuilder.append(String.valueOf(f));
		return this;
	}
	
	public SelectionBuilder smallThen (float f){
		stringBuilder.append(SMALL_THEN);
		stringBuilder.append(String.valueOf(f));
		return this;
	}
	
	public SelectionBuilder bigOrEquealTo (float f){
		stringBuilder.append(BIG_OR_EQUEAL);
		stringBuilder.append(String.valueOf(f));
		return this;
	}
	
	public SelectionBuilder smallOrEquealTo (float f){
		stringBuilder.append(SMALL_OR_EQUEAL);
		stringBuilder.append(String.valueOf(f));
		return this;
	}
	
	
	public SelectionBuilder or(){
		stringBuilder.append(OR);
		return this;
	}
	
	public SelectionBuilder and(){
		stringBuilder.append(AND);
		return this;
	}
	
	
	public SelectionBuilder barcketLeft(){
		stringBuilder.append(BRACKET_LEFT);
		return this;
	}
	
	public SelectionBuilder barcketRight(){
		stringBuilder.append(BRACKET_RIGHT);
		return this;
	}
	
	
	public SelectionBuilder like(String like){
		stringBuilder.append(LIKE);
		stringBuilder.append("\"%" + like + "%\"");
		return this;
	}
	
	public String build (){
		return stringBuilder.toString();
	}
	

}
