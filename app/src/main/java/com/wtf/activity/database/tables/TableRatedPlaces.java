//package com.wtf.activity.database.tables;
//
//import android.content.ContentValues;
//import android.content.Context;
//import android.database.Cursor;
//import android.database.sqlite.SQLiteDatabase;
//import android.net.Uri;
//
//import com.wtf.activity.database.BaseContentProvider;
//import com.wtf.activity.database.ContentProvaiderDataSourseAdapter;
//import com.wtf.activity.database.providers.FriendContentProvider;
//import com.wtf.activity.objects.Business;
//import com.wtf.activity.objects.Friend;
//
///**
// * Created by Niv on 3/23/2016.
// */
//public class TableRatedPlaces extends ContentProvaiderDataSourseAdapter<Business> {
//
//// table name
//public static final String TABLE_PROFILE= "ratedPlaces";
//
//// columns names
//public final static String TAG_ID            = "_id";
//public final static String TAG_NAME            = "Name";
//public final static String TAG_EXTERNAL_ID   = "ExternalID";
//public final static String TAG_BUSINESS_ID   = "businessId";
//public final static String TAG_PHONE            = "Phone";
//public final static String TAG_PHONE2           = "Phone2";
//public final static String TAG_OPENING            = "Opening";
//public final static String TAG_CHECKINS            = "CheckIns";
//public final static String TAG_SPECIAL_RANK           = "SpecialRank";
//public final static String TAG_NUM_GIFTS            = "NumGifts";
//public final static String TAG_CATEGORY            = "Category";
//public final static String TAG_FAX            = "fax";
//public final static String TAG_COUNTRY            = "Country";
//public final static String TAG_IMAGE            = "image";
//public final static String TAG_CITY            = "City";
//public final static String TAG_STREET            = "Street";
//public final static String TAG_HOUSE            = "HouseNum";
//public final static String TAG_WEBSITE            = "website";
//public final static String TAG_FACEBOOK            = "FacebookLink";
//public final static String TAG_DESC            = "BusinessDesc";
//public final static String TAG_RANK           = "rank";
//public final static String TAG_NUM_COUPONS           = "NumCoupons";
//public final static String TAG_STATUS           = "Status";
//public final static String TAG_EDITED           = "Edited";
//public final static String TAG_LAT           = "lat";
//public final static String TAG_LNG           = "lng";
//public final static String TAG_ADMIN_RANK       = "AdminRank";
//public final static String TAG_SHOW_ADMIN_RANK          = "ShowAdminRank";
//public final static String TAG_CREATED         = "Created";
//public final static String TAG_NUM_RANKERS         = "NumRankers";
//public final static String TAG_SORT        = "sort";
//public final static String TAG_DISTANCE         = "Distance";
//
//
//
//public static final String[] ALL_COLUMNS = {
//        };
//
//public static final int COLUMN_SIZE = ALL_COLUMNS.length;
//
//// Database creation SQL statement
//private static final String DATABASE_CREATE = "create table " + TABLE_PROFILE
//        + "("
//        + TAG_ID               + " integer primary key autoincrement, "
//        + TAG_USERID        + " text " + ", "
//        + TAG_FBID          + " text " + ", "
//        + TAG_LAT           + " text " + ", "
//        + TAG_LNG           + " text " + ", "
//        + TAG_FIRST_NAME    + " text " + ", "
//        + TAG_LAST_NAME     + " text " + ", "
//        + TAG_IMAGE         + " text " + ", "
//        + TAG_LOCATION_DESC            + " text"
//        +");";
//
//public static void onCreate(SQLiteDatabase database) {
//        database.execSQL(DATABASE_CREATE);
//        }
//
//public static void onUpgrade(SQLiteDatabase database, int oldVersion,
//        int newVersion) {
//        database.execSQL("DROP TABLE IF EXISTS " + TABLE_PROFILE);
//        onCreate(database);
//        }
//
//
//public static TableFriends instance;
//
//
//
//public static TableFriends getInstance (Context context){
//        if (instance == null){
//        instance = new TableFriends(context);
//        }
//        return instance;
//        }
//
//
//private TableFriends(Context context) {
//        super(context);
//
//        }
//
//
//@Override
//protected Uri getProviderUri() {
//        return FriendContentProvider.CONTENT_URI;
//        }
//
//@Override
//public void close() {
//        if (databasehelper != null){
//        databasehelper.close();
//        }
//
//        instance = null;
//        }
//
//
//
//@Override
//protected void onCreateDataSourse(BaseContentProvider contentProvider) {
//        super.onCreateDataSourse(contentProvider);
//
//        }
//
//
//@Override
//public Friend cursorToEntity(Cursor cursor) {
//
//        long id =  cursor.getLong(cursor.getColumnIndexOrThrow(TableFriends.TAG_ID));
//        String user_id=cursor.getString(cursor.getColumnIndexOrThrow(TableFriends.TAG_USERID));
//        String fbID=cursor.getString(cursor.getColumnIndexOrThrow(TableFriends.TAG_FBID));
//        String lat=cursor.getString(cursor.getColumnIndexOrThrow(TableFriends.TAG_LAT));
//        String lng=cursor.getString(cursor.getColumnIndexOrThrow(TableFriends.TAG_LNG));
//        String FirstName=cursor.getString(cursor.getColumnIndexOrThrow(TableFriends.TAG_FIRST_NAME));
//        String LastName=cursor.getString(cursor.getColumnIndexOrThrow(TableFriends.TAG_LAST_NAME));
//        String image=cursor.getString(cursor.getColumnIndexOrThrow(TableFriends.TAG_IMAGE));
//        String locationDesc=cursor.getString(cursor.getColumnIndexOrThrow(TableFriends.TAG_LOCATION_DESC));
//
//        Friend friend= new Friend(id,fbID,lat,lng,user_id,FirstName,LastName,image,locationDesc);
//
//
//        return friend;
//        }
//
//@Override
//public ContentValues entityToContentValue(Friend entity) {
//        ContentValues values = new ContentValues();
//
//
//        values.put(TableFriends.TAG_USERID, entity.getID());
//        values.put(TableFriends.TAG_ID      , entity.getUser_id());
//        values.put(TableFriends.TAG_FBID         , entity.getFBID());
//        values.put(TableFriends.TAG_FIRST_NAME   , entity.getFirstName());
//        values.put(TableFriends.TAG_LAST_NAME    , entity.getLastName());
//        values.put(TableFriends.TAG_IMAGE        , entity.getImage());
//        values.put(TableFriends.TAG_LOCATION_DESC, entity.getLocationDesc());
//        values.put(TableFriends.TAG_LAT          , entity.getLat());
//        values.put(TableFriends.TAG_LNG          , entity.getLng());
//        return values;
//        }
//
//
//
//@Override
//protected BaseContentProvider createContentProvider() {
//        return new FriendContentProvider();
//        }
//
//
//@Override
//public synchronized Friend insert(Friend entity) {
//        return super.insert(entity);
//
//        }
//
//public Friend isEntryAllradyExsit (Friend toCheck){
//
//        try {
//        Friend msg = readEntityByID(toCheck.getUser_id());
//        if (msg != null){
//        return msg;
//        }
//        } catch (Exception e) {
//        return null;
//        }
//
//
//        return null;
//
//        }
//
//public Friend insertOrUpdate (Friend toAdd) {
//        Friend check = isEntryAllradyExsit(toAdd);
//        if (check != null){
//        return update(check.getUser_id(), toAdd);
//        }else{
//        return insert(toAdd);
//        }
//        }
//        }
//
//
