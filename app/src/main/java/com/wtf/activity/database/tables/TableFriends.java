package com.wtf.activity.database.tables;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;


import com.wtf.activity.database.BaseContentProvider;
import com.wtf.activity.database.ContentProvaiderDataSourseAdapter;
import com.wtf.activity.database.providers.FriendContentProvider;
import com.wtf.activity.objects.Friend;

/**
 * Created by NIVO on 13/12/2015.
 */
public class TableFriends extends ContentProvaiderDataSourseAdapter<Friend> {

    // table name
    public static final String TABLE_PROFILE = "friend";

    // columns names
    public final static String TAG_ID = "_id";
    public final static String TAG_USERID = "userId";
    public final static String TAG_FBID = "fbID";
    public final static String TAG_LAT = "lat";
    public final static String TAG_LNG = "lng";
    public final static String TAG_FIRST_NAME = "FirstName";
    public final static String TAG_LAST_NAME = "LastName";
    public final static String TAG_IMAGE = "image";
    public final static String TAG_LOCATION_DESC = "locationDesc";
    public final static String TAG_PHONE= "phone";


    public static final String[] ALL_COLUMNS = {
            TAG_ID, TAG_FBID, TAG_LNG, TAG_FIRST_NAME, TAG_LAT, TAG_LAST_NAME, TAG_IMAGE, TAG_LOCATION_DESC, TAG_USERID,TAG_PHONE
    };

    public static final int COLUMN_SIZE = ALL_COLUMNS.length;

    // Database creation SQL statement
    private static final String DATABASE_CREATE = "create table " + TABLE_PROFILE
            + "("
            + TAG_ID + " integer primary key autoincrement, "
            + TAG_USERID + " text " + ", "
            + TAG_FBID + " text " + ", "
            + TAG_LAT + " text " + ", "
            + TAG_LNG + " text " + ", "
            + TAG_FIRST_NAME + " text " + ", "
            + TAG_LAST_NAME + " text " + ", "
            + TAG_IMAGE + " text " + ", "
            + TAG_PHONE + " text " + ", "
            + TAG_LOCATION_DESC + " text"
            + ");";

    public static void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }

    public static void onUpgrade(SQLiteDatabase database, int oldVersion,
                                 int newVersion) {
        database.execSQL("DROP TABLE IF EXISTS " + TABLE_PROFILE);
        onCreate(database);
    }


    public static TableFriends instance;


    public static TableFriends getInstance(Context context) {
        if (instance == null) {
            instance = new TableFriends(context);
        }
        return instance;
    }


    private TableFriends(Context context) {
        super(context);

    }


    @Override
    protected Uri getProviderUri() {
        return FriendContentProvider.CONTENT_URI;
    }

    @Override
    public void close() {
        if (databasehelper != null) {
            databasehelper.close();
        }

        instance = null;
    }


    @Override
    protected void onCreateDataSourse(BaseContentProvider contentProvider) {
        super.onCreateDataSourse(contentProvider);

    }


    @Override
    public Friend cursorToEntity(Cursor cursor) {

        long id = cursor.getLong(cursor.getColumnIndexOrThrow(TableFriends.TAG_ID));
        String user_id = cursor.getString(cursor.getColumnIndexOrThrow(TableFriends.TAG_USERID));
        String fbID = cursor.getString(cursor.getColumnIndexOrThrow(TableFriends.TAG_FBID));
        String lat = cursor.getString(cursor.getColumnIndexOrThrow(TableFriends.TAG_LAT));
        String lng = cursor.getString(cursor.getColumnIndexOrThrow(TableFriends.TAG_LNG));
        String FirstName = cursor.getString(cursor.getColumnIndexOrThrow(TableFriends.TAG_FIRST_NAME));
        String LastName = cursor.getString(cursor.getColumnIndexOrThrow(TableFriends.TAG_LAST_NAME));
        String image = cursor.getString(cursor.getColumnIndexOrThrow(TableFriends.TAG_IMAGE));
        String locationDesc = cursor.getString(cursor.getColumnIndexOrThrow(TableFriends.TAG_LOCATION_DESC));
        String phone = cursor.getString(cursor.getColumnIndexOrThrow(TableFriends.TAG_PHONE));


        Friend friend = new Friend(id, fbID, lat, lng, user_id, FirstName, LastName, image, locationDesc,phone);


        return friend;
    }

    @Override
    public ContentValues entityToContentValue(Friend entity) {
        ContentValues values = new ContentValues();

        values.put(TableFriends.TAG_USERID, entity.getID());
        values.put(TableFriends.TAG_FBID, entity.getFBID());
        values.put(TableFriends.TAG_FIRST_NAME, entity.getFirstName());
        values.put(TableFriends.TAG_LAST_NAME, entity.getLastName());
        values.put(TableFriends.TAG_IMAGE, entity.getImage());
        values.put(TableFriends.TAG_LOCATION_DESC, entity.getLocationDesc());
        values.put(TableFriends.TAG_LAT, entity.getLat());
        values.put(TableFriends.TAG_LNG, entity.getLng());
        values.put(TableFriends.TAG_PHONE, entity.getPhone());
        return values;
    }


    @Override
    protected BaseContentProvider createContentProvider() {
        return new FriendContentProvider();
    }


    @Override
    public synchronized Friend insert(Friend entity) {
        return super.insert(entity);

    }

    public Friend isEntryAllradyExsit(Friend toCheck) {

        try {
            Friend msg = readEntityByID(toCheck.getUser_id());
            if (msg != null) {
                return msg;
            }
        } catch (Exception e) {
            return null;
        }


        return null;

    }

    public Friend insertOrUpdate(Friend toAdd) {
        Friend check = isEntryAllradyExsit(toAdd);
        if (check != null) {
            return update(check.getUser_id(), toAdd);
        } else {
            return insert(toAdd);
        }
    }
}

