/**
 * 
 */
package com.wtf.activity.database;

import java.util.ArrayList;
import java.util.Set;

import android.content.ContentValues;
import android.content.Context;
import android.content.CursorLoader;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.os.Bundle;

import com.wtf.activity.database.tables.TableObserver;

/**
 * @author natiapplications
 *
 */
public abstract class ContentProvaiderDataSourseAdapter<T> {


	public static final String DEFAULT_TEXT_VALUE = "NA";
	public static final int DEFAULT_INT_VALUE = 0;
	public static final String DEFAULT_TIME_VALUE = "(strftime('%s', 'now'))";
	public static final String DEFAULT_TYPE_VALUE ="0";


	protected ArrayList<TableObserver> observers = new ArrayList<TableObserver>();

	protected Context context;
	protected SQLiteOpenHelper databasehelper;
	protected SQLiteDatabase database;
	protected String[] allColumns;
	protected String tableName;
	protected String columnsID;
	protected Uri provaiderURI;

	public ContentProvaiderDataSourseAdapter (Context context){
		this.context = context;
		onCreateDataSourse(createContentProvider());
	}

	protected void onCreateDataSourse (BaseContentProvider contentProvider){
		databasehelper = contentProvider.getDatabaseHelper();
		database = contentProvider.getDatabase();
		allColumns = contentProvider.getALL_COLUMNS();
		tableName = contentProvider.getTABLE_NAME();
		columnsID = contentProvider.getCOLUMN_ID();
		provaiderURI = getProviderUri();
	}


	public abstract T cursorToEntity(Cursor cursor);
	public abstract ContentValues entityToContentValue(T entity);
	public abstract void close();
	protected abstract Uri getProviderUri ();
	protected abstract BaseContentProvider createContentProvider ();


	synchronized public T insert(T entity) {

		Uri uri = context.getContentResolver().insert
				(provaiderURI, entityToContentValue(entity));

		notifyObservers(TableObserver.ACTION_INSERT);

		return readEntityByID(Long.parseLong(uri.getLastPathSegment()));
	}



	synchronized public boolean delete(long id,T entity) {

		long numDeleted = context.getContentResolver().delete(provaiderURI,
				columnsID + " = " + id, null);

		boolean result = numDeleted > 0 ? true:false;

		if (result){
			notifyObservers(TableObserver.ACTION_DELETE);
		}

		return result;
	}

	synchronized public void deleteAll() {
		context.getContentResolver().delete(provaiderURI, null, null);

		notifyObservers(TableObserver.ACTION_DELETE);

	}


	synchronized public void deleteByFillter(Bundle conditions) {
		String conString = cretateCondintionsStringByBundle(conditions);
		context.getContentResolver().delete(provaiderURI, conString, null);

		notifyObservers(TableObserver.ACTION_DELETE);
	}


	synchronized public void deleteByFillter(String selection) {
		context.getContentResolver().delete(provaiderURI, selection, null);

		notifyObservers(TableObserver.ACTION_DELETE);
	}

	synchronized public T update(long id,T entity) {

		context.getContentResolver().update(provaiderURI, entityToContentValue(entity)
				, columnsID + " = " + id, null);

		notifyObservers(TableObserver.ACTION_UPDATE);

		return entity;
	}



	synchronized public T readEntityByID(long entityId) {
		Cursor cursor = context.getContentResolver().query(provaiderURI,
				allColumns, columnsID + " = " + entityId, null,
				null);
		if (cursor != null) {
			cursor.moveToFirst();
			T newItem = cursorToEntity(cursor);
			cursor.close();
			return newItem;
		}
		return null;
	}


	synchronized public ArrayList<T> readAll(String sortedBy) {
		return read("", sortedBy);
	}


	synchronized public ArrayList<T> read(Bundle condintions, String sortedBy) {
		Cursor cursor = null;

		if (condintions == null){
			cursor = context.getContentResolver().query(provaiderURI,
					allColumns, null, null,sortedBy);
		}else{
			String conString = cretateCondintionsStringByBundle(condintions);
			cursor =context.getContentResolver().query(provaiderURI,
					allColumns, conString, null, sortedBy);
		}

		ArrayList<T> items = new ArrayList<T>();
		if (cursor != null) {
			cursor.moveToFirst();
			while (!cursor.isAfterLast()) {
				T item = cursorToEntity(cursor);
				items.add(item);
				cursor.moveToNext();
			}
			cursor.close();
		}

		return items;
	}


	synchronized public ArrayList<T> read(String selection, String sortedBy) {
		Cursor cursor = null;

		if (selection.isEmpty()){
			cursor = context.getContentResolver().query(provaiderURI,
					allColumns, null, null,sortedBy);
		}else{
			cursor =context.getContentResolver().query(provaiderURI,
					allColumns, selection, null, sortedBy);
		}

		ArrayList<T> items = new ArrayList<T>();
		if (cursor != null) {
			if (cursor != null) {
				cursor.moveToFirst();
				while (!cursor.isAfterLast()) {

					T item = cursorToEntity(cursor);
					items.add(item);
					cursor.moveToNext();
				}
				cursor.close();
			}
		}

		return items;
	}

	synchronized public Cursor readCursor(String sortedBy) {
		return readCursor(sortedBy);
	}

	synchronized public CursorLoader readLouderCursor(String sortedBy) {
		CursorLoader cursorLoader = new CursorLoader(context, provaiderURI,
				allColumns, null, null, sortedBy);
		return cursorLoader;
	}

	synchronized public CursorLoader readLouderCursor(Bundle conditions,String sortedBy) {

		CursorLoader loader = null;
		if (conditions == null){

			loader = new CursorLoader(context, provaiderURI,
					allColumns, null, null, sortedBy);
		}else{
			String conString = cretateCondintionsStringByBundle(conditions);
			loader = new CursorLoader(context, provaiderURI,
					allColumns, conString, null, sortedBy);
		}

		return loader;
	}


	synchronized public CursorLoader readLouderCursor(String selection,String sortedBy) {

		CursorLoader loader = null;
		if (selection == null){

			loader = new CursorLoader(context, provaiderURI,
					allColumns, null, null, sortedBy);
		}else{
			loader = new CursorLoader(context, provaiderURI,
					allColumns, selection, null, sortedBy);
		}

		return loader;
	}

	synchronized public Cursor readCursor(Bundle condintions, String sortedBy) {
		Cursor cursor = null;
		if (condintions == null){
			cursor = context.getContentResolver().query(provaiderURI,
					allColumns, null, null,  sortedBy);
		}else{
			String conString = cretateCondintionsStringByBundle(condintions);
			cursor = context.getContentResolver().query(provaiderURI,
					allColumns, conString, null,  sortedBy);
		}
		return cursor;
	}


	synchronized public Cursor readCursor(String selection, String sortedBy) {
		Cursor cursor = null;
		if (selection == null){
			cursor = context.getContentResolver().query(provaiderURI,
					allColumns, null, null,  sortedBy);
		}else{
			cursor = context.getContentResolver().query(provaiderURI,
					allColumns, selection, null,  sortedBy);
		}
		return cursor;
	}

	public  String cretateCondintionsStringByBundle (Bundle condintions){
		StringBuilder conString = new StringBuilder();
		Set<String> keys = condintions.keySet();
		int counter = 0;
		for (String key : keys) {
			if (condintions.get(key) instanceof String){
				conString.append(key + " = " + "\""+ condintions.get(key) + "\"");
			}else{
				conString.append(key + " = " + condintions.get(key));
			}
			if (counter < keys.size()-1){
				conString.append(" and ");
			}
			counter++;
		}
		return conString.toString();
	}


	public void addObserver (TableObserver observer){
		if (!this.observers.contains(observer)){
			this.observers.add(observer);
		}
	}

	public  boolean removeObserver (TableObserver observer){
		return this.observers.remove(observer);
	}

	protected void notifyObservers (int action){
		for (int i = 0; i < observers.size(); i++) {
			try{
				observers.get(i).onTableChanged(this.tableName,action);
			}catch (Exception e){
				continue;
			}catch (Error e){
				continue;
			}
		}
	}
}
