package com.wtf.activity.database.tables;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import com.wtf.activity.database.BaseContentProvider;
import com.wtf.activity.database.ContentProvaiderDataSourseAdapter;
import com.wtf.activity.database.providers.BusinessCheckedInContentProvider;
import com.wtf.activity.objects.Business;
import com.wtf.activity.objects.CheckIn;
import com.wtf.activity.objects.Friend;

/**
 * Created by NIVO on 13/12/2015.
 */
public class TableCheckIns extends ContentProvaiderDataSourseAdapter<Business> {

    // table name
    public static final String TABLE_PROFILE = "checkins";

    // columns names
    public final static String TAG_ID           = "_id";
    public final static String TAG_NAME         = "Name";
    public final static String TAG_CITY         = "City";
    public final static String TAG_EXTERNAL_ID  = "ExternalID";
    public final static String TAG_BUSINESS_ID  = "businessId";
    public final static String TAG_DATE_CHECKED = "dateChecked";
    public final static String TAG_LAT          = "lat";
    public final static String TAG_LNG          = "lng";
    public final static String TAG_CATEGORY     = "Category";
    public final static String TAG_IMAGE  = "image";




    public static final String[] ALL_COLUMNS = {
            TAG_ID, TAG_NAME, TAG_LNG,TAG_IMAGE, TAG_CITY, TAG_LAT, TAG_EXTERNAL_ID, TAG_BUSINESS_ID, TAG_DATE_CHECKED, TAG_CATEGORY
    };

    public static final int COLUMN_SIZE = ALL_COLUMNS.length;

    // Database creation SQL statement
    private static final String DATABASE_CREATE = "create table " + TABLE_PROFILE
            + "("
            + TAG_ID +              " integer primary key autoincrement, "
            + TAG_NAME             + " text " + ", "
            + TAG_CITY             + " text " + ", "
            + TAG_EXTERNAL_ID      + " text " + ", "
            + TAG_BUSINESS_ID      + " text " + ", "
            + TAG_DATE_CHECKED     + " text " + ", "
            + TAG_CATEGORY         + " text " + ", "
            + TAG_IMAGE         + " text " + ", "
            + TAG_LAT              + " text " + ", "
            + TAG_LNG              + " text"
            + ");";

    public static void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }

    public static void onUpgrade(SQLiteDatabase database, int oldVersion,
                                 int newVersion) {
        database.execSQL("DROP TABLE IF EXISTS " + TABLE_PROFILE);
        onCreate(database);
    }


    public static TableCheckIns instance;


    public static TableCheckIns getInstance(Context context) {
        if (instance == null) {
            instance = new TableCheckIns(context);
        }
        return instance;
    }


    private TableCheckIns(Context context) {
        super(context);

    }


    @Override
    protected Uri getProviderUri() {
        return BusinessCheckedInContentProvider.CONTENT_URI;
    }

    @Override
    public void close() {
        if (databasehelper != null) {
            databasehelper.close();
        }

        instance = null;
    }


    @Override
    protected void onCreateDataSourse(BaseContentProvider contentProvider) {
        super.onCreateDataSourse(contentProvider);

    }


    @Override
    public Business cursorToEntity(Cursor cursor) {

        long id = cursor.getLong(cursor.getColumnIndexOrThrow(TableCheckIns.TAG_ID));
        String name = cursor.getString(cursor.getColumnIndexOrThrow(TableCheckIns.TAG_NAME));
        String category    = cursor.getString(cursor.getColumnIndexOrThrow(TableCheckIns.TAG_CATEGORY));
        String lat = cursor.getString(cursor.getColumnIndexOrThrow(TableCheckIns.TAG_LAT));
        String lng = cursor.getString(cursor.getColumnIndexOrThrow(TableCheckIns.TAG_LNG));
        String city = cursor.getString(cursor.getColumnIndexOrThrow(TableCheckIns.TAG_CITY));
        String date = cursor.getString(cursor.getColumnIndexOrThrow(TableCheckIns.TAG_DATE_CHECKED));
        String businesID = cursor.getString(cursor.getColumnIndexOrThrow(TableCheckIns.TAG_BUSINESS_ID));
        String extID = cursor.getString(cursor.getColumnIndexOrThrow(TableCheckIns.TAG_EXTERNAL_ID));
        String IMAGE = cursor.getString(cursor.getColumnIndexOrThrow(TableCheckIns.TAG_IMAGE));


        Business business = new Business(id,name,category,lat,lng,city,date,businesID,extID,IMAGE);


        return business;
    }

    @Override
    public ContentValues entityToContentValue(Business entity) {
        ContentValues values = new ContentValues();

        values.put(TableCheckIns.TAG_BUSINESS_ID, entity.getID());
        values.put(TableCheckIns.TAG_CATEGORY, entity.getCategory());
        values.put(TableCheckIns.TAG_DATE_CHECKED, entity.getDateCheckedIn());
        values.put(TableCheckIns.TAG_CITY, entity.getCity());
        values.put(TableCheckIns.TAG_EXTERNAL_ID, entity.getExternalID());
        values.put(TableCheckIns.TAG_NAME, entity.getName());
        values.put(TableCheckIns.TAG_LAT, entity.getLat());
        values.put(TableCheckIns.TAG_LNG, entity.getLng());
        values.put(TableCheckIns.TAG_IMAGE, entity.getImage());
        return values;
    }


    @Override
    protected BaseContentProvider createContentProvider() {
        return new BusinessCheckedInContentProvider();
    }


    @Override
    public synchronized Business insert(Business entity) {
        return super.insert(entity);
    }

    public Business isEntryAllradyExsit(Business toCheck) {

        try {
            Business msg = readEntityByID(toCheck.getTableID());
            if (msg != null) {
                return msg;
            }
        } catch (Exception e) {
            return null;
        }


        return null;

    }

    public Business insertOrUpdate(Business toAdd) {
        Business check = isEntryAllradyExsit(toAdd);
        if (check != null) {
            return update(check.getTableID(), toAdd);
        } else {
            return insert(toAdd);
        }
    }
}

