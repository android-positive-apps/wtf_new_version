package com.wtf.activity.database.tables;

/**
 * Created by natiapplications on 25/08/15.
 */
public interface TableObserver {

    public static final int ACTION_UPDATE = 0;
    public static final int ACTION_DELETE = 1;
    public static final int ACTION_INSERT = 2;


    public static final String[] ACTION_NAME = {"UPDATE","DELETE","INSERT"};

    public void onTableChanged(String tableName, int action);
}
