/**
 * 
 */
package com.wtf.activity.util;
import java.util.List;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.media.RingtoneManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;

import com.wtf.activity.main.MyApp;
import com.wtf.activity.R;


/**
 * @author natiapplications
 *
 */
public class NotificationUtil {
	
	public static final int CHAT_NOTIFICATION_ID = 1;
	public static final int BID_NOTIFICATION_ID = 2;
	public static final int CANCEL_BID_NOTIFICATION_ID = 3;
	public static final int PACKAGE_PAYED_NOTIFICATION_ID = 4;
	public static final int PACKAGE_EXPERIED_NOTIFICATION_ID = 5;
	public static final int TIMED_OUT_NOTIFICATION_ID = 6;
	

	
	private static void notify (Class<?> activity,
			String extraKey,
			String messageContent,
			int notificationId,
			String extra,boolean notificationSettings){
		
		if (!notificationSettings){
			return;
		}
		NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
				MyApp.appContext)
				.setSmallIcon(R.drawable.ic_launcher)
				.setContentTitle(messageContent)
				.setStyle(new NotificationCompat.BigTextStyle().bigText
						(messageContent))
				.setContentText(messageContent)
				.setSound( RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));

		Intent resultIntent = new Intent(MyApp.appContext, activity);
		resultIntent.putExtra(extraKey, extra);
		TaskStackBuilder stackBuilder = TaskStackBuilder.create(MyApp.appContext);
		stackBuilder.addParentStack(activity);
		stackBuilder.addNextIntent(resultIntent);
		PendingIntent resultPendingIntent =
		        stackBuilder.getPendingIntent(
		            0,
		            PendingIntent.FLAG_UPDATE_CURRENT
		        );
		mBuilder.setContentIntent(resultPendingIntent);
		
		NotificationManager mNotificationManager = (NotificationManager) 
				MyApp.appContext.getSystemService(Context.NOTIFICATION_SERVICE);
		mNotificationManager.notify(notificationId, mBuilder.build());
	}
	
	



	public static void setBadge(Context context, int count) {
	    String launcherClassName = getLauncherClassName(context);
	    if (launcherClassName == null) {
	        return;
	    }
	    Intent intent = new Intent("android.intent.action.BADGE_COUNT_UPDATE");
	    intent.putExtra("badge_count", count);
	    intent.putExtra("badge_count_package_name", context.getPackageName());
	    intent.putExtra("badge_count_class_name", launcherClassName);
	    context.sendBroadcast(intent);
	}

	public static String getLauncherClassName(Context context) {

	    PackageManager pm = context.getPackageManager();

	    Intent intent = new Intent(Intent.ACTION_MAIN);
	    intent.addCategory(Intent.CATEGORY_LAUNCHER);

	    List<ResolveInfo> resolveInfos = pm.queryIntentActivities(intent, 0);
	    for (ResolveInfo resolveInfo : resolveInfos) {
	        String pkgName = resolveInfo.activityInfo.applicationInfo.packageName;
	        if (pkgName.equalsIgnoreCase(context.getPackageName())) {
	            String className = resolveInfo.activityInfo.name;
	            return className;
	        }
	    }
	    return null;
	}

}
