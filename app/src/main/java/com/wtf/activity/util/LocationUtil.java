package com.wtf.activity.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;

import com.wtf.activity.location.Coordinates;


public class LocationUtil {
	
private final String TAG = "LocationUtil";
	
	public static final String KEY_CITY = "City";
	public static final String KEY_STREET = "Street";
	public static final String KEY_HAUSE = "Hause";
	public static final String KEY_COUNTRY = "Country";
	
	
	private static LocationUtil inctance;
	
	
	private Context context;
	
	
	private LocationUtil (Context context){
		this.context = context;
	}

	public static LocationUtil getInstance (Context context){
		if (inctance == null){
			inctance = new LocationUtil(context);
		}
		return inctance;
	}
	
	public void getPickupLocationByPickupAddress(String fullAddress,locationUtilCallbac callbac) {
		Geocoder geocoder = new Geocoder(context);
		List<Address> addresses;
		try {
			addresses = geocoder.getFromLocationName(fullAddress, 1);
		} catch (IOException e) {
			addresses = new ArrayList<Address>();
		}

		if (addresses.size() > 0) {
			double latitude = addresses.get(0).getLatitude();
			double longitude = addresses.get(0).getLongitude();
			Log.e(TAG, "lat - " + latitude + "lon - " + longitude);
			Coordinates coordinates = new Coordinates(latitude,longitude);
			if (callbac != null){
				callbac.onLocationReceived(true, coordinates);
			}
			return;
		}
		if (callbac != null){
			callbac.onLocationReceived(false, null);
		}
	}


	
	
	public static double[] getBoundsAroundLocation (int radius,Coordinates location){
		double[] result = new double[4];
		 double kmInLongitudeDegree = 111.320 * Math.cos(location.lat / 180.0
                 * Math.PI);

         double deltaLat = radius / 111.1;
         double deltaLong = radius / kmInLongitudeDegree;

         double minLat = location.lat - deltaLat;
         double maxLat = location.lat + deltaLat;
         double minLong = location.lon - deltaLong;
         double maxLong = location.lon + deltaLong;
         
         result[0] = minLat;
         result[1] = maxLat;
         result[2] = minLong;
         result[3] = maxLong;
         
         return result;
		
	}
	public void getLocationDescription(final Coordinates location,
			final locationUtilCallbac listener) {

		new getAddressTask(location.lat,location.lon,listener).execute();

	}
	
	

	
	
	public class locationUtilCallbac {
		public void onDescriptionsRceived(HashMap<String, String> locationDescription){

		}
		public void onLocationReceived(boolean success,Coordinates coordinates){

		}
	}
	

	
	private class getAddressTask extends AsyncTask<Void, Void, Void> {
		
		
		private final String STATUS = "status";
		private final String OK = "OK";
		private final String RESULT = "results";
		private final String ADDRESS_COMPONETS = "address_components";
		private final String LONG_NAME = "long_name";
		private final String TYPES = "types";
		private final String STREET_NUMBER = "street_number";
		private final String LOCALITY = "locality";
		private final String ROUTE = "route";
		private final String COUNTRY = "country";
		
		
		private JSONObject result;
		private String language;
		private double lat,lon;
		private locationUtilCallbac listener;
		
		

		public getAddressTask(double lat, double lon,
							  locationUtilCallbac listener) {
			super();
			this.lat = lat;
			this.lon = lon;
			this.listener = listener;
		}

		@Override
		protected void onPreExecute() {
			
			language = "en";
		}

		@Override
		protected Void doInBackground(Void... urls) {
		
			result = getLocationInfo(lat, lon,
					language);
			return  null;
		}

		@Override
		protected void onPostExecute(Void unused) {
		
			String city = "";
			String address = "";
			String streetNumber = "";
			String country = "";
			
			try {
				String status = result.getString(STATUS).toString();
				

				if (status.equalsIgnoreCase(OK)) {
					JSONArray results = result.getJSONArray(RESULT);
					JSONObject zero = results.getJSONObject(0);
					JSONArray address_components = zero
							.getJSONArray(ADDRESS_COMPONETS);
					for (int i = 0; i < address_components.length(); i++) {
						JSONObject r = address_components.getJSONObject(i);
						String long_name = r.getString(LONG_NAME);
						JSONArray typesArray = r.getJSONArray(TYPES);
						String Type = typesArray.getString(0);
						
						if (TextUtils.isEmpty(long_name) == false
								|| !long_name.equals(null)
								|| long_name.length() > 0 || long_name != "") {
							if (Type.equalsIgnoreCase(STREET_NUMBER)) {
								streetNumber = long_name;
							} else if (Type.equalsIgnoreCase(LOCALITY)) {
								city = long_name;
							} else if (Type.equalsIgnoreCase(ROUTE)) {
								address = long_name;
							}else if (Type.equalsIgnoreCase(COUNTRY)) {
								country = long_name;
							}
						}
						
					}

				}

			} catch (Exception e) {}finally{
				final HashMap<String, String> addressResult = new HashMap<String,String>();
				addressResult.put(KEY_CITY,city.replace(" ", " "));
				addressResult.put(KEY_STREET, address.replace(" ", " "));
				addressResult.put(KEY_HAUSE,streetNumber.replace(" ", " "));
				addressResult.put(KEY_COUNTRY,country.replace(" ", " "));
				listener.onDescriptionsRceived(addressResult);
			}
			
		}

	}

	// Send Request To Google
	public JSONObject getLocationInfo(double lat, double lng, String lang) {
		

		HttpGet httpGet = new HttpGet(
				"http://maps.google.com/maps/api/geocode/json?latlng=" + lat
						+ "," + lng + "&sensor=false&language=" + lang);
		HttpClient client = new DefaultHttpClient();
		HttpResponse response;
		StringBuilder stringBuilder = new StringBuilder();

		try {
			response = client.execute(httpGet);
			HttpEntity entity = response.getEntity();
			InputStream stream = entity.getContent();
			BufferedReader in = new BufferedReader(new InputStreamReader(
					stream, "UTF-8"));
			String line;
			while ((line = in.readLine()) != null) {
				stringBuilder.append(line);
			}
			
		} catch (ClientProtocolException e) { 
		} catch (IOException e) {
		}

		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject = new JSONObject(stringBuilder.toString());
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return jsonObject;
	}

}
