/**
 * 
 */
package com.wtf.activity.util;

import android.content.Context;

import com.wtf.activity.R;
import com.wtf.activity.main.MyApp;

import java.text.DecimalFormat;


/**
 * @author natiapplications
 *
 */
public class DistanceDescriptor {
	
	private double value;
	private String description;
	public DistanceDescriptor (double value){
		this.value = value;
		buildDescription ();
	}
	
	
	private void buildDescription() {
		if (value >= MyApp.userSettings.getDistanceUnitFactor()){
			value /= MyApp.userSettings.getDistanceUnitFactor();
			description  = new DecimalFormat("##.##").format(value) +
					" " + MyApp.userSettings.getDistanceUnitName();
		}else {
			description  = new DecimalFormat("##.##").format(value) +
					" " + "m";
		}
		
		
	}
	
	public String describe () {
		if (description != null && !description.isEmpty()){
			return description;
		}
		return "0.0" +
				" " + "m";
	}

	/**
	 * Calculates the distance in km between two lat/long points
	 * using the haversine formula
	 */
	public static double getDistancelATlNG(double lat1, double lng1, double lat2, double lng2) {
		int r = 6371; // average radius of the earth in km
		double dLat = Math.toRadians(lat2 - lat1);
		double dLon = Math.toRadians(lng2 - lng1);
		double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
				Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
						* Math.sin(dLon / 2) * Math.sin(dLon / 2);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		double d = r * c;
		return d;
	}
}
