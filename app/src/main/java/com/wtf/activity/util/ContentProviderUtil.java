/**
 * 
 */
package com.wtf.activity.util;





import java.io.File;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.LocationManager;
import android.net.Uri;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.wtf.activity.R;


/**
 * @author natiapplications
 *
 */
public class ContentProviderUtil {
	/*Uri uri = Uri.parse("geo:0,0?q=" + location.replace(" ", "+"));
               showMap(uri);
               */
	public static void showMap(Uri geoLocation, Activity activity) {
		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.setData(geoLocation);
		if (intent.resolveActivity(activity.getPackageManager()) != null) {
			activity.startActivity(intent);
		}
	}
	
	public static void openLocationChooser (Activity activity,String lat,String lon) {
		Uri location = Uri.parse("geo:0,0?q="+lat + "," + lon);
		 Intent intent = new Intent(Intent.ACTION_VIEW,location);
        activity.startActivity(intent);
	}
	
	public static void openSMSScreen (Activity activity,String message,String address) {
        Intent intent = new Intent("android.intent.action.VIEW");
        Uri data = Uri.parse("sms:"+address);
        intent.setData(data);
        intent.putExtra("address", address);
        intent.putExtra("sms_body", message);
       
        
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        activity.startActivity(intent);
	}
	
	
	public static void openGallery (Activity activity,int requestCode){
		Intent pickPhoto = new Intent(Intent.ACTION_PICK,
		           MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
		
		activity.startActivityForResult(pickPhoto, requestCode);
	}
	
	public static void openGallery (Fragment fragment,int requestCode){
		Intent pickPhoto = new Intent(Intent.ACTION_PICK,
		           MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
		
		fragment.startActivityForResult(pickPhoto , requestCode);
	}
	
	public static void openGalleryFromChildFragment (Fragment fragment,int requestCode){
		Intent pickPhoto = new Intent(Intent.ACTION_PICK,
		           MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
		
		fragment.getParentFragment().startActivityForResult(pickPhoto, requestCode);
	}
	
	
	
	public static void openRecordVideoFromChildFragment(Fragment fragment,
			int requestCode,String fileName) {
		Intent intent = new Intent(
				MediaStore.ACTION_VIDEO_CAPTURE);
		
		intent.putExtra(MediaStore.EXTRA_FINISH_ON_COMPLETION, true);
		intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 0);
		intent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 10);
	    if (intent.resolveActivity(fragment.getActivity().getPackageManager()) != null) {
	    	fragment.getParentFragment().startActivityForResult(intent,requestCode);
	    }
		
	}

	
	public static void openRecordAudioFromFragment(Fragment fragment,
			int requestCode) {
		Intent intent = new Intent(
				MediaStore.Audio.Media.RECORD_SOUND_ACTION);
		fragment.startActivityForResult(intent, requestCode);

	}
	
	public static void openRecordAudioFromChildFragment(Fragment fragment,
			int requestCode) {
		Intent intent = new Intent(
				MediaStore.Audio.Media.RECORD_SOUND_ACTION);
		fragment.getParentFragment().startActivityForResult(intent, requestCode);

	}

	public static void openCameraFromChildFragment(Fragment fragment,
			int requestCode) {
		Intent intent = new Intent(
				MediaStore.ACTION_IMAGE_CAPTURE);
		fragment.getParentFragment().startActivityForResult(intent,requestCode);
	}
	
	public static void openAudioGalleryFromChildFragment (Fragment fragment,int requestCode){
		Intent pickPhoto = new Intent(Intent.ACTION_PICK,
		           MediaStore.Audio.Media.EXTERNAL_CONTENT_URI);
		
		fragment.getParentFragment().startActivityForResult(pickPhoto , requestCode);
	}
	
	public static void openVideoGalleryFromChildFragment (Fragment fragment,int requestCode){
		Intent mediaChooser = new Intent(Intent.ACTION_GET_CONTENT);
		mediaChooser.setType("video/*, images/*");
		fragment.getParentFragment().startActivityForResult(mediaChooser , requestCode);
	}
	
	public static void playMediaByUrl (Context context,String type,String url){
		if (url == null){
			return;
		}
		Log.i("onitemclicklog", "url = " + url);
		
		try {
			Intent intent = new Intent();
			intent.setAction(Intent.ACTION_VIEW);
			intent.setDataAndType(Uri.parse(url),type);
			context.startActivity(intent);
		} catch (Exception e) {
			try {
				Intent intent = new Intent(Intent.ACTION_VIEW);
				intent.setDataAndType(Uri.parse("file://"+url), type);
				context.startActivity(intent);
			} catch (Exception e2) {
				ToastUtil.toaster("Can not playing this file. file path:" + url, true);
			}
			
		}
	}
	
	
	
	public static void deleteFileFromSdcard (final String filePath){
		if (filePath == null||filePath.isEmpty()){
			return;
		}
		try {
			new Thread(new Runnable() {
				
				@Override
				public void run() {
					File file = new File(filePath);
					if (file != null){
						if (file.exists()){
							boolean deleted = file.delete();	
						}
					}
					
				}
			}).start();
			
		} catch (Exception e) {
			ToastUtil.toaster("Can not delete this file. path:" + filePath, true);
		}
		
	}
	
	public static void openCamera (Fragment fragment){
		Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		fragment.startActivity(takePicture);
	}
	
	public static void openCameraForResult (Fragment fragment,int requestCode){
		Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		fragment.startActivityForResult(takePicture, requestCode);
	}
	
	public static void openCameraForResultWithUri (Fragment fragment,int requestCode,Uri uri){
		Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		takePicture.setData(uri);
		fragment.startActivityForResult(takePicture,requestCode);
	}
	
	public static void openImageInGallery (Activity activity,String phth) {
		Intent intent = null;
		intent = new Intent(Intent.ACTION_VIEW);
		intent.setDataAndType(Uri.parse("file://" + phth), "image/*");
		activity.startActivity(intent);
	}
	
	public static void openImageInGallery (Fragment fragment,String phth) {
		Intent intent = null;
		intent = new Intent(Intent.ACTION_VIEW);
		intent.setDataAndType(Uri.parse("file://"+phth), "image/*");
		fragment.startActivity(intent);
	}
	
	public static void mackCallPhone (Fragment fragment,String phone) {
		
		Uri callUri = Uri.parse("tel://" + phone);
		Intent callIntent = new Intent(Intent.ACTION_CALL,callUri);
		callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_USER_ACTION);
		fragment.startActivity(callIntent);
	}
	public static void openConnectionSettings (Context context) {
		context.startActivity(new Intent
				(Settings.ACTION_WIRELESS_SETTINGS));
	}
	
	public static boolean isGPSEnebled(Context context){
		LocationManager locationManager = (LocationManager)
				context.getSystemService(Context.LOCATION_SERVICE);
		Criteria c = new Criteria();
        c.setAccuracy(Criteria.ACCURACY_FINE);
        final String PROVIDER = locationManager.getBestProvider(c, false);
    	boolean isGpsEnabled = locationManager.isProviderEnabled(PROVIDER);
    	return isGpsEnabled;
	}
	
	public static void openGPSSettings (Context context) {
		context.startActivity(new Intent(
				Settings.ACTION_LOCATION_SOURCE_SETTINGS));
	}
	
	public static void openGPSSettings (Fragment fragment) {
		fragment.startActivity(new Intent(
				Settings.ACTION_LOCATION_SOURCE_SETTINGS));
	}
	
	public static void openGPSSettings (Activity activiy) {
		activiy.startActivity(new Intent(
				Settings.ACTION_LOCATION_SOURCE_SETTINGS));
	}
	
	public static void openGPSSettingsForResult (Activity activity,int requestCode) {
		activity.startActivityForResult(new Intent(
				Settings.ACTION_LOCATION_SOURCE_SETTINGS), requestCode);
	}
	
	public static void openGPSSettingsForResult (Fragment fragment,int requestCode) {
		fragment.startActivityForResult(new Intent(
				Settings.ACTION_LOCATION_SOURCE_SETTINGS),requestCode);
	}
	
	public static void openBrowser (Fragment fragment,String url){
		Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
		fragment.getActivity().startActivity(browserIntent);
	}
	
	public static void openBrowser (Activity activity,String url){
		try {
			Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
			activity.startActivity(browserIntent);
		} catch (Exception e) {
			e.printStackTrace();
			ToastUtil.toaster(activity.getString(R.string.error_getting_business_web), false);
		}
	}
	
	public static void openWaze (Activity activity,String address) {
		
		try
		{
			Uri location = Uri.parse("waze://?q="+address);
			Intent intent = new Intent(Intent.ACTION_VIEW,location);
			activity.startActivity(intent);
		}
		catch ( Exception ex  )
		{
		   Intent intent =
		    new Intent( Intent.ACTION_VIEW, Uri.parse( "market://details?id=com.waze" ) );
		   activity.startActivity(intent);
		}
	}
	
	public static void openContactListToChoose (Fragment fragment,int requestCode) {
		Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_LOOKUP_URI);
		intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
	    fragment.startActivityForResult(intent, requestCode);
	}
	

	
	public static void openCameraForResultFile (Fragment fragment,int requestCode){
		Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		Uri imageFileUri = Uri.parse("file:///sdcard/picture.jpg");
		//Uri imageFile = Uri.fromFile(new File(MapOApp.appContext.getContentResolver()+ "/picture.jpg"));
		takePicture.putExtra(MediaStore.EXTRA_OUTPUT, imageFileUri);
		fragment.startActivityForResult(takePicture,requestCode);
	}
	
	public static void openCameraForResultFile (Fragment fragment,String filepath,int requestCode){
		Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		Uri imageFileUri = Uri.parse("file://"+filepath);
		takePicture.putExtra(MediaStore.EXTRA_OUTPUT, imageFileUri);
		fragment.startActivityForResult(takePicture,requestCode);
	}
	
	public static Uri openCameraForResultFile (Activity activity,String filepath,int requestCode){
		Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		Uri imageFileUri = Uri.parse("file://"+filepath);
		takePicture.putExtra(MediaStore.EXTRA_OUTPUT, imageFileUri);
		activity.startActivityForResult(takePicture,requestCode);
		return imageFileUri;
	}
	
	public static void openDialer(Context context, String tel){
		try {
			Intent intent = new Intent(Intent.ACTION_DIAL);
			intent.setData(Uri.parse("tel:" + tel));
			context.startActivity(intent);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public static boolean isTelephonyEnabled(Context context){
		TelephonyManager tm = (TelephonyManager)context.getSystemService(context.TELEPHONY_SERVICE);
		return tm != null && tm.getSimState()==TelephonyManager.SIM_STATE_READY;
	}
	public static void openMailApp(Context context, String to, String subject, String content){
		
		Intent i = new Intent(Intent.ACTION_SEND);
		i.setType("message/rfc822");
		i.putExtra(Intent.EXTRA_EMAIL  , new String[]{to});
		i.putExtra(Intent.EXTRA_SUBJECT, subject);
		i.putExtra(Intent.EXTRA_TEXT   , content);
		try {
			context.startActivity(Intent.createChooser(i, "Send mail..."));
		} catch (android.content.ActivityNotFoundException ex) {
		    System.out.println("There are no email clients installed.");
		}

	}
	
	public static void luanchShareChooser (Context context,String title,String message) {
		Intent i = new Intent(Intent.ACTION_SEND);
		i.setType("text/plain"); 
	     
		i.putExtra(Intent.EXTRA_TEXT, message);
		context.startActivity(Intent.createChooser(i,title));
	}
	

}
