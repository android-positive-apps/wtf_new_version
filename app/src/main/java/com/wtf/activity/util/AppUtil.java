/**
 * 
 */
package com.wtf.activity.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicInteger;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.content.res.Resources;
import android.graphics.Point;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.os.Build;
import android.text.Html;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.wtf.activity.main.MyApp;
import com.wtf.activity.location.LocationService;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.wtf.activity.objects.Size;


/**
 * @author Nati Gabay
 *
 */
public class AppUtil {



	public static AtomicInteger sNextGeneratedId = new AtomicInteger(1);
	public static int timerCounter;
	/**
	 * get the application version name
	 *
	 * @param context - context of current application
	 * @return the application version name
	 */
	public  static String getApplicationVersion(Context context) {
		String appVersion = "";
		try {
			PackageManager manager = context.getPackageManager();
			PackageInfo info = manager.getPackageInfo(
					context.getPackageName(), 0);
			appVersion =  info.versionName;
		} catch (Exception e) {
		}
		return appVersion;
	}


	/**
	 * set up activity to display on full screen mood
	 *
	 * @param activity - activity to set up
	 */
	public static void makeFullScreenActivity (Activity activity){
		activity.requestWindowFeature(Window.FEATURE_NO_TITLE);
		activity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
	}

	/**
	 * set up activity to display on full screen mood
	 *
	 * @param activity - activity to set up
	 */
	public static void makeNoTitleActivity (Activity activity){
		activity.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		/*activity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);*/
	}

	public static void setPerventFromUserTakingScreenShote (Activity activity){

		try {
			activity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,WindowManager.LayoutParams.FLAG_SECURE);
		} catch (Exception e) {}

	}

	public static void setTextFont (TextView tv,String fontName){
		Typeface typeface = Typeface.createFromAsset(MyApp.appContext.getAssets(), fontName);
		tv.setTypeface(typeface);
	}

	public static void setTextFont (Context context, TextView tv,String fontName){
		Typeface typeface = Typeface.createFromAsset(context.getAssets(), fontName);
		tv.setTypeface(typeface);
	}

	public static void setTextFont (String fontName,TextView... tv){
		Typeface typeface = Typeface.createFromAsset(MyApp.appContext.getAssets(), fontName);
		for (int i = 0; i < tv.length; i++) {
			tv[i].setTypeface(typeface);
		}
	}

	public static void hideKeyBoard (Activity activity){
		activity.getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
		);
	}

	public static void setTextFonts (Context context,View view){

		ArrayList<View> childs = new ArrayList<View>();

		if (view instanceof TextView){
			String font = (String) view.getTag();
			Log.e("fontLog", "font = " + font);
			if (font != null){
				try {
					TextView tv = (TextView)view;
					Typeface typeface = Typeface.createFromAsset(context.getAssets(), font);
					tv.setTypeface(typeface);
				} catch (Exception e) {}

			}
		}
		if (view instanceof Button){
			String font = (String) view.getTag();
			Log.e("fontLog","font = " + font);
			if (font != null){
				Button btn = (Button)view;
				Typeface typeface = Typeface.createFromAsset(context.getAssets(), font);
				btn.setTypeface(typeface);
			}
		}
		if (view instanceof EditText){
			String font = (String) view.getTag();
			Log.e("fontLog","font = " + font);
			if (font != null){
				EditText et = (EditText)view;
				Typeface typeface = Typeface.createFromAsset(context.getAssets(), font);
				et.setTypeface(typeface);
			}
		}
		if (view instanceof ViewGroup){
			ViewGroup group = (ViewGroup)view;
			for(int i=0; i<group.getChildCount(); ++i) {
				View nextChild = group.getChildAt(i);
				childs.add(nextChild);
			}
			if (childs.size() > 0){
				for (int i = 0; i < childs.size(); i++) {
					setTextFonts(context,childs.get(i));
				}
			}else{
				return;
			}
		}

	}
	public static boolean isNetworkAvailable(final Context context) {
		final ConnectivityManager connectivityManager = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));
		return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
	}
	/**
	 * Check the device to make sure it has the Google Play Services APK. If
	 * it doesn't, display a dialog that allows users to download the APK from
	 * the Google Play Store or enable it in the device's system settings.
	 */
	public static boolean checkPlayServices(Activity activity, int requestCode) {
		int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(activity);
		if (resultCode != ConnectionResult.SUCCESS) {
			if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
				GooglePlayServicesUtil.getErrorDialog(resultCode, activity,
						requestCode).show();
			} else {
				ToastUtil.toaster("the google play service is not supported in current device !", false);
				activity.finish();
			}
			return false;
		}
		return true;
	}

	public static void stopLocationService (Context context) {
		try {
			context.stopService(new Intent(context,LocationService.class));
		} catch (Exception e) {}
	}


	public static void startLocationService(Activity activity)
	{
		if (checkPlayServices(activity,9000)){
			Log.e("locationService", "ceck location service");
			try {
				activity.stopService(new Intent(activity,LocationService.class));
			} catch (Exception e) {}

			Intent mServiceIntent = new Intent(activity, LocationService.class);
			activity.startService(mServiceIntent);
		}

	}



	public static String cleanText (String toClean){
		String result = "";
		try {
			for (int i = 0; i < toClean.length(); i++) {
				char temp = toClean.charAt(i);
				if (temp >= '0' && temp <='9'){
					result += temp;
				}

			}
		} catch (Exception e) {}
		return result;
	}

	public static void linkifyReadMore (TextView toLink,String toSet) {
    	/*Pattern pattern = Pattern.compile("read more");
    	toLink.setText(toSet);
    	Linkify.addLinks(toLink,pattern, "");*/
		String text = "<font color=#414b55>"+toSet+"</font> <font color=#672283>read more</font>";
		toLink.setText(Html.fromHtml(text));
	}

	public static void printKeyHash(Context context){
		// Add code to print out the key hash
		try {
			PackageInfo info = context.getPackageManager().getPackageInfo(
					"com.positiveapps.picpack",
					PackageManager.GET_SIGNATURES);
			for (Signature signature : info.signatures) {
				MessageDigest md = MessageDigest.getInstance("SHA");
				md.update(signature.toByteArray());
				Log.e("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
			}
		} catch (NameNotFoundException e) {
			Log.d("KeyHash:", e.toString());
		} catch (NoSuchAlgorithmException e) {
			Log.d("KeyHash:", e.toString());
		}
	}



	public static void saveScreenDimention(Activity activity) {

		WindowManager w = activity.getWindowManager();
		Display d = w.getDefaultDisplay();
		DisplayMetrics metrics = new DisplayMetrics();
		d.getMetrics(metrics);
		// since SDK_INT = 1;
		int widthPixels = metrics.widthPixels;
		int heightPixels = metrics.heightPixels;
		// includes window decorations (statusbar bar/menu bar)
		if (Build.VERSION.SDK_INT >= 14 && Build.VERSION.SDK_INT < 17)
			try {
				widthPixels = (Integer) Display.class.getMethod("getRawWidth").invoke(d);
				heightPixels = (Integer) Display.class.getMethod("getRawHeight").invoke(d);
			} catch (Exception ignored) {
			}
		// includes window decorations (statusbar bar/menu bar)
		if (Build.VERSION.SDK_INT >= 17)
			try {
				Point realSize = new Point();
				Display.class.getMethod("getRealSize", Point.class).invoke(d, realSize);
				widthPixels = realSize.x;
				heightPixels = realSize.y;
			} catch (Exception ignored) {
			}
		MyApp.appManager.setScreenSize(new Size(heightPixels, widthPixels));

	}

	public static void showKeyboard (final EditText et) {
		et.postDelayed(new Runnable() {

			@Override
			public void run() {
				InputMethodManager imm = (InputMethodManager)
						MyApp.appContext.getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.showSoftInput(et, InputMethodManager.SHOW_IMPLICIT);
			}
		}, 200);

	}

	public static void hideKeyBoard (EditText toHide){
		InputMethodManager imm = (InputMethodManager)MyApp.appContext.getSystemService(
				Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(toHide.getWindowToken(), 0);
	}

	public static LayoutInflater getAppInflater () {
		LayoutInflater layoutInflater
				= (LayoutInflater)MyApp.appContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		return layoutInflater;
	}

	public static LayoutInflater getAppInflater (Context context) {
		LayoutInflater layoutInflater
				= (LayoutInflater)context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		return layoutInflater;
	}

	public static void setKeyboardVisibilityListener(Activity activity, final KeyboardVisibilityListener keyboardVisibilityListener) {
		final View contentView = activity.findViewById(android.R.id.content);
		contentView.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
			private int mPreviousHeight;

			@Override
			public void onGlobalLayout() {
				int newHeight = contentView.getHeight();
				if (mPreviousHeight != 0) {
					if (mPreviousHeight > newHeight) {
						// Height decreased: keyboard was shown
						keyboardVisibilityListener.onKeyboardVisibilityChanged(true);
					} else if (mPreviousHeight < newHeight) {
						// Height increased: keyboard was hidden
						keyboardVisibilityListener.onKeyboardVisibilityChanged(false);
					} else {
						// No change
					}
				}
				mPreviousHeight = newHeight;
			}
		});
	}

	public interface KeyboardVisibilityListener {
		void onKeyboardVisibilityChanged(boolean keyboardVisible);
	}

	public static int getRundomIntBetween (int from,int to){
		Random r = new Random();
		return  r.nextInt(to-from) + from;
	}


	public static int generateViewId() {
		for (;;) {
			final int result = sNextGeneratedId.get();
			// aapt-generated IDs have the high byte nonzero; clamp to the range under that.
			int newValue = result + 1;
			if (newValue > 0x00FFFFFF) newValue = 1; // Roll over to 1, not 0.
			if (sNextGeneratedId.compareAndSet(result, newValue)) {
				return result;
			}
		}
	}

	public static String generateName (int lenght){
		String base = "abcdifghijklmnopqrstuvwxyz 1234567890 ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		String result = "";
		for (int i = 0; i < lenght; i++) {
			int rand = new Random().nextInt(base.length()-1);
			result += base.charAt(rand);
		}
		return result;
	}
	

	

	
	/*public static PendingIntent startAlramNegotiationTimeOut (long packageId,int duration) {
		AlarmManager alarmMgr = (AlarmManager)MyApp.appContext.
			getSystemService(Context.ALARM_SERVICE);
		Intent intent = new Intent(MyApp.appContext, CheckItemsTimesReceiver.class);
		intent.putExtra(PaymentTimeOutActivity.EXTRA_PACKAGE_ID, packageId);
		PendingIntent alarmIntent = PendingIntent.getBroadcast(MyApp.appContext, 0, intent, 0);
		alarmMgr.set(AlarmManager.ELAPSED_REALTIME_WAKEUP,
		        SystemClock.elapsedRealtime() +
		        duration * 1000, alarmIntent);
		return alarmIntent;
	} 


	public static void startCheckItemsDateAlarmService () {
		AlarmManager alarmMgr = (AlarmManager)MyApp.appContext.
				getSystemService(Context.ALARM_SERVICE);
		Intent intent = new Intent(MyApp.appContext,CheckItemsTimesReceiver.class);
		PendingIntent alarmIntent = PendingIntent.getBroadcast(MyApp.appContext, 0, intent, 0);
		alarmMgr.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,
		        30000,
		        AlarmManager.INTERVAL_FIFTEEN_MINUTES, alarmIntent);
	}*/

	public static Timer displayTimer(final Activity activity
			,final TextView txtTimer,int duration,final TimerListener listener){
		timerCounter = duration;
		Date date = new Date(timerCounter * 1000);
		txtTimer.setText(new SimpleDateFormat("mm:ss").format(date));
		final Timer mTimer = new Timer();
		mTimer.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				try{
					activity.runOnUiThread(new Runnable() {
											   @Override
											   public void run() {
												   if(timerCounter == 0){
													   listener.onFinish(mTimer);
													   return;
												   }
												   timerCounter--;
												   listener.onIncrement(timerCounter, mTimer);
												   Date d = new Date(timerCounter * 1000);
												   txtTimer.setText(new SimpleDateFormat("mm:ss").format(d));
											   }
										   }
					);
				}catch(Exception ex){
					Log.e("timerLog", "Failed to update timer");
				}
			}
		}, 500, 1000);

		return mTimer;
	}


	/**
	 * This method converts dp unit to equivalent pixels, depending on device density. 
	 *
	 * @param dp A value in dp (density independent pixels) unit. Which we need to convert into pixels
	 * @param context Context to get resources and device specific display metrics
	 * @return A float value to represent px equivalent to dp depending on device density
	 */
	public static float convertDpToPixel(float dp, Context context){
		Resources resources = context.getResources();
		DisplayMetrics metrics = resources.getDisplayMetrics();
		float px = dp * (metrics.densityDpi / 160f);
		return px;
	}

	/**
	 * This method converts device specific pixels to density independent pixels.
	 *
	 * @param px A value in px (pixels) unit. Which we need to convert into db
	 * @param context Context to get resources and device specific display metrics
	 * @return A float value to represent dp equivalent to px value
	 */
	public static float convertPixelsToDp(float px, Context context){
		Resources resources = context.getResources();
		DisplayMetrics metrics = resources.getDisplayMetrics();
		float dp = px / (metrics.densityDpi / 160f);
		return dp;
	}

	public interface TimerListener {
		public void onIncrement(int leftTime, Timer timer);
		public void onFinish(Timer timer);
	}
}
