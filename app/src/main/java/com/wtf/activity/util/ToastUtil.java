package com.wtf.activity.util;


import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import com.wtf.activity.main.MyApp;


public class ToastUtil {
	
	public static void toaster(String message, boolean longer){
		try {
			if (longer){
				Toast.makeText(MyApp.appContext, message, Toast.LENGTH_LONG).show();
			}else{
				Toast.makeText(MyApp.appContext, message, Toast.LENGTH_SHORT).show();
			}
		} catch (Exception e) {
		}
	}
	
	public static void customToster(int layoutResource,boolean length,ToastInitialyzer initialyzer) {

		try {
			LayoutInflater inflater = AppUtil.getAppInflater();
			View layout = inflater.inflate(layoutResource,null);

			Toast toast = new Toast(MyApp.appContext);
			toast.setView(layout);
		
			if (!length){
				toast.setDuration(Toast.LENGTH_SHORT);
			}else{
				toast.setDuration(Toast.LENGTH_LONG);
			}
			if(initialyzer != null){
				initialyzer.init(toast, layout);
				initialyzer.onShow(toast);
			}
			toast.show();
		} catch (Exception e) {}

	}
	
	public class ToastInitialyzer {
		public void init(Toast toast,View view){}
		public void onShow(Toast toast){}
	}
}
