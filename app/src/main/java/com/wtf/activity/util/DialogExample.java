package com.wtf.activity.util;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

import com.wtf.activity.R;
import com.wtf.activity.main.MainActivity;

/**
 * Created by Niv on 3/1/2016.
 */
public class DialogExample {

    public static void openLocationDialog(final Context context) {

        LayoutInflater factory = LayoutInflater.from(context);
        final View deleteDialogView = factory.inflate(
                R.layout.location_dialog, null);
        final AlertDialog deleteDialog = new AlertDialog.Builder(context).create();
        deleteDialog.setView(deleteDialogView);
        deleteDialogView.findViewById(R.id.openSettingsB).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                ContentProviderUtil.openGPSSettings(context);

                //your business logic
                deleteDialog.dismiss();
            }
        });
        deleteDialogView.findViewById(R.id.okB).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                deleteDialog.dismiss();

            }
        });

        deleteDialog.show();


    }

}
