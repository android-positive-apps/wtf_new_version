package com.wtf.activity.util;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Build;
import android.support.v7.widget.SwitchCompat;

import com.wtf.activity.R;

/**
 * Created by Niv on 4/12/2016.
 */
public class SwitchUtil {

    public static void switchColor(boolean checked, SwitchCompat switchCompat, Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
//            incognitoSwitch.getThumbDrawable().setColorFilter(checked ? Color.BLACK : Color.WHITE, PorterDuff.Mode.MULTIPLY);
//            incognitoSwitch.getTrackDrawable().setColorFilter(!checked ? Color.BLACK : Color.WHITE, PorterDuff.Mode.MULTIPLY);
            switchCompat.getThumbDrawable().setColorFilter(Color.WHITE, PorterDuff.Mode.MULTIPLY);
//            switchCompat.getTrackDrawable().setColorFilter(!checked ? Color.BLACK  : context.getResources().getColor(R.color.wtf_light_blue), PorterDuff.Mode.MULTIPLY);
            switchCompat.getTrackDrawable().setColorFilter(!checked ? Color.BLACK  : context.getResources().getColor(R.color.wtf_red), PorterDuff.Mode.MULTIPLY);
        }
    }
}
