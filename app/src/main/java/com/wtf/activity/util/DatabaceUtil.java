/**
 * 
 */
package com.wtf.activity.util;

import java.util.ArrayList;

/**
 * @author natiapplications
 *
 */
public class DatabaceUtil {
	
	private static  final String sperator = "__@__";
	
	public static ArrayList<String> createStringListByStoredDBString (String storedDB){
		ArrayList<String > result = new ArrayList<String>();
		try {
			String[] strArray = storedDB.split(sperator);
			for (int i = 0; i < strArray.length; i++) {
				result.add(strArray[i]);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return result;
	}
	
	public static String createStoredDbStringByStringList (ArrayList<String> toConvert){
		String result = "";
		try {
			for (int i = 0; i < toConvert.size(); i++) {
				result+=toConvert.get(i);
				if (i < toConvert.size()-1){
					result+=sperator;
				}
			}
		} catch (Exception e) {
			result = "NA";
		}
		return result;
	}
	

}
