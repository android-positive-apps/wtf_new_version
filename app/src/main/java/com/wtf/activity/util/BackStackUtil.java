/**
 * 
 */
package com.wtf.activity.util;

import java.util.ArrayList;

import android.support.v4.app.FragmentManager;


/**
 * @author Nati Gabay
 *
 */
public class BackStackUtil {
	
	
	public static ArrayList<String> BackStackList = new ArrayList<String>();
	
	
	public static void addToBackStack (String fragmentTag){
		if (!isAllradyOnBackStack(fragmentTag)){
			BackStackList.add(fragmentTag);
		}
	}
	
	private static boolean isAllradyOnBackStack (String fragmentTag){
		for (int i = 0; i < BackStackList.size(); i++) {
			if (fragmentTag.equalsIgnoreCase(BackStackList.get(i))){
				return true;
			}
		}
		return false;
	}
	
	public static void removeFromBackStack (String fragmentTag) {
		int position = getTagPostion(fragmentTag);
		if (position >= 0 && position < BackStackList.size()){
			BackStackList.remove(position);
		}
	}
	
	public static int getTagPostion (String fragmentTag) {
		for (int i = 0; i < BackStackList.size(); i++) {
			if (fragmentTag.equalsIgnoreCase(BackStackList.get(i))){
				return i;
			}
		}
		return -1;
	}
	
	public static boolean popToBackStack (String fragmentTag, FragmentManager fm){
		int position = getTagPostion(fragmentTag);
		if (position < 0){
			return false;
		}
		int distance = (BackStackList.size() - position)-1;
		if (distance <= 0){
			return false;
		}
		for (int i = 0; i < distance; i++) {
			fm.popBackStack();
		}
		return true;
	}

}
