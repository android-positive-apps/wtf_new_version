/**
 * 
 */
package com.wtf.activity.util;



import android.app.Activity;
import android.app.ProgressDialog;


/**
 * @author Nati Gabay
 *
 */
public class ProgressDialogUtil {
	
	public static ProgressDialog showProgressDialog (Activity activity, String message){
		ProgressDialog dialog = new ProgressDialog(activity);
		dialog.setMessage(message);
		dialog.setCancelable(false);
		
		dialog.show();
		return dialog;
	}
	
	
	public static void dismisDialog (ProgressDialog toDismis){
		try {
			if (toDismis != null && toDismis.isShowing()){
				toDismis.dismiss();
			}
		} catch (Exception e) {}
	}

}
