/**
 * 
 */
package com.wtf.activity.util;



import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.wtf.activity.R;


/**
 * @author natiapplications
 *
 */
public class FragmentsUtil {
	
	
	public static void addFragment(FragmentManager fm, Fragment fragment,
			int containerId) {
		try {
			FragmentTransaction transaction = fm.beginTransaction();
			transaction.replace(containerId, fragment).commit();
		} catch (Exception e) {
			fm.beginTransaction()
			.replace(containerId, fragment)
			.commitAllowingStateLoss();
		}
	}

	public static void openFragment(FragmentManager fm, Fragment fragment,
			int containerId) {

		FragmentTransaction transaction = fm.beginTransaction();
		transaction.replace(containerId, fragment).addToBackStack(null)
				.commit();
	}
	
	public static void openFragmentRghitToLeft (FragmentManager fm, Fragment fragment, int containerId){
		try {
			FragmentTransaction transaction = fm.beginTransaction();
			transaction.setCustomAnimations
			(R.anim.pop_right_in, R.anim.pop_right_out,R.anim.slide_right_in,R.anim.slide_right_out);
			transaction.replace
			(containerId,fragment).addToBackStack(null).
			commit();
		} catch (Exception e) {}
		catch (Error e) {}
		
	}
	
	public static void addFragmentRghitToLeft(FragmentManager fm, Fragment fragment,
			int containerId) {
		try {
			FragmentTransaction transaction = fm.beginTransaction();
			transaction.setCustomAnimations
			(R.anim.pop_right_in, R.anim.pop_right_out,R.anim.slide_right_in,R.anim.slide_right_out);
			transaction.replace(containerId, fragment).commit();
		} catch (Exception e) {
			FragmentTransaction transaction = fm.beginTransaction();
			transaction.setCustomAnimations
			(R.anim.pop_right_in, R.anim.pop_right_out,R.anim.slide_right_in,R.anim.slide_right_out);
			transaction.replace(containerId, fragment)
			.commitAllowingStateLoss();
		}
	}
	
	public static void addFragmentLeftToRghit(FragmentManager fm, Fragment fragment,
			int containerId) {
		try {
			FragmentTransaction transaction = fm.beginTransaction();
			transaction.setCustomAnimations
			(R.anim.slide_right_in,R.anim.slide_right_out,R.anim.pop_right_in, R.anim.pop_right_out);
			transaction.replace(containerId, fragment).commit();
		} catch (Exception e) {
			FragmentTransaction transaction = fm.beginTransaction();
			transaction.setCustomAnimations
			(R.anim.slide_right_in,R.anim.slide_right_out,R.anim.pop_right_in, R.anim.pop_right_out);
			transaction.replace(containerId, fragment)
			.commitAllowingStateLoss();
		}
	}
	
     
     public static void openFragmentDownToUp (FragmentManager fm, Fragment fragment, int containerId){
 		try {
 			FragmentTransaction transaction = fm.beginTransaction();
 	 		transaction.setCustomAnimations
 	 		(R.anim.slide_in_fragment, R.anim.slide_out_fragment,R.anim.pop_slid_in,R.anim.pop_slide_out);
 	 		transaction.replace
 	 		(containerId,fragment).addToBackStack(null).
 	 		commit();
		} catch (Exception e) {}
 		catch (Error e) {}
 		
 	}
     
     public static void addFragmentDownToUp(FragmentManager fm, Fragment fragment,
 			int containerId) {
 		try {
 			FragmentTransaction transaction = fm.beginTransaction();
 			transaction.setCustomAnimations
 			(R.anim.slide_in_fragment, R.anim.slide_out_fragment,R.anim.pop_slid_in,R.anim.pop_slide_out);
 			transaction.replace(containerId, fragment).commit();
 		} catch (Exception e) {
 			FragmentTransaction transaction = fm.beginTransaction();
 			transaction.setCustomAnimations
 			(R.anim.slide_in_fragment, R.anim.slide_out_fragment,R.anim.pop_slid_in,R.anim.pop_slide_out);
 			transaction.replace(containerId, fragment)
 			.commitAllowingStateLoss();
 		}
 	}
	
	

}
