/**
 * 
 */
package com.wtf.activity.ui;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;

/**
 * @author Maor
 *
 */
public class RoundedImageView2 extends BaseImageView {

	/**
	 * 
	 * @param context
	 */
	public RoundedImageView2(Context context) {
		super(context,null);
	}
	
	
	/**
	 * 
	 * @param context
	 * @param attrs
	 */
	public RoundedImageView2(Context context, AttributeSet attrs) {
		super(context, attrs, 0);
	}
	
	/**
	 * @param context
	 * @param attrs
	 * @param defStyleAttr
	 */
	public RoundedImageView2(Context context, AttributeSet attrs,
							 int defStyleAttr) {
		super(context, attrs, defStyleAttr);
	}

	

	  public static Bitmap getBitmap(int width, int height) {
		    float radius = 90.0f;
	        Bitmap bitmap = Bitmap.createBitmap(width, height,
	                Bitmap.Config.ARGB_8888);

	        Canvas canvas = new Canvas(bitmap);
	        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
	        paint.setColor(Color.BLACK);
	        canvas.drawRoundRect(new RectF(0.0f, 0.0f, width, height),radius, radius, paint);

	        return bitmap;
	    }

	    @Override
	    public Bitmap getBitmap() {
	        return getBitmap(getWidth(), getHeight());
	    }
	
    
   
	
	

	
	
}
